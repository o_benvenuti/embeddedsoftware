/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#define HardwareVersion 0x00
#define FirmwareVersion_Majeur 0x01
#define FirmwareVersion_Mineur 0x00
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define MCP73113_STAT_Pin GPIO_PIN_15
#define MCP73113_STAT_GPIO_Port GPIOA
#define EN_3V_Pin GPIO_PIN_2
#define EN_3V_GPIO_Port GPIOD
#define STWLC33_EN_Pin GPIO_PIN_14
#define STWLC33_EN_GPIO_Port GPIOG
#define int0_3D_Pin GPIO_PIN_3
#define int0_3D_GPIO_Port GPIOB
#define CS_M_Pin GPIO_PIN_12
#define CS_M_GPIO_Port GPIOC
#if PLATFORMID == 112
#define CS_AG_Pin GPIO_PIN_13
#define CS_AG_GPIO_Port GPIOC
#define int1_3D_Pin GPIO_PIN_4
#define int1_3D_GPIO_Port GPIOB
#define int1_3D_EXTI_IRQn EXTI4_IRQn
#elif PLATFORMID == 113
#define CS_AG_Pin GPIO_PIN_4
#define CS_AG_GPIO_Port GPIOB
#define int1_3D_Pin GPIO_PIN_13
#define int1_3D_GPIO_Port GPIOC
#define int1_3D_EXTI_IRQn EXTI15_10_IRQn
#else
#warning "ERROR : PLATFORMID UNDEFINED"
#endif
#define STM_RTS_Pin GPIO_PIN_12
#define STM_RTS_GPIO_Port GPIOA
#define CS_Flash_Pin GPIO_PIN_11
#define CS_Flash_GPIO_Port GPIOC
#define STM_CTS_Pin GPIO_PIN_11
#define STM_CTS_GPIO_Port GPIOA
#define STM_RX_Pin GPIO_PIN_10
#define STM_RX_GPIO_Port GPIOA
#define LCD_CS_Pin GPIO_PIN_10
#define LCD_CS_GPIO_Port GPIOC
#define LCD_RST_Pin GPIO_PIN_9
#define LCD_RST_GPIO_Port GPIOC
#define Blue_MODE_Pin GPIO_PIN_8
#define Blue_MODE_GPIO_Port GPIOA
#define STM_TX_Pin GPIO_PIN_9
#define STM_TX_GPIO_Port GPIOA
#define DEN_A_G_Pin GPIO_PIN_8
#define DEN_A_G_GPIO_Port GPIOB
#define DRDY_M_Pin GPIO_PIN_9
#define DRDY_M_GPIO_Port GPIOB
#define LCD_WR_Pin GPIO_PIN_7
#define LCD_WR_GPIO_Port GPIOC
#define LCD_DC_Pin GPIO_PIN_8
#define LCD_DC_GPIO_Port GPIOC
#define LCD_RD_Pin GPIO_PIN_6
#define LCD_RD_GPIO_Port GPIOC
#define SPI2_MISO_Pin GPIO_PIN_2
#define SPI2_MISO_GPIO_Port GPIOC
#define STWLC33_INT_Pin GPIO_PIN_15
#define STWLC33_INT_GPIO_Port GPIOB
#define STWLC33_GP2_Pin GPIO_PIN_14
#define STWLC33_GP2_GPIO_Port GPIOB
#define SDA_Touch_Pin GPIO_PIN_11
#define SDA_Touch_GPIO_Port GPIOB
#define DB1_Pin GPIO_PIN_1
#define DB1_GPIO_Port GPIOA
#define DB4_Pin GPIO_PIN_4
#define DB4_GPIO_Port GPIOA
#define DB2_Pin GPIO_PIN_2
#define DB2_GPIO_Port GPIOA
#define SPI2_MOSI_Pin GPIO_PIN_3
#define SPI2_MOSI_GPIO_Port GPIOC
#define Blue_RX_IND_Pin GPIO_PIN_12
#define Blue_RX_IND_GPIO_Port GPIOB
#define SCL_Touch_Pin GPIO_PIN_13
#define SCL_Touch_GPIO_Port GPIOB
#define SPI2_SCK_Pin GPIO_PIN_10
#define SPI2_SCK_GPIO_Port GPIOB
#define DB7_Pin GPIO_PIN_7
#define DB7_GPIO_Port GPIOA
#define DB6_Pin GPIO_PIN_6
#define DB6_GPIO_Port GPIOA
#define DB5_Pin GPIO_PIN_5
#define DB5_GPIO_Port GPIOA
#define DB3_Pin GPIO_PIN_3
#define DB3_GPIO_Port GPIOA
#define DB0_Pin GPIO_PIN_0
#define DB0_GPIO_Port GPIOA
#define CTRL_Backlight_Pin GPIO_PIN_1
#define CTRL_Backlight_GPIO_Port GPIOB
#define RST_Display_Pin GPIO_PIN_0
#define RST_Display_GPIO_Port GPIOB
#define INT_Disp_Pin GPIO_PIN_5
#define INT_Disp_GPIO_Port GPIOC
#define INT_Disp_EXTI_IRQn EXTI9_5_IRQn

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
 #define USE_FULL_ASSERT    1U 

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
