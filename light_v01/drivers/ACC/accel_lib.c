//--------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------
#include <trace.h>
#include "LSM9DS1_Includes.h"
#include "math_3d.h"
#include "magcal.h"
#include "al_tools.h"
#include "al_database.h"

// Orientation of LSM9DS1 component on REV_1.3
// view : from the top of the board
// component is on the bottom side of the board
//   (down)Z--Y  +----------------+
//         | a/g |                |
//         X     |+---+     ^     |
//               ||.  |     |     |
//         X     ||   |  Forward  |
//         | mag |+---+           |
//   (down)Z--Y  +----------------+

//--------------------------------------------------------------------
// Types
//--------------------------------------------------------------------
enum agm_state
{
    idle    = 0,
    ready,
};

struct agm_context
{
    enum agm_state state;
    float gyro_bias[3];
};


//--------------------------------------------------------------------
// Variables
//--------------------------------------------------------------------
static struct agm_context _agm_local_ctx = { 0 };

//--------------------------------------------------------------------
// API
//--------------------------------------------------------------------
int8_t agm_init(void)
{
    // with only one static context, we need
    // to check if it has been previously released
    if ( _agm_local_ctx.state != idle )
    {
        debug_printf("already running\n");
        return -1;
    }

    /* wait for component to initialize */
    HAL_Delay(50);

    /* init the LSM9DS1 SPI link */
    int8_t ret = LSM9DS1_LSM9DS1(&hspi2, CS_AG_GPIO_Port, CS_AG_Pin,
                                 CS_M_GPIO_Port, CS_M_Pin);
    if ( ret < 0 )
    {
        return ret;
    }

    ret = LSM9DS1_begin();
    if ( ret < 0 )
    {
        debug_printf("AGM - INIT FAIL - Failed to communicate with LSM9DS1\n");
        return ret;
    }

    // reset local context
    memset(&_agm_local_ctx, 0, sizeof(_agm_local_ctx));

    /* write biases to magnetometer offset registers */
    ret = LSM9DS1_magOffset(0,MAG_BIAS_X);
    if ( ret < 0 )
    {
        return ret;
    }

    ret = LSM9DS1_magOffset(1,MAG_BIAS_Y);
    if ( ret < 0 )
    {
        return ret;
    }

    ret = LSM9DS1_magOffset(2,MAG_BIAS_Z);
    if ( ret < 0 )
    {
        return ret;
    }

    //*********************************************************
    // reset accelerometer bias & gyroscope bias to zero
    // these lines will be deleted if acc&gyr calibration is nedded
    int32_t aBias[3] = {0};
    al_database_set_acc_cal((uint8_t *) aBias, sizeof(aBias));
    //*********************************************************

    if(magcal_get_calibration_from_flash()!=0)
    {
        debug_printf("FLASH - unable to load mag calibration\n");
    }
    if(al_database_get_acc_cal((uint8_t *)aBias,sizeof(aBias))!=0)
    {
        debug_printf("FLASH - unable to load acc calibration\n");
    }
    if(al_database_get_gyr_cal((uint8_t*)_agm_local_ctx.gyro_bias,
                               sizeof(_agm_local_ctx.gyro_bias)) !=0 )
    {
        debug_printf("FLASH - unable to load gyr calibration\n");
    }

    LSM9DS1_PowerDown();

    _agm_local_ctx.state = ready;

    return 0;
}

//--------------------------------------------------------------------
int agm_gyro_update_bias(float * axes_bias)
{
    int rc = 0;

    // sanity check
    if ( ! axes_bias )
    {
        debug_printf("invalid parameter\n");
        return -1;
    }

    // update local context
    memcpy(&_agm_local_ctx.gyro_bias[0], axes_bias,
           sizeof(_agm_local_ctx.gyro_bias));

    // save to flash
    rc = al_database_set_gyr_cal((uint8_t *)_agm_local_ctx.gyro_bias,
                                 sizeof(_agm_local_ctx.gyro_bias));
    if ( rc )
    {
        debug_printf("save gyro calibration failed\n");
        return -1;
    }

    return 0;
}

//--------------------------------------------------------------------
int agm_gyro_compensated_dps(float * axes_dps)
{
    int rc = 0;

    // sanity check
    if ( ! axes_dps )
    {
        debug_printf("invalid parameter\n");
        return -1;
    }

    if ( _agm_local_ctx.state == idle )
    {
        debug_printf("not initialised\n");
        return -1;
    }

    rc = LSM9DS1_GetGyroscopeValuesDPS(&axes_dps[0], &axes_dps[1], &axes_dps[2]);
    if ( rc )
    {
        return -1;
    }

    axes_dps[0] -= _agm_local_ctx.gyro_bias[0];
    axes_dps[1] -= _agm_local_ctx.gyro_bias[1];
    axes_dps[2] -= _agm_local_ctx.gyro_bias[2];
    return 0;
}

//--------------------------------------------------------------------
void AGM_DEBUG (void)
{
    uint8_t STATUS_REG_val;

    LSM9DS1_mReadByte (STATUS_REG_0, &STATUS_REG_val);
    debug_printf("STATUS_REG_0 - 0x%02X\n",STATUS_REG_val);

    uint8_t CTRL_REG5_XL_val;
    LSM9DS1_mReadByte (CTRL_REG5_XL, &CTRL_REG5_XL_val);
    debug_printf("CTRL_REG5_XL - 0x%02X\n",CTRL_REG5_XL_val);

    uint8_t CTRL_REG6_XL_val;
    LSM9DS1_mReadByte (CTRL_REG6_XL, &CTRL_REG6_XL_val);
    debug_printf("CTRL_REG6_XL - 0x%02X\n",CTRL_REG6_XL_val);
}

//--------------------------------------------------------------------
int agm_set_flick_sensitivity(uint8_t sensitivity)
{
    if ( _agm_local_ctx.state == idle )
    {
        debug_printf("not initialised\n");
        return -1;
    }

    // sensitivity between 1 (harder) & 5 (easier)
    if ( sensitivity < 1 )
    {
        sensitivity = 1;
    }
    else if ( sensitivity > 5 )
    {
        sensitivity = 5;
    }

    // LSM9DS1_configGyroInt(((YLIE_XL)||(YHIE_XL)),false,false);
    /* Multiply the threshold register value by 64 to get threshold value. */
    /* duration in samples */
    const uint8_t duration  = 1 ;
    const bool    wait      = false;
    const bool    decrement = false;
    LSM9DS1_configGyroThs((6 - sensitivity) * 2560, Y_AXIS,
                          duration, wait, decrement);

    return 0;
}
//--------------------------------------------------------------------
// Private
//--------------------------------------------------------------------

