//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <al_tools.h>
#include <trace.h>
#include "LSM9DS1_Includes.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define LSM9DS1_COMMUNICATION_TIMEOUT 1000
#define LSM9DS1_TEMP_SCALE  16.0 // sensitivity 16 LSB/ degree
#define LSM9DS1_TEMP_BIAS   25.0  // output 0 = 25C

//----------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------
// Units of these values would be DPS (or g's or Gs's) per ADC tick.
const float gyroscope_resolution_array    [3]={0.00875 ,0.0175  ,0.07             };// DPS  /LSB
const float accelerometer_resolution_array[4]={0.000061,0.000122,0.000244,0.000732};// G    /LSB
const float magnetometer_resolution_array [4]={0.00014 ,0.00029 ,0.00043 ,0.00058 };// GAUSS/LSB

static GPIO_TypeDef* CS_XG_PORT;
static GPIO_TypeDef* CS_MAG_PORT;
static uint16_t CS_XG_PIN;
static uint16_t CS_MAG_PIN;

static SPI_HandleTypeDef *SPI_AGM;

IMUSettings settings;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void LSM9DS1_SPI_select(GPIO_TypeDef* csPort, uint16_t CS_PIN);
static void LSM9DS1_SPI_deselect(GPIO_TypeDef* csPort, uint16_t CS_PIN);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int8_t LSM9DS1_GetAccAxesRaw(AxesRaw_t* buff)
{
    int8_t rc;
    uint8_t temp[6];

    // sanity check
    if ( ! buff )
    {
        return -1;
    }

    /* if there is new data available */
    rc = LSM9DS1_accelAvailable();
    if ( rc <= 0 )
    {
        return -1;
    }

    // Read 6 bytes, beginning at OUT_X_L_XL
    rc = LSM9DS1_xgReadBytes(OUT_X_L_XL, temp, 6);
    if ( rc )
    {
        return -1;
    }

    buff->AXIS_X = (temp[1] << 8) | temp[0];
    buff->AXIS_Y = (temp[3] << 8) | temp[2];
    buff->AXIS_Z = (temp[5] << 8) | temp[4];

#if PLATFORMID == 113
    buff->AXIS_X = -buff->AXIS_X;
    buff->AXIS_Z = -buff->AXIS_Z;
#endif

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetMagAxesRaw(AxesRaw_t* buff)
{
    int8_t rc;
    uint8_t temp[6];

    //sanity check
    if ( ! buff )
    {
        return -1;
    }

    rc = LSM9DS1_magAvailable(ALL_AXIS);
    if ( rc <= 0 )
    {
        return -1;
    }

    // Read 6 bytes, beginning at OUT_X_L_M
    rc  = LSM9DS1_mReadBytes(OUT_X_L_M, temp, 6);
    if ( rc )
    {
        return -1;
    }

    buff->AXIS_X = (temp[1] << 8) | temp[0];
    buff->AXIS_Y = (temp[3] << 8) | temp[2];
    buff->AXIS_Z = (temp[5] << 8) | temp[4];

#if PLATFORMID == 113
    buff->AXIS_X = -buff->AXIS_X;
    buff->AXIS_Z = -buff->AXIS_Z;
#endif

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetGyroAxesRaw(AxesRaw_t * buff)
{
    int8_t ret = -1;
    uint8_t temp[6];

    // sanity check
    if ( ! buff )
    {
        return -1;
    }

    // verify that device
    ret = LSM9DS1_gyroAvailable(ALL_AXIS);
    if ( ret <= 0 )
    {
        return -1;
    }

    ret = LSM9DS1_xgReadBytes(OUT_X_L_G, temp, 6); // Read 6 bytes, beginning at OUT_X_L_G
    if ( ret )
    {
        return -1;
    }

    // convert buffer to raw values
    buff->AXIS_X = (temp[1] << 8) | temp[0];
    buff->AXIS_Y = (temp[3] << 8) | temp[2];
    buff->AXIS_Z = (temp[5] << 8) | temp[4];

    // invert axes orientation on platform 1.3
#if PLATFORMID == 113
    buff->AXIS_X = -buff->AXIS_X;
    buff->AXIS_Z = -buff->AXIS_Z;
#endif

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_LSM9DS1(SPI_HandleTypeDef *hspi_acc,
        GPIO_TypeDef *_CS_XG_PORT, uint16_t _CS_XG_Pin,
        GPIO_TypeDef *_CS_M_PORT, uint16_t _CS_M_Pin)
{
    SPI_AGM = hspi_acc;
    CS_XG_PORT = _CS_XG_PORT;
    CS_XG_PIN = _CS_XG_Pin;
    CS_MAG_PORT = _CS_M_PORT;
    CS_MAG_PIN = _CS_M_Pin;

    __HAL_SPI_ENABLE(SPI_AGM);

    memset(&settings, 0, sizeof(settings));

    return LSM9DS1_init(CS_XG_PIN, CS_MAG_PIN);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_init(uint16_t xgAddr, uint16_t mAddr)
{
    int8_t ret;
    settings.device.agAddress = xgAddr;
    settings.device.mAddress = mAddr;

    //MODE SPI
    ret = LSM9DS1_xgWriteByte(CTRL_REG9, 0x04);
    if(ret < 0){ return ret;}
    //MODE SPI
    ret = LSM9DS1_mWriteByte(CTRL_REG3_M, 0x80);
    if(ret < 0){ return ret;}

    /**
     *
     * @brief GYRO
     *
     */
    settings.gyro.enabled = true;
    settings.gyro.enableX = true;
    settings.gyro.enableY = true;
    settings.gyro.enableZ = true;
    // gyro sample rate: value between 1-6
    // 1 = 14.9    4 = 238
    // 2 = 59.5    5 = 476
    // 3 = 119     6 = 952
    settings.gyro.sampleRate = G_ODR_595;
    // gyro scale can be 245, 500, or 2000
    settings.gyro.scale = G_SCALE_2000DPS;
    // gyro cutoff frequency: value between 0-3
    // Actual value of cutoff frequency depends
    // on sample rate.
    settings.gyro.bandwidth = 3;
    settings.gyro.lowPowerEnable = false;
    settings.gyro.HPFEnable = false;
    // Gyro HPF cutoff frequency: value between 0-9
    // Actual value depends on sample rate. Only applies
    // if gyroHPFEnable is true.
    settings.gyro.HPFCutoff = 9;
    settings.gyro.flipX = false;
    settings.gyro.flipY = false;
    settings.gyro.flipZ = false;
    settings.gyro.orientation = 0;
    settings.gyro.latchInterrupt = false;

    /**
     *
     * @brief ACCEL
     *
     */
    settings.accel.enabled = true;
    settings.accel.enableX = true;
    settings.accel.enableY = true;
    settings.accel.enableZ = true;
    // accel scale can be 2, 4, 8, or 16
    settings.accel.scale = A_SCALE_2G;
    // accel sample rate can be 1-6
    // 1 = 10 Hz    4 = 238 Hz
    // 2 = 50 Hz    5 = 476 Hz
    // 3 = 119 Hz   6 = 952 Hz
    settings.accel.sampleRate = XL_ODR_50;
    // Accel cutoff freqeuncy can be any value between -1 - 3.
    // -1 = bandwidth determined by sample rate
    // 0 = 408 Hz   2 = 105 Hz
    // 1 = 211 Hz   3 = 50 Hz
    settings.accel.bandwidth = -1;
    settings.accel.highResEnable = false;
    // accelHighResBandwidth can be any value between 0-3
    // LP cutoff is set to a factor of sample rate
    // 0 = ODR/50    2 = ODR/9
    // 1 = ODR/100   3 = ODR/400
    settings.accel.highResBandwidth = 0;

    /**
     *
     * @brief MAG
     *
     */
    settings.mag.enabled = false;
    // mag scale can be 4, 8, 12, or 16
    settings.mag.scale = M_SCALE_4GS;
    // mag data rate can be 0-7
    // 0 = 0.625 Hz  4 = 10 Hz
    // 1 = 1.25 Hz   5 = 20 Hz
    // 2 = 2.5 Hz    6 = 40 Hz
    // 3 = 5 Hz      7 = 80 Hz
    settings.mag.sampleRate = M_ODR_80;
    settings.mag.tempCompensationEnable = true;
    // magPerformance can be any value between 0-3
    // 0 = Low power mode      2 = high performance
    // 1 = medium performance  3 = ultra-high performance
    settings.mag.XYPerformance = 3;
    settings.mag.ZPerformance = 3;
    settings.mag.lowPowerEnable = false;
    // magOperatingMode can be 0-2
    // 0 = continuous conversion
    // 1 = single-conversion
    // 2 = power down
    settings.mag.operatingMode = 2;

    /**
     *
     * @brief DIVERS
     *
     */
    settings.temp.enabled = true;

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_begin()
{
    uint8_t xgTest = 0;
    uint8_t mTest  = 0;
    int8_t ret = 0;

    LSM9DS1_constrainScales();

    LSM9DS1_initSPI();	// Initialize SPI

    // To verify communication, we can read from the WHO_AM_I register of
    // each device. Store those in a variable so we can return them.
    ret = LSM9DS1_mReadByte (WHO_AM_I_M, &mTest);	// Read the gyro WHO_AM_I
    if(ret != 0) return ret;
    ret = LSM9DS1_xgReadByte(WHO_AM_I_XG, &xgTest);	// Read the accel/mag WHO_AM_I
    if(ret != 0) return ret;
    /* test if WHO I AM registers contain the correct data */
    if (((xgTest << 8) | mTest) != ((WHO_AM_I_AG_RSP << 8) | WHO_AM_I_M_RSP))
    {
        return -1;
    }

    // Gyro initialization stuff:
    LSM9DS1_initGyro();	// This will "turn on" the gyro. Setting up interrupts, etc.

    // Accelerometer initialization stuff:
    LSM9DS1_initAccel(); // "Turn on" all axes of the accel. Set up interrupts, etc.

    // Magnetometer initialization stuff:
    LSM9DS1_initMag(); // "Turn on" all axes of the mag. Set up interrupts, etc.

    // Once everything is initialized, return 0:
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_initGyro()
{
    int8_t ret;
    uint8_t tempRegValue = 0;

    // CTRL_REG1_G (Default value: 0x00)
    // [ODR_G2][ODR_G1][ODR_G0][FS_G1][FS_G0][0][BW_G1][BW_G0]
    // ODR_G[2:0] - Output data rate selection
    // FS_G[1:0] - Gyroscope full-scale selection
    // BW_G[1:0] - Gyroscope bandwidth selection

    // To disable gyro, set sample rate bits to 0. We'll only set sample
    // rate if the gyro is enabled.
    if (settings.gyro.enabled)
    {
        tempRegValue = (settings.gyro.sampleRate & 0x07) << 5;
    }
    switch (settings.gyro.scale)
    {
        case 500:
            tempRegValue |= (0x1 << 3);
            break;
        case 2000:
            tempRegValue |= (0x3 << 3);
            break;
            // Otherwise we'll set it to 245 dps (0x0 << 4)
    }
    tempRegValue |= (settings.gyro.bandwidth & 0x3);
    ret = LSM9DS1_xgWriteByte(CTRL_REG1_G, tempRegValue);
    if(ret < 0){ return ret;}

    // CTRL_REG2_G (Default value: 0x00)
    // [0][0][0][0][INT_SEL1][INT_SEL0][OUT_SEL1][OUT_SEL0]
    // INT_SEL[1:0] - INT selection configuration
    // OUT_SEL[1:0] - Out selection configuration
    ret = LSM9DS1_xgWriteByte(CTRL_REG2_G, 0x00);
    if(ret < 0){ return ret;}

    // CTRL_REG3_G (Default value: 0x00)
    // [LP_mode][HP_EN][0][0][HPCF3_G][HPCF2_G][HPCF1_G][HPCF0_G]
    // LP_mode - Low-power mode enable (0: disabled, 1: enabled)
    // HP_EN - HPF enable (0:disabled, 1: enabled)
    // HPCF_G[3:0] - HPF cutoff frequency
    tempRegValue = settings.gyro.lowPowerEnable ? (1<<7) : 0;
    if (settings.gyro.HPFEnable)
    {
        tempRegValue |= (1<<6) | (settings.gyro.HPFCutoff & 0x0F);
    }
    ret = LSM9DS1_xgWriteByte(CTRL_REG3_G, tempRegValue);
    if(ret < 0){ return ret;}

    // CTRL_REG4 (Default value: 0x38)
    // [0][0][Zen_G][Yen_G][Xen_G][0][LIR_XL1][4D_XL1]
    // Zen_G - Z-axis output enable (0:disable, 1:enable)
    // Yen_G - Y-axis output enable (0:disable, 1:enable)
    // Xen_G - X-axis output enable (0:disable, 1:enable)
    // LIR_XL1 - Latched interrupt (0:not latched, 1:latched)
    // 4D_XL1 - 4D option on interrupt (0:6D used, 1:4D used)
    tempRegValue = 0;
    if (settings.gyro.enableZ) tempRegValue |= (1<<5);
    if (settings.gyro.enableY) tempRegValue |= (1<<4);
    if (settings.gyro.enableX) tempRegValue |= (1<<3);
    if (settings.gyro.latchInterrupt) tempRegValue |= (1<<1);
    ret = LSM9DS1_xgWriteByte(CTRL_REG4, tempRegValue);
    if(ret < 0){ return ret;}

    // ORIENT_CFG_G (Default value: 0x00)
    // [0][0][SignX_G][SignY_G][SignZ_G][Orient_2][Orient_1][Orient_0]
    // SignX_G - Pitch axis (X) angular rate sign (0: positive, 1: negative)
    // Orient [2:0] - Directional user orientation selection
    tempRegValue = 0;
    if (settings.gyro.flipX) tempRegValue |= (1<<5);
    if (settings.gyro.flipY) tempRegValue |= (1<<4);
    if (settings.gyro.flipZ) tempRegValue |= (1<<3);
    ret = LSM9DS1_xgWriteByte(ORIENT_CFG_G, tempRegValue);
    if(ret < 0){ return ret;}

    /* interrupts */
    /* interrupt active low on gyroscope */
    ret = LSM9DS1_configInt(XG_INT1, INT1_IG_G, INT_ACTIVE_LOW, INT_PUSH_PULL);
    if(ret < 0){ return ret;}
    ret = LSM9DS1_configGyroInt(((YHIE_XL)),false,false);
    if(ret < 0){ return ret;}

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_initAccel()
{
    int8_t ret;
    uint8_t tempRegValue = 0;

    //	CTRL_REG5_XL (0x1F) (Default value: 0x38)
    //	[DEC_1][DEC_0][Zen_XL][Yen_XL][Zen_XL][0][0][0]
    //	DEC[0:1] - Decimation of accel data on OUT REG and FIFO.
    //		00: None, 01: 2 samples, 10: 4 samples 11: 8 samples
    //	Zen_XL - Z-axis output enabled
    //	Yen_XL - Y-axis output enabled
    //	Xen_XL - X-axis output enabled
    if (settings.accel.enableZ) tempRegValue |= (1<<5);
    if (settings.accel.enableY) tempRegValue |= (1<<4);
    if (settings.accel.enableX) tempRegValue |= (1<<3);

    ret = LSM9DS1_xgWriteByte(CTRL_REG5_XL, tempRegValue);
    if(ret < 0){ return ret;}

    // CTRL_REG6_XL (0x20) (Default value: 0x00)
    // [ODR_XL2][ODR_XL1][ODR_XL0][FS1_XL][FS0_XL][BW_SCAL_ODR][BW_XL1][BW_XL0]
    // ODR_XL[2:0] - Output data rate & power mode selection
    // FS_XL[1:0] - Full-scale selection
    // BW_SCAL_ODR - Bandwidth selection
    // BW_XL[1:0] - Anti-aliasing filter bandwidth selection
    tempRegValue = 0;
    // To disable the accel, set the sampleRate bits to 0.
    if (settings.accel.enabled)
    {
        tempRegValue |= (settings.accel.sampleRate & 0x07) << 5;
    }
    switch (settings.accel.scale)
    {
        case 4:
            tempRegValue |= (0x2 << 3);
            break;
        case 8:
            tempRegValue |= (0x3 << 3);
            break;
        case 16:
            tempRegValue |= (0x1 << 3);
            break;
            // Otherwise it'll be set to 2g (0x0 << 3)
    }
    if (settings.accel.bandwidth >= 0)
    {
        tempRegValue |= (1<<2); // Set BW_SCAL_ODR
        tempRegValue |= (settings.accel.bandwidth & 0x03);
    }
    ret = LSM9DS1_xgWriteByte(CTRL_REG6_XL, tempRegValue);
    if(ret < 0){ return ret;}

    // CTRL_REG7_XL (0x21) (Default value: 0x00)
    // [HR][DCF1][DCF0][0][0][FDS][0][HPIS1]
    // HR - High resolution mode (0: disable, 1: enable)
    // DCF[1:0] - Digital filter cutoff frequency
    // FDS - Filtered data selection
    // HPIS1 - HPF enabled for interrupt function
    tempRegValue = 0;
    if (settings.accel.highResEnable)
    {
        tempRegValue |= (1<<7); // Set HR bit
        tempRegValue |= (settings.accel.highResBandwidth & 0x3) << 5;
    }
    ret = LSM9DS1_xgWriteByte(CTRL_REG7_XL, tempRegValue);
    if(ret < 0){ return ret;}

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_magOffset(uint8_t axis, int16_t offset)
{
    int8_t ret;
    if (axis > 2) return -2;
    uint8_t msb, lsb;
    msb = (offset & 0xFF00) >> 8;
    lsb = offset & 0x00FF;
    ret = LSM9DS1_mWriteByte(OFFSET_X_REG_L_M + (2 * axis), lsb);
    if(ret < 0){return ret;}
    ret = LSM9DS1_mWriteByte(OFFSET_X_REG_H_M + (2 * axis), msb);
    if(ret < 0){return ret;}
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_initMag()
{
    int8_t ret = -1;
    uint8_t tempRegValue = 0;

    // CTRL_REG1_M (Default value: 0x10)
    // [TEMP_COMP][OM1][OM0][DO2][DO1][DO0][0][ST]
    // TEMP_COMP - Temperature compensation
    // OM[1:0] - X & Y axes op mode selection
    //	00:low-power, 01:medium performance
    //	10: high performance, 11:ultra-high performance
    // DO[2:0] - Output data rate selection
    // ST - Self-test enable
    if (settings.mag.tempCompensationEnable) tempRegValue |= (1<<7);
    tempRegValue |= (settings.mag.XYPerformance & 0x3) << 5;
    tempRegValue |= (settings.mag.sampleRate & 0x7) << 2;
    ret = LSM9DS1_mWriteByte(CTRL_REG1_M, tempRegValue);
    if(ret < 0){return ret;}

    // CTRL_REG2_M (Default value 0x00)
    // [0][FS1][FS0][0][REBOOT][SOFT_RST][0][0]
    // FS[1:0] - Full-scale configuration
    // REBOOT - Reboot memory content (0:normal, 1:reboot)
    // SOFT_RST - Reset config and user registers (0:default, 1:reset)
    tempRegValue = 0;
    switch (settings.mag.scale)
    {
        case 8:
            tempRegValue |= (0x1 << 5);
            break;
        case 12:
            tempRegValue |= (0x2 << 5);
            break;
        case 16:
            tempRegValue |= (0x3 << 5);
            break;
            // Otherwise we'll default to 4 gauss (00)
    }
    ret = LSM9DS1_mWriteByte(CTRL_REG2_M, tempRegValue); // +/-4Gauss
    if(ret < 0){return ret;}

    // CTRL_REG3_M (Default value: 0x03)
    // [I2C_DISABLE][0][LP][0][0][SIM][MD1][MD0]
    // I2C_DISABLE - Disable I2C interace (0:enable, 1:disable)
    // LP - Low-power mode cofiguration (1:enable)
    // SIM - SPI mode selection (0:write-only, 1:read/write enable)
    // MD[1:0] - Operating mode
    //	00:continuous conversion, 01:single-conversion,
    //  10,11: Power-down
    tempRegValue = 0;
    if (settings.mag.lowPowerEnable)
        tempRegValue |= (1<<5);
    tempRegValue |= (settings.mag.operatingMode & 0x3);
    ret = LSM9DS1_mWriteByte(CTRL_REG3_M, tempRegValue); // Continuous conversion mode
    if(ret < 0){return ret;}

    // CTRL_REG4_M (Default value: 0x00)
    // [0][0][0][0][OMZ1][OMZ0][BLE][0]
    // OMZ[1:0] - Z-axis operative mode selection
    //	00:low-power mode, 01:medium performance
    //	10:high performance, 10:ultra-high performance
    // BLE - Big/little endian data
    tempRegValue = 0;
    tempRegValue = (settings.mag.ZPerformance & 0x3) << 2;
    ret = LSM9DS1_mWriteByte(CTRL_REG4_M, tempRegValue);
    if(ret < 0){return ret;}

    // CTRL_REG5_M (Default value: 0x00)
    // [0][BDU][0][0][0][0][0][0]
    // BDU - Block data update for magnetic data
    //	0:continuous, 1:not updated until MSB/LSB are read
    tempRegValue = 0;
    ret = LSM9DS1_mWriteByte(CTRL_REG5_M, tempRegValue);
    if(ret < 0){return ret;}
    return 0;
}

/**
 * @brief  Polls the accelerometer status register to check if new data is
 * available.
 * @param  -
 * @retval: 1  - New data available
 *			 0  - No new data available
 *			 -1 - Error during communication
 */
int8_t LSM9DS1_accelAvailable()
{
    uint8_t status;
    int8_t ret = -1;

    ret = LSM9DS1_xgReadByte(STATUS_REG_1, &status);
    if( ret < 0 )
    {
        return ret;
    }
    return (status & (1<<0));
}

/**
 * @brief  Polls the gyroscope status register to check if new data is
 * available.
 * @param  -
 * @retval: 1  - New data available
 *			 0  - No new data available
 *			 -1 - Error during communication
 */
int8_t LSM9DS1_gyroAvailable()
{
    uint8_t status;
    int8_t ret = -1;

    ret = LSM9DS1_xgReadByte(STATUS_REG_1, &status);
    if( ret < 0 )
    {
        return ret;
    }
    return ((status & (1<<1)) >> 1);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_tempAvailable()
{
    uint8_t status;
    int8_t ret = -1;

    ret = LSM9DS1_xgReadByte(STATUS_REG_1, &status);
    if( ret < 0 )
    {
        return ret;
    }
    return ((status & (1<<2)) >> 2);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_magAvailable(lsm9ds1_axis axis)
{
    uint8_t status;
    int8_t ret = -1;

    ret = LSM9DS1_mReadByte(STATUS_REG_M, &status);
    if( ret < 0 )
    {
        return ret;
    }
    return ((status & (1<<axis)) >> axis);
}


int8_t LSM9DS1_readTemp(float * lsm_temperature)
{
    int8_t ret;
    int8_t rc;
    uint8_t temp[2];
    int16_t raw_temp;

    // Check the pointer address is not NULL
    if ( ! lsm_temperature )
    {
        return -1;
    }

    /* if there is new data available */
    rc = LSM9DS1_tempAvailable();
    if ( rc <= 0 )
    {
        return -1;
    }


    // Read 2 bytes, beginning at OUT_TEMP_L
    ret = LSM9DS1_xgReadBytes(OUT_TEMP_L, temp, 2);
    if ( ret < 0 )
    {
        return -1;
    }

    raw_temp = ((int16_t)temp[1] << 8) | temp[0];
    *lsm_temperature = (float)(raw_temp / LSM9DS1_TEMP_SCALE + LSM9DS1_TEMP_BIAS);

    return 0;
}

//----------------------------------------------------------------------
float LSM9DS1_calcGyro(int16_t gyro)
{
    switch (settings.gyro.scale)
    {
        case 245:
        {
            return gyro * gyroscope_resolution_array[0];
        }
        case 500:
        {
            return gyro * gyroscope_resolution_array[1];
        }
        case 2000:
        {
            return gyro * gyroscope_resolution_array[2];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
float LSM9DS1_calcAccel(int16_t accel)
{
    switch (settings.accel.scale)
    {
        case 2:
        {
            return accel * accelerometer_resolution_array[0];
        }
        case 4:
        {
            return accel * accelerometer_resolution_array[1];
        }
        case 8:
        {
            return accel * accelerometer_resolution_array[2];
        }
        case 16:
        {
            return accel * accelerometer_resolution_array[3];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
float LSM9DS1_calcMag(int16_t mag)
{
    switch (settings.mag.scale)
    {
        case 4:
        {
            return mag * magnetometer_resolution_array[0];
        }
        case 8:
        {
            return mag * magnetometer_resolution_array[1];
        }
        case 12:
        {
            return mag * magnetometer_resolution_array[2];
        }
        case 16:
        {
            return mag * magnetometer_resolution_array[3];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
float LSM9DS1_Inv_calcGyro(float gyro)
{
    switch (settings.gyro.scale)
    {
        case 245:
        {
            return gyro / gyroscope_resolution_array[0];
        }
        case 500:
        {
            return gyro / gyroscope_resolution_array[1];
        }
        case 2000:
        {
            return gyro / gyroscope_resolution_array[2];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
float LSM9DS1_Inv_calcAccel(float accel)
{
    switch (settings.accel.scale)
    {
        case 2:
        {
            return accel / accelerometer_resolution_array[0];
        }
        case 4:
        {
            return accel / accelerometer_resolution_array[1];
        }
        case 8:
        {
            return accel / accelerometer_resolution_array[2];
        }
        case 16:
        {
            return accel / accelerometer_resolution_array[3];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
float LSM9DS1_Inv_calcMag(float mag)
{
    switch (settings.mag.scale)
    {
        case 4:
        {
            return mag / magnetometer_resolution_array[0];
        }
        case 8:
        {
            return mag / magnetometer_resolution_array[1];
        }
        case 12:
        {
            return mag / magnetometer_resolution_array[2];
        }
        case 16:
        {
            return mag / magnetometer_resolution_array[3];
        }
        default:
        {
            return 0.0;
        }
    }
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setGyroScale(uint16_t gScl)
{
    // Read current value of CTRL_REG1_G:
    uint8_t ctrl1RegValue;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG1_G, &ctrl1RegValue);
    if(ret == 0)
    {
        // Mask out scale bits (3 & 4):
        ctrl1RegValue &= 0xE7;
        switch (gScl)
        {
            case 500:
                ctrl1RegValue |= (0x1 << 3);
                settings.gyro.scale = 500;
                break;
            case 2000:
                ctrl1RegValue |= (0x3 << 3);
                settings.gyro.scale = 2000;
                break;
            default: // Otherwise we'll set it to 245 dps (0x0 << 4)
                settings.gyro.scale = 245;
                break;
        }
        LSM9DS1_xgWriteByte(CTRL_REG1_G, ctrl1RegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setAccelScale(uint8_t aScl)
{
    // We need to preserve the other bytes in CTRL_REG6_XL. So, first read it:
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG6_XL, &tempRegValue);
    if(ret == 0)
    {
        // Mask out accel scale bits:
        tempRegValue &= 0xE7;

        switch (aScl)
        {
            case 4:
                tempRegValue |= (0x2 << 3);
                settings.accel.scale = 4;
                break;
            case 8:
                tempRegValue |= (0x3 << 3);
                settings.accel.scale = 8;
                break;
            case 16:
                tempRegValue |= (0x1 << 3);
                settings.accel.scale = 16;
                break;
            default: // Otherwise it'll be set to 2g (0x0 << 3)
                settings.accel.scale = 2;
                break;
        }
        LSM9DS1_xgWriteByte(CTRL_REG6_XL, tempRegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setMagScale(uint8_t mScl)
{
    // We need to preserve the other bytes in CTRL_REG6_XM. So, first read it:
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_mReadByte(CTRL_REG2_M, &tempRegValue);
    if(ret == 0)
    {
        // Then mask out the mag scale bits:
        tempRegValue &= 0xFF^(0x3 << 5);

        switch (mScl)
        {
            case 8:
                tempRegValue |= (0x1 << 5);
                settings.mag.scale = 8;
                break;
            case 12:
                tempRegValue |= (0x2 << 5);
                settings.mag.scale = 12;
                break;
            case 16:
                tempRegValue |= (0x3 << 5);
                settings.mag.scale = 16;
                break;
            default: // Otherwise we'll default to 4 gauss (00)
                settings.mag.scale = 4;
                break;
        }

        // And write the new register value back into CTRL_REG6_XM:
        LSM9DS1_mWriteByte(CTRL_REG2_M, tempRegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setGyroODR(uint8_t gRate)
{
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG1_G, &tempRegValue);
    if(ret == 0)
    {
        // Then mask out the gyro ODR bits:
        tempRegValue &= 0xFF^(0x7 << 5);
        tempRegValue |= (gRate & 0x07) << 5;
        // Update our settings struct
        settings.gyro.sampleRate = gRate & 0x07;
        // And write the new register value back into CTRL_REG1_G:
        LSM9DS1_xgWriteByte(CTRL_REG1_G, tempRegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setAccelODR(uint8_t aRate)
{
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG6_XL, &tempRegValue);
    if(ret == 0)
    {
        // Then mask out the accel ODR bits:
        tempRegValue &= 0x1F;
        // Then shift in our new ODR bits:
        tempRegValue |= ((aRate & 0x07) << 5);
        settings.accel.sampleRate = aRate & 0x07;
        // And write the new register value back into CTRL_REG1_XM:
        LSM9DS1_xgWriteByte(CTRL_REG6_XL, tempRegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setMagODR(uint8_t mRate)
{
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_mReadByte(CTRL_REG1_M, &tempRegValue);
    if(ret == 0)
    {
        // Then mask out the mag ODR bits:
        tempRegValue &= 0xFF^(0x7 << 2);
        // Then shift in our new ODR bits:
        tempRegValue |= ((mRate & 0x07) << 2);
        settings.mag.sampleRate = mRate & 0x07;
        // And write the new register value back into CTRL_REG5_XM:
        LSM9DS1_mWriteByte(CTRL_REG1_M, tempRegValue);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configInt(interrupt_select interrupt, uint8_t generator,
        h_lactive activeLow, pp_od pushPull)
{
    uint8_t tempRegValue =  0;
    int8_t  ret          = -1;
    // Write to INT1_CTRL or INT2_CTRL. [interupt] should always be one of
    // those two values.
    // [generator] should be an OR'd list of values from the interrupt_generators enum
    ret = LSM9DS1_xgWriteByte(interrupt, generator);
    if(ret < 0){return ret;}

    ret = LSM9DS1_xgReadByte(CTRL_REG8, &tempRegValue);
    if(ret < 0){return ret;}
    WRITEBIT(tempRegValue,5,activeLow);
    WRITEBIT(tempRegValue,4,pushPull );
    ret = LSM9DS1_xgWriteByte(CTRL_REG8, tempRegValue);
    if(ret < 0){return ret;}
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configInactivity(uint8_t duration, uint8_t threshold, bool sleepOn)
{
    int8_t ret = -1;
    uint8_t temp = 0;

    temp = threshold & 0x7F;
    if (sleepOn) temp |= (1<<7);
    ret = LSM9DS1_xgWriteByte(ACT_THS, temp);
    if(ret < 0)
    {
        return ret;
    }

    return LSM9DS1_xgWriteByte(ACT_DUR, duration);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_getInactivity(uint8_t * inactivity)
{
    uint8_t tempRegValue;
    int8_t ret = LSM9DS1_xgReadByte(STATUS_REG_0, &tempRegValue);
    if(ret == 0)
    {
        *inactivity &= (0x10);
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configAccelInt(uint8_t generator, bool andInterrupts)
{
    // Use variables from accel_interrupt_generator, OR'd together to create
    // the [generator]value.
    uint8_t temp = generator;
    if (andInterrupts) temp |= 0x80;
    return LSM9DS1_xgWriteByte(INT_GEN_CFG_XL, temp);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configAccelThs(uint8_t threshold, lsm9ds1_axis axis, uint8_t duration, bool wait)
{
    // Write threshold value to INT_GEN_THS_?_XL.
    // axis will be 0, 1, or 2 (x, y, z respectively)
    if(LSM9DS1_xgWriteByte(INT_GEN_THS_X_XL + axis, threshold)<0)
    {
        return -1;
    }

    // Write duration and wait to INT_GEN_DUR_XL
    uint8_t temp;
    temp = (duration & 0x7F);
    if (wait) temp |= 0x80;
    return LSM9DS1_xgWriteByte(INT_GEN_DUR_XL, temp);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_getAccelIntSrc(uint8_t * AccelIntSrc)
{
    uint8_t intSrc;
    int8_t ret = LSM9DS1_xgReadByte(INT_GEN_SRC_XL, &intSrc);

    if(ret == -1) return ret;
    // Check if the IA_XL (interrupt active) bit is set
    if (intSrc & (1<<6))
    {
        *AccelIntSrc = (intSrc & 0x3F);
    }

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configGyroInt(uint8_t generator, bool aoi, bool latch)
{
    // Use variables from accel_interrupt_generator, OR'd together to create
    // the [generator]value.
    /* aoi -> 0: OR combination; 1: AND combination */
    uint8_t temp = generator;
    if (aoi) temp |= 0x80;
    if (latch) temp |= 0x40;
    return LSM9DS1_xgWriteByte(INT_GEN_CFG_G, temp);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configGyroThs(int16_t threshold, lsm9ds1_axis axis, uint8_t duration, bool wait, bool decrement)
{
    int8_t ret = -1;
    uint8_t buffer[2];
    buffer[0] = (threshold & 0x7F00) >> 8;
    buffer[1] = (threshold & 0x00FF);
    // Write threshold value to INT_GEN_THS_?H_G and  INT_GEN_THS_?L_G.
    // axis will be 0, 1, or 2 (x, y, z respectively)
    ret = LSM9DS1_xgWriteByte(INT_GEN_THS_XH_G +     (axis * 2), buffer[0]);
    if(ret < 0){return ret;}
    ret = LSM9DS1_xgWriteByte(INT_GEN_THS_XH_G + 1 + (axis * 2), buffer[1]);
    if(ret < 0){return ret;}
    // Write duration and wait to INT_GEN_DUR_G
    uint8_t temp = 0;
    temp = (duration & 0x7F);
    WRITEBIT(temp,7,wait);
    ret = LSM9DS1_xgWriteByte(INT_GEN_DUR_G, temp);
    if(ret < 0){return ret;}

    /* 1: decrement / 0: reset */
    ret = LSM9DS1_xgReadByte(INT_GEN_THS_XH_G,&temp);
    if(ret < 0){return ret;}
    WRITEBIT(temp,7,decrement);
    ret = LSM9DS1_xgWriteByte(INT_GEN_THS_XH_G,temp);
    if(ret < 0){return ret;}
    return 0;
}

//----------------------------------------------------------------------
uint8_t LSM9DS1_getGyroIntSrc()
{
    uint8_t intSrc;
    int8_t ret = LSM9DS1_xgReadByte(INT_GEN_SRC_G, &intSrc);
    if(ret == -1) return ret;

    // Check if the IA_G (interrupt active) bit is set
    if (intSrc & (1<<6))
    {
        return (intSrc & 0x3F);
    }

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configMagInt(uint8_t generator, h_lactive activeLow, bool latch)
{
    // Mask out non-generator bits (0-4)
    uint8_t config = (generator & 0xE0);
    // IEA bit is 0 for active-low, 1 for active-high.
    if (activeLow == INT_ACTIVE_HIGH) config |= (1<<2);
    // IEL bit is 0 for latched, 1 for not-latched
    if (!latch) config |= (1<<1);
    // As long as we have at least 1 generator, enable the interrupt
    if (generator != 0) config |= (1<<0);

    return LSM9DS1_mWriteByte(INT_CFG_M, config);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_configMagThs(uint16_t threshold)
{
    // Write high eight bits of [threshold] to INT_THS_H_M
    if(LSM9DS1_mWriteByte(INT_THS_H_M, (uint8_t)((threshold & 0x7F00) >> 8)) < 0)
    {
        return -1;
    }
    // Write low eight bits of [threshold] to INT_THS_L_M
    return LSM9DS1_mWriteByte(INT_THS_L_M, (uint8_t)(threshold & 0x00FF));
}

//----------------------------------------------------------------------
int8_t LSM9DS1_getMagIntSrc()
{
    uint8_t intSrc;
    int8_t ret = LSM9DS1_mReadByte(INT_SRC_M, &intSrc);

    if(ret < 0){ return ret;}
    // Check if the INT (interrupt active) bit is set
    if (intSrc & (1<<0))
    {
        return (intSrc & 0xFE);
    }

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_sleepGyro(bool enable)
{
    uint8_t temp;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG9, &temp);
    if(ret != 0) return ret;
    if (enable) temp |= (1<<6);
    else temp &= ~(1<<6);
    LSM9DS1_xgWriteByte(CTRL_REG9, temp);
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_enableFIFO(bool enable)
{
    uint8_t temp;
    int8_t ret = LSM9DS1_xgReadByte(CTRL_REG9, &temp);
    if(ret != 0) return ret;
    if (enable) temp |= (1<<1);
    else temp &= ~(1<<1);
    return LSM9DS1_xgWriteByte(CTRL_REG9, temp);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_setFIFO(fifoMode_type fifoMode, uint8_t fifoThs)
{
    // Limit threshold - 0x1F (31) is the maximum. If more than that was asked
    // limit it to the maximum.
    uint8_t threshold = fifoThs <= 0x1F ? fifoThs : 0x1F;
    return LSM9DS1_xgWriteByte(FIFO_CTRL, ((fifoMode & 0x7) << 5) | (threshold & 0x1F));
}

//----------------------------------------------------------------------
int8_t LSM9DS1_getFIFOSamples(uint8_t * sample)
{
    uint8_t temp;
    int8_t ret = LSM9DS1_xgReadByte(FIFO_SRC, &temp);
    if(ret < 0){ return ret;}
    * sample = temp & 0x3F;
    return ret;
}

//----------------------------------------------------------------------
void LSM9DS1_constrainScales()
{
    if ((settings.gyro.scale != 245) && (settings.gyro.scale != 500) &&
            (settings.gyro.scale != 2000))
    {
        settings.gyro.scale = 245;
    }

    if ((settings.accel.scale != 2) && (settings.accel.scale != 4) &&
            (settings.accel.scale != 8) && (settings.accel.scale != 16))
    {
        settings.accel.scale = 2;
    }

    if ((settings.mag.scale != 4) && (settings.mag.scale != 8) &&
            (settings.mag.scale != 12) && (settings.mag.scale != 16))
    {
        settings.mag.scale = 4;
    }
}

//----------------------------------------------------------------------
int8_t LSM9DS1_xgWriteByte(uint8_t subAddress, uint8_t data)
{
    // Whether we're using I2C or SPI, write a byte using the
    // gyro-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIwriteByte(CS_XG_PORT, settings.device.agAddress,
                                subAddress, data);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_mWriteByte(uint8_t subAddress, uint8_t data)
{
    // Whether we're using I2C or SPI, write a byte using the
    // accelerometer-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIwriteByte(CS_MAG_PORT, settings.device.mAddress, subAddress, data);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_xgReadByte(uint8_t subAddress, uint8_t * byte)
{
    // Whether we're using I2C or SPI, read a byte using the
    // gyro-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIreadByte(CS_XG_PORT, settings.device.agAddress,
                               subAddress, byte);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_xgReadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count)
{
    // Whether we're using I2C or SPI, read multiple bytes using the
    // gyro-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIreadBytes(CS_XG_PORT, settings.device.agAddress,
                                subAddress, dest, count);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_mReadByte(uint8_t subAddress, uint8_t * byte)
{
    // Whether we're using I2C or SPI, read a byte using the
    // accelerometer-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIreadByte(CS_MAG_PORT, settings.device.mAddress,
                               subAddress, byte);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_mReadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count)
{
    // Whether we're using I2C or SPI, read multiple bytes using the
    // accelerometer-specific I2C address or SPI CS pin.
    return LSM9DS1_SPIreadBytes(CS_MAG_PORT, settings.device.mAddress,
                                subAddress, dest, count);
}

//----------------------------------------------------------------------
void LSM9DS1_initSPI()
{
    LSM9DS1_SPI_deselect(CS_XG_PORT, CS_XG_PIN);
    LSM9DS1_SPI_deselect(CS_MAG_PORT, CS_MAG_PIN);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_SPIwriteByte(GPIO_TypeDef* csPort, uint16_t csPin,
        uint8_t subAddress, uint8_t data)
{
    uint8_t tbuf[2] = { subAddress & 0x3F, data };
    int8_t ret = -1;
    LSM9DS1_SPI_select(csPort, csPin); // Initiate communication

    // If write, bit 0 (MSB) should be 0
    // If single write, bit 1 should be 0
    if(HAL_SPI_Transmit(SPI_AGM, tbuf, 2, 200) == HAL_OK) ret = 0;

    LSM9DS1_SPI_deselect(csPort, csPin); // Close communication
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_SPIreadByte(GPIO_TypeDef* csPort, uint16_t csPin,
        uint8_t subAddress, uint8_t * byte)
{
    return LSM9DS1_SPIreadBytes(csPort, csPin, subAddress, byte, 1);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_SPIreadBytes(GPIO_TypeDef* csPort, uint16_t csPin,
        uint8_t subAddress, uint8_t * dest, uint8_t count)
{
    // To indicate a read, set bit 0 (msb) of first byte to 1
    uint8_t rAddress = 0x80 | (subAddress & 0x3F);
    // Mag SPI port is different. If we're reading multiple bytes,
    // set bit 1 to 1. The remaining six bytes are the address to be read
    if ((csPin == settings.device.mAddress) && count > 1)
        rAddress |= 0x40;

    LSM9DS1_SPI_select(csPort, csPin); // Initiate communication

    if(HAL_SPI_Transmit(SPI_AGM, &rAddress, 1, 200)!=HAL_OK)
    {
        LSM9DS1_SPI_deselect(csPort, csPin); // Close communication
        return -1;
    }
    if(HAL_SPI_Receive(SPI_AGM, dest, count, 200) != HAL_OK)
    {
        LSM9DS1_SPI_deselect(csPort, csPin); // Close communication
        return -1;
    }
    LSM9DS1_SPI_deselect(csPort, csPin); // Close communication
    return 0;
}

//----------------------------------------------------------------------
void LSM9DS1_SPI_select(GPIO_TypeDef* csPort, uint16_t CS_PIN)
{
    HAL_GPIO_WritePin(csPort, CS_PIN, GPIO_PIN_RESET);
}

//----------------------------------------------------------------------
void LSM9DS1_SPI_deselect(GPIO_TypeDef* csPort, uint16_t CS_PIN)
{
    HAL_GPIO_WritePin(csPort, CS_PIN, GPIO_PIN_SET);
    //HAL_Delay(1);
}

//----------------------------------------------------------------------
int8_t LSM9DS1_PowerDown(void)
{
    int rc = 0;

    // accelerometer
    rc = LSM9DS1_PowerDownAcc();
    if ( rc )
    {
        return -1;
    }

    // gyroscope
    rc = LSM9DS1_PowerDownGyro();
    if ( rc )
    {
        return -1;
    }

    // magnetometer
    rc = LSM9DS1_PowerDownMag();
    if ( rc )
    {
        return -1;
    }

    /* turn off Data Enable */
    HAL_GPIO_WritePin(DEN_A_G_GPIO_Port, DEN_A_G_Pin, GPIO_PIN_RESET);

    return 0;
}

//----------------------------------------------------------------------
int LSM9DS1_PowerDownAcc(void)
{
    int rc = 0;
    AxesRaw_t buff;
    uint8_t temp = 0;

    // read current configuration
    rc = LSM9DS1_xgReadByte(CTRL_REG6_XL, &temp);
    if ( rc )
    {
        debug_printf("unable to read register 0x%08X\n", CTRL_REG6_XL);
        return -1;
    }

    // set ODR to Power Off the device
    CLEARBITS8(temp, 5, 3);

    // kick off the command
    rc = LSM9DS1_xgWriteByte(CTRL_REG6_XL, temp);
    if ( rc )
    {
        debug_printf("unable to write register 0x%08X\n", CTRL_REG6_XL);
        return -1;
    }

    // acknowledge unread data
    LSM9DS1_GetAccAxesRaw(&buff);

    return 0;
}

//----------------------------------------------------------------------
int LSM9DS1_PowerDownGyro(void)
{
    int rc = 0;
    AxesRaw_t buff;
    uint8_t temp = 0;

    // read current configuration
    rc = LSM9DS1_xgReadByte(CTRL_REG1_G, &temp);
    if ( rc )
    {
        debug_printf("unable to read register 0x%08X\n", CTRL_REG1_G);
        return -1;
    }

    // set ODR to power Off the device
    CLEARBITS8(temp, 5, 3);

    // kick off the command
    rc = LSM9DS1_xgWriteByte(CTRL_REG1_G, temp);
    if ( rc )
    {
        debug_printf("unable to write register 0x%08X\n", CTRL_REG1_G);
        return -1;
    }

    // acknowledge unread data
    LSM9DS1_GetGyroAxesRaw(&buff);

    return 0;
}

//----------------------------------------------------------------------
int LSM9DS1_PowerDownMag(void)
{
    int rc = 0;
    AxesRaw_t buff;

    /* Power Off Magnetometer */
    rc = LSM9DS1_mWriteByte(CTRL_REG3_M, 2);
    if ( rc )
    {
        debug_printf("unable to write register 0x%08X\n", CTRL_REG3_M);
        return -1;
    }

    LSM9DS1_GetMagAxesRaw(&buff);

    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_PowerUp(void)
{
    AxesRaw_t buff;

    /* this will start both Accelerometer & gyroscope */
    int8_t ret = LSM9DS1_setGyroODR(settings.gyro.sampleRate);
    if(ret < 0){return ret;}

    ret = LSM9DS1_TurnOnMagContinuousConversion();
    if ( ret )
    {
        debug_printf("unable to start magnetometer");
        return ret;
    }

    HAL_GPIO_WritePin(DEN_A_G_GPIO_Port, DEN_A_G_Pin, GPIO_PIN_SET);

    /* a certain number of samples need to be discarded after a power up */
    for(int i = 0 ; i < 18 ; i++)
    {
        LSM9DS1_GetMagAxesRaw(&buff);
        LSM9DS1_GetAccAxesRaw(&buff);
        LSM9DS1_GetGyroAxesRaw(&buff);
    }
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetAccelerometerValuesG(float *x, float *y, float *z)
{
    int8_t ret = -1;
    AxesRaw_t accDataRaw = {0};
    if(LSM9DS1_GetAccAxesRaw(&accDataRaw)==0)
    {
        *x = LSM9DS1_calcAccel(accDataRaw.AXIS_X); // Store x-axis values into x
        *y = LSM9DS1_calcAccel(accDataRaw.AXIS_Y); // Store y-axis values into y
        *z = LSM9DS1_calcAccel(accDataRaw.AXIS_Z); // Store z-axis values into z
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetMagnetometerValuesGs(float *x, float *y, float *z)
{
    int8_t ret = -1;
    AxesRaw_t magDataRaw = {0};
    if(LSM9DS1_GetMagAxesRaw(&magDataRaw) == 0)
    {
        *x = LSM9DS1_calcMag(magDataRaw.AXIS_X); // Store x-axis values into x
        *y = LSM9DS1_calcMag(magDataRaw.AXIS_Y); // Store y-axis values into y
        *z = LSM9DS1_calcMag(magDataRaw.AXIS_Z); // Store z-axis values into z
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetMagnetometerValuesuT50(float *x, float *y, float *z)
{
    int8_t ret = -1;
    /* 1 microtesla = 1000nT = 10 mGs */
    /* 100 uT = 1Gs   */
    /* uT/50 = Gs x 2 */
    if(LSM9DS1_GetMagnetometerValuesGs(x,y,z)==0)
    {
        *x *= 2.0f;
        *y *= 2.0f;
        *z *= 2.0f;
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetMagnetometerValuesuT(float *x, float *y, float *z)
{
    int8_t ret = -1;
    /* 1 microtesla = 1000nT = 10 mGs */
    /* 100 uT = 1Gs   */
    if(LSM9DS1_GetMagnetometerValuesGs(x,y,z)==0)
    {
        *x *= 100.0f;
        *y *= 100.0f;
        *z *= 100.0f;
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetMagnetometerValuesmGs(float *x, float *y, float *z)
{
    int8_t ret = -1;
    if(LSM9DS1_GetMagnetometerValuesGs(x,y,z)==0)
    {
        *x *= 1000.0f;
        *y *= 1000.0f;
        *z *= 1000.0f;
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_GetGyroscopeValuesDPS(float *x, float *y, float *z)
{
    int8_t ret = -1;
    AxesRaw_t gyrDataRaw = {0};
    if(LSM9DS1_GetGyroAxesRaw(&gyrDataRaw)==0)
    {
        *x = LSM9DS1_calcGyro(gyrDataRaw.AXIS_X); // Store x-axis values into x
        *y = LSM9DS1_calcGyro(gyrDataRaw.AXIS_Y); // Store y-axis values into y
        *z = LSM9DS1_calcGyro(gyrDataRaw.AXIS_Z); // Store z-axis values into z
        ret = 0;
    }
    return ret;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_TurnOnMagForOneSample(void)
{
    //		int8_t ret = LSM9DS1_mWriteByte(CTRL_REG3_M, 1); // Single conversion mode
    //		/* delay to let the component boot & aquire data */
    //		if(ret == 0) HAL_Delay(10);
    //		return ret;

    // magnetometer is now set in continuous conversion mode
    return 0;
}

//----------------------------------------------------------------------
int8_t LSM9DS1_TurnOnMagContinuousConversion(void)
{
    return LSM9DS1_mWriteByte(CTRL_REG3_M, 0);// continuous conversion mode
}

//----------------------------------------------------------------------
int8_t LSM9DS1_HardReset(void)
{
    LSM9DS1_PowerDown();
    LSM9DS1_mWriteByte(CTRL_REG8, 0x80); /* 1000 0000 reboot memory content*/
    LSM9DS1_mWriteByte(CTRL_REG8, 0x01); /* 0000 0001 reboot software reset */
    LSM9DS1_PowerUp();
    return 0;
}

