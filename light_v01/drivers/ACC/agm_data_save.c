//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdint.h>

// internal
#include <eflash.h>
#include <trace.h>

// local
#include "agm_data_save.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define DATASAVE_FLASH_IDX    0
#define DATASAVE_SECT_SIZE    0x1000     // 4kB

// NOTE: that's dirty but i actually don't have
// to change the database api in order to debug
// IllBeBack library. So, yes..I use the memory
// from dive record which is not used in this app
#define SAVEAGM_DATACOUNT_START     0x15000
#define SAVEAGM_DATA_START          0x16000
#define SAVEAGM_DATA_SIZE

//----------------------------------------------------------------------
// Private variables
//----------------------------------------------------------------------
static uint32_t datacount = 0;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
uint32_t agm_data_save_size(void)
{
    int rc = 0;
    uint32_t data_size = 0;

    // read datacount from flash
    rc = eflash_read(DATASAVE_FLASH_IDX, SAVEAGM_DATACOUNT_START,
                     (uint8_t *)&data_size, sizeof(data_size));
    if ( rc )
    {
        debug_printf("unable to retrieve data size from flash\n");
        return 0;
    }

    return (data_size == 0xFFFFFFFF) ? 0 : data_size;
}

//----------------------------------------------------------------------
int agm_data_save_get_buffer(size_t offset, uint8_t * buffer, size_t len)
{
    int rc = 0;

    // sanity check
    if ( ! buffer || (len < sizeof(agm_data_save_entry_t)) )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // read data@offset from flash
    rc = eflash_read(DATASAVE_FLASH_IDX,
                     (SAVEAGM_DATA_START + (offset * sizeof(agm_data_save_entry_t))),
                     (uint8_t *)buffer, sizeof(agm_data_save_entry_t));
    if ( rc )
    {
        debug_printf("unable to retrieve data from flash\n");
        return -1;
    }

    return sizeof(agm_data_save_entry_t);
}

//----------------------------------------------------------------------
int agm_data_save_start(void)
{
    int rc = 0;
    datacount = 0;
    agm_data_save_entry_t start_data = {0};

    rc = eflash_read(DATASAVE_FLASH_IDX, SAVEAGM_DATACOUNT_START,
                     (uint8_t*)&datacount, sizeof(datacount));
    if ( rc )
    {
        debug_printf("cannot retrieve data count\n");
        return -1;
    }

    // if that's the first record reset counter
    if ( datacount == 0xFFFF )
    {
        datacount = 0;
    }

    // mark record session with 0 in each field
    rc = agm_data_save_add(&start_data);
    if ( rc )
    {
        debug_printf("cannot mark the start of the dive\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int agm_data_save_stop(void)
{
    int rc = 0;

    // save datacount to flash for future use
    rc = eflash_erase_sector(DATASAVE_FLASH_IDX,SAVEAGM_DATACOUNT_START);
    if ( rc )
    {
        debug_printf("cannot erase sector of data count\n");
        return -1;
    }

    //
    rc = eflash_prog(DATASAVE_FLASH_IDX,SAVEAGM_DATACOUNT_START,
                     (uint8_t*)&datacount, sizeof(datacount));
    if ( rc )
    {
        debug_printf("cannot write current data count\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int agm_data_save_add(agm_data_save_entry_t * data)
{
    int rc = 0;
    uint32_t next_addr = SAVEAGM_DATA_START;

    // next free address in flash
    next_addr = next_addr + (datacount * sizeof(agm_data_save_entry_t));

    // NOTE: we consider - to save time - that the whole
    // flash has been erased before to start a new record
    rc = eflash_prog(DATASAVE_FLASH_IDX, next_addr,
                     (uint8_t*)data, sizeof(agm_data_save_entry_t));
    if ( rc )
    {
        debug_printf("failed to save data entry in flash\n");
        return -1;
    }

    // update counter
    datacount++;

    return 0;
}

//----------------------------------------------------------------------
void agm_data_save_reset(void)
{
    eflash_erase_sector(DATASAVE_FLASH_IDX,SAVEAGM_DATACOUNT_START);
    datacount = 0;
    eflash_prog(DATASAVE_FLASH_IDX,SAVEAGM_DATACOUNT_START, (uint8_t*)&datacount, sizeof(datacount));
    eflash_erase_sector(DATASAVE_FLASH_IDX,SAVEAGM_DATA_START);
}

//----------------------------------------------------------------------
void agm_data_save_print(void) 
{
    int rc = 0;
    datacount = 0;
    agm_data_save_entry_t d = {0};

    // read datacount from flash
    rc = eflash_read(DATASAVE_FLASH_IDX, SAVEAGM_DATACOUNT_START, 
                     (uint8_t *)&datacount, sizeof(datacount));
    if ( rc )
    {
        return;
    }

#ifdef DEBUG 
    printf("ax,ay,az,gx,gy,gz,mx,my,mz,depth,dT(sec)\n");
#endif 
 
    for(int i = 0 ; i < datacount ; i++) 
    {
        // read data in flash
    	rc = eflash_read(DATASAVE_FLASH_IDX,
    	                 SAVEAGM_DATA_START + (i*sizeof(agm_data_save_entry_t)),
                         (uint8_t*)&d,sizeof(agm_data_save_entry_t));
        if ( rc )
        {
            break;
        }
 
#ifdef DEBUG 
    	printf("%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,\n",
                d.ax, d.ay, d.az,
                d.gx, d.gy, d.gz,
                d.mx, d.my, d.mz,
                d.depth, d.dTsec);
#endif 
    } 
} 
