#ifndef _AGM_DATA_SAVE_H_
#define _AGM_DATA_SAVE_H_

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct agm_data_save_entry
{
    int16_t   ax ;
    int16_t   ay ;
    int16_t   az ;
    int16_t   gx ;
    int16_t   gy ;
    int16_t   gz ;
    int16_t   mx ;
    int16_t   my ;
    int16_t   mz ;
    float     temp;
    float     depth;
    float     dTsec;
}agm_data_save_entry_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
uint32_t agm_data_save_size(void);
void agm_data_save_print(void) ;
int agm_data_save_start(void);
int agm_data_save_stop(void);
int  agm_data_save_add(agm_data_save_entry_t * data);
void agm_data_save_reset(void);
int agm_data_save_get_buffer(size_t offset, uint8_t * buffer, size_t len);

#endif //_AGM_DATA_SAVE_H_
