/*
 * acc_types.h
 *
 *  Created on: 4 juil. 2016
 *      Author: Olivier
 */

#ifndef ACC_TYPES_H_
#define ACC_TYPES_H_

//#include "LSM9DS1_Includes.h"

//////////////////////////////////////////////////////////////
//		Types
//////////////////////////////////////////////////////////////
#define RAD2DEG	 57.2957795130823f
#define DEG2RAD	 0.01745329251994f
#define G_CONSTANT (9.80665)

typedef struct
{
	float r[3][3];
} rotation_t;

typedef struct
{
	float offset_x;
	float offset_y;
	float offset_z;

	float scale_factor_x[3];
	float scale_factor_y[3];
	float scale_factor_z[3];
} mag_calibration_v2_t;

typedef struct
{
	int32_t x;
	int32_t y;
	int32_t z;
} vectorI_t;


typedef struct {
	float w;
	float x;
	float y;
	float z;
} QuaternionFloat;


typedef struct {
	float psi;
	float theta;
	float phi;
} EulerAngles;

typedef struct {
	float pitch;
	float yaw;
	float roll;
} PyrRot;

/**
 * @brief Raw Values
 * Direct type alias
 */
typedef struct
{
	int16_t AXIS_X;
	int16_t AXIS_Y;
	int16_t AXIS_Z;
} AxesRaw_t;

typedef struct
{
	float X;
	float Y;
	float Z;
} Axes_t;

#define accel_lib_raw_axes_t	AxesRaw_t


/**
 * @brief MASQUE des Types de calibration ACC
 * Valeurs exclusives, sauf la derniere
 */
#define CALIBRATION_GPS_RIGHT	0		//GPS face vers la droite
#define CALIBRATION_GPS_LEFT	1		//GPS face vers la gauche
#define CALIBRATION_GPS_UP		2		//Debout, GPS en haut de face
#define CALIBRATION_GPS_DOWN	4		//Debout, GPS en bas derriere
#define CALIBRATION_GPS_SKY		8		//A plat, GPS vers le ciel
#define CALIBRATION_GPS_GROUND	16		//A plat, GPS vers le sol

#define CALIBRATION_GYRO		32

#define	CALIBRATION_CALCULATE	64		//Calibration acc terminee, demande de calcul, bodyframe et sauvegarde

#define CALIBRATION_WITH_GUN	128

#endif /* ACC_TYPES_H_ */
