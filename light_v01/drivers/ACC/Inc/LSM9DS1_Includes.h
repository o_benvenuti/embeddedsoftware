/*
 * LSM9DS1_Includes.h
 *
 *  Created on: 2 janv. 2018
 *      Author: AixSonic
 */

#ifndef LSM9DS1_INCLUDES_H_
#define LSM9DS1_INCLUDES_H_

#include "main.h"
#include <stdbool.h>
#include <string.h>
#include <math.h>

//#include "MadgwickAHRS.h"
//#include "AHRS_v2.h"
//#include "math_3d.h"
#include "LSM9DS1_Registers.h"
#include "LSM9DS1_Types.h"
#include "acc_types.h"
#include "accel_lib.h"
#include "LSM9DS1.h"
//#include "stm32fxxx_hal.h"
//#include "tm_stm32_delay.h"
//#include "eeprom_utils.h"
//#include "AHRS_v2.h"


////Mag Calibration done! bias X=1828 Y=1220 Z=1346
//#define MAG_BIAS_X (uint16_t)1828
//#define MAG_BIAS_Y (uint16_t)1220
//#define MAG_BIAS_Z (uint16_t)1346

//Magnetometer Biases are : X=2160 Y=1489 Z=994
#define MAG_BIAS_X (uint16_t)0
#define MAG_BIAS_Y (uint16_t)0
#define MAG_BIAS_Z (uint16_t)0


// We'll store the gyro, accel, and magnetometer readings in a series of
// public class variables. Each sensor gets three variables -- one for each
// axis. Call readGyro(), readAccel(), and readMag() first, before using
// these variables!
// These values are the RAW signed 16-bit readings from the sensors.
extern int16_t gx, gy, gz; // x, y, and z axis readings of the gyroscope
extern int16_t ax, ay, az; // x, y, and z axis readings of the accelerometer
extern int16_t mx, my, mz; // x, y, and z axis readings of the magnetometer
//extern int16_t temperature; // Chip temperature
extern int32_t mBiasRaw[3];


/**
 * @brief Basic Magnetic parameters
 */
#define		DECLINATION	1.56f
#define 	HARDWARE_NORTH_OFFSET 0.0f

//Valeur max du repos
#define		REST_ACC_MAGNITUDE				1.2f
#define		REST_ECRAN_ACC_MAGNITUDE		1.8f



#endif /* LSM9DS1_INCLUDES_H_ */
