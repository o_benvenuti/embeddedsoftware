#ifndef __ACCEL_LIB__H
#define __ACCEL_LIB__H

//--------------------------------------------------------------------
// API
//--------------------------------------------------------------------
int8_t agm_init(void);
void AGM_DEBUG(void);
int agm_gyro_update_bias(float * axes_bias);
int agm_gyro_compensated_dps(float * axes_dps);
int agm_set_flick_sensitivity(uint8_t sensitivity);

#endif  // __ACCEL_LIB__H
