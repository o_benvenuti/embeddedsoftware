/*
 * ms5837_driver.h
 *
 *  Created on: 27 sept. 2017
 *      Author: Olivier
 */

#ifndef MS5837_DRIVER_H_
#define MS5837_DRIVER_H_

#include "stm32l4xx_hal.h"
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

#define RN5837_I2C_ADDRESS		(uint8_t)0xEC

#define	RN5837_CMD_RESET					0x1E

#define	RN5837_CMD_CONVERT_D1_OSR256		0x40
#define	RN5837_CMD_CONVERT_D1_OSR512		0x42
#define	RN5837_CMD_CONVERT_D1_OSR1024		0x44
#define	RN5837_CMD_CONVERT_D1_OSR2048		0x46
#define	RN5837_CMD_CONVERT_D1_OSR4096		0x48
#define	RN5837_CMD_CONVERT_D1_OSR8192		0x4A
#define	RN5837_CMD_CONVERT_D2_OSR256		0x50
#define	RN5837_CMD_CONVERT_D2_OSR512		0x52
#define	RN5837_CMD_CONVERT_D2_OSR1024		0x54
#define	RN5837_CMD_CONVERT_D2_OSR2048		0x56
#define	RN5837_CMD_CONVERT_D2_OSR4096		0x58
#define	RN5837_CMD_CONVERT_D2_OSR8192		0x5A

#define	RN5837_CMD_ADC_READ					0x00
#define	RN5837_CMD_PROM_READ				0xA0	//A0 to AE

#define FLUID_DENSITY (uint32_t)997

int MS5837_Init(I2C_HandleTypeDef *hi2c_pressure);

void MS5837_I2CwriteByte(uint8_t subAddress, uint8_t data);

uint8_t MS5837_I2CreadByte(uint8_t subAddress);

uint16_t MS5837_I2CreadWord(uint8_t subAddress);

int MS5837_I2CreadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count);

void MS5837_RESET(void);

uint16_t MS5837_READ_PROM(uint8_t index);

uint32_t MS5837_ADC_READ(void);

int MS5837_GetPressionAndTemp(int32_t *pression, int32_t *temperature);

float mbarToDepth(int32_t mbar,int32_t startlevel);

int get_PressureTempDepth(int32_t *pressure, int32_t *temp, float *depth, int32_t startlevel);

void MS5837_Test(void);




#endif /* MS5837_DRIVER_H_ */
