/*
 * ms5837_driver.c
 *
 *  Created on: 27 sept. 2017
 *      Author: Olivier
 */
#include <stdio.h>
#include <math.h>

#include <al_tools.h>
#include <trace.h>

#include "Inc/ms5837_driver.h"

static uint16_t W0, C1, C2, C3, C4, C5, C6;

static I2C_HandleTypeDef *I2C_PRESSURE;
static uint8_t I2C_ADDRESS = 0x00;

int MS5837_Init(I2C_HandleTypeDef *hi2c_pressure)
{

	if(!hi2c_pressure)
	{
		debug_printf("Invalid argument\n");
		return -1;
	}

	I2C_PRESSURE = hi2c_pressure;
	I2C_ADDRESS = RN5837_I2C_ADDRESS;

	__HAL_I2C_ENABLE(I2C_PRESSURE);


	MS5837_RESET();

	W0 = MS5837_READ_PROM(0);
	C1 = MS5837_READ_PROM(1);
	C2 = MS5837_READ_PROM(2);
	C3 = MS5837_READ_PROM(3);
	C4 = MS5837_READ_PROM(4);
	C5 = MS5837_READ_PROM(5);
	C6 = MS5837_READ_PROM(6);

	return 0;
}


void MS5837_I2CwriteByte(uint8_t subAddress, uint8_t data)
{
	uint8_t tbuf[2] = { subAddress, data };

	// If write, bit 0 (MSB) should be 0
	// If single write, bit 1 should be 0
	HAL_I2C_Master_Transmit(I2C_PRESSURE, (I2C_ADDRESS & 0x7F), tbuf, 2, 100);

}

uint8_t MS5837_I2CreadByte(uint8_t subAddress)
{
	uint8_t temp;
	// Use the multiple read function to read 1 byte.
	// Value is returned to `temp`.
	MS5837_I2CreadBytes(subAddress, &temp, 1);
	return temp;
}

uint16_t MS5837_I2CreadWord(uint8_t subAddress)
{
	uint16_t res;

	//Read 7 words with 2 bytes length each
	MS5837_I2CreadBytes(subAddress, (uint8_t*)&res, sizeof(uint16_t));

	uint16_t ret = __builtin_bswap16(res);

	return ret;
}

int MS5837_I2CreadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count)
{
    int rc = HAL_I2C_Mem_Read(I2C_PRESSURE, I2C_ADDRESS, subAddress, I2C_MEMADD_SIZE_8BIT, dest, (uint16_t)count, 100);

    return (rc == HAL_OK ? true : false);
}

void MS5837_I2Ccommand(uint8_t subAddress)
{
	uint8_t tbuf = subAddress;

	// If write, bit 0 (MSB) should be 0
	// If single write, bit 1 should be 0
	HAL_I2C_Master_Transmit(I2C_PRESSURE, (I2C_ADDRESS), &tbuf, 1, 100);

}

void MS5837_RESET(void)
{
	MS5837_I2Ccommand(RN5837_CMD_RESET);
	HAL_Delay(500);
}

uint16_t MS5837_READ_PROM(uint8_t index)
{
	uint16_t res = 0;

	res = MS5837_I2CreadWord(RN5837_CMD_PROM_READ | (index<<1));

	return res;
}

uint32_t MS5837_ADC_READ(void)
{
	uint32_t ret = 0;
	uint8_t res[3] = {0};

#if 0
	uint32_t tickstart = HAL_GetTick();
	if(I2C_WaitOnFlagUntilTimeout(&I2C_PRESSURE, I2C_FLAG_BUSY, SET, 100, tickstart) != HAL_OK)
	{
		return 0;
	}
#endif
	HAL_Delay(20);

//	MS5837_I2CreadBytes(RN5837_CMD_ADC_READ, (uint8_t*) &res, 3);
	MS5837_I2CreadBytes(RN5837_CMD_ADC_READ, res, 3);
	ret = ((uint32_t) res[0] << 16);
	ret |= ((uint32_t) res[1] << 8);
	ret |= ((uint32_t) res[2]);

	return ret;
}

unsigned char crc4(uint16_t n_prom[]) // n_prom defined as 8x unsigned int (n_prom[8])
{
	int cnt; // simple counter
	uint16_t n_rem = 0; // crc remainder
	uint8_t n_bit;
	n_prom[0] = ((n_prom[0]) & 0x0FFF); // CRC byte is replaced by 0
	n_prom[7] = 0; // Subsidiary value, set to 0
	for (cnt = 0; cnt < 16; cnt++) // operation is performed on bytes
	{ // choose LSB or MSB
		if (cnt % 2 == 1)
			n_rem ^= (uint16_t) ((n_prom[cnt >> 1]) & 0x00FF);
		else
			n_rem ^= (uint16_t) (n_prom[cnt >> 1] >> 8);
		for (n_bit = 8; n_bit > 0; n_bit--)
		{
			if (n_rem & (0x8000))
				n_rem = (n_rem << 1) ^ 0x3000;
			else
				n_rem = (n_rem << 1);
		}
	}
	n_rem = ((n_rem >> 12) & 0x000F); // final 4-bit remainder is CRC code
	return (n_rem ^ 0x00);
}

/* This function implements paragraph
 * "PRESSURE AND TEMPERATURE CALCULATION extracted from the MS5837 datasheet
 * Maximum values for calculation results:
 * Pmin = 0 bar / Pmax = 30 bar
 * Tmin = -20 degC / Tmax = 85 degC Tref = 20 degC
 *
 * Output parameters are:
 * - Pression: expressed in mBar
 * - Temperature: expressed in degC*/
int MS5837_GetPressionAndTemp(int32_t *pression, int32_t *temperature)
{
    uint32_t D1, D2;

    // sanity check
    if ( ! pression || ! temperature ) {
        debug_printf("invalid parameters\n");
        return -1;
    }

	/****************************************************************/
	/* Data sensor capture */
	/****************************************************************/

	/* Read digital pressure value */
	MS5837_I2Ccommand(RN5837_CMD_CONVERT_D1_OSR8192);
	D1 = MS5837_ADC_READ();
	if ( D1 >  16777215 )
	{
	    D1 =  16777215;
	}

	/* Read digital temperature value */
	MS5837_I2Ccommand(RN5837_CMD_CONVERT_D2_OSR8192);
	D2 = MS5837_ADC_READ();
	if ( D2 >  16777215 )
	{
	    D2 =  16777215;
	}

	 /* if data is invalid */
	if((D1 == 0) || (D2 == 0) || (D1 == D2)) {
        return -1;
    }

	/**********************************************************************************************/
	/* PRESSURE AND TEMPERATURE CALCULATION - 1st ORDER */
	/**********************************************************************************************/
	/*** Calculate temperature** */
	/* Difference between actual and reference temperature */
	/*  dT = D2 - C5 * 2^8 */
	/* volatile */ int32_t dT = D2 - (C5*256);
	if(dT < -16776960) dT = -16776960;
	if(dT >  16777215) dT =  16777215;

	/* Actual temperature (-40�85�C with 0.01�C resolution) */
	/* TEMP = 20�C+dT*TEMPSENS = 2000+dT *C6 /2^23 */
	/* volatile */ float TEMP = (float)2000 + ((float)dT * (float)C6 / (float)8388608);
	if(TEMP < -4000) TEMP = -4000;
	if(TEMP >  8500) TEMP =  8500;



	/*** Calculate pressure ***/
	/* Offset at actual temperature */
	/*  OFF = OFFt1 + TCO*dT = C2*2^16 + ((C4*dT)/2^7) */
	/* volatile */ float OFF = (float)C2 * (float)65536 + (((float)C4 * (float)dT)/ (float)128);
	if(OFF < -17179344900) OFF = -17179344900;
	if(OFF >  25769410560) OFF =  25769410560;

	/* Sensitivity at actual temperature */
	/*  SENS = SENSt1 + TCS * dT = C1 * 2^15+(C3*dT)/2^8 */
	/* volatile */ float SENS = (float)C1 * (float)32768 + ((float)C3 * (float)dT) / (float)256;
	if(SENS < -8589672450) SENS = -8589672450;
	if(SENS >  12884705280) SENS =  12884705280;

	/* Temperature compensated pressure (0�30 bar with 0.25mbar resolution) */
	/*  P = D1 * SENS - OFF = (D1 * SENS / 2^21 - OFF) / 2^13 */
	/* volatile */ float PRESSION = (((float)D1 * (float)SENS / (float)2097152) - (float)OFF) / (float)8192;
	if(PRESSION < 0     ) PRESSION =       0;
	if(PRESSION > 300000) PRESSION =  300000;

	/**********************************************************************************************/
	/* SECOND ORDER TEMPERATURE COMPENSATION */
	/**********************************************************************************************/
	/* Implementation of second order temperature compensation */
	/* Input parameters: Pressure and Temperature parameters values first order (OFF, SENS and
	 * TEMPS) */
	/* volatile */ float Ti;
	/* volatile */ float OFFi;
	/* volatile */ float SENSi;
	/* volatile */ float OFF2;
	/* volatile */ float SENS2;
	/* volatile */ float TEMP2;
	/* volatile */ float PRESSION2;


	if ((TEMP / 100.0) < 20.0)
	{
	    /* Low Temperature */
	    Ti = (3.0 * (float)dT * (float)dT) / (float)8589934592;
	    OFFi  = ( 3.0 * (TEMP + 2000.0) * (TEMP + 2000.0) ) / 2.0;
	    SENSi = ( 5.0 * (TEMP + 2000.0) * (TEMP + 2000.0) ) / 8.0;

	    if ((TEMP / 100.0) < -15.0)
	    {
	        /* very low temperature */
	        OFFi  = OFFi  + 7 * ((TEMP + 1500.0)*(TEMP + 1500.0));
	        SENSi = SENSi + 4 * ((TEMP + 1500.0)*(TEMP + 1500.0));
	    }
	}
	else
	{
	    /* High Temperature */
	    Ti = (2.0 * dT * dT) / (float)137438953472;
	    OFFi = ((TEMP - 2000.0) * (TEMP - 2000.0)) / 16.0;
	    SENSi = 0;

	}

	/* Calculate pressure and temperature 2nd order */
	OFF2 = OFF - OFFi;
	SENS2 = SENS - SENSi;
	TEMP2 = (TEMP - Ti) / 100.0; // Value calculated in �C
	PRESSION2 = ((((float)D1 * SENS2) / 2097152.0 - OFF2) / 8192.0) / 10.0; // Value calculated in mbar

	*pression = (int32_t)PRESSION2;
	*temperature = (int32_t)TEMP2;

	return 0;
}

#define LATITUDE 43.3 /* marseille */
float mbarToDepth(int32_t mbar, int32_t startlevel)
{
	  double   X,GR,DepthTerm,P;
	  double Del = 0;

	  X = sin(LATITUDE/57.29578);
	  X = X*X;

	  P = ((double)mbar*100.0f-startlevel)/10000.0;
	  /* GR=GRAVITY VARIATION WITH LATITUDE: ANON (1970) BULLETIN GEODESIQUE */
	  GR      = 9.780318 * (1.0 + (0.0052788 + 0.0000236 * X) * X) + 0.000001092 * P;
	  DepthTerm = (((-0.00000000000000182 * P + 0.0000000002279) * P - 0.000022512) * P + 9.72659) * P;
	  return (float)((DepthTerm / GR) + (Del / 9.8));
}

int get_PressureTempDepth(int32_t *pressure, int32_t *temp, float *depth, int32_t startlevel)
{
    int rc = 0;
	int32_t current_pressure = 0;
	int32_t current_temperature = 0;
	float   current_depth = 0;

	/* Get sensor Data */
    rc = MS5837_GetPressionAndTemp(&current_pressure, &current_temperature);
    if ( rc ) {
        debug_printf("Unable to retrieve data from sensor\n");
        return rc;
    }

	/* Calculate depth */
	current_depth = mbarToDepth(current_pressure, startlevel);
	if (current_depth < 0)
	{
		current_depth = 0;
	}

	if(ISBETWEEN(current_pressure,500,20000))
	{
		*pressure  = current_pressure;
		*temp      = current_temperature;
		*depth     = current_depth;
	}

	return 0;
}

// DEPTH IN METERS FROM PRESSURE IN DECIBARS USING
// SAUNDERS AND FOFONOFF'S METHOD.
// DEEP-SEA RES., 1976, 23, 109-111.
// FORMULA REFITTED FOR 1980 EQUATION OF STATE
// ----------------------------------------------------------
// UNITS:
//   PRESSURE     P      DECIBARS
//   LATITUDE     LAT    DEGREES
//   DEPTH        DEPTH  METERS
//   DTN. HEIGHT  DEL    DYN. METERS (geopotential anomaly)
// ----------------------------------------------------------
// CHECKVALUE:
//   1.) DEPTH = 9712.653 M for P=10000 DECIBARS, LAT=30 DEG, DEL=0
//   ABOVE FOR STANDARD OCEAN: T=0 DEG CELSIUS; S=35 (PSS-78)
// ----------------------------------------------------------
// Original fortran code is found in:
//   UNESCO technical papers in marine science 44 (1983) -
//   'Algorithms for computation of fundamental properties of seawater'
// ----------------------------------------------------------
// Translated to object pascal by:
//   Dr. Jan Schulz, 20. May 2008, www.code10.info
//  https://goo.gl/Dq5c8W



float get_CurrentAltitude(void) /* m */
{
	int32_t current_pressure = 0;
	MS5837_GetPressionAndTemp(&current_pressure, NULL);
	return mbar2altitude(current_pressure);
}

void MS5837_Test(void)
{
#if defined(DEBUG)
    uint16_t prom[8];
    memset(prom, 0, 16);

    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ, (uint8_t*)prom, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+2, (uint8_t*)prom+2, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+4, (uint8_t*)prom+4, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+6, (uint8_t*)prom+6, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+8, (uint8_t*)prom+8, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+10, (uint8_t*)prom+10, 2);
    MS5837_I2CreadBytes(RN5837_CMD_PROM_READ+12, (uint8_t*)prom+12, 2);

    debug_printf("%d", crc4(prom));
    W0 = MS5837_READ_PROM(0);
    C1 = MS5837_READ_PROM(1);
    C2 = MS5837_READ_PROM(2);
    C3 = MS5837_READ_PROM(3);
    C4 = MS5837_READ_PROM(4);
    C5 = MS5837_READ_PROM(5);
    C6 = MS5837_READ_PROM(6);

    MS5837_I2Ccommand(RN5837_CMD_CONVERT_D1_OSR8192);
    uint32_t D1 = MS5837_ADC_READ();

    MS5837_I2Ccommand(RN5837_CMD_CONVERT_D2_OSR8192);
    uint32_t D2 = MS5837_ADC_READ();

    int64_t dT = D2 - C5 * 256;

    int64_t OFF = (int64_t) C2 * 65536 + ((int64_t) C4 * dT) / 128;
    int64_t SENS = (int64_t) C1 * 32768 + ((int64_t) C3 * dT) / 256;

    int64_t TEMP = (2000 + dT * C6 / 8388608) / 100;
    int64_t PRESSION = ((((int64_t) D1 * SENS / 2097152) - (int64_t) OFF) / 8192) / 10;

    debug_printf("Res:%lld , %Ld", TEMP, PRESSION);
#endif // DEBUG
}

