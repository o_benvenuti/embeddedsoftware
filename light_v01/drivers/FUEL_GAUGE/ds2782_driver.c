/*
 * ds2782_driver.c
 *
 *  Created on: 7 juil. 2017
 *      Author: Olivier
 */
/** @defgroup DS2782_DRIVER
 * @{
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include "Inc/ds2782_driver.h"
#include <trace.h>
//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static I2C_HandleTypeDef *BAT_I2C;
static int i2c_failure_counter = 0;

/* Buffer used for transmission */
uint8_t aTxBuffer[1];

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
/** Write data to the given register
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_write(uint8_t subAddress, uint8_t data);

/** Read 8bits data from the given register
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_read(uint8_t regAddress, uint8_t *data, uint16_t dataLength);

/** Read 16bits data from the given register
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_read16(uint8_t regAddress, int16_t *data);

/** merge two bytes in one word
 * @param 1st byte
 * @param 2nd byte
 * @returns 16 bit word
 */
static uint16_t BAT_get16BitData(uint8_t msbByte, uint8_t lsbByte);

/** Calibrate the fuel gauge and return its status
 * @return true on success false if not
 */
bool get_FuelGaugeCalibration(void);

/** Read and return accumulate current register
 * @return ACR register value
 */
uint16_t BAT_readAcrReg(void);

/** Program accumulate current register
 * @return true on success false on error
 */
bool BAT_setAcrReg(uint16_t reg);

/** Read, convert and return remaining
 *  active absolute capacity converted value
 *
 * @return RAAC register converted value
 */
float BAT_readRAACValue(void);

/** Read, convert and return remaining
 *  standby absolute capacity converted value
 *
 * @return RSAC register converted value
 */
float BAT_readRSACValue(void);

/** Read and return average current value
 *
 * @return IAV register value
 */
float BAT_readIavReg(void);

/** Read and return remaining
 *  active absolute capacity value
 *
 * @return RAAC register value
 */
uint16_t BAT_readRAACReg(void);

/** Read and return remaining standby
 *  absolute capacity value
 *
 * @return RSAC register value
 */
uint16_t BAT_readRSACReg(void);

/** Read and return full capacity
 *  register value
 *
 * @return Full register value
 */
uint16_t BAT_readFULLReg(void);

/** Read and return active empty
 *  register value
 *
 * @return Active empty register value
 */
uint16_t BAT_readAEReg(void);

/** Read and return standby empty
 *  register value
 *
 * @return Standby empty register value
 */
uint16_t BAT_readSEReg(void);

/** Read and return age scalar
 *  register value
 *
 * @return Age scalar register value
 */
uint8_t BAT_readASReg(void);

/** Read and return status
 *  register value
 *
 * @return Status register value
 */
uint8_t BAT_readStatusReg(void);

/** Read and return eeprom
 *  register value
 *
 * @return eeprom register value
 */
uint8_t BAT_readEEPROMReg(void);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int BAT_Init(I2C_HandleTypeDef *hI2C)
{
    int rc = 0;
    BAT_I2C = hI2C;

    rc = HAL_I2C_IsDeviceReady(BAT_I2C, (DS2782_I2C_ADDRESS & 0x7F), 1, TIMEOUT_I2C);
    if (rc != HAL_OK)
    {
        rc = -1;
    }
    else
    {
        rc = 0;
    }
    return rc;
}

//----------------------------------------------------------------------
uint8_t BAT_hwInit(void)
{
    int rc = 0;

    //BATTERIE LTP523450
    /**
     * Voir description calcul ici : https://www.maximintegrated.com/en/app-notes/index.mvp/id/3584
     */

    /** Sense Resistor Gain Register
     * The Sense Resistor Gain (RSGAIN) Register stores the calibration
     * factor that produces accurate readings in the Current Register
     * when a reference voltage is forced across SNS and VSS. This is
     * an 11-bit value with an LSB value of 1/1024. It is stored in
     * Addresses 78h and 79h, and has a range of 0 to 1.999. The
     * nominal value for this register is 1.000.
     * ValueStored (78h) = (RSGAIN * 1024) >> 8 = (1.02 * 1024) >> 8 = 02h
     * ValueStored (79h) = 1.02 * 1024 = CAh
    **/
    aTxBuffer[0] = 0x02;
    rc = BAT_write(RSGAIN_MSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0xca;
    rc = BAT_write(RSGAIN_LSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Control Register
     * The Control Register is stored in Address 60h and the bits are
     * formatted as described in the DS2780 data sheet. No calculations are required.
     * UVEN = 0 and PMOD = 0. No Low power mode is enabled.
    **/
    aTxBuffer[0] = 0x00; //
    rc = BAT_write(CONTROL, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    // Accumulation Bias
    aTxBuffer[0] = 0x00;
    rc = BAT_write(AB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Aging Capacity Register
     * The Aging Capacity Register stores the rated capacity used in estimating
     * the decrease in battery capacity during normal use. It is an unsigned
     * register with an LSB value of 6.25uVhr/RSNS. It is stored in Addresses
     * 62h and 63h, and has a range of 0uVhrs to 409.59375mVhrs. Assuming the
     * Sense Resistor has a value of 20 mOhms, the range is 0mAhrs to
     * 20479.68755mAhrs in 0.3125mAhrs steps.
     * AgingCapacity_uVhrs = AgingCapacity_uAhrs x SenseResistor_mOhms
     * AgingCapacity_uVhrs = 1015 mAhrs * 10 mOhms = 10000 uVhrs
     * ValueStored (62h) =  (AgingCapacity_uVhrs / 6.25 uVhrs) >> 8 = 06h
     * ValueStored (63h) = AgingCapacity_uVhrs / 6.25 uVhrs = 58h
    **/

    aTxBuffer[0] = 0x06; //
    rc = BAT_write(AC_MSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x58; //
    rc = BAT_write(AC_LSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Charge Voltage Register
     * The Charge Voltage Register stores the charge voltage threshold
     * used to detect a fully charged state. This is an unsigned
     * register with an LSB value of 19.52mV. It is stored in Address 64h,
     * and has a range of 0 to 4.9776V.
     * ValueStored (64h) = ChargeVoltage_V / 19.52mV
     * ValueStored (64h) = 4.2V / 0.01952V = d7h
    **/
    aTxBuffer[0] = 0xd7;
    rc = BAT_write(VCHG, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Minimum Charge Current Register
     * The Minimum Charge Current Register stores the charge current
     * threshold that detects a fully charged state. This is an unsigned
     * register with an LSB value of 50�V. It is stored in Address 65h,
     * and has a range of 0 to 12.75mV. Assuming the Sense Resistor
     * has a value of 20mO, the range is 0mA to 637.5mA in 2.5mA steps.
     * ChargeCurrent_�V = ChargeCurrent_mA * SenseResistor_mOhms
     * ChargeCurrent_�V = 70 mA * 10 mOhms = 700 �V
     * ValueStored (65h) = ChargeCurrent_�V / 50 �V = 0eh
    **/
    aTxBuffer[0] = 0x0e;
    rc = BAT_write(IMIN, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Active Empty Voltage Register
     * The Active Empty Voltage Register stores the voltage threshold
     * used to detect the Active Empty point. This is an unsigned register
     * with an LSB value of 19.52mV. It is stored in Address 66h,
     * and has a range of 0 to 4.9776V.
     *
     * ValueStored (66h) = AEVoltage_V / 19.52 mV = 3.05 V /  0.01952 V = 9ch
    **/
    aTxBuffer[0] = 0x99;
    rc = BAT_write(VAE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Active Empty Voltage Register
     * The Active Empty Current Register stores the discharge current
     * threshold that detects the Active Empty point. This is an
     * unsigned register with an LSB value of 200�V. It is stored
     * in Address 67h, and has a range of 0 to 51.2mV. Assuming
     * the Sense Resistor has a value of 20mO, the range is 0mA
     * to 2560mA in 10mA steps.
     *
     * AECurrent_�V = AECurrent_mA * SenseResistor_mOhms
     * AECurrent_�V = 20 mA * 10 mOhms = 200 �V
     *
     * ValueStored (67h) = AECurrent_�V / 200 �V = 01h
    **/
    aTxBuffer[0] = 0x01;
    rc = BAT_write(IAE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Active Empty 40 Register
     * The Active Empty 40 Register stores the Active Empty point value
     * for +40�C (as shown in Figure 11 of the DS2780 data sheet).
     * This is an unsigned register with an LSB value of parts per
     * million of the Full point at +40�C. It is stored in Address 68h,
     * and has a range of 0 to 24.9% of the Full point for +40�C.
     * ValueStored (68h) = ActiveEmpty_40_mAhrs / (Full_40_mAhrs * (2^-10))
     * ValueStored (68h) = 6 mAhrs / (1015 mAhrs * (2^-10)) = 06h
    **/
    aTxBuffer[0] = 0x01;
    rc = BAT_write(ACTIVE_EMPTY_40, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Active Empty 40 Register
     * The Sense Resistor Prime (RSNSP) Register stores the value of the
     * Sense Resistor that computes the absolute capacity results. This
     * is an unsigned register with an LSB value of 1mhos. It is stored
     * in Address 69h, and has a range of 1mhos to 255mhos, which is 1O
     * to 3.922mO.
     *
     * ValueStored (69h) = 1 / SenseResistor_Ohms = 1 / 0.010 Ohms = 64h
    **/
    aTxBuffer[0] = 0x64;
    rc = BAT_write(RSNSP, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Full 40 Register
     * This value is an unsigned register with an LSB value of 6.25�Vhr/RSNS
     * It is stored in addresses 6Ah and 6Bh and has a range of 0�Vhrs to
     * 40959.375 (=409.59375mVhrs/RSNS assuming the sense resistor has a value
     * of 10mOhms) in 0.6249 mAhr steps (409.5937/(2^16))
    **/
    aTxBuffer[0] = 0x06;
    rc = BAT_write(FULL_40_MSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x58;
    rc = BAT_write(FULL_40_LSB, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /* Full Slopes  */
    aTxBuffer[0] = 0x10;
    rc = BAT_write(FULL_3040_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x17;
    rc = BAT_write(FULL_2030_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x3c;
    rc = BAT_write(FULL_1020_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x49;
    rc =  BAT_write(FULL_0010_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /* Active Empty Slopes */
    aTxBuffer[0] = 0x03;
    rc = BAT_write(AE_3040_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x06;
    rc = BAT_write(AE_2030_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x0a;
    rc = BAT_write(AE_1020_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x17;
    rc = BAT_write(AE_0010_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /* Standby Empty Slopes */
    aTxBuffer[0] = 0x01;
    rc = BAT_write(SE_3040_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x02;
    rc = BAT_write(SE_2030_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x04;
    rc = BAT_write(SE_1020_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x05;
    rc = BAT_write(SE_0010_SLOPE, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    /** Sense Resistor Temperature Coefficient Register
     * The Sense Resistor Temperature Coefficient (RSTCO) Register
     * stores the temperature coefficient of the sense resistor.
     * This is an 8-bit value with an LSB value of 30.5176ppm/�C.
     * It is stored in Address 7Ah, and has a range of 0 to 7782ppm/�C.
     * A value of 0 disables the temperature-compensation function.
     *
     * ValueStored (7Ah) = RSTCO / 30.5176 = 0 / 30.5176 = 00h
    **/
    aTxBuffer[0] = 0;
    rc = BAT_write(RSTC, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    aTxBuffer[0] = 0x44;
    rc = BAT_write(FUNC_COMMAND_REG, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    HAL_Delay(1000);

    /** Age Scalar (AS)
     * In accordance with the DS2782 datasheet a value of 95% is recommanded as the starting AS value
     * at the time of pack manufacture to allow learning a larger capacity on batteries that have an
     * initial capacity greater than the nominal capacity programmed in the cell characteristic table.
    **/
    aTxBuffer[0] = 0x7a;
    rc = BAT_write(AS_REG, aTxBuffer[0]);
    if ( rc )
    {
        debug_printf("unable to write fuel gauge configuration\n");
        return -1;
    }

    if ( false == get_FuelGaugeCalibration() )
    {
        debug_printf("invalid calibration\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
uint8_t BAT_readTemperature(float *BatteryTemperature)
{
    uint8_t rawData[2];
    uint16_t rawTemp2 = 0;
    uint8_t rc = 0;

    rc = BAT_read(TEMP_MSB_REG, rawData, 2);

    if (rc != 0)
    {
        // error
        return -1;
    }
    else
    {
        // success
        rawTemp2 = (((rawData[0] & ~(1 << 7)) << 3) | ((rawData[1] >> 5) & 0xF));
        *BatteryTemperature = (float) (rawTemp2 * 0.125);
        return 0;
    }
}

//----------------------------------------------------------------------
uint8_t BAT_readCurrent(float *CurrentValue)
{
    /*
     * The units of measurement for current are dependent on the value of
     * the sense resistor.
     */
    uint8_t rc = 0;

#if 0
    uint8_t sense_res_raw;

    if ( BAT_read(RSNSP, &sense_res_raw, 1) )
        return false;
    if (sense_res_raw == 0)
    {
        //Erreur !! Resistance = 0
        return false;
    }
#endif

    uint8_t rawData[2] = {0};
    uint16_t rawRes = 0;

    rc = BAT_read(CURRENT_MSB_REG, rawData, 2);
    if (rc != 0)
    {
        // error
        return -1;
    }
    else
    {
        // success
        rawRes = BAT_get16BitData(rawData[0], rawData[1]);
#if 1
        //rawRes&=~(1<<15);
        if (rawRes & 0x8000)
        {
            // (DS2782_CURRENT_UNITS / (RSNSP_RESISTOR_VALUE * 1000)) mA
            *CurrentValue = (float) (rawRes - 65536) * 0.1563f;
        }
        else
        {
            //(DS2782_CURRENT_UNITS / (RSNSP_RESISTOR_VALUE * 1000))
            *CurrentValue = (float) (rawRes) * 0.1563f;
        }
#endif
        return 0;
    }
}

//----------------------------------------------------------------------
uint8_t BAT_readVoltage(float *VoltageValue)
{
    uint8_t rawData[2];
    uint16_t rawVolt = 0;
    uint8_t rc = 0;

    rc = BAT_read(VOLT_MSB_REG, rawData, 2);

    if (rc != 0)
    {
        // error
        return -1;
    }
    else
    {
        // success
        rawVolt = (((rawData[0] & ~(1 << 7)) << 3) | ((rawData[1] >> 5) & 0xF));
        *VoltageValue = (float) (rawVolt * 4.88)/1000.0f;
        return 0; // Convert mV to V
    }

}

//----------------------------------------------------------------------
bool FlagBatteryIsAlmostEmpty(void)
{
    uint8_t rc = 0;
    float VoltageValue, Value = 0.0f;

    rc = BAT_readVoltage(&VoltageValue);

    if (rc == 0)
    {
        Value = VoltageValue;
    }

    // WORKAROUND:
    // sometime the sensor returns 0
    // discard this wrong value
    if ( Value == 0.0 ) {
        return false;
    }

    /* If the battery voltage measured is below the threshold
    BATTERY_ALMOST_EMPTY_THRESHOLD then the function shall be return
    "true".
     */
    if ( Value < BATTERY_ALMOST_EMPTY_THRESHOLD ) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------
uint8_t BAT_LoadValue(uint8_t *BatteryPercent)
{
    uint8_t RSRCRegVal = 0;  //unit [%]

    /* return 0 in case of success, otherwise -1 */
    if (BAT_read(RSRC_REG, &RSRCRegVal, 1) != 0)
    {
        return -1;
    }
    else
    {
        *BatteryPercent = RSRCRegVal;
        return 0;
    }

}

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
uint8_t BAT_write(uint8_t subAddress, uint8_t data)
{
    int rc;
    uint8_t tbuf[2] = {subAddress, data};

    rc = HAL_I2C_Master_Transmit(BAT_I2C, (DS2782_I2C_ADDRESS & 0x7F), &tbuf[0], 2, TIMEOUT_I2C);
    if ( rc != HAL_OK ) {
        rc = -1;
    }

    return rc;
}

//----------------------------------------------------------------------
uint8_t BAT_read(uint8_t regAddress, uint8_t *data, uint16_t dataLength)
{
    int rc;

    rc = HAL_I2C_Mem_Read(BAT_I2C, DS2782_I2C_ADDRESS, (uint16_t)regAddress, I2C_MEMADD_SIZE_8BIT, data, dataLength, TIMEOUT_I2C);
    if ( rc != HAL_OK ) {
        rc = -1;
        i2c_failure_counter++;

        if ( i2c_failure_counter > I2C_MAX_FAILURE )
        {
            debug_printf("too many read attempts: reset I2C\n");
            i2c_failure_counter = 0;
            HAL_I2C_DeInit(BAT_I2C);
            HAL_Delay(10);
            HAL_I2C_Init(BAT_I2C);

            return -1;
        }
    }

    return rc;
}

//----------------------------------------------------------------------
uint8_t BAT_read16(uint8_t regAddress, int16_t *data)
{
    int rc;

    rc = HAL_I2C_Mem_Read(BAT_I2C, DS2782_I2C_ADDRESS, (uint16_t)regAddress,
            I2C_MEMADD_SIZE_8BIT, (uint8_t*)data, 2, TIMEOUT_I2C);
    if ( rc != HAL_OK ) {
        rc = -1;
        i2c_failure_counter++;

        if ( i2c_failure_counter > I2C_MAX_FAILURE )
        {
            i2c_failure_counter = 0;
            HAL_I2C_DeInit(BAT_I2C);
            HAL_Delay(10);
            HAL_I2C_Init(BAT_I2C);
        }
    }

    return rc;
}

//----------------------------------------------------------------------
static uint16_t BAT_get16BitData(uint8_t msbByte, uint8_t lsbByte)
{
    uint16_t data16Bit = (msbByte << 8) | (lsbByte);
    return data16Bit;
}

//----------------------------------------------------------------------
bool get_FuelGaugeCalibration(void)
{
    uint8_t rc = 0;
    float VoltageValue, Value = 0.0f;

    rc = BAT_readVoltage(&VoltageValue);

    if (rc == 0)
    {
        Value = VoltageValue;
    }
    /* 1015 mAh = Valeur de charge max obtenue lors de la caracterisation
     des batteries dans la pire cas)*/
    float BatteryMaxCapacity = 1015;

    /* Estimation du pourcentage de charge */
    float LoadRate = 0;

    /* Registre ACR */
    uint16_t ACR_value = 0;
    LoadRate = ((Value-3.05)/0.011)/100.0f;

    ACR_value = (uint16_t)((BatteryMaxCapacity*LoadRate)/0.625);

    if (BAT_setAcrReg(ACR_value)) return true;
    else return false;
}

//----------------------------------------------------------------------
uint16_t BAT_readAcrReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;
    //volatile float CurrentAccumulated = 0;
    if (BAT_read(ACR_MSB_REG, rawData, 2))
        return 0;//false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;
}

//----------------------------------------------------------------------
bool BAT_setAcrReg(uint16_t reg)
{
    uint8_t buf;
    buf = (reg >> 8) & 0xFF;
    if ( BAT_write(ACR_MSB_REG, buf) )
        return false;
    buf = reg & 0xFF;
    if ( BAT_write(ACR_LSB_REG, buf) )
        return false;
    return true;
}

//----------------------------------------------------------------------
float BAT_readRAACValue(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;

    if ( BAT_read(RAAC_MSB_REG, rawData, 2) )
        return false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);

    if(rawRes &0x8000){
        return ((float)(rawRes-65536)*1.6); // 1LSB = 1.6mAhr
    }
    else
    {
        return ((float)(rawRes)*1.6); // 1LSB = 1.6mAhr
    }
}

//----------------------------------------------------------------------
float BAT_readRSACValue(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;

    if ( BAT_read(RSAC_MSB_REG, rawData, 2) )
        return false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);

    if(rawRes &0x8000){
        return ((float)(rawRes-65536)*1.6); // 1LSB = 1.6mAhr
    }
    else
    {
        return ((float)(rawRes)*1.6); // 1LSB = 1.6mAhr
    }
}

//----------------------------------------------------------------------
float BAT_readIavReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;
    volatile float CurrentAverage = 0;
    if ( BAT_read(IAVG_MSB_REG, rawData, 2) )
        return false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);

    if (rawRes & 0x8000)
    {
        CurrentAverage = ((float) (rawRes - 65536) * 0.1563f);  //convert to mAh
    }
    else
        CurrentAverage = ((float) (rawRes) * 0.1563f);

    return CurrentAverage;
}

//----------------------------------------------------------------------
uint16_t BAT_readRAACReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;

    if ( BAT_read(RAAC_MSB_REG, rawData, 2) )
        return false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;

}

//----------------------------------------------------------------------
uint16_t BAT_readRSACReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;

    if ( BAT_read(RSAC_MSB_REG, rawData, 2) )
        return false;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;
}

//----------------------------------------------------------------------
uint16_t BAT_readFULLReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;
    if ( BAT_read(FULL_MSB_REG, &rawData[0], 2) )
        return 255;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;
}

//----------------------------------------------------------------------
uint16_t BAT_readAEReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;
    if ( BAT_read(AE_MSB_REG, &rawData[0], 2) )
        return 255;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;
}

//----------------------------------------------------------------------
uint16_t BAT_readSEReg(void)
{
    uint8_t rawData[2];
    uint16_t rawRes = 0;
    if ( BAT_read(SE_MSB_REG, &rawData[0], 2) )
        return 255;
    rawRes = BAT_get16BitData(rawData[0], rawData[1]);
    return rawRes;
}

//----------------------------------------------------------------------
uint8_t BAT_readASReg(void)
{
    uint8_t ASRegVal;
    if ( BAT_read(AS_REG, &ASRegVal, 1) )
        return 255;
    return ASRegVal;
}

//----------------------------------------------------------------------
uint8_t BAT_readStatusReg(void)
{
    uint8_t statusRegVal;
    if ( BAT_read(STATUS, &statusRegVal, 1) )
        return 255;
    return statusRegVal;
}

//----------------------------------------------------------------------
uint8_t BAT_readEEPROMReg(void)
{
    uint8_t statusRegVal;
    if ( BAT_read(EEPROM_REG, &statusRegVal, 1) )
        return 255;
    return statusRegVal;
}

/** @} */
