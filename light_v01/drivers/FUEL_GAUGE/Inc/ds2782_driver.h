/*
 * ds2782_driver.h
 *
 *  Created on: 7 juil. 2017
 *      Author: Olivier
 */

#ifndef DS2782_DRIVER_H_
#define DS2782_DRIVER_H_
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include "stm32l4xx_hal.h"
#include <stdbool.h>
#include <string.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define TIMEOUT_I2C                     100
#define I2C_MAX_FAILURE                 16
#define DS2782_I2C_ADDRESS              0x34<<1
#define BATTERY_ALMOST_EMPTY_THRESHOLD  3.0
/* Current unit measurement in uA for a 1 milli-ohm sense resistor */
#define DS2782_CURRENT_UNITS            1563
#define RSNSP_RESISTOR_VALUE            10          //en milli-ohm

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef enum
{
    empty = 0,
    almost_empty = 1,
    full = 2
} battery_status_t;

typedef enum
{
    STATUS = 0x01,                  /*STATUS - Status Register*/
    RAAC_MSB_REG,                   /*RAAC - Remaining Active Absolute Capacity MSB*/
    RAAC_LSB_REG, RSAC_MSB_REG,     /*RSAC - Remaining Standby Absolute Capacity MSB*/
    RSAC_LSB_REG, RARC_REG,         /*RARC - Remaining Active Relative Capacity*/
    RSRC_REG,                       /*RSRC - Remaining Standby Relative Capacity*/
    IAVG_MSB_REG,                   /*IAVG - Average Current Register MSB*/
    IAVG_LSB_REG, TEMP_MSB_REG,     /*TEMP - Temperature Register MSB*/
    TEMP_LSB_REG, VOLT_MSB_REG,     /*VOLT - Voltage Register MSB*/
    VOLT_LSB_REG, CURRENT_MSB_REG,  /*CURRENT - Current Register MSB*/
    CURRENT_LSB_REG, ACR_MSB_REG,   /*ACR - Accumulated Current Register MSB*/
    ACR_LSB_REG, ACRL_MSB_REG,      /*Low Accumulated Current Register MSB*/
    ACRL_LSB_REG, AS_REG,           /*AS - Age Scalar*/
    SFR_REG,                        /*SFR - Special Feature Register*/
    FULL_MSB_REG,                   /*FULL - Full Capacity MSB*/
    FULL_LSB_REG, AE_MSB_REG,       /*AE - Active Empty MSB*/
    AE_LSB_REG, SE_MSB_REG,         /*SE - Standby Empty MSB*/
    SE_LSB_REG, EEPROM_REG = 0x1F,  /*EEPROM - EEPROM Register */
    USR_EEPROM_REG = 0x20,          /*User EEPROM, Lockable, Block 0 [20 to 2F]*/
    ADD_USR_EEPROM_REG = 0x30,      /*Additional User EEPROM, Lockable, Block 0 [30 to 37]*/
    PARAM_EEPROM_REG = 0x60,        /*Parameter EEPROM, Lockable, Block 1 [60 to 7F]*/
    UNIQUE_ID_REG = 0xF0,           /*Unique ID [F0 to F7]*/
    FUNC_COMMAND_REG = 0xFE         /*Function Command Register */

} RegAddr;

typedef enum
{

    CONTROL = 0x60,         //Control Register
    AB = 0x61,              //Accumulation Bias
    AC_MSB = 0x62,          //Aging Capacity MSB
    AC_LSB = 0x63,          //Aging Capacity LSB
    VCHG = 0x64,            //Charge Voltage
    IMIN = 0x65,            //Minimum Charge Current
    VAE = 0x66,             //Active Empty Voltage
    IAE = 0x67,             //Active Empty Current
    ACTIVE_EMPTY_40,        //0x68
    RSNSP,                  //Sense Resistor Prime	//0x69
    FULL_40_MSB,	        //0x6A
    FULL_40_LSB,
    FULL_3040_SLOPE,
    FULL_2030_SLOPE,
    FULL_1020_SLOPE,
    FULL_0010_SLOPE,
    AE_3040_SLOPE,
    AE_2030_SLOPE,
    AE_1020_SLOPE,
    AE_0010_SLOPE,
    SE_3040_SLOPE,
    SE_2030_SLOPE,
    SE_1020_SLOPE,
    SE_0010_SLOPE,
    RSGAIN_MSB,             //Sense Resistor Gain MSB
    RSGAIN_LSB,             //Sense Resistor Gain LSB
    RSTC,                   //Sense Resistor Temp. Coeff.
    FRSGAIN_MSB,            //Factory Gain MSB
    FRSGAIN_LSB,            //Factory Gain LSB
    I2C_SLAVE_ADDR = 0x7E   //2-Wire Slave Address
} ParamEepromReg;

typedef enum
{
    PORF = 0x02,        //Power-On Reset Flag � Useful for reset detection, see text below.
    UVF = 0x04,         //Under-Voltage Flag
    LEARNF = 0x10,      //Learn Flag � When set to 1, a charge cycle can be used to learn battery capacity.
    SEF = 0x20,         //Standby Empty Flag
    AEF = 0x40,         //Active Empty Flag
    CHGTF = 0x80,       //Charge Termination Flag
} StatusReg;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
/** This function allows to initialize I2C link with the fuel gauge device
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
int BAT_Init(I2C_HandleTypeDef *hI2C);

/** This function allows to store battery parameters into the Fuel Gauge EEPROM
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_hwInit(void);

/** This functions allows to read temperature information from fuel gauge
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_readTemperature(float *BatteryTemperature);

/** This functions allows to read current information from fuel gauge
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_readCurrent(float *CurrentValue);

/** This functions allows to read accumulated current information from fuel gauge
 *
 * @returns
 *   0 on error,
 *   accumulated current value on success
 */
uint16_t BAT_readAcrReg(void);

/** This functions allows to read voltage information from fuel gauge
 *
 * @returns
 *   0 on success,
 *   -1 on error
 */
uint8_t BAT_readVoltage(float *VoltageValue);

/** This function checks if battery voltage is under specified value
 *
 * @returns true if battery under value BATTERY_ALMOST_EMPTY_THRESHOLD,
 *          false if not
 */
bool FlagBatteryIsAlmostEmpty(void);

/** This function get the remaining standby relative capacity
 *
 * @returns 0 on success -1 on error
 */
uint8_t BAT_LoadValue(uint8_t *BatteryPercent);


#endif /* DS2782_DRIVER_H_ */
