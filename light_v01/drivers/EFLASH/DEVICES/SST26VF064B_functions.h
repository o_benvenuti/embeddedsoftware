#ifndef SST26VF064B_FUNCTIONS_H
#define SST26VF064B_FUNCTIONS_H
/**
 * @file:  SST26VF064B_functions.h
 *
 * @brief
 *
 * @date   19/03/2019
 *
 * @platform
 *    S1 TRACK REV 1.2
 *
 */
//----------------------------------------------------------------------
// Required includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define EFLASH_SST26_SECTOR_SIZE        (uint32_t)0x1000    // 4096 bytes
#define EFLASH_SST26_PAGE_SIZE          (uint32_t)0x100     // 256 bytes
#define EFLASH_SST26_64MB_SIZE          (uint32_t)0x800000

/* SST26 SPI Flash Device Operation Instructions */
/* Configuration */
#define EFLASH_SST26_CMD_RSTEN            0x66		/* Reset Enable */
#define EFLASH_SST26_CMD_RST              0x99		/* Reset Memory */
#define EFLASH_SST26_CMD_RDSR             0x05		/* Read Status Register */
#define EFLASH_SST26_CMD_WRSR             0x01		/* Write Status Register */
#define EFLASH_SST26_CMD_RDCR             0x35		/* Read Configuration Register */
#define EFLASH_SST26_B9_CMD               0xB9		/*   */

/* Read */
#define EFLASH_SST26_CMD_READ             0x03		/* Read Data Bytes */
#define EFLASH_SST26_CMD_HIGH_SPEED_READ  0x0B		/* Read Memory at Higher Speed */
#define EFLASH_SST26_CMD_SB               0xC0		/* Set Burst Length */
#define EFLASH_SST26_CMD_RBSPI            0xEC		/* Set Read Burst With Wrap */

/* Identification */
#define EFLASH_SST26_CMD_JDEC_ID          0x9F		/* JEDEC ID Read */
#define EFLASH_SST26_CMD_SFDP             0x5A		/* Serial Flash Discoverable Parameters */

/* Write */
#define EFLASH_SST26_CMD_WREN             0x06		/* Write Enable */
#define EFLASH_SST26_CMD_WRDI             0x04		/* Write Disable */
#define EFLASH_SST26_CMD_SE               0x20		/* 4KB Sector Erase instruction */
#define EFLASH_SST26_CMD_BE               0xD8		/* Block Erase instruction */
#define EFLASH_SST26_CMD_CE               0xC7		/* Full Chip Erase instruction */
#define EFLASH_SST26_CMD_WRITE            0x02		/* Page Program */
#define EFLASH_SST26_CMD_WRSU             0xB0		/* Suspends Program/Erase */
#define EFLASH_SST26_CMD_WRRE             0x30		/* Resumes Program/Erase */

/* Protection */

#define EFLASH_SST26_CMD_RBPR             0x72		/* Read Block-Protection Register */
#define EFLASH_SST26_CMD_WBPR             0x42		/* Write Block-Protection Register */
#define EFLASH_SST26_CMD_LBPR             0x8D		/* Lock Down Block-Protection Register */
#define EFLASH_SST26_CMD_nVWLDR           0xE8		/* non-Volatile Write LockDown Register */
#define EFLASH_SST26_CMD_ULBPR            0x98		/* Global Block Protection Unlock */
#define EFLASH_SST26_CMD_RSID             0x88		/* Read Security ID */
#define EFLASH_SST26_CMD_PSID             0xA5		/* Program User Security ID area */
#define EFLASH_SST26_CMD_LSID             0x85		/* Lockout Security ID Programming */

#define EFLASH_SST26_DUMMY_BYTE           0xFF

#define EFLASH_SST26_8MB_ID               0xBF2643      /* OLD JEDEC Read-ID Data */

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int eflash_sst26_discover(eflash_device_ctx_t ** context);


#endif	/* SST26VF064B_FUNCTIONS_H */

