//--------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <trace.h>

#include <main.h>
#include <stm32l4xx_hal.h>
#include <spi.h>
#include "../eflash_private.h"
#include "SST26VF064B_functions.h"

//--------------------------------------------------------------------
// Prototypes
//--------------------------------------------------------------------
int eflash_sst26_erase_chip(void);
int eflash_sst26_erase_sector(uint32_t SectorAddr);
int eflash_sst26_read_buf(uint32_t addr, void * buf, size_t len);
int eflash_sst26_prog_buf(uint32_t addr, void * buf, size_t len);

void _sst26_spi_select(void);
void _sst26_spi_deselect(void);
void _sst26_write_enable(void);
int16_t eflash_sst26_DisableBlockProtection(void);
void eflash_sst26_WakeUp(void);
int eflash_sst26_Page_Program(uint32_t Dst, void *buffer, size_t size);
uint32_t eflash_sst26_ReadID(void);
uint8_t eflash_sst26_SendByte(uint8_t byte);
uint8_t eflash_sst26_SendBytes(uint8_t *bytes, uint16_t len);
void eflash_sst26_WaitForWriteEnd(void);
uint8_t SFLASH_ReadState(void);
bool eflash_sst26_WriteState(uint8_t data);
uint8_t SFLASH_ReadBusy(void);
void eflash_sst26_ResetEn(void);
void eflash_sst26_Reset(void);

//--------------------------------------------------------------------
// Variables
//--------------------------------------------------------------------
eflash_device_ctx_t  eflash_sst26_ctx =
{
    .prog           = eflash_sst26_prog_buf,
    .read           = eflash_sst26_read_buf,
    .erase_chip     = eflash_sst26_erase_chip,
    .erase_sector   = eflash_sst26_erase_sector,
};

//--------------------------------------------------------------------
// API
//--------------------------------------------------------------------
int eflash_sst26_discover(eflash_device_ctx_t ** context)
{
    // sanity check
    if ( ! context )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // wake the device up
    eflash_sst26_WakeUp();

    // read JEDEC ID from the device
    switch ( eflash_sst26_ReadID() )
    {
        case EFLASH_SST26_8MB_ID:
        {
            debug_printf("Microchip flash: discovered SST26 64Mb\n");
            strcpy(eflash_sst26_ctx.dev_info.name, "SST26VF064B");
            eflash_sst26_ctx.dev_info.sector_size = EFLASH_SST26_SECTOR_SIZE;
            eflash_sst26_ctx.dev_info.total_size = EFLASH_SST26_64MB_SIZE;
            break;
        }
        default:
        {
            debug_printf("Microchip flash: unknown device\n");
            return -1;
        }
    }

    // make sure all blocks are available
    eflash_sst26_DisableBlockProtection();

    // return device context
    *context = &eflash_sst26_ctx;

    return 0;
}

//--------------------------------------------------------------------
int eflash_sst26_read_buf(uint32_t addr, void * buf, size_t len)
{
    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();

    /* Send "Read from Memory " instruction */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_READ);

    /* Send ReadAddr high nibble address byte to read from */
    eflash_sst26_SendByte((addr & 0xFF0000) >> 16);
    /* Send ReadAddr medium nibble address byte to read from */
    eflash_sst26_SendByte((addr & 0xFF00) >> 8);
    /* Send ReadAddr low nibble address byte to read from */
    eflash_sst26_SendByte(addr & 0xFF);

    // kick off actual command
    if (HAL_SPI_Receive(&hspi2, buf, len, 1000) != HAL_OK)
    {
        _sst26_spi_deselect();
        return -1;
    }

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    return 0;
}

//--------------------------------------------------------------------
int eflash_sst26_prog_buf(uint32_t addr, void * buf, size_t len)
{
    uint8_t *byte_ptr = (uint8_t*) buf;
    uint32_t in_buffer_idx                   = 0;
    uint32_t nb_bytes_to_write               = 0;
    uint32_t target_addr                     = addr;
    uint32_t current_page_start_address      = 0;
    uint32_t current_page_end_address        = 0;
    uint32_t available_space_in_current_page = 0;

    if ( len == 1 )
    {
        /* Enable the write access to the FLASH */
        _sst26_write_enable();

        /* Deselect the FLASH: Chip Select high */
        _sst26_spi_deselect();
        /* Select the FLASH: Chip Select low */
        _sst26_spi_select();
        /* Send "Byte Program" instruction */
        eflash_sst26_SendByte(EFLASH_SST26_CMD_WRITE);
        /* Send WriteAddr high nibble address byte to write to */
        eflash_sst26_SendByte((addr & 0xFF0000) >> 16);
        /* Send WriteAddr medium nibble address byte to write to */
        eflash_sst26_SendByte((addr & 0xFF00) >> 8);
        /* Send WriteAddr low nibble address byte to write to */
        eflash_sst26_SendByte(addr & 0xFF);
        /* Send the byte */
        eflash_sst26_SendByte(*byte_ptr);
        /* Deselect the FLASH: Chip Select high */
        _sst26_spi_deselect();
        /* Wait till the end of Flash writing */
        eflash_sst26_WaitForWriteEnd();

        return 0;
    }

    while (in_buffer_idx < len)
    {
        nb_bytes_to_write = len - in_buffer_idx;

        current_page_start_address = (target_addr/EFLASH_SST26_PAGE_SIZE)*EFLASH_SST26_PAGE_SIZE;
        current_page_end_address = current_page_start_address + EFLASH_SST26_PAGE_SIZE;
        available_space_in_current_page = current_page_end_address - target_addr;
        // if buffer is bigger than available space :
        // cap size to write to available space
        if(nb_bytes_to_write > available_space_in_current_page)
        {
            nb_bytes_to_write = available_space_in_current_page;
        }
        if(nb_bytes_to_write > 0)
        {
            _sst26_write_enable();
            /*
             See datasheet SST26V section 5.20 :
             When executing Page-Program, the memory range for
             the SST26VF064B/064BA is divided into 256 Byte
             page boundaries. The device handles shifting of more
             than 256 Bytes of data by maintaining the last 256
             Bytes of data as the correct data to be programmed. If
             the target address for the Page-Program instruction is
             not the beginning of the page boundary (A[7:0] are not
             all zero), and the number ofbytes of data input exceeds
             or overlaps the end of the address of the page boundary,
             the excess data inputs wrap around and will be programmed at the
             start of that target page.
             */
            eflash_sst26_Page_Program(target_addr, (byte_ptr + in_buffer_idx), nb_bytes_to_write);
            eflash_sst26_WaitForWriteEnd();
            // next address to start write will be actual address + size we just wrote
            target_addr += nb_bytes_to_write;
            // update buffer index to write new data
            in_buffer_idx += nb_bytes_to_write;
        }
    }

    return 0;
}

//--------------------------------------------------------------------
int eflash_sst26_erase_sector(uint32_t SectorAddr)
{
    if ( SectorAddr % EFLASH_SST26_SECTOR_SIZE )
    {
        debug_printf("address must be aligned on sector size\n");
        return -1;
    }

    /* Enable the write access to the FLASH */
    _sst26_write_enable();

    /* Sector Erase */
    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();

    /* Send Sector Erase instruction */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_SE);

    /* Send SectorAddr high nibble address byte */
    eflash_sst26_SendByte((SectorAddr & 0xFF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
    eflash_sst26_SendByte((SectorAddr & 0xFF00) >> 8);
    /* Send SectorAddr low nibble address byte */
    eflash_sst26_SendByte(SectorAddr & 0xFF);
    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Wait till the end of Flash writing */
    eflash_sst26_WaitForWriteEnd();

    return 0;
}

//--------------------------------------------------------------------
int eflash_sst26_erase_chip(void)
{
    /* Enable the write access to the FLASH */
    _sst26_write_enable();

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Bulk Erase */
    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();
    /* Send Bulk Erase instruction  */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_CE);
    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Wait till the end of Flash writing */
    eflash_sst26_WaitForWriteEnd();

    return 0;
}

//--------------------------------------------------------------------
// Helpers
//--------------------------------------------------------------------
void _sst26_spi_select(void)
{
    HAL_GPIO_WritePin(GPIOC, CS_Flash_Pin, GPIO_PIN_RESET);
}

//--------------------------------------------------------------------
void _sst26_spi_deselect(void)
{
    HAL_GPIO_WritePin(GPIOC, CS_Flash_Pin, GPIO_PIN_SET);
}

//--------------------------------------------------------------------
void _sst26_write_enable(void)
{
    // deselect the FLASH: Chip Select high
    _sst26_spi_deselect();

    // select the FLASH: Chip Select low
    _sst26_spi_select();

    // send "Write Enable" instruction
    eflash_sst26_SendByte(EFLASH_SST26_CMD_WREN);

    // release the flash chip select
    _sst26_spi_deselect();
}

//--------------------------------------------------------------------
int16_t eflash_sst26_DisableBlockProtection(void)
{
    // allow next command
    _sst26_write_enable();

    // chip select the flash
    _sst26_spi_deselect();
    _sst26_spi_select();

    // no idea why...
    eflash_sst26_WriteState(0x00);

    // release the flash chip select
    _sst26_spi_deselect();

    // allow next command
    _sst26_write_enable();

    // chip select the flash
    _sst26_spi_deselect();
    _sst26_spi_select();

    // actual command to release locks
    eflash_sst26_SendByte(EFLASH_SST26_CMD_ULBPR);

    // release the flash chip select
    _sst26_spi_deselect();

    return 0;
}

//--------------------------------------------------------------------
void eflash_sst26_WakeUp(void)
{
    _sst26_spi_select();
    HAL_Delay(100);
    _sst26_spi_deselect();

    eflash_sst26_WaitForWriteEnd();
}

/************************************************************************/
/* PROCEDURE:	Page_Program						*/
/*									*/
/* This procedure does page programming.  The destination               */
/* address should be provided.                                  	*/
/* The data array of 256 bytes contains the data to be programmed.      */
/* Since the size of the data array is 256 bytes rather than 256 bytes, this page program*/
/* procedure programs only 256 bytes                                    */
/* Assumption:  Address being programmed is already erased and is NOT	*/
/*		block protected.					*/
/* Input:								*/
/*		Dst:		Destination Address 000000H - 7FFFFFH	*/
/************************************************************************/
int eflash_sst26_Page_Program(uint32_t Dst, void *buffer, size_t size)
{
    uint8_t * byte_ptr = (uint8_t*) buffer;
    _sst26_spi_deselect(); /* enable device */
    _sst26_spi_select(); /* enable device */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_WRITE); /* send Byte Program command */
    /* Send SectorAddr high nibble address byte */
    eflash_sst26_SendByte((Dst & 0xFF0000) >> 16);
    /* Send SectorAddr medium nibble address byte */
    eflash_sst26_SendByte((Dst & 0xFF00) >> 8);
    /* Send SectorAddr low nibble address byte */
    eflash_sst26_SendByte(Dst & 0xFF);
    /* Deselect the FLASH: Chip Select high */


    eflash_sst26_SendBytes(byte_ptr, size);

    _sst26_spi_deselect(); /* disable device */

    return 0;
}

//--------------------------------------------------------------------
uint32_t eflash_sst26_ReadID(void)
{
    uint8_t byte[3];

    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();

    /* Send "JEDEC ID Read" instruction */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_JDEC_ID);

    /* Read a byte from the FLASH */
    byte[0] = eflash_sst26_SendByte(EFLASH_SST26_DUMMY_BYTE);

    /* Read a byte from the FLASH */
    byte[1] = eflash_sst26_SendByte(EFLASH_SST26_DUMMY_BYTE);

    /* Read a byte from the FLASH */
    byte[2] = eflash_sst26_SendByte(EFLASH_SST26_DUMMY_BYTE);

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    return (byte[0] << 16) | (byte[1] << 8) | byte[2];
}

/**
 * @brief  Sends a byte through the SPI interface and return the byte received
 *         from the SPI bus.
 * @param  byte: byte to send.
 * @retval The value of the received byte.
 */
uint8_t eflash_sst26_SendByte(uint8_t byte)
{
    uint8_t rbuf[1] = { 0x00 };

    HAL_SPI_TransmitReceive(&hspi2, &byte, rbuf, 1, 1000);

    return rbuf[0];
}

//--------------------------------------------------------------------
uint8_t eflash_sst26_SendBytes(uint8_t *bytes, uint16_t len)
{
    if (HAL_SPI_Transmit(&hspi2, bytes, len, 1000) != HAL_OK)
        return 0;
    else
        return 1;

}

/**
 * @brief  Polls the status of the Write In Progress (WIP) flag in the FLASH's
 *         status register and loop until write operation has completed.
 * @param  None
 * @retval None
 */
void eflash_sst26_WaitForWriteEnd(void)
{
    uint8_t flashstatus = 0;


    /* Loop as long as the memory is busy with a write cycle */
    do
    {
        /* Deselect the FLASH: Chip Select high */
        _sst26_spi_deselect();

        /* Select the FLASH: Chip Select low */
        _sst26_spi_select();

        /* Send "Read Status Register" instruction */
        eflash_sst26_SendByte(EFLASH_SST26_CMD_RDSR);

        /* Send a dummy byte to generate the clock needed by the FLASH
		 and put the value of the status register in FLASH_Status variable */
        flashstatus = eflash_sst26_SendByte(EFLASH_SST26_DUMMY_BYTE);
    } while (flashstatus & 0x80); /* Write in progress */

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();
}


/*
 ************************************************************
 *  @brief	SFLASH_ReadState()
 *  @return	M25P16
 *	@param  none
 *	@return
 ************************************************************
 */
uint8_t SFLASH_ReadState(void)
{
    uint8_t temp = 0;

    _sst26_spi_select();

    eflash_sst26_SendByte(EFLASH_SST26_CMD_RDSR);
    temp = eflash_sst26_SendByte(0);

    _sst26_spi_deselect();

    return temp;
}

/*
 ************************************************************
 *  @brief	SFLASH_WriteState()
 *  @return
 *	@param  dat
 *	@return  true - false
 ************************************************************
 */
bool eflash_sst26_WriteState(uint8_t data)
{
    _sst26_spi_select();
    eflash_sst26_SendByte(EFLASH_SST26_CMD_WRSR);
    eflash_sst26_SendByte(data);
    _sst26_spi_deselect();

    return true;
}

/*
 ************************************************************
 *  @brief	SFLASH_ReadBusy()
 *  @return
 *	@param  none
 *	@return
 ************************************************************
 */
uint8_t SFLASH_ReadBusy(void)
{
    uint8_t temp = SFLASH_ReadState();
    if (temp & 0x01)
        return 1;
    else
        return 0;
}

/************************************************************************/
/* PROCEDURE: ResetEn                                                   */
/*									*/
/* This procedure Enables acceptance of the RST (Reset) operation.	*/
/************************************************************************/
void eflash_sst26_ResetEn(void)
{
    /* Enable the write access to the FLASH */
    _sst26_write_enable();

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Bulk Erase */
    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();
    /* send instruction RESET ENABLE */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_RSTEN);
    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Wait till the end of Flash writing */
    eflash_sst26_WaitForWriteEnd();
}

/************************************************************************/
/* PROCEDURE: Reset                                     		*/
/*									*/
/* This procedure resets the device in to normal operating Ready mode.	*/
/*									*/
/************************************************************************/
void eflash_sst26_Reset(void)
{
    /* Enable the write access to the FLASH */
    _sst26_write_enable();

    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Bulk Erase */
    /* Select the FLASH: Chip Select low */
    _sst26_spi_select();
    /* send instruction RESET */
    eflash_sst26_SendByte(EFLASH_SST26_CMD_RST);
    /* Deselect the FLASH: Chip Select high */
    _sst26_spi_deselect();

    /* Wait till the end of Flash writing */
    eflash_sst26_WaitForWriteEnd();
}



