/**
 * @file:  eflash_s25.c
 *
 * @brief
 *  SPANSION S25 flash drivers
 *
 * @date   12/03/2019
 *
 * @platform
 *    S1 TRACK REV 1.3
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <string.h>

// sdk
#include <al_tools.h>
#include <spi.h>
#include <trace.h>

// local
#include "eflash_s25.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
bool eflash_s25_wip(void);
static int _s25_write_enable(void);
static void _s25_spi_select(void);
static void _s25_spi_deselect(void);
static int _s25_wait_completion(int command);

int eflash_s25_erase_chip(void);
int eflash_s25_erase_sector(uint32_t sector_address);
int eflash_s25_prog_buf(uint32_t addr, void * buf, size_t len);
int eflash_s25_read_buf(uint32_t addr, void * buf, size_t len);

//--------------------------------------------------------------------
// Variables
//--------------------------------------------------------------------
eflash_device_ctx_t  eflash_s25_ctx =
{
    .prog           = eflash_s25_prog_buf,
    .read           = eflash_s25_read_buf,
    .erase_chip     = eflash_s25_erase_chip,
    .erase_sector   = eflash_s25_erase_sector,
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int eflash_s25_discover(eflash_device_ctx_t ** context)
{
    int rc;
    uint8_t command, buffer[8] = { 0 };

    // chip select the flash
    _s25_spi_select();

    // send reset enable command
    command = EFLASH_S25_CMD_RSTENABLE;
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: reset enable\n");
        return -1;
    }

    // send actual reset command
    command = EFLASH_S25_CMD_RESETCHIP;
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: soft reset\n");
        return -1;
    }

    // chip deselect the flash
    _s25_spi_deselect();

    // wait for software reset to be processed
    for (int i = 0; i < 8; ++i)
    {
        rc = eflash_s25_wip();
        if ( rc == false ) { break; }
        HAL_Delay(100);
    }

    // verify that flash is not still locked
    if ( rc == true )
    {
        debug_printf("software reset too long\n");
        return -1;
    }

    // chip select the flash
    _s25_spi_select();

    // send read ID command
    command = EFLASH_S25_CMD_READ_RDID;
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: read JEDEC ID\n");
        return -1;
    }

    // retrieve actual ID from the flash
    rc = HAL_SPI_Receive(&hspi2, &buffer[0], 4, 100);
    if ( rc )
    {
        debug_printf("unable to read JEDEC ID\n");
        return -1;
    }

    // chip deselect the flash
    _s25_spi_deselect();

    // check that received ID is from cypress
    if ( buffer[0] != EFLASH_S25_VAL_CYPRESSID )
    {
        debug_printf("not a cypress flash\n");
        return -1;
    }

    // check that received ID is the model expected
    if ( buffer[1] != EFLASH_S25_VAL_DEVICEID )
    {
        debug_printf("unknown cypress flash device\n");
        return -1;
    }

    // check flash model
    switch ( buffer[2] )
    {
        case EFLASH_S25_VAL_128MBID:
        {
            debug_printf("cypress flash: 128Mb\n");
            strcpy(eflash_s25_ctx.dev_info.name, "S25FL128L");
            eflash_s25_ctx.dev_info.sector_size = EFLASH_S25_SECTOR_SIZE;
            eflash_s25_ctx.dev_info.total_size = EFLASH_S25_128MB_SIZE;
            break;
        }
        case EFLASH_S25_VAL_256MBID:
        {
            debug_printf("cypress flash: 256Mb\n");
            strcpy(eflash_s25_ctx.dev_info.name, "S25FL256L");
            eflash_s25_ctx.dev_info.sector_size = EFLASH_S25_SECTOR_SIZE;
            eflash_s25_ctx.dev_info.total_size = EFLASH_S25_256MB_SIZE;
            break;
        }
        default:
        {
            debug_printf("unknown cypress flash density\n");
            return -1;
        }
    }

    // chip select the flash
    _s25_spi_select();

    // send command to remove global lock
    command = EFLASH_S25_CMD_DEL_GLOCK;
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: program data\n");
    }

    // release chip select of the flash
    _s25_spi_deselect();

    *context = &eflash_s25_ctx;
    return rc;
}

//----------------------------------------------------------------------
int eflash_s25_read_buf(uint32_t addr, void * buf, size_t len)
{
    int rc;
    uint8_t buffer[4] = { 0 };

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // chip select the flash
    _s25_spi_select();

    // construct read command with data address
    buffer[0] = EFLASH_S25_CMD_READ_DATA;
    buffer[1] = (uint8_t)(addr >> 16 );
    buffer[2] = (uint8_t)(addr >> 8  );
    buffer[3] = (uint8_t)(addr & 0xff);

    // kick off the command
    rc = HAL_SPI_Transmit(&hspi2, &buffer[0], 4, 100);
    if ( rc )
    {
        debug_printf("unable to send command: read data\n");
        _s25_spi_deselect();
        return -1;
    }

    // start reading data in blocking mode
    rc = HAL_SPI_Receive(&hspi2, buf, len, 1000);
    if ( rc )
    {
        debug_printf("unable to read from flash %d@0x%lX\n", len, addr);
        _s25_spi_deselect();
        return -1;
    }

    // deselect the flash
    _s25_spi_deselect();

    return 0;
}

//----------------------------------------------------------------------
int eflash_s25_prog_buf(uint32_t addr, void * buf, size_t len)
{
    int rc;
    uint8_t buffer[4] = { 0 };
    size_t remaining_len = len;
    uint32_t current_addr = addr, next_page;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // loop over data to be written
    while ( remaining_len )
    {
        // enable upcoming program command
        rc = _s25_write_enable();
        if ( rc )
        {
            return -1;
        }

        // chip select the flash
        _s25_spi_select();

        // construct program command with data address
        buffer[0] = EFLASH_S25_CMD_PROG_DATA;
        buffer[1] = (uint8_t)(current_addr >> 16 );
        buffer[2] = (uint8_t)(current_addr >> 8  );
        buffer[3] = (uint8_t)(current_addr & 0xff);

        // kick off the command
        rc = HAL_SPI_Transmit(&hspi2, &buffer[0], 4, 100);
        if ( rc )
        {
            debug_printf("unable to send command: program data\n");
            _s25_spi_deselect();
            return -1;
        }

        // verify that we are not overwriting the next page
        // if so, keep remaining data for the next loop
        next_page = (current_addr | (EFLASH_S25_PAGE_SIZE-1)) + 1;
        if ( (current_addr + remaining_len) > next_page )
        {
            len = next_page - current_addr;
        }
        else
        {
            len = remaining_len;
        }

        // send the buffer of data to program
        rc = HAL_SPI_Transmit(&hspi2, buf, len, 100);
        if ( rc )
        {
            debug_printf("unable to send command: program data\n");
            _s25_spi_deselect();
            return -1;
        }

        // release the flash chip select
        _s25_spi_deselect();

        // wait and read status
        rc = _s25_wait_completion(EFLASH_S25_CMD_PROG_DATA);
        if ( rc ) { return rc; }

        // compute remaining data to be programmed
        buf = buf + len;
        remaining_len = remaining_len - len;
        current_addr = current_addr + len;
    }

    return 0;
}

//----------------------------------------------------------------------
int eflash_s25_erase_sector(uint32_t sector_address)
{
    int rc = 0;
    uint8_t buffer[4];

    // check address alignment validity
    if ( sector_address % EFLASH_S25_SECTOR_SIZE)
    {
        debug_printf("sector address must be aligned on 0x%X\n",
                              (unsigned int)EFLASH_S25_SECTOR_SIZE);
        return -1;
    }

    // enable upcoming erase command
    rc = _s25_write_enable();
    if ( rc )
    {
        return -1;
    }

    // chip select the flash
    _s25_spi_select();

    // construct program command with data address
    buffer[0] = EFLASH_S25_CMD_SECTERASE;
    buffer[1] = (uint8_t)(sector_address >> 16 );
    buffer[2] = (uint8_t)(sector_address >> 8  );
    buffer[3] = (uint8_t)(sector_address & 0xff);

    // kick off the command
    rc = HAL_SPI_Transmit(&hspi2, &buffer[0], 4, 100);
    if ( rc )
    {
        debug_printf("unable to send command: erase sector\n");
        _s25_spi_deselect();
        return -1;
    }

    // release flash chip select
    _s25_spi_deselect();

    // wait and read status
    rc = _s25_wait_completion(EFLASH_S25_CMD_SECTERASE);

    return rc;
}

//----------------------------------------------------------------------
int eflash_s25_erase_chip(void)
{
    int rc = 0;
    uint8_t command;

    // enable upcoming erase command
    rc = _s25_write_enable();
    if ( rc )
    {
        return -1;
    }

    // chip select the flash
    _s25_spi_select();

    // kick off the command
    command = EFLASH_S25_CMD_CHIPERASE;
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: erase chip\n");
        _s25_spi_deselect();
        return -1;
    }

    // release flash chip select
    _s25_spi_deselect();

    // wait and read status
    rc = _s25_wait_completion(EFLASH_S25_CMD_CHIPERASE);

    return rc;
}

//----------------------------------------------------------------------
bool eflash_s25_wip(void)
{
    int rc = 0;
    uint8_t buffer;

    // chip select the flash
    _s25_spi_select();

    // kick off the command
    buffer = EFLASH_S25_CMD_READSTAT1;
    rc = HAL_SPI_Transmit(&hspi2, &buffer, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: read status\n");
        _s25_spi_deselect();
        return -1;
    }

    // start reading status in blocking mode
    rc = HAL_SPI_Receive(&hspi2, &buffer, 1, 1000);
    if ( rc )
    {
        debug_printf("unable to read status from flash\n");
        _s25_spi_deselect();
        return -1;
    }

    // release flash chip select
    _s25_spi_deselect();

    return (buffer & 0x01);
}

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
static void _s25_spi_select(void)
{
    // set flash's SPI chip select PIN to low level
    HAL_GPIO_WritePin(GPIOC, CS_Flash_Pin, GPIO_PIN_RESET);
}

//----------------------------------------------------------------------
static void _s25_spi_deselect(void)
{
    // set flash's SPI chip select PIN to high level
    HAL_GPIO_WritePin(GPIOC, CS_Flash_Pin, GPIO_PIN_SET);
}

//----------------------------------------------------------------------
static int _s25_write_enable(void)
{
    int rc = 0;
    uint8_t command = EFLASH_S25_CMD_WR_ENABLE;

    // chip select the flash
    _s25_spi_select();

    // kick off the command
    rc = HAL_SPI_Transmit(&hspi2, &command, 1, 100);
    if ( rc )
    {
        debug_printf("unable to send command: write enable\n");
    }

    // release CS pin
    _s25_spi_deselect();

    return rc;
}

//----------------------------------------------------------------------
static int _s25_wait_completion(int command)
{
    uint32_t start_tick = 0;
    uint32_t timeout = 0;

    switch(command)
    {
        case EFLASH_S25_CMD_PROG_DATA:
        {
            timeout = EFLASH_S25_TIME_PP_MAX;
            break;
        }
        case EFLASH_S25_CMD_SECTERASE:
        {
            timeout = EFLASH_S25_TIME_SE_MAX;
            break;
        }
        case EFLASH_S25_CMD_CHIPERASE:
        {
            timeout = EFLASH_S25_TIME_CE_MAX;
            break;
        }
        default:
        {
            return -1;
        }
    }

    // timestamp of command start
    start_tick = HAL_GetTick();

    while ( eflash_s25_wip() )
    {
        // wait for typical time
        HAL_Delay(1);

        // loop till timeout
        if ( DeltaT_Calculation(start_tick) > timeout )
        {
            debug_printf("command too long\n");
            return -1;
        }
    }

    return 0;
}
