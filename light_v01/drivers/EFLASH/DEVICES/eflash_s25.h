#ifndef _EFLASH_S25_H_
#define _EFLASH_S25_H_
/**
 * @file:  eflash_s25.h
 *
 * @brief
 *
 * @date   12/03/2019
 *
 * @platform
 *    S1 TRACK REV 1.3
 *
 */
//----------------------------------------------------------------------
// Required includes
//----------------------------------------------------------------------
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

// local
#include "../eflash_private.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define EFLASH_S25_PAGE_SIZE        0x100       // 256B
#define EFLASH_S25_SECTOR_SIZE      0x1000      // 4kB
#define EFLASH_S25_BLOCK_SIZE       0x10000     // 64kB
#define EFLASH_S25_256MB_SIZE       0x2000000     // 32MB
#define EFLASH_S25_128MB_SIZE       0x1000000     // 16MB

#define EFLASH_S25_CMD_READ_RDID    0x9F
#define EFLASH_S25_CMD_READ_DATA    0x03
#define EFLASH_S25_CMD_PROG_DATA    0x02
#define EFLASH_S25_CMD_SECTERASE    0x20
#define EFLASH_S25_CMD_CHIPERASE    0xC7
#define EFLASH_S25_CMD_WR_ENABLE    0x06
#define EFLASH_S25_CMD_READSTAT1    0x05
#define EFLASH_S25_CMD_READSTAT2    0x07
#define EFLASH_S25_CMD_RSTENABLE    0x66
#define EFLASH_S25_CMD_RESETCHIP    0x99

#define EFLASH_S25_VAL_CYPRESSID    0x01
#define EFLASH_S25_VAL_DEVICEID     0x60
#define EFLASH_S25_VAL_128MBID      0x18
#define EFLASH_S25_VAL_256MBID      0x19
#define EFLASH_S25_CMD_DEL_GLOCK    0x98

// timing constants
#define EFLASH_S25_TIME_CE_TYP    60000
#define EFLASH_S25_TIME_CE_MAX    70000
#define EFLASH_S25_TIME_SE_TYP    50
#define EFLASH_S25_TIME_SE_MAX    100
#define EFLASH_S25_TIME_PP_TYP    2
#define EFLASH_S25_TIME_PP_MAX    6

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int eflash_s25_discover(eflash_device_ctx_t ** context);

#endif  // _EFLASH_S25_H_
