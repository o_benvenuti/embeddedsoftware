//--------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------
#include <string.h>
#include <trace.h>

#include "eflash_private.h"
#include "DEVICES/eflash_s25.h"
#include "DEVICES/SST26VF064B_functions.h"

//--------------------------------------------------------------------
// Variables
//--------------------------------------------------------------------
static int device_number_available = 0;

static eflash_device_ctx_t * eflash_available_ctx[EFLASH_MAX_DEVICES] =
{
        NULL,
        NULL,
};

//--------------------------------------------------------------------
// Helpers
//--------------------------------------------------------------------
static inline eflash_device_ctx_t * _get_device_context(int index)
{

    // is index valid ?
    if ( index > device_number_available )
    {
        debug_printf("invalid device number\n");
        return NULL;
    }

    // translate index to dev pointer
    return eflash_available_ctx[index];
}

//--------------------------------------------------------------------
// API
//--------------------------------------------------------------------
int eflash_discover(void)
{
    int rc = 0;
    device_number_available = 0;

    // attempt with microchip flash
    rc = eflash_sst26_discover(&eflash_available_ctx[device_number_available]);
    if ( rc == 0 )
    {
        ++device_number_available;
    }

    // attempt with cypress flash
    rc = eflash_s25_discover(&eflash_available_ctx[device_number_available]);
    if ( rc == 0 )
    {
        ++device_number_available;
    }

    return 0;
}

//--------------------------------------------------------------------
int eflash_device_availables(void)
{
    return device_number_available;
}

//--------------------------------------------------------------------
int eflash_get_config(int index, int type, void * buf, size_t len)
{
    eflash_device_ctx_t * dev = NULL;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid param\n");
        return -1;
    }

    // translate index to dev pointer
    dev = _get_device_context(index);
    if ( ! dev )
    {
        debug_printf("invalid device number\n");
        return -1;
    }

    switch ( type )
    {
        case EFLASH_CONFIG_DEV_INFO:
        {
            // check output buffer
            if ( len < sizeof(eflash_device_info_t) )
            {
                debug_printf("output buffer too small\n");
                return -1;
            }

            // return device information
            memcpy(buf, &(dev->dev_info), sizeof(eflash_device_info_t));
            break;
        }
        default:
        {
            debug_printf("unknown config type\n");
            return -1;
        }
    }

    return 0;
}

//--------------------------------------------------------------------
int eflash_prog(int index, uint32_t addr, void * buf, size_t len)
{
    eflash_device_ctx_t * dev = NULL;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid param\n");
        return -1;
    }

    // translate index to dev pointer
    dev = _get_device_context(index);
    if ( ! dev )
    {
        debug_printf("invalid device number\n");
        return -1;
    }

    // is the command available ?
    if ( ! dev->prog )
    {
        debug_printf("program command not provided: %s\n", dev->dev_info.name);
        return -1;
    }

    // process the command if provided
    return dev->prog(addr, buf, len);
}

//--------------------------------------------------------------------
int eflash_read(int index, uint32_t addr, void * buf, size_t len)
{
    eflash_device_ctx_t * dev = NULL;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid param\n");
        return -1;
    }

    // translate index to dev pointer
    dev = _get_device_context(index);
    if ( ! dev )
    {
        debug_printf("invalid device number\n");
        return -1;
    }

    // is the command available ?
    if ( ! dev->read )
    {
        debug_printf("read command not provided: %s\n", dev->dev_info.name);
        return -1;
    }

    // process the command if provided
    return dev->read(addr, buf, len);
}

//--------------------------------------------------------------------
int eflash_erase_sector(int index, uint32_t addr)
{
    eflash_device_ctx_t * dev = NULL;

    // translate index to dev pointer
    dev = _get_device_context(index);
    if ( ! dev )
    {
        debug_printf("invalid device number\n");
        return -1;
    }

    // is the command available ?
    if ( ! dev->erase_sector )
    {
        debug_printf("erase sector command not provided: %s\n",
                     dev->dev_info.name);
        return -1;
    }

    // process the command if provided
    return dev->erase_sector(addr);
}

//--------------------------------------------------------------------
int eflash_erase_chip(int index)
{
    eflash_device_ctx_t * dev = NULL;

    // translate index to dev pointer
    dev = _get_device_context(index);
    if ( ! dev )
    {
        debug_printf("invalid device number\n");
        return -1;
    }

    // is the command available ?
    if ( ! dev->erase_chip )
    {
        debug_printf("erase chip command not provided: %s\n",
                     dev->dev_info.name);
        return -1;
    }

    // process the command if provided
    return dev->erase_chip();
}

