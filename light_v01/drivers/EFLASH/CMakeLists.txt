include_app()       # main.h
include_freertos()  # HAL
include_libraries(TRACE TOOLS)

INCLUDE_DIRECTORIES(Inc)

FILE (GLOB_RECURSE SOURCE_FILES *.c)

ADD_LIBRARY (EFLASH ${SOURCE_FILES})