#ifndef _EFLASH_H_
#define _EFLASH_H_
//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define EFLASH_MAX_DEVICES  2
#define EFLASH_STR_MAX_LEN  64
//----------------------------------------------------------------------
// Required includes
//----------------------------------------------------------------------
#include <stdint.h>
#include <stddef.h>

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct eflash_device_info
{
    char name[EFLASH_STR_MAX_LEN];

    uint32_t page_size;
    uint32_t sector_size;
    uint32_t block_size;
    uint32_t total_size;
}eflash_device_info_t;

typedef enum eflash_config
{
    EFLASH_CONFIG_DEV_INFO,
}eflash_config_t;
//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
/**
  * @brief  attempt to initialise known flash devices
  *         and append to a list on success
  *
  * @return 0 on success -1 if not
  **/
int eflash_discover(void);

/**
  * @brief  return requested configuration
  *
  * @param[in]  index   device number
  * @param[in]  type    see 'eflash_config_t' definition above
  * @param[out] buf     output buffer where config will be copied
  * @param[in]  len     max size of output buffer
  *
  * @return 0 on success -1 if not
  **/
int eflash_get_config(int index, int type, void * buf, size_t len);

/**
  * @brief  program len data from buf to flash addr
  *
  * @param[in]  index   device number
  * @param[in]  addr    target program address
  * @param[in]  buf     input buffer containing data to be programmed
  * @param[in]  len     length of data to program
  *
  * @return 0 on success -1 if not
  **/
int eflash_prog(int index, uint32_t addr, void * buf, size_t len);

/**
  * @brief  read len data from flash addr to buf
  *
  * @param[in]  index   device number
  * @param[in]  addr    target address to read
  * @param[out] buf     output buffer where data will be written
  * @param[in]  len     length of data to read
  *
  * @return 0 on success -1 if not
  **/
int eflash_read(int index, uint32_t addr, void * buf, size_t len);

/**
  * @brief  erase a sector of the flash device
  *
  * @param[in]  index   device number
  * @param[in]  addr    target sector address to erase
  *
  * @return 0 on success -1 if not
  **/
int eflash_erase_sector(int index, uint32_t addr);

/**
  * @brief  erase the whole device
  *
  * @param[in]  index   device number
  *
  * @return 0 on success -1 if not
  **/
int eflash_erase_chip(int index);

int eflash_device_availables(void);


#endif /*_EFLASH_H_*/
