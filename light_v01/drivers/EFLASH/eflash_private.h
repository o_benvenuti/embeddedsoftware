#ifndef _EFLASH_PRIVATE_H_
#define _EFLASH_PRIVATE_H_
//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
#include <stdint.h>
#include <stdlib.h>

#include <eflash.h>

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct _eflash_device_ctx
{
    eflash_device_info_t dev_info;
    int (* erase_chip) (void);
    int (* erase_sector) (uint32_t sector_address);
    int (* prog) (uint32_t addr, void * buffer, size_t length);
    int (* read) (uint32_t addr, void * buffer, size_t length);
} eflash_device_ctx_t;

#endif /* _EFLASH_PRIVATE_H_ */
