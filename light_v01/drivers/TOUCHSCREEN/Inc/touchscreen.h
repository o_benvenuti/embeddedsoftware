/*
 * touchscreen.h
 *
 *  Created on: 7 d�c. 2017
 *      Author: Benjamin
 */

#ifndef TOUCHSCREEN_H_
#define TOUCHSCREEN_H_

#ifdef __cplusplus
extern "C" {
#endif

#define I2C_TIMEOUT 500
#define I2C_TOUCH_ADDRESS 			(0x55<<1)
#define TOUCHSCREEN_REG_VERSION 	0x00
#define TOUCHSCREEN_REG_STATUS 		0x01
#define TOUCHSCREEN_REG_CTRL 		0x02
#define TOUCHSCREEN_REG_T_TO_IDLE 	0x03
#define TOUCHSCREEN_REG_COORD_XY 	0x10
#define XY_COORD_TAB_MAX_SIZE 		40
#define TOUCHSCREEN_SWIPE_SIZE 		50

/* struct for XY regs reading in touchscreen controller */
typedef struct
{
	uint8_t	y_h			: 3	,
	 	 	reserved	: 1	,
	 	 	x_h 		: 3 ,
	 	 	valid		: 1	;
	uint8_t x_l				;
	uint8_t y_l				;
	uint8_t z				; /* used as spacer for controller registers */
} xyz_data_t;

/* struct for reading touch infos in touchscreen controller */
typedef struct
{
	uint8_t 	fingers		: 4		,
				reserved	: 4		;
	uint8_t 	keys				;
	xyz_data_t 	xyz_data[10]		;
} stx_report_data_t;

/* contains x & y coord */
typedef struct
{
	uint8_t	x;
	uint8_t y;
} xy_coord_t;

/* enum used to get multiple gestures types */
typedef enum
{
	touch 		= 0,
	swipe_left 	= 1,
	swipe_right = 2,
	swipe_up 	= 3,
	swipe_down 	= 4
}gestures_t;

/* used to aggregate all info from a touch interrupt */
typedef struct
{
	xy_coord_t xy_coord;
	gestures_t gesture;
}touch_info_t;

extern SemaphoreHandle_t touchscreen_flag;

#ifdef DEBUG
void touch_debug(void);
#endif /* DEBUG */

int touchscreen_Init(void);
touch_info_t get_TouchInfo(void);
void touchscreen_PowerDown(void);
int touchscreen_get_xy_coordinates(void);
int touchscreen_WriteReg(uint8_t reg_address, uint8_t value);
void touchscreen_ReadReg(uint8_t reg_address, uint8_t * value);
void touchscreen_get_coordinates(uint8_t *count, uint32_t *x0, uint32_t *y0, uint32_t *x1, uint32_t *y1);
gestures_t get_Gesture(xy_coord_t xy_coord_tab[]);


#ifdef __cplusplus
}
#endif /*extern "C" */

#endif /* TOUCHSCREEN_H_ */
