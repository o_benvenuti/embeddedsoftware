/*
 * touchscreen.c
 *
 *  Created on: 7 d�c. 2017
 *      Author: Benjamin
 */


#include <cmsis_os.h>
#include <i2c.h>
#include <trace.h>
#include <touchscreen.h>
#include <stm32l4xx_hal.h>

SemaphoreHandle_t touchscreen_flag = 0;
static StaticSemaphore_t _touch_semaphore_hdl;
static I2C_HandleTypeDef * I2C_TOUCH;

/* touchscreen global variables */
xy_coord_t   xy_coord_tab[XY_COORD_TAB_MAX_SIZE];
uint32_t     xy_coord_tab_index;

/**
 * @brief  initialization of touchscreen controller.
 * @param  none
 * @retval None
 */
int touchscreen_Init(void)
{
    int rc = 0;

	I2C_TOUCH = &hi2c2;

	if ( ! touchscreen_flag )
	{
	    // initialize mutex that notify touchscreen event
	    touchscreen_flag = xSemaphoreCreateBinaryStatic(&_touch_semaphore_hdl);
	}

	/*Reset Display & touch controller*/
	HAL_GPIO_WritePin(RST_Display_GPIO_Port, RST_Display_Pin,  GPIO_PIN_RESET);
	HAL_Delay(1); /* ms */
	HAL_GPIO_WritePin(RST_Display_GPIO_Port, RST_Display_Pin, GPIO_PIN_SET);

	/* wait for touchscreen controller initialization */
	HAL_Delay(50); /* ms */

	/* write control register to init phase, no power down, no reset */
	rc = touchscreen_WriteReg(TOUCHSCREEN_REG_CTRL, 0x00);
	if ( rc == -1 )
	{
		return -1;
	}

	/* power down mode :
	 * When host sets Power Down bit, touch sensor controller will enter power down mode. Host can clear Power
	 * Down bit to wake up the controller.
	 */

	/* write timeout to idle register to No idle (0xFF is disabled, otherwise, value is in seconds) */
	rc = touchscreen_WriteReg(TOUCHSCREEN_REG_T_TO_IDLE, 0x00);
	if( rc == -1 )
	{
		return -1;
	}

	return 0;
}

/**
 * @brief  Used to write a value in the touchscreen controller
 * For writing register to I2C device, host has to tell I2C device
 * the Start Register Address in each I2C Register Write transaction.
 * Register values to the I2C device will be written to the address starting from the Start
 * Register Address described in Register Write I2C transaction
 * @param  	reg_address : specifies the register address to write in
 * 			value 		: the value to write in the register
 * @retval None
*/
int touchscreen_WriteReg(uint8_t reg_address, uint8_t value)
{
    int rc = 0;
	uint8_t tbuf[2];

	tbuf[0] = reg_address;
	tbuf[1] = value;

	rc = HAL_I2C_Master_Transmit(I2C_TOUCH, I2C_TOUCH_ADDRESS,
	                             tbuf, sizeof(tbuf), I2C_TIMEOUT);
	if ( rc != HAL_OK)
	{
		debug_printf("transmit error: 0x%02X = 0x%02X\n", value, reg_address);
		return -1;
	}

	 // Set Reg. address back to 0x10 for fast Coordinates access.
	tbuf[0] = TOUCHSCREEN_REG_COORD_XY;
	rc = HAL_I2C_Master_Transmit(I2C_TOUCH, I2C_TOUCH_ADDRESS,
	                             tbuf, 1, I2C_TIMEOUT);
	if ( rc != HAL_OK )
	{
		debug_printf("I2C error transmitting in reg 0x%02X\n", tbuf[0]);
		return -1;
	}

    return 0;
}

/**
 * @brief  Used to read a value in the touchscreen controller
 * For reading register value from I2C device,
 * host has to tell I2C device the Start Register Address
 * before reading corresponding register value.
 *      WRITE desired address -> READ value
 * @param  	reg_address : specifies the register address to read from
 * 			value 		: returns the register content
 * @retval None
*/
void touchscreen_ReadReg(uint8_t reg_address, uint8_t * value)
{
	uint8_t tbuf[1];
	tbuf[0] = reg_address;

	if(HAL_I2C_Master_Transmit(I2C_TOUCH, I2C_TOUCH_ADDRESS, (uint8_t *)tbuf, sizeof(tbuf), I2C_TIMEOUT) != HAL_OK)
	{
	    debug_printf("I2C error transmitting in reg 0x%02X\n", reg_address);
	}

	if(HAL_I2C_Master_Receive(I2C_TOUCH, I2C_TOUCH_ADDRESS, (uint8_t *)tbuf, sizeof(tbuf), I2C_TIMEOUT) != HAL_OK)
	{
		debug_printf("I2C error receiving from reg 0x%02X\n", reg_address);
	}

	// return value
	*value = tbuf[0];

	 // Set Reg. address back to 0x10 for fast Coordinates access.
	tbuf[0] = TOUCHSCREEN_REG_COORD_XY;
	if(HAL_I2C_Master_Transmit(I2C_TOUCH, I2C_TOUCH_ADDRESS, (uint8_t *)tbuf, 1, I2C_TIMEOUT) != HAL_OK)
	{
	    debug_printf("I2C error transmitting in reg 0x%02X\n", tbuf[0]);
	}
}

/**
 * @brief  Used to get last coordinates stored in touchscreen controller
 * @param  	count 	: number of fingers detected (2 max)
 * 			x0 		: x position of the finger 0
 * 			y0 		: y position of the finger 0
 * 			x1 		: x position of the finger 1
 * 			y1 		: y position of the finger 1
 * @retval None
*/
void touchscreen_get_coordinates(uint8_t *count, uint32_t *x0, uint32_t *y0, uint32_t *x1, uint32_t *y1)
{
	uint8_t buf[42] = {0};
	stx_report_data_t *pdata;

	/* Set point detected count to 0. */
	*count = 0;

	/* Read Coordinates from default Reg. address 0x10. */
	HAL_I2C_Master_Receive(I2C_TOUCH, I2C_TOUCH_ADDRESS, (uint8_t *)buf, sizeof(buf), I2C_TIMEOUT);

	/* convert raw data into intelligible structs */
	pdata = (stx_report_data_t *) buf;

	/* X & Y need to be swapped */
 	if (pdata->xyz_data[0].valid)
	{
		*y0 = 128 - (pdata->xyz_data[0].x_h << 8 | pdata->xyz_data[0].x_l);
		*x0 = 		 pdata->xyz_data[0].y_h << 8 | pdata->xyz_data[0].y_l;
		(*count)++;
	}
 	else
 	{
 		*y0 = 0;
 		*x0 = 0;
 	}

 	if (pdata->xyz_data[1].valid)
	{
		*y1 = 128 - (pdata->xyz_data[1].x_h << 8 | pdata->xyz_data[1].x_l);
		*x1 = 		 pdata->xyz_data[1].y_h << 8 | pdata->xyz_data[1].y_l;
		(*count)++;
	}
	 else
 	{
 		*y1 = 0;
 		*x1 = 0;
 	}

}

/**
 * @brief  Used to get last coordinates stored in touchscreen controller
 * @param  	x1 		: x position of the detected touch
 * 			y1 		: y position of the detected touch
 * @retval None
*/
int touchscreen_get_xy_coordinates(void)
{
    uint32_t x, y;
	uint8_t buf[42] = {0};
	stx_report_data_t *pdata;

	/* Read Coordinates from default Reg. address 0x10. */
	HAL_I2C_Master_Receive(I2C_TOUCH, I2C_TOUCH_ADDRESS, (uint8_t *)buf, sizeof(buf), I2C_TIMEOUT);

	/* convert raw data into intelligible structs */
	pdata = (stx_report_data_t *) buf;

	/* X & Y need to be swapped */
 	if (pdata->xyz_data[0].valid)
	{
		y = 128 - (pdata->xyz_data[0].x_h << 8 | pdata->xyz_data[0].x_l);
		x = 		pdata->xyz_data[0].y_h << 8 | pdata->xyz_data[0].y_l;
	}
	else if (pdata->xyz_data[1].valid)
	{
		y = 128 - (pdata->xyz_data[1].x_h << 8 | pdata->xyz_data[1].x_l);
		x = 		pdata->xyz_data[1].y_h << 8 | pdata->xyz_data[1].y_l;
	}
	else /* if no valid data available */
	{
		y = 0;
		x = 0;
		return 0;   // this is not an error
	}

    if(xy_coord_tab_index == (XY_COORD_TAB_MAX_SIZE - 1)) /* if tab full */
    {
        return -1;
    }

    /* add touch deteted to tab */
    xy_coord_tab[xy_coord_tab_index].x = x;
    xy_coord_tab[xy_coord_tab_index].y = y;
    xy_coord_tab_index++;

    return 0;
}

/**
 * @brief  Used to analyse last touchs detected and return coord or gesture
 * @param  None
 * @retval struct of gesture detected and coord if touch detected
*/
touch_info_t get_TouchInfo(void)
{
	uint8_t i;
	uint32_t som_x = 0;
	uint32_t som_y = 0;
	touch_info_t touch_info;
	uint8_t Hmin=0;
	uint8_t Hmax=0;
	uint8_t Vmin=0;
	uint8_t Vmax=0;

	/* get min & max values along all tab of last coord detected */
	for (i=0; i<xy_coord_tab_index; i++)
	{
		if(xy_coord_tab[i].x > xy_coord_tab[Hmax].x) Hmax=i;
		if(xy_coord_tab[i].x < xy_coord_tab[Hmin].x) Hmin=i;
		if(xy_coord_tab[i].y > xy_coord_tab[Vmax].y) Vmax=i;
		if(xy_coord_tab[i].y < xy_coord_tab[Vmin].y) Vmin=i;
	}
	if( (xy_coord_tab[Hmax].x - xy_coord_tab[Hmin].x) > TOUCHSCREEN_SWIPE_SIZE) /* if gesture swipe */
	{
		if(Hmin > Hmax) touch_info.gesture = swipe_left;
		else touch_info.gesture = swipe_right;
		touch_info.xy_coord.x = 0;
		touch_info.xy_coord.y = 0;
	}
	else if( (xy_coord_tab[Vmax].y - xy_coord_tab[Vmin].y) > TOUCHSCREEN_SWIPE_SIZE) /* if gesture swipe */
	{
		if(Vmin > Vmax) touch_info.gesture = swipe_up;
		else touch_info.gesture = swipe_down;
		touch_info.xy_coord.x = 0;
		touch_info.xy_coord.y = 0;
	}
	else /* if touch gesture */
	{
		touch_info.gesture = touch;

		/* calculate average value */
	    for (i = 0 ; i < xy_coord_tab_index ; i++)
	    {
	    	som_x += xy_coord_tab[i].x;
	    	som_y += xy_coord_tab[i].y;
	    }
	    touch_info.xy_coord.x = som_x/xy_coord_tab_index;
	    touch_info.xy_coord.y = som_y/xy_coord_tab_index;

	}
	/* re-init the tab index and clear tab*/
	xy_coord_tab_index = 0;
	for(i = 0; i < XY_COORD_TAB_MAX_SIZE ; i++)
	{
		xy_coord_tab[i].x = 0;
		xy_coord_tab[i].y = 0;
	}

	// reset touchscreen interface
	xSemaphoreTake(touchscreen_flag, 0);

    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	return touch_info;
}

void touchscreen_PowerDown(void)
{
	touchscreen_WriteReg(TOUCHSCREEN_REG_CTRL, 0x02);
}










