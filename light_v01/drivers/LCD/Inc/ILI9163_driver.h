/************************************************************************
	ili9163lcd.h

    ILI9163 128x128 LCD library
    Copyright (C) 2012 Simon Inns

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Email: simon.inns@gmail.com

************************************************************************/

#ifndef ILI9163LCD_H_
#define ILI9163LCD_H_

#include "stm32l4xx_hal.h"
#include <stdint.h>
#include <stdbool.h>
#include "font5x7.h"
#include "gfxfont.h"
#include "color.h"

/***********************************
 * 
 * Initialisation of SPI Interface
 * 
 ***********************************/
#define SCR_WIDTH	128
#define SCR_HEIGHT	160

// Screen orientation defines:
// 0 = Ribbon at top
// 1 = Ribbon at left
// 2 = Ribbon at right
// 3 = Ribbon at bottom

#define LCD_ORIENTATION0	96
#define LCD_ORIENTATION1	160
#define LCD_ORIENTATION2	192
#define LCD_ORIENTATION3	0
/*
 #define LCD_ORIENTATION0	0
#define LCD_ORIENTATION1	96
#define LCD_ORIENTATION2	160
#define LCD_ORIENTATION3	192

 */

typedef enum
{
  scr_normal = 0,
  scr_CW     = 1,/// X-Y Exchange,Y-Mirror
  scr_CCW    = 2,/// X-Y Exchange,X-Mirror
  scr_180    = 3 /// X-Mirror,Y-Mirror: Bottom to top; Right to left; RGB
} ScrOrientation_TypeDef;


// some other misc. constants
enum
{
	// MADCTL bits
	ILI9163_MADCTL_MH	= 0x04,	// bit 2 = 0 for refresh left -> right, 1 for refresh right -> left
	ILI9163_MADCTL_RGB	= 0x00,	// bit 3 = 0 for RGB color order
	ILI9163_MADCTL_BGR	= 0x08,	// bit 3 = 1 for BGR color order
	ILI9163_MADCTL_ML	= 0x10,	// bit 4 = 0 for refresh top -> bottom, 1 for bottom -> top
	ILI9163_MADCTL_MV	= 0x20,	// bit 5 = 0 for column, row order (portrait), 1 for row, column order (landscape)
	ILI9163_MADCTL_MX	= 0x40,	// bit 6 = 0 for left -> right, 1 for right -> left order
	ILI9163_MADCTL_MY	= 0x80,	// bit 7 = 0 for top -> bottom, 1 for bottom -> top

};



// ILI9163 LCD Controller Commands
#define NOP 					0x00
#define SOFT_RESET 				0x01
#define GET_RED_CHANNEL 		0x06
#define GET_GREEN_CHANNEL 		0x07
#define GET_BLUE_CHANNEL 		0x08
#define GET_PIXEL_FORMAT 		0x0C
#define GET_POWER_MODE 			0x0A
#define GET_ADDRESS_MODE 		0x0B
#define GET_DISPLAY_MODE 		0x0D
#define GET_SIGNAL_MODE 		0x0E
#define GET_DIAGNOSTIC_RESULT 	0x0F
#define ENTER_SLEEP_MODE 		0x10
#define EXIT_SLEEP_MODE 		0x11
#define ENTER_PARTIAL_MODE 		0x12
#define ENTER_NORMAL_MODE 		0x13
#define EXIT_INVERT_MODE 		0x20
#define ENTER_INVERT_MODE 		0x21
#define SET_GAMMA_CURVE 		0x26
#define SET_DISPLAY_OFF			0x28
#define SET_DISPLAY_ON 			0x29
#define SET_COLUMN_ADDRESS 		0x2A
#define SET_PAGE_ADDRESS 		0x2B
#define WRITE_MEMORY_START 		0x2C
#define WRITE_LUT 				0x2D
#define READ_MEMORY_START 		0x2E
#define SET_PARTIAL_AREA 		0x30
#define SET_SCROLL_AREA 		0x33
#define SET_TEAR_OFF 			0x34
#define SET_TEAR_ON 			0x35
#define SET_ADDRESS_MODE 		0x36
#define SET_SCROLL_START 		0X37
#define EXIT_IDLE_MODE 			0x38
#define ENTER_IDLE_MODE 		0x39
#define SET_PIXEL_FORMAT 		0x3A
#define WRITE_MEMORY_CONTINUE 	0x3C
#define READ_MEMORY_CONTINUE 	0x3E
#define SET_TEAR_SCANLINE 		0x44
#define GET_SCANLINE 			0x45
#define READ_ID1				0xDA
#define READ_ID2				0xDB
#define READ_ID3				0xDC
#define FRAME_RATE_CONTROL1		0xB1
#define FRAME_RATE_CONTROL2		0xB2
#define FRAME_RATE_CONTROL3		0xB3
#define DISPLAY_INVERSION		0xB4
#define SOURCE_DRIVER_DIRECTION	0xB7
#define GATE_DRIVER_DIRECTION	0xB8
#define POWER_CONTROL1			0xC0
#define POWER_CONTROL2			0xC1
#define POWER_CONTROL3			0xC2
#define POWER_CONTROL4			0xC3
#define POWER_CONTROL5			0xC4
#define VCOM_CONTROL1			0xC5
#define VCOM_CONTROL2			0xC6
#define VCOM_OFFSET_CONTROL		0xC7
#define WRITE_ID4_VALUE			0xD3
#define NV_MEMORY_FUNCTION1		0xD7
#define NV_MEMORY_FUNCTION2		0xDE
#define POSITIVE_GAMMA_CORRECT	0xE0
#define NEGATIVE_GAMMA_CORRECT	0xE1
#define GAM_R_SEL				0xF2

// Macros and in-lines:

// Translates a 3 byte RGB value into a 2 byte value for the LCD (values should be 0-31)
inline uint16_t decodeRgbValue(uint8_t r, uint8_t g, uint8_t b)
{
	return (b << 11) | (g << 6) | (r);
}	

// This routine takes a row number from 0 to 20 and
// returns the x coordinate on the screen (0-127) to make
// it easy to place text
inline uint8_t lcdTextX(uint8_t x) { return x*6; }

// This routine takes a column number from 0 to 20 and
// returns the y coordinate on the screen (0-127) to make
// it easy to place text
inline uint8_t lcdTextY(uint8_t y) { return y*8; }


void LCD_Init(GPIO_TypeDef *_CS_PORT, uint16_t _CS_Pin,
		GPIO_TypeDef *_BACKLIGHT_GPIO_Port, uint16_t _BACKLIGHT_Pin,
		GPIO_TypeDef *_LCD_DC_GPIO_Port, uint16_t _LCD_DC_Pin,
		GPIO_TypeDef *_LCD_RD_GPIO_Port, uint16_t _LCD_RD_Pin,
		GPIO_TypeDef *_LCD_WR_GPIO_Port, uint16_t _LCD_WR_Pin,
		GPIO_TypeDef *_LCD_RST_GPIO_Port, uint16_t _LCD_RST_Pin,
		GPIO_TypeDef *_LCD_D07_GPIO_Port);

void LCD_DeInit(void);
void LCD_BackLight(bool bTurnOn);
void LCD_SetAddrWindow(uint8_t XS, uint8_t XE, uint8_t YS, uint8_t YE);
void LCD_Orientation(ScrOrientation_TypeDef orientation);
void LCD_Clear(uint16_t color);
void LCD_Pixel(int16_t X, int16_t Y, uint16_t color);
void LCD_MIRE(bool bDisplay);
void LCD_Sleep(void);
void LCD_WakeUp(void);

void LCD_OFF(void);
void LCD_ON(void);

void LCD_HReset(void);
uint8_t LCD_Send16(uint16_t word, uint16_t count);
void LCD_Write_Cmd(uint8_t data);

void LCD_Pixel(int16_t X, int16_t Y, uint16_t color);
void LCD_Write_ColorData(uint16_t color);
void LCD_HLine(int16_t x, int16_t y, int16_t w, uint16_t color);
void LCD_VLine(int16_t x, int16_t y, int16_t h, uint16_t color);
void LCD_Line(int16_t X1, int16_t Y1, int16_t X2, int16_t Y2, uint16_t color);
void LCD_Rect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color);
//void LCD_FillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint16_t color);
void LCD_BackLight(bool bTurnOn);
void LCD_setFont(const GFXfont *f);
void st_GetStr_Sizes(uint8_t scale, uint16_t *_X, uint16_t *_Y, char *str);

void drawCircle(uint8_t x0, uint8_t y0, uint8_t r,uint16_t color);
void drawArcCircle(uint8_t x0, uint8_t y0, uint8_t r, uint16_t start, uint16_t end, uint16_t color);
void drawCircleHelper(uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint16_t color);

void fillCircle(uint8_t x0, uint8_t y0, uint8_t r,uint16_t color);

void fillCircleHelper(uint8_t x0, uint8_t y0, uint8_t r,uint8_t cornername, uint8_t delta, uint16_t color);

void LCD_DrawTriangle(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1,uint8_t x2, uint8_t y2, uint16_t color);

void LCD_FillTriangle(uint8_t x0, uint8_t y0,
 uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color);

void LCD_FillTriangle_AA(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t colorF, uint16_t colorB);

void fillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h,
 uint16_t color);

void drawRoundRect(uint8_t x, uint8_t y, uint8_t w,
 uint8_t, uint8_t r, uint16_t color);

void fillRoundRect(uint8_t x, uint8_t y, uint8_t w,
 uint8_t h, uint8_t r, uint16_t color);

void st_PutStr5x7(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color);

void drawChar(uint8_t x, uint8_t y, unsigned char c,
 uint16_t color, uint8_t size);

//void drawBitmap(uint8_t x, uint8_t y, uint8_t *bitmap, uint8_t w, uint8_t h);

void drawBitmapRect(uint8_t x, uint8_t y, uint8_t *bitmap, uint16_t startx, uint16_t starty, uint16_t realW, uint16_t realH, uint8_t w, uint8_t h, uint32_t color32, bool bMASK_Black);

/** return color 565 of true rgb color */
uint16_t RGB565(uint8_t R,uint8_t G,uint8_t B);
uint16_t TORGB565(uint32_t RGB);
uint32_t RGB(uint8_t R, uint8_t G, uint8_t B);  ///color

//	LCD function prototypes
void lcdReset(void);
void lcdWriteCommand(uint8_t address);
void LCD_Write_Data(uint8_t parameter);
void LCD_Write_Data_16(uint8_t dataByte1, uint8_t dataByte2);
void lcdInitialise(uint8_t orientation);

void lcdClearDisplay(uint16_t colour);
void lcdPlot(uint8_t x, uint8_t y, uint16_t colour);
void lcdLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour);
void lcdRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour);
void lcdFilledRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour);
void lcdCircle(int16_t xCentre, int16_t yCentre, int16_t radius, uint16_t colour);

void lcdPutCh(unsigned char character, uint8_t x, uint8_t y, uint16_t fgColour, uint16_t bgColour);
void lcdPutS(const char *string, uint8_t x, uint8_t y, uint16_t fgColour, uint16_t bgColour);

void lcdBitmap(const unsigned char *data, uint8_t width, uint8_t heigth, uint8_t x, uint8_t y, uint16_t fgColour, uint16_t bgColour);

//void ILI9163_pset(UG_S16 x, UG_S16 y, UG_COLOR col);
//UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
//UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );

#endif /* ILI9163LCD_H_ */

void change_BaklightIntensity (uint8_t percent);
uint8_t get_BaklightIntensity (void);
void pulseBacklight(void);
