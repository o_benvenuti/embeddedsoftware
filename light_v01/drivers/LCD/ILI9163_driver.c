/************************************************************************
 ili9163lcd.c

 ILI9163 128x128 LCD library
 Copyright (C) 2012 Simon Inns

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Email: simon.inns@gmail.com

 ************************************************************************
 *
 * 	changed routines to HW-SPI:		16.12.15 	grn
 *
 *   added lcdBitmap function:		31.8.16 	grn
 *
 ***********************************************************************/

#include <stdbool.h>
#include <string.h>
#include <math.h>

#include <main.h>
#include <gpio.h>
#include <spi.h>
#include <tim.h>
#include <stm32l4xx_hal.h>
#include <ILI9163_driver.h>


static GPIO_TypeDef* CS_Port;
static GPIO_TypeDef* LCD_RST_Port;
static GPIO_TypeDef* LCD_DC_Port;
static GPIO_TypeDef* LCD_WR_Port;
static GPIO_TypeDef* LCD_RD_Port;
static GPIO_TypeDef* LCD_D07_Port;
static GPIO_TypeDef* BACKLIGHT_Port;

static uint16_t CS_PIN;
static uint16_t LCD_RST_PIN;
static uint16_t LCD_DC_PIN;
static uint16_t LCD_WR_PIN;
static uint16_t LCD_RD_PIN;
static uint16_t BACKLIGHT_Pin;


#define colSTART					0x00
#define rowSTART					0x00

#define LCD_SELECT()				HAL_GPIO_WritePin(CS_Port, CS_PIN, GPIO_PIN_RESET)
#define LCD_DESELECT()				//HAL_GPIO_WritePin(CS_Port, CS_PIN, GPIO_PIN_SET)

#define LCD_RESET()					HAL_GPIO_WritePin(LCD_RST_Port, LCD_RST_PIN, GPIO_PIN_RESET)
#define LCD_UNRESET() 				HAL_GPIO_WritePin(LCD_RST_Port, LCD_RST_PIN, GPIO_PIN_SET)

#define LCD_DC_CMD()				HAL_GPIO_WritePin(LCD_DC_Port, LCD_DC_Pin, GPIO_PIN_RESET)
#define LCD_DC_DATA()				HAL_GPIO_WritePin(LCD_DC_Port, LCD_DC_Pin, GPIO_PIN_SET)

#define LCD_RD_ON()					HAL_GPIO_WritePin(LCD_RD_Port, LCD_RD_Pin, GPIO_PIN_RESET)
#define LCD_RD_OFF()				HAL_GPIO_WritePin(LCD_RD_Port, LCD_RD_Pin, GPIO_PIN_SET)

#define LCD_WR_ON()					HAL_GPIO_WritePin(LCD_WR_Port, LCD_WR_Pin, GPIO_PIN_RESET)
#define LCD_WR_OFF()				HAL_GPIO_WritePin(LCD_WR_Port, LCD_WR_Pin, GPIO_PIN_SET)

static uint8_t colstart;
static uint8_t rowstart;
uint8_t scr_width;
uint8_t scr_height;

uint16_t cursor_x = 0;
uint16_t cursor_y = 0;
GFXfont *gfxFont = NULL;
bool wrap = true;

inline static void LCD_WRITE_PP(uint8_t c)
{
	LCD_D07_Port->ODR &= 0xFF00;
	LCD_D07_Port->ODR |= c;
}

#ifndef _swap_uint8_t
/**
 @brief swap data a, b, function is not part of the API, but is a utility
 */
#define _swap_uint8_t(a, b) { uint8_t t = a; a = b; b = t; }
#endif

static void Internal_Init_LCD(void);

void LCD_Init(GPIO_TypeDef *_CS_PORT, uint16_t _CS_Pin, GPIO_TypeDef *_BACKLIGHT_GPIO_Port, uint16_t _BACKLIGHT_Pin,
		GPIO_TypeDef *_LCD_DC_GPIO_Port, uint16_t _LCD_DC_Pin, GPIO_TypeDef *_LCD_RD_GPIO_Port, uint16_t _LCD_RD_Pin,
		GPIO_TypeDef *_LCD_WR_GPIO_Port, uint16_t _LCD_WR_Pin, GPIO_TypeDef *_LCD_RST_GPIO_Port, uint16_t _LCD_RST_Pin,
		GPIO_TypeDef *_LCD_D07_GPIO_Port)
{


	CS_Port = _CS_PORT;
	CS_PIN = _CS_Pin;

	BACKLIGHT_Port = _BACKLIGHT_GPIO_Port;
	BACKLIGHT_Pin = _BACKLIGHT_Pin;

	LCD_DC_Port = _LCD_DC_GPIO_Port;
	LCD_DC_PIN = _LCD_DC_Pin;

	LCD_WR_Port = _LCD_WR_GPIO_Port;
	LCD_WR_PIN = _LCD_WR_Pin;

	LCD_D07_Port = _LCD_D07_GPIO_Port;

	LCD_RST_Port = _LCD_RST_GPIO_Port;
	LCD_RST_PIN = _LCD_RST_Pin;

	LCD_RD_Port = _LCD_RD_GPIO_Port;
	LCD_RD_PIN = _LCD_RD_Pin;



	//Configure Backlight as OFF
	LCD_BackLight(false);

	Internal_Init_LCD();
	//LCD_Clear(COLOR565_BLACK);
	//GUI_Clear();
}

void LCD_DeInit(void)
{
	LCD_Sleep();
	//LCD_Disable();
}

void LCD_Sleep(void)
{
	LCD_Write_Cmd(0x12);
}

void LCD_WakeUp(void)
{
	LCD_Write_Cmd(0x13);
}

void LCD_OFF(void)
{
	LCD_BackLight(false);
	LCD_Write_Cmd(SET_DISPLAY_OFF);
}

void LCD_ON(void)
{
	LCD_Write_Cmd(SET_DISPLAY_ON);
	LCD_BackLight(true);
}

void Internal_Init_LCD(void)
{
	//DC = 1 datos, DC=0 comandos
	//cs 1=disable LCD, 0=enable LCD
	colstart = colSTART;
	rowstart = rowSTART;
	scr_height = SCR_HEIGHT;
	scr_width = SCR_WIDTH;
	LCD_HReset();

	lcdInitialise(LCD_ORIENTATION2);
}

void LCD_BackLight(bool bTurnOn)
{
  change_BaklightIntensity((uint8_t)bTurnOn * 100);
}

uint16_t RGB565(uint8_t R, uint8_t G, uint8_t B)  ///color
{
	return ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3);
}

uint16_t TORGB565(uint32_t color)
{
	uint8_t R = (color & 0x00FF0000) >> 16;
	uint8_t G = (color & 0x0000FF00) >> 8;
	uint8_t B = (color & 0x000000FF);

	return ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3);
}

uint32_t RGB(uint8_t R, uint8_t G, uint8_t B)  ///color
{
	return ((R << 16) | (G << 8) | (B));
}

#if 0
void st_PutStr5x7(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color)
{
	// scale equals 1 drawing faster
	if (scale == 1)
	{
		while (*str)
		{
			drawChar(X, Y, *str++, color, scale);
			if (X < scr_width - 6)
			{
				X += 6;
			}
			else if (Y < scr_height - 8)
			{
				X = 0;
				Y += 8;
			}
			else
			{
				X = 0;
				Y = 0;
			}
		};
	}
	else
	{
		while (*str)
		{
			drawChar(X, Y, *str++, color, scale);
			if (X < scr_width - (scale * 5) + scale)
			{
				X += (scale * 5) + scale;
			}
			else if (Y < scr_height - (scale * 7) + scale)
			{
				X = 0;
				Y += (scale * 7) + scale;
			}
			else
			{
				X = 0;
				Y = 0;
			}
		};
	}
}

void drawChar(uint8_t x, uint8_t y, unsigned char c, uint16_t color, uint8_t size)
{
	if (!gfxFont)
	{
		if ((x >= scr_width) || // Clip right
				(y >= scr_height) || // Clip bottom
				((x + 6 * size - 1) < 0) || // Clip left
				((y + 8 * size - 1) < 0))   // Clip top
			return;

		//if((c >= 176)) c++; // Handle 'classic' charset behavior
		LCD_SELECT();
		int8_t i, j;
		for (i = 0; i < 6; i++)
		{
			uint8_t line;
			if (i < 5)
				line = ((c >= 0x20) && (c <= 0x7F)) ? Font5x7[(c - 32) * 5 + i] : Font5x7[(c - 64) * 5 + i];
			else
				line = 0x0;
			for (j = 0; j < 8; j++, line >>= 1)
			{
				if (line & 0x1)
				{
					if (size == 1)
						LCD_Pixel(x + i, y + j, color);
					else
						LCD_FillRect(x + (i * size), y + (j * size), size, size, color);
				}
			}
		}LCD_DESELECT();
	}
	else
	{
		c -= (uint8_t) gfxFont->first;
		GFXglyph *glyph = &gfxFont->glyph[c];
		uint8_t *bitmap = (uint8_t *) gfxFont->bitmap;

		uint16_t bo = glyph->bitmapOffset;
		uint8_t w = glyph->width, h = glyph->height;
		int8_t xo = glyph->xOffset, yo = glyph->yOffset;
		uint8_t xx, yy, bits = 0, bit = 0;
		int16_t xo16 = 0, yo16 = 0;

		if (size > 1)
		{
			xo16 = xo;
			yo16 = yo;
		}

		LCD_SELECT();
		for (yy = 0; yy < h; yy++)
		{
			for (xx = 0; xx < w; xx++)
			{
				if (!(bit++ & 7))
				{
					bits = bitmap[bo++];
				}
				if (bits & 0x80)
				{
					if (size == 1)
					{
						LCD_Pixel(x + xo + xx, y + yo + yy, color);
					}
					else
					{
						LCD_FillRect(x + (xo16 + xx) * size, y + (yo16 + yy) * size, size, size, color);
					}
				}
				bits <<= 1;
			}
		}LCD_DESELECT();
	}
}

void LCD_setFont(const GFXfont *f)
{
	if (f)
	{            // Font struct pointer passed in?
		if (!gfxFont)
		{ // And no current font struct?
		  // Switching from classic to new font behavior.
		  // Move cursor pos down 6 pixels so it's on baseline.
			cursor_y += 6;
		}
	}
	else if (gfxFont)
	{ // NULL passed.  Current font struct defined?
	  // Switching from new to classic font behavior.
	  // Move cursor pos up 6 pixels so it's at top-left of char.
		cursor_y -= 6;
	}
	gfxFont = (GFXfont *) f;
}

void LCD_PutStr(uint8_t scale, uint8_t X, uint8_t Y, char *str, uint16_t color)
{
	cursor_x = X;
	cursor_y = Y;
	while (*str)
	{
		uint8_t c = *str++;
		if (!gfxFont)
		{
			// scale equals 1 drawing faster
			if (scale == 1)
			{
				drawChar(X, Y, c, color, scale);
				if (X < scr_width - 6)
				{
					X += 6;
				}
				else if (Y < scr_height - 8)
				{
					X = 0;
					Y += 8;
				}
				else
				{
					X = 0;
					Y = 0;
				}
			}
			else
			{
				drawChar(X, Y, c, color, scale);
				if (X < scr_width - (scale * 5) + scale)
				{
					X += (scale * 5) + scale;
				}
				else if (Y < scr_height - (scale * 7) + scale)
				{
					X = 0;
					Y += (scale * 7) + scale;
				}
				else
				{
					X = 0;
					Y = 0;
				}
			}
		}
		else
		{
			if (c == '\n')
			{
				cursor_x = 0;
				cursor_y += (int16_t) scale * (uint8_t) gfxFont->yAdvance;
			}
			else if (c != '\r')
			{
				uint8_t first = gfxFont->first;
				if ((c >= first) && (c <= (uint8_t) gfxFont->last))
				{
					GFXglyph *glyph = &(((GFXglyph *) gfxFont->glyph)[c - first]);
					uint8_t w = glyph->width, h = glyph->height;
					if ((w > 0) && (h > 0))
					{ // Is there an associated bitmap?
						int16_t xo = (int8_t) glyph->xOffset; // sic
						if (wrap && ((cursor_x + scale * (xo + w)) > scr_width))
						{
							cursor_x = 0;
							cursor_y += (int16_t) scale * (uint8_t) gfxFont->yAdvance;
						}
						drawChar(cursor_x, cursor_y, c, color, scale);
					}
					cursor_x += (uint8_t) glyph->xAdvance * (int16_t) scale;
				}
			}
		}
	}
}

void st_GetStr_Sizes(uint8_t scale, uint16_t *_X, uint16_t *_Y, char *str)
{
	uint16_t X = *_X;
	uint16_t Y = *_Y;

	cursor_x = 0;
	cursor_y = 0;

	while (*str)
	{
		uint8_t c = *str++;
		if (!gfxFont)
		{
			// scale equals 1 drawing faster
			if (scale == 1)
			{
				if (X < scr_width - 6)
				{
					X += 6;
				}
				else if (Y < scr_height - 8)
				{
					X = 0;
					Y += 8;
				}
				else
				{
					X = 0;
					Y = 0;
				}
			}
			else
			{
				if (X < scr_width - (scale * 5) + scale)
				{
					X += (scale * 5) + scale;
				}
				else if (Y < scr_height - (scale * 7) + scale)
				{
					X = 0;
					Y += (scale * 7) + scale;
				}
				else
				{
					X = 0;
					Y = 0;
				}
			}
		}
		else
		{
			if (c == '\n')
			{
				cursor_x = 0;
				cursor_y += (int16_t) scale * (uint8_t) gfxFont->yAdvance;
			}
			else if (c != '\r')
			{
				uint8_t first = gfxFont->first;
				if ((c >= first) && (c <= (uint8_t) gfxFont->last))
				{
					GFXglyph *glyph = &(((GFXglyph *) gfxFont->glyph)[c - first]);
					uint8_t w = glyph->width, h = glyph->height;
					if ((w > 0) && (h > 0))
					{ // Is there an associated bitmap?
						int16_t xo = (int8_t) glyph->xOffset; // sic
						if (wrap && ((cursor_x + scale * (xo + w)) > scr_width))
						{
							cursor_x = 0;
							cursor_y += (int16_t) scale * (uint8_t) gfxFont->yAdvance;
						}
					}
					cursor_x += (uint8_t) glyph->xAdvance * (int16_t) scale;
				}
			}
		}
	}
	if (!gfxFont)
	{
		*_X = X;
		*_Y = Y;
	}
	else
	{
		*_X = cursor_x;
		*_Y = cursor_y;
	}
}

void drawBitmap(uint8_t x, uint8_t y, uint8_t *bitmap, uint8_t w, uint8_t h)
{
	uint16_t *pBMP = (uint16_t*)bitmap;

	//LCD_SELECT();
	LCD_SetAddrWindow(x, y, x + w - 1, y + h - 1);

	uint16_t count = (w * h);
	for (uint16_t cpt = 0; cpt < count; cpt++)
	{
		uint16_t word = pBMP[cpt];
		LCD_Write_Data_16(word >> 8, word);
	}
//	LCD_SendBytes(bitmap, w * h * 2);
	//LCD_DESELECT();
}

void drawBitmapRect(uint8_t x, uint8_t y, uint8_t *bitmap, uint16_t startx, uint16_t starty, uint16_t realW, uint16_t realH, uint8_t w, uint8_t h, uint32_t color32, bool bMASK_Black)
{
	uint16_t *pBMP = (uint16_t*)bitmap;

	//LCD_SELECT();
	uint16_t mX = x + realW - 1;
	uint16_t mY = y + realH - 1;
	if (mX >= scr_width)
		mY = scr_width-1;
	if (mY >= scr_height)
		mY = scr_height-1;

	LCD_SetAddrWindow(x, y, mX, mY);

	uint16_t count = (realH * realW);
	for (uint16_t cptY = starty; cptY < (starty+realH); cptY++)
	{
		uint16_t offsetY = cptY * w;

		for (uint16_t cptX = startx; cptX < (startx+realW); cptX++)
		{
			uint16_t word = pBMP[offsetY + cptX];
			if (color32 == 0)
			{
				if (!bMASK_Black)
					LCD_Write_Data_16(0, 0);
			}
			else if (color32 == 0x00FFFFFF)
			{
				LCD_Write_Data_16(word >> 8, word);
			}
			else
			{
				float lum = ((float)(word & 0x001F)) / (31.0f);
				//Conversion en luminance
				uint8_t finalColorR = ((color32 >> 16) & 0xFF);
				uint8_t finalColorG = ((color32 >> 8) & 0xFF);
				uint8_t finalColorB = (color32 & 0xFF);

				if (lum > 0.0f)
				{
					finalColorR = (uint8_t)(finalColorR * lum);
					finalColorG = (uint8_t)(finalColorG * lum);
					finalColorB = (uint8_t)(finalColorB * lum);
					word = RGB565(finalColorR, finalColorG, finalColorB);
					LCD_Write_Data_16(word >> 8, word);
				}
				else
					LCD_Write_Data_16(0, 0);
			}
		}
	}
//	LCD_SendBytes(bitmap, w * h * 2);
	//LCD_DESELECT();
}

void LCD_MIRE(bool bDisplay)
{
	if (bDisplay)
	{
		LCD_FillRect(0, 0, 16, 96, COLOR565_WHITE);
		LCD_FillRect(18, 0, 16, 96, COLOR565_YELLOW);
		LCD_FillRect(36, 0, 16, 96, COLOR565_CYAN);
		LCD_FillRect(54, 0, 16, 96, COLOR565_GREEN);
		LCD_FillRect(72, 0, 16, 96, COLOR565_MAGENTA);
		LCD_FillRect(90, 0, 16, 96, COLOR565_RED);
		LCD_FillRect(108, 0, 18, 96, COLOR565_BLUE);
		LCD_FillRect(0, 96, 128, 16, COLOR565_BLACK);
		LCD_FillRect(0, 112, 128, 16, COLOR565_WHITE_SMOKE);
		LCD_FillRect(126, 0, 18, 96, COLOR565_BURLY_WOOD);
		LCD_FillRect(144, 0, 18, 96, COLOR565_DARK_GOLDEN_ROD);

		st_PutStr5x7(1, 8, 97, "S1", COLOR565_WHITE);
		st_PutStr5x7(1, 26, 97, "/ Dive 1.0", COLOR565_WHITE);

	}
	else
		LCD_FillRect(0, 0, 128, 128, COLOR565_BLACK);

}
void testLCD(void)
{
	LCD_BackLight(true);
	//drawBitmap(12, 4, logo_protectH.pixel_data, logo_protectH.width, logo_protectH.height);
	uint8_t dx, dy;
	uint16_t i, j;
	dx = 0;
	dy = 20;
	int nbTest;
	for (nbTest = 0; nbTest < 1; nbTest++)
	{
		LCD_FillRect(0, 0, 16, 128, COLOR565_WHITE);
		LCD_FillRect(16, 0, 16, 128, COLOR565_YELLOW);
		LCD_FillRect(32, 0, 16, 128, COLOR565_CYAN);
		LCD_FillRect(48, 0, 16, 128, COLOR565_GREEN);
		LCD_FillRect(64, 0, 16, 128, COLOR565_MAGENTA);
		LCD_FillRect(80, 0, 16, 128, COLOR565_RED);
		LCD_FillRect(96, 0, 16, 128, COLOR565_BLUE);
		LCD_FillRect(112, 0, 128, 128, COLOR565_BLACK);

		dy = 0;
		while ((80 - dy))
		{
			LCD_Pixel(64, 79 + dy, COLOR565_GOLD);
			dy++;
		}
		dx = 0;
		while ((64 - dx))
		{
			LCD_Pixel(63 + dx, 80, COLOR565_GOLD);
			dx++;
		}
		dy = 0;
		while ((80 - dy))
		{
			LCD_Pixel(64, 80 - dy, COLOR565_GOLD);
			dy++;
		}
		dx = 0;
		while ((64 - dx))
		{
			LCD_Pixel(64 - dx, 80, COLOR565_GOLD);
			dx++;
		}
		dx = 0;
		while ((64 - dx))
		{
			drawCircle(64, 80, dx + 15, COLOR565_GOLD + dx);
			dx++;
		}
		HAL_Delay(2000);

		LCD_SetAddrWindow(0, 0, scr_width, scr_height);

		LCD_SetAddrWindow(0, 0, scr_width, scr_height);
		HAL_Delay(10);
		st_PutStr5x7(1, 2, 2, "ProtectHunt", COLOR565_WHITE);
		st_PutStr5x7(1, 2, 10, "v1.0", COLOR565_WHITE);
		HAL_Delay(10000);

	}
	LCD_BackLight(false);
}

/**
 * Internal version (no select/deselect)
 * @param XS
 * @param XE
 * @param YS
 * @param YE
 */
void LCD_SetAddrWindow(uint8_t XS, uint8_t YS, uint8_t XE, uint8_t YE)
{
	LCD_Write_Cmd(SET_COLUMN_ADDRESS);
	LCD_Write_Data(0x00); // XSH
	LCD_Write_Data(XS + colSTART); // XSL
	LCD_Write_Data(0x00); // XEH
	LCD_Write_Data(XE + colSTART); // XEL (128 pixels x)

	LCD_Write_Cmd(SET_PAGE_ADDRESS);
	LCD_Write_Data(0x00);
	LCD_Write_Data(YS + rowSTART);
	LCD_Write_Data(0x00);
	LCD_Write_Data(YE + rowSTART); // 160 pixels y

	LCD_Write_Cmd(WRITE_MEMORY_START);
}
#endif
/********************************************
 * 
 * Low-level LCD driving functions
 * 
 **********************************************/

// Reset the LCD hardware
void LCD_HReset(void)
{
	//Chipselect ON
	LCD_SELECT();
	LCD_RD_OFF();
	LCD_WR_ON();

	// Reset pin is active low (0 = reset, 1 = ready)
	LCD_RESET();
	HAL_Delay(15);
	LCD_UNRESET();

	HAL_Delay(120);
	////LCD_DESELECT();
}

void LCD_Write_Cmd(uint8_t data)
{
	////LCD_SELECT();
	LCD_DC_CMD(); 		//D/C on = commando
	//load Data
	LCD_WRITE_PP(data);
	//Wait until transmission complete
	LCD_WR_ON();
//	HAL_Delay(1);
	LCD_WR_OFF();

	LCD_DC_DATA();
	////LCD_DESELECT();
}

void LCD_Write_Data(uint8_t data)
{
	////LCD_SELECT();

//	LCD_DC_DATA();
	//load Data
	LCD_WRITE_PP(data);
	//Wait until transmission complete
	LCD_WR_ON();
//	HAL_Delay(1);
	LCD_WR_OFF();

	////LCD_DESELECT();
}
//Send spi data
void LCD_Write_Data_16(uint8_t dataByte1, uint8_t dataByte2)
{
	////LCD_SELECT();

	LCD_DC_DATA();

	//load Data
	LCD_WRITE_PP(dataByte1);
	//Wait until transmission complete
	LCD_WR_ON();
//	HAL_Delay(1);
	LCD_WR_OFF();

	LCD_WRITE_PP(dataByte2);
	//Wait until transmission complete
	LCD_WR_ON();
	//HAL_Delay(1);
	LCD_WR_OFF();

	////LCD_DESELECT();
}
// Initialise the display with the require screen orientation
void lcdInitialise(uint8_t orientation)
{
	LCD_Write_Cmd(EXIT_SLEEP_MODE);
	HAL_Delay(5); // Wait for the screen to wake up

	LCD_Write_Cmd(SET_PIXEL_FORMAT);
	LCD_Write_Data(0x05); // 16 bits per pixel

	LCD_Write_Cmd(SET_GAMMA_CURVE);
	LCD_Write_Data(0x00); // Select gamma curve 3

	LCD_Write_Cmd(GAM_R_SEL);
	LCD_Write_Data(0x01); // Gamma adjustment enabled

	LCD_Write_Cmd(POSITIVE_GAMMA_CORRECT);
	LCD_Write_Data(0x3f); // 1st Parameter
	LCD_Write_Data(0x25); // 2nd Parameter
	LCD_Write_Data(0x1c); // 3rd Parameter
	LCD_Write_Data(0x1e); // 4th Parameter
	LCD_Write_Data(0x20); // 5th Parameter
	LCD_Write_Data(0x12); // 6th Parameter
	LCD_Write_Data(0x2a); // 7th Parameter
	LCD_Write_Data(0x90); // 8th Parameter
	LCD_Write_Data(0x24); // 9th Parameter
	LCD_Write_Data(0x11); // 10th Parameter
	LCD_Write_Data(0x00); // 11th Parameter
	LCD_Write_Data(0x00); // 12th Parameter
	LCD_Write_Data(0x00); // 13th Parameter
	LCD_Write_Data(0x00); // 14th Parameter
	LCD_Write_Data(0x00); // 15th Parameter

	LCD_Write_Cmd(NEGATIVE_GAMMA_CORRECT);
	LCD_Write_Data(0x20); // 1st Parameter
	LCD_Write_Data(0x20); // 2nd Parameter
	LCD_Write_Data(0x20); // 3rd Parameter
	LCD_Write_Data(0x20); // 4th Parameter
	LCD_Write_Data(0x05); // 5th Parameter
	LCD_Write_Data(0x00); // 6th Parameter
	LCD_Write_Data(0x15); // 7th Parameter
	LCD_Write_Data(0xa7); // 8th Parameter
	LCD_Write_Data(0x3d); // 9th Parameter
	LCD_Write_Data(0x18); // 10th Parameter
	LCD_Write_Data(0x25); // 11th Parameter
	LCD_Write_Data(0x2a); // 12th Parameter
	LCD_Write_Data(0x2b); // 13th Parameter
	LCD_Write_Data(0x2b); // 14th Parameter
	LCD_Write_Data(0x3a); // 15th Parameter
#if 1
	LCD_Write_Cmd(FRAME_RATE_CONTROL1);
	LCD_Write_Data(0x0E); // DIVA = 8
	LCD_Write_Data(0x14); // VPA = 8
//	LCD_Write_Data(0x0A); // DIVA = 8
//	LCD_Write_Data(0x14); // VPA = 8

	LCD_Write_Cmd(FRAME_RATE_CONTROL2);
	LCD_Write_Data(0x0E); // DIVA = 8
	LCD_Write_Data(0x14); // VPA = 8
//	LCD_Write_Data(0x0A); // DIVA = 8
//	LCD_Write_Data(0x14); // VPA = 8

	LCD_Write_Cmd(FRAME_RATE_CONTROL3);
	LCD_Write_Data(0x0E); // DIVA = 8
	LCD_Write_Data(0x14); // VPA = 8
#endif

	LCD_Write_Cmd(DISPLAY_INVERSION);
	LCD_Write_Data(0x07); // NLA = 1, NLB = 1, NLC = 1 (all on Frame Inversion)

	LCD_Write_Cmd(POWER_CONTROL1);
	LCD_Write_Data(0x0a); // VRH = 10:  GVDD = 4.30
	LCD_Write_Data(0x00); // VC = 2: VCI1 = 2.65

	LCD_Write_Cmd(POWER_CONTROL2);
	LCD_Write_Data(0x02); // BT = 2: AVDD = 2xVCI1, VCL = -1xVCI1, VGH = 5xVCI1, VGL = -2xVCI1

	LCD_Write_Cmd(VCOM_CONTROL1);
	LCD_Write_Data(0x2F); // VMH = 80: VCOMH voltage = 4.5
	LCD_Write_Data(0x3E); // VML = 91: VCOML voltage = -0.225

	LCD_Write_Cmd(VCOM_OFFSET_CONTROL);
	LCD_Write_Data(0x40); // nVM = 0, VMF = 64: VCOMH output = VMH, VCOML output = VML

	//LCD_SELECT();

	LCD_Write_Cmd(SET_COLUMN_ADDRESS);
	LCD_Write_Data(0x00); // XSH
	LCD_Write_Data(0); // XSL
	LCD_Write_Data(0x00); // XEH
	LCD_Write_Data(SCR_WIDTH); // XEL (128 pixels x)

	LCD_Write_Cmd(SET_PAGE_ADDRESS);
	LCD_Write_Data(0x00);
	LCD_Write_Data(0);
	LCD_Write_Data(0x00);
	LCD_Write_Data(SCR_HEIGHT); // 160 pixels y

	// Select display orientation
	LCD_Orientation(scr_CW);

	// Set the display to on
	LCD_Write_Cmd(SET_DISPLAY_ON);
	LCD_Write_Cmd(WRITE_MEMORY_START);

	uint16_t FS = (SCR_WIDTH * SCR_HEIGHT);
	LCD_Send16(COLOR565_BLACK, FS);

	//   LCD_Write_Cmd(SET_ADDRESS_MODE);
//	LCD_Write_Data(LCD_ORIENTATION0);

}
#if 0
{
	LCD_Write_Cmd(EXIT_SLEEP_MODE);
	HAL_Delay(5); // Wait for the screen to wake up

	LCD_Write_Cmd(SET_PIXEL_FORMAT);
	LCD_Write_Data(0x05);// 16 bits per pixel

	LCD_Write_Cmd(SET_GAMMA_CURVE);
	LCD_Write_Data(0x04);// Select gamma curve 3

	LCD_Write_Cmd(GAM_R_SEL);
	LCD_Write_Data(0x00);// Gamma adjustment enabled

	LCD_Write_Cmd(POSITIVE_GAMMA_CORRECT);
	LCD_Write_Data(0x3f);// 1st Parameter
	LCD_Write_Data(0x25);// 2nd Parameter
	LCD_Write_Data(0x1c);// 3rd Parameter
	LCD_Write_Data(0x1e);// 4th Parameter
	LCD_Write_Data(0x20);// 5th Parameter
	LCD_Write_Data(0x12);// 6th Parameter
	LCD_Write_Data(0x2a);// 7th Parameter
	LCD_Write_Data(0x90);// 8th Parameter
	LCD_Write_Data(0x24);// 9th Parameter
	LCD_Write_Data(0x11);// 10th Parameter
	LCD_Write_Data(0x00);// 11th Parameter
	LCD_Write_Data(0x00);// 12th Parameter
	LCD_Write_Data(0x00);// 13th Parameter
	LCD_Write_Data(0x00);// 14th Parameter
	LCD_Write_Data(0x00);// 15th Parameter

	LCD_Write_Cmd(NEGATIVE_GAMMA_CORRECT);
	LCD_Write_Data(0x20);// 1st Parameter
	LCD_Write_Data(0x20);// 2nd Parameter
	LCD_Write_Data(0x20);// 3rd Parameter
	LCD_Write_Data(0x20);// 4th Parameter
	LCD_Write_Data(0x05);// 5th Parameter
	LCD_Write_Data(0x00);// 6th Parameter
	LCD_Write_Data(0x15);// 7th Parameter
	LCD_Write_Data(0xa7);// 8th Parameter
	LCD_Write_Data(0x3d);// 9th Parameter
	LCD_Write_Data(0x18);// 10th Parameter
	LCD_Write_Data(0x25);// 11th Parameter
	LCD_Write_Data(0x2a);// 12th Parameter
	LCD_Write_Data(0x2b);// 13th Parameter
	LCD_Write_Data(0x2b);// 14th Parameter
	LCD_Write_Data(0x3a);// 15th Parameter

	LCD_Write_Cmd(FRAME_RATE_CONTROL1);
	LCD_Write_Data(0x08);// DIVA = 8
	LCD_Write_Data(0x08);// VPA = 8

	LCD_Write_Cmd(DISPLAY_INVERSION);
	LCD_Write_Data(0x07);// NLA = 1, NLB = 1, NLC = 1 (all on Frame Inversion)

	LCD_Write_Cmd(POWER_CONTROL1);
	LCD_Write_Data(0x0a);// VRH = 10:  GVDD = 4.30
	LCD_Write_Data(0x02);// VC = 2: VCI1 = 2.65

	LCD_Write_Cmd(POWER_CONTROL2);
	LCD_Write_Data(0x02);// BT = 2: AVDD = 2xVCI1, VCL = -1xVCI1, VGH = 5xVCI1, VGL = -2xVCI1

	LCD_Write_Cmd(VCOM_CONTROL1);
	LCD_Write_Data(0x50);// VMH = 80: VCOMH voltage = 4.5
	LCD_Write_Data(0x5b);// VML = 91: VCOML voltage = -0.225

	LCD_Write_Cmd(VCOM_OFFSET_CONTROL);
	LCD_Write_Data(0x40);// nVM = 0, VMF = 64: VCOMH output = VMH, VCOML output = VML

	LCD_Write_Cmd(SET_COLUMN_ADDRESS);
	LCD_Write_Data(0x00);// XSH
	LCD_Write_Data(0x00);// XSL
	LCD_Write_Data(0x00);// XEH
	LCD_Write_Data(SCR_WIDTH);// XEL (128 pixels x)

	LCD_Write_Cmd(SET_PAGE_ADDRESS);
	LCD_Write_Data(0x00);
	LCD_Write_Data(0x00);
	LCD_Write_Data(0x00);
	LCD_Write_Data(SCR_HEIGHT);// 128 pixels y

	// Select display orientation
	LCD_Write_Cmd(SET_ADDRESS_MODE);
	LCD_Write_Data(orientation);

	// Set the display to on
	LCD_Write_Cmd(SET_DISPLAY_ON);
	LCD_Write_Cmd(WRITE_MEMORY_START);
}

#endif
// LCD graphics functions -----------------------------------------------------------------------------------

void LCD_Orientation(ScrOrientation_TypeDef orientation)
{
	LCD_Write_Cmd(SET_ADDRESS_MODE); // Memory data access control:
	switch (orientation)
	{
		case scr_CW:
			scr_width = SCR_HEIGHT;
			scr_height = SCR_WIDTH;
			LCD_Write_Data(ILI9163_MADCTL_MV | ILI9163_MADCTL_MY | ILI9163_MADCTL_BGR); // X-Y Exchange,Y-Mirror
			break;
		case scr_CCW:
			scr_width = SCR_HEIGHT;
			scr_height = SCR_WIDTH;
			LCD_Write_Data(ILI9163_MADCTL_MV | ILI9163_MADCTL_MX | ILI9163_MADCTL_BGR); // X-Y Exchange,Y-Mirror
			break;
		case scr_180:
			scr_width = SCR_WIDTH;
			scr_height = SCR_HEIGHT;
			LCD_Write_Data(ILI9163_MADCTL_MX | ILI9163_MADCTL_MX | ILI9163_MADCTL_BGR); // X-Y Exchange,Y-Mirror
			break;
		default:
			scr_width = SCR_WIDTH;
			scr_height = SCR_HEIGHT;
			LCD_Write_Data(ILI9163_MADCTL_BGR); // X-Y Exchange,Y-Mirror
			break;
	}
}

uint8_t LCD_Send16(uint16_t word, uint16_t count)
{
	for (uint16_t cpt = 0; cpt < count; cpt++)
		LCD_Write_Data_16(word >> 8, word);

	return 0;
}
#if 0
void LCD_Clear(uint16_t color)
{
	LCD_FillRect(0, 0, scr_width, scr_height, color);
}

void LCD_Pixel(int16_t X, int16_t Y, uint16_t color)
{
	if ((X >= scr_width) || (Y >= scr_height))
		return;
	if ((X < 0) || (Y < 0))
		return;

	//LCD_SELECT();
	LCD_SetAddrWindow(X, Y, X, Y);

	LCD_Write_ColorData(color);

	//LCD_DESELECT();
}

void LCD_HLine(int16_t x, int16_t y, int16_t w, uint16_t color)
{
	if (x < 0)
	{
		w = w + x;
		x = 0;
	}
	if (x+w > scr_width)
		w = (scr_width - x);

	if (w <= 0)
		return;

	LCD_SetAddrWindow(x, y, x + w - 1, y);

	LCD_Send16(color, w);

}
void LCD_Write_ColorData(uint16_t color)
{
	LCD_Send16(color, 1);
}

void LCD_VLine(int16_t x, int16_t y, int16_t h, uint16_t color)
{
	if (y < 0)
	{
		h = h + y;
		y = 0;
	}
	if (y+h > scr_height)
		h = (scr_height - y);

	if (h <= 0)
		return;

	//LCD_SELECT();
	LCD_SetAddrWindow(x, y, x, y + h - 1);

	LCD_Send16(color, h);
}

void LCD_Line(int16_t X1, int16_t Y1, int16_t X2, int16_t Y2, uint16_t color)
{
	int16_t dX = X2 - X1;
	int16_t dY = Y2 - Y1;
	int16_t dXsym = (dX > 0) ? 1 : -1;
	int16_t dYsym = (dY > 0) ? 1 : -1;

	if (dX == 0)
	{
		if (Y2 > Y1)
			LCD_VLine(X1, Y1, (Y2-Y1)+1, color);
		else
			LCD_VLine(X1, Y2, (Y1-Y2)+1, color);
		return;
	}
	if (dY == 0)
	{
		if (X2 > X1)
			LCD_HLine(X1, Y1, (X2-X1)+1, color);
		else
			LCD_HLine(X2, Y1, (X1-X2)+1, color);
		return;
	}

	dX *= dXsym;
	dY *= dYsym;
	int16_t dX2 = dX << 1;
	int16_t dY2 = dY << 1;
	int16_t di;

	//LCD_SELECT();
	if (dX >= dY)
	{
		di = dY2 - dX;
		while (X1 != X2)
		{
			LCD_Pixel(X1, Y1, color);
			X1 += dXsym;
			if (di < 0)
			{
				di += dY2;
			}
			else
			{
				di += dY2 - dX2;
				Y1 += dYsym;
			}
		}
	}
	else
	{
		di = dX2 - dY;
		while (Y1 != Y2)
		{
			LCD_Pixel(X1, Y1, color);
			Y1 += dYsym;
			if (di < 0)
			{
				di += dX2;
			}
			else
			{
				di += dX2 - dY2;
				X1 += dXsym;
			}
		}
	}
	LCD_Pixel(X1, Y1, color);

	//LCD_DESELECT();
}

void LCD_Rect(uint16_t X1, uint16_t Y1, uint16_t X2, uint16_t Y2, uint16_t color)
{

	LCD_HLine(X1, Y1, (X2-X1)+1, color);
	LCD_HLine(X1, Y2, (X2-X1)+1, color);
	LCD_VLine(X1, Y1, (Y2-Y1)+1, color);
	LCD_VLine(X2, Y1, (Y2-Y1)+1, color);
}

void LCD_FillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint16_t color)
{
	if ((x > scr_width) || (y > scr_height))
		return;
	if ((x + w - 1) >= scr_width)
		w = scr_width - x;
	if ((y + h - 1) >= scr_height)
		h = scr_height - y;

	uint16_t FS = (w * h);

	//LCD_SELECT();
	LCD_SetAddrWindow(x, y, x + w - 1, y + h - 1);

	LCD_Send16(color, FS);

	//LCD_DESELECT();
}

//###################################### Primitivas utilizandos algoritmos encontrados en Adafruit_GFX ##################################################
void drawCircle(uint8_t x0, uint8_t y0, uint8_t r, uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	LCD_Pixel(x0, y0 + r, color);
	LCD_Pixel(x0, y0 - r, color);
	LCD_Pixel(x0 + r, y0, color);
	LCD_Pixel(x0 - r, y0, color);

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		LCD_Pixel(x0 + x, y0 + y, color);
		LCD_Pixel(x0 - x, y0 + y, color);
		LCD_Pixel(x0 + x, y0 - y, color);
		LCD_Pixel(x0 - x, y0 - y, color);
		LCD_Pixel(x0 + y, y0 + x, color);
		LCD_Pixel(x0 - y, y0 + x, color);
		LCD_Pixel(x0 + y, y0 - x, color);
		LCD_Pixel(x0 - y, y0 - x, color);
	}
}
//###################################### Primitivas utilizandos algoritmos encontrados en Adafruit_GFX ##################################################
void drawCircle2(uint8_t x0, uint8_t y0, uint8_t r, uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	LCD_Pixel(x0, y0 + r, color);
	LCD_Pixel(x0, y0 - r, color);
	LCD_Pixel(x0 + r, y0, color);
	LCD_Pixel(x0 - r, y0, color);

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		//Down quarter
		//4.5h a 6h
		LCD_Pixel(x0 + x, y0 + y, color);
		//6h a 7.5h
		LCD_Pixel(x0 - x, y0 + y, color);

		//Up quarter
		//0 a 1.5h quarter
		LCD_Pixel(x0 + x, y0 - y, color);
		//10.5 a 12h quarter
		LCD_Pixel(x0 - x, y0 - y, color);

		//3h a 4.5h
		LCD_Pixel(x0 + y, y0 + x, color);
		//7.5h a 9h
		LCD_Pixel(x0 - y, y0 + x, color);
		//1.5h a 3h
		LCD_Pixel(x0 + y, y0 - x, color);
		//9h a 10.5h
		LCD_Pixel(x0 - y, y0 - x, color);
	}
}

void drawCircleHelper(uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint16_t color)
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;
		if (cornername & 0x4)
		{
			LCD_Pixel(x0 + x, y0 + y, color);
			LCD_Pixel(x0 + y, y0 + x, color);
		}
		if (cornername & 0x2)
		{
			LCD_Pixel(x0 + x, y0 - y, color);
			LCD_Pixel(x0 + y, y0 - x, color);
		}
		if (cornername & 0x8)
		{
			LCD_Pixel(x0 - y, y0 + x, color);
			LCD_Pixel(x0 - x, y0 + y, color);
		}
		if (cornername & 0x1)
		{
			LCD_Pixel(x0 - y, y0 - x, color);
			LCD_Pixel(x0 - x, y0 - y, color);
		}
	}
}

void fillCircle(uint8_t x0, uint8_t y0, uint8_t r, uint16_t color)
{
	LCD_VLine(x0, y0 - r, 2 * r + 1, color);
	fillCircleHelper(x0, y0, r, 3, 0, color);
}

void fillCircleHelper(uint8_t x0, uint8_t y0, uint8_t r, uint8_t cornername, uint8_t delta, uint16_t color)
{

	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y)
	{
		if (f >= 0)
		{
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		if (cornername & 0x1)
		{
			LCD_VLine(x0 + x, y0 - y, 2 * y + 1 + delta, color);
			LCD_VLine(x0 + y, y0 - x, 2 * x + 1 + delta, color);
		}
		if (cornername & 0x2)
		{
			LCD_VLine(x0 - x, y0 - y, 2 * y + 1 + delta, color);
			LCD_VLine(x0 - y, y0 - x, 2 * x + 1 + delta, color);
		}
	}
}
void LCD_DrawTriangle(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color)
{
	LCD_Line(x0, y0, x1, y1, color);
	LCD_Line(x1, y1, x2, y2, color);
	LCD_Line(x2, y2, x0, y0, color);

}

void LCD_FillTriangle(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t color)
{

	int16_t a, b, y, last;

	// Sort coordinates by Y order (y2 >= y1 >= y0)
	if (y0 > y1)
	{
		_swap_uint8_t(y0, y1);
		_swap_uint8_t(x0, x1);
	}
	if (y1 > y2)
	{
		_swap_uint8_t(y2, y1);
		_swap_uint8_t(x2, x1);
	}
	if (y0 > y1)
	{
		_swap_uint8_t(y0, y1);
		_swap_uint8_t(x0, x1);
	}

	if (y0 == y2)
	{ // Handle awkward all-on-same-line case as its own thing
		a = b = x0;
		if (x1 < a)
			a = x1;
		else if (x1 > b)
			b = x1;
		if (x2 < a)
			a = x2;
		else if (x2 > b)
			b = x2;
		LCD_HLine(a, y0, b - a + 1, color);
		return;
	}

	int16_t dx01 = x1 - x0, dy01 = y1 - y0, dx02 = x2 - x0, dy02 = y2 - y0, dx12 = x2 - x1, dy12 = y2 - y1;
	int32_t sa = 0, sb = 0;

	// For upper part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline y1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if y0=y1
	// (flat-topped triangle).
	if (y1 == y2)
		last = y1;   // Include y1 scanline
	else
		last = y1 - 1; // Skip it

	for (y = y0; y <= last; y++)
	{
		a = x0 + sa / dy01;
		b = x0 + sb / dy02;
		sa += dx01;
		sb += dx02;
		/* longhand:
		 a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			_swap_uint8_t(a, b);
		LCD_HLine(a, y, b - a + 1, color);
	}

	// For lower part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if y1=y2.
	sa = dx12 * (y - y1);
	sb = dx02 * (y - y0);
	for (; y <= y2; y++)
	{
		a = x1 + sa / dy12;
		b = x0 + sb / dy02;
		sa += dx12;
		sb += dx02;
		/* longhand:
		 a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			_swap_uint8_t(a, b);
		LCD_HLine(a, y, b - a + 1, color);
	}
}


void LCD_FillTriangle_AA(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint16_t colorF, uint16_t colorB)
{

	int16_t a, b, y, last;

	// Sort coordinates by Y order (y2 >= y1 >= y0)
	if (y0 > y1)
	{
		_swap_uint8_t(y0, y1);
		_swap_uint8_t(x0, x1);
	}
	if (y1 > y2)
	{
		_swap_uint8_t(y2, y1);
		_swap_uint8_t(x2, x1);
	}
	if (y0 > y1)
	{
		_swap_uint8_t(y0, y1);
		_swap_uint8_t(x0, x1);
	}

	if (y0 == y2)
	{ // Handle awkward all-on-same-line case as its own thing
		a = b = x0;
		if (x1 < a)
			a = x1;
		else if (x1 > b)
			b = x1;
		if (x2 < a)
			a = x2;
		else if (x2 > b)
			b = x2;
		LCD_HLine(a, y0, b - a + 1, colorF);
		LCD_Pixel(a, y0, colorB);
		LCD_Pixel(a+b - a, y0, colorB);
		return;
	}

	int16_t dx01 = x1 - x0, dy01 = y1 - y0, dx02 = x2 - x0, dy02 = y2 - y0, dx12 = x2 - x1, dy12 = y2 - y1;
	int32_t sa = 0, sb = 0;

	// For upper part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline y1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if y0=y1
	// (flat-topped triangle).
	if (y1 == y2)
		last = y1;   // Include y1 scanline
	else
		last = y1 - 1; // Skip it

	for (y = y0; y <= last; y++)
	{
		a = x0 + sa / dy01;
		b = x0 + sb / dy02;
		sa += dx01;
		sb += dx02;
		/* longhand:
		 a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			_swap_uint8_t(a, b);
		LCD_HLine(a, y, b - a + 1, colorF);
		LCD_Pixel(a, y, colorB);
		LCD_Pixel(a+b - a, y, colorB);
	}

	// For lower part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if y1=y2.
	sa = dx12 * (y - y1);
	sb = dx02 * (y - y0);
	for (; y <= y2; y++)
	{
		a = x1 + sa / dy12;
		b = x0 + sb / dy02;
		sa += dx12;
		sb += dx02;
		/* longhand:
		 a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			_swap_uint8_t(a, b);
		LCD_HLine(a, y, b - a + 1, colorF);
		LCD_Pixel(a, y, colorB);
		LCD_Pixel(a+b - a, y, colorB);
	}
}

void drawRoundRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t r, uint16_t color)
{
	// smarter version
	LCD_HLine(x + r, y, w - 2 * r, color); // Top
	LCD_HLine(x + r, y + h - 1, w - 2 * r, color); // Bottom
	LCD_VLine(x, y + r, h - 2 * r, color); // Left
	LCD_VLine(x + w - 1, y + r, h - 2 * r, color); // Right
	// draw four corners
	drawCircleHelper(x + r, y + r, r, 1, color);
	drawCircleHelper(x + w - r - 1, y + r, r, 2, color);
	drawCircleHelper(x + w - r - 1, y + h - r - 1, r, 4, color);
	drawCircleHelper(x + r, y + h - r - 1, r, 8, color);
}

void fillRoundRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t r, uint16_t color)
{
	// smarter version
	LCD_FillRect(x + r, y, w - 2 * r, h, color);

	// draw four corners
	fillCircleHelper(x + w - r - 1, y + r, r, 1, h - 2 * r - 1, color);
	fillCircleHelper(x + r, y + r, r, 2, h - 2 * r - 1, color);
}

void ILI9163_pset(UG_S16 x, UG_S16 y, UG_COLOR col)
{
	uint32_t addr;
	uint8_t r, g, b;

	if (x < 0)
		return;
	if (y < 0)
		return;

	addr = x + y * scr_width;
	addr <<= 1;

	r = col >> 16 & 0xFF;
	g = col >> 8 & 0xFF;
	b = col & 0xFF;

	r >>= 3;
	g >>= 2;
	b >>= 3;

	col = b | (g << 5) | (r << 11);

	LCD_Pixel(x, y, col);
}

/* Hardware accelerator for UG_DrawLine (Platform: STM32F4x9) */
UG_RESULT _HW_DrawLine(UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR col)
{
	uint8_t r, g, b;

	r = col >> 16 & 0xFF;
	g = col >> 8 & 0xFF;
	b = col & 0xFF;

	r >>= 3;
	g >>= 2;
	b >>= 3;

	col = b | (g << 5) | (r << 11);

	LCD_Line(x1, y1, (x2 - x1) + 1, (y2 - y1) + 1, col);

	return UG_RESULT_OK;
}

/* Hardware accelerator for UG_FillFrame (Platform: STM32F4x9) */
UG_RESULT _HW_FillFrame(UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR col)
{
	uint8_t r, g, b;

	r = col >> 16 & 0xFF;
	g = col >> 8 & 0xFF;
	b = col & 0xFF;

	r >>= 3;
	g >>= 2;
	b >>= 3;

	col = b | (g << 5) | (r << 11);

	LCD_FillRect(x1, y1, (x2 - x1) + 1, (y2 - y1) + 1, col);

	return UG_RESULT_OK;
}
#endif

void change_BaklightIntensity (uint8_t percent)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, percent);
}

uint8_t get_BaklightIntensity (void)
{
	return __HAL_TIM_GET_COMPARE(&htim3, TIM_CHANNEL_4);
}

void pulseBacklight(void)
{
	for (float in  = 4.712; in < 10.995; in = in + 0.00628)
	{
		change_BaklightIntensity(((sin(in) * 127.5 + 127.5)*100)/255);
		HAL_Delay(3);
	}
}
