/*
 * RTC.c
 *
 *  Created on: 19 d�c. 2017
 *      Author: Benjamin
 */
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

#include <rtc.h>
#include <stm32l4xx_hal.h>
#include <User_RTC.h>

#include <al_tools.h>

static bool bClockMode12h = false;
static bool bDateModemmdd = false;

/*
Register				Minimum (hex)	Maximum (hex)
Seconds					00				59
Minutes					00				59
Hours (24-hour mode)	00				23
Day						01				07
Date					01				31
Month					01				12
Year					00				99
*/
void RTC_SetTime_old (uint8_t Hours, uint8_t Minutes, uint8_t Seconds, uint8_t WeekDay, uint8_t Month, uint8_t Date,uint8_t Year )
{
  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

  //HAL_RTC_DeInit(&hrtc);
	/**Initialize RTC and set the Time and Date
    */
  sTime.Hours = Hours;
  sTime.Minutes = Minutes;
  sTime.Seconds = Seconds;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sDate.WeekDay = WeekDay;
  sDate.Month = Month;
  sDate.Date = Date;
  sDate.Year = Year;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  HAL_RTC_Init(&hrtc);
}

void RTC_GetTime_old (uint8_t *Hours, uint8_t *Minutes, uint8_t *Seconds, uint8_t *WeekDay, uint8_t *Month, uint8_t *Date,uint8_t *Year )
{
    uint32_t tmpreg = (uint32_t) RTC->TR;

	*Hours = (uint8_t)((tmpreg & (RTC_TR_HT | RTC_TR_HU)) >> 16);
	*Minutes = (uint8_t)((tmpreg & (RTC_TR_MNT | RTC_TR_MNU)) >>8);
	*Seconds = (uint8_t)(tmpreg & (RTC_TR_ST | RTC_TR_SU));

    tmpreg = (uint32_t) RTC->DR;

	*Year = (uint8_t)((tmpreg & (RTC_DR_YT | RTC_DR_YU)) >> 16);
	*Month = (uint8_t)((tmpreg & (RTC_DR_MT | RTC_DR_MU)) >> 8);
	*Date = (uint8_t)(tmpreg & (RTC_DR_DT | RTC_DR_DU));
	*WeekDay = (uint8_t)((tmpreg & (RTC_DR_WDU)) >> 13);
}

void RTC_SetTime (TimeType * Time)
{
  RTC_TimeTypeDef sTime;
  RTC_DateTypeDef sDate;

  //HAL_RTC_DeInit(&hrtc);
	/**Initialize RTC and set the Time and Date
    */
  sTime.Hours = Time->Hours;
  sTime.Minutes = Time->Minutes;
  sTime.Seconds = Time->Seconds;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sDate.WeekDay = Time->WeekDay;
  sDate.Month = Time->Month;
  sDate.Date =Time->Date;
  sDate.Year = Time->Year;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  HAL_RTC_Init(&hrtc);
}


TimeType RTC_GetTime(void)
{
	TimeType Time;

	uint32_t tmpreg = (uint32_t) RTC->TR;

    Time.Hours = (uint8_t)((tmpreg & (RTC_TR_HT | RTC_TR_HU)) >> 16);
    Time.Minutes = (uint8_t)((tmpreg & (RTC_TR_MNT | RTC_TR_MNU)) >>8);
    Time.Seconds = (uint8_t)(tmpreg & (RTC_TR_ST | RTC_TR_SU));

    tmpreg = (uint32_t) RTC->DR;

    Time.Year = (uint8_t)((tmpreg & (RTC_DR_YT | RTC_DR_YU)) >> 16);
    Time.Month = (uint8_t)((tmpreg & (RTC_DR_MT | RTC_DR_MU)) >> 8);
    Time.Date = (uint8_t)(tmpreg & (RTC_DR_DT | RTC_DR_DU));
    Time.WeekDay = (uint8_t)((tmpreg & (RTC_DR_WDU)) >> 13);
    return Time;
}

struct tm TimeType2tm(TimeType t)
{
   struct tm  mytm = { 0 };
   mytm.tm_hour = bcd2int(t.Hours  )    ;
   mytm.tm_mday = bcd2int(t.Date   )    ;
   mytm.tm_min  = bcd2int(t.Minutes)    ;
   mytm.tm_mon  = bcd2int(t.Month  )-1  ;
   mytm.tm_sec  = bcd2int(t.Seconds)    ;
   mytm.tm_wday = bcd2int(t.WeekDay)    ;
   mytm.tm_year = bcd2int(t.Year   )+100;
   return mytm;
}

TimeType tm2TimeType(struct tm tm)
{
   TimeType  myt = { 0 };
   myt.Date    = int2bcd(tm.tm_mday    );
   myt.Hours   = int2bcd(tm.tm_hour    );
   myt.Minutes = int2bcd(tm.tm_min     );
   myt.Month   = int2bcd(tm.tm_mon +1  );
   myt.Seconds = int2bcd(tm.tm_sec     );
   myt.WeekDay = int2bcd(tm.tm_wday    );
   myt.Year    = int2bcd(tm.tm_year-100);
   return myt;
}

void normalizeTimeType(TimeType * t)
{
	struct tm tmpTime = TimeType2tm(*t);
	mktime(&tmpTime);
	*t = tm2TimeType(tmpTime);
}

float get_TimeDiffInSeconds(TimeType startTime, TimeType stopTime)
{
	struct tm  ta = TimeType2tm(startTime);
	struct tm  tb = TimeType2tm(stopTime);
	time_t TA =  mktime(&ta);
	time_t TB =  mktime(&tb);
	float diff = (float)difftime(TB, TA);
	return diff;
}

void RTC_Set12hMode(bool set)
{
	bClockMode12h = set;
}

bool RTC_is12hMode(void)
{
	return bClockMode12h;
}

void RTC_SetmmddMode(bool set)
{
	bDateModemmdd = set;
}

bool RTC_ismmddMode(void)
{
	return bDateModemmdd;
}
