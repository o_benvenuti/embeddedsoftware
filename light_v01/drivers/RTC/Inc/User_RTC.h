/*
 * RTC.h
 *
 *  Created on: 19 d�c. 2017
 *      Author: Benjamin
 */

#ifndef RTC_RTC_H_
#define RTC_RTC_H_
#include <time.h>
#include <stdbool.h>

typedef struct
{
  uint8_t Hours;            /*!< Specifies the RTC Time Hour.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 12 if the RTC_HourFormat_12 is selected.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 23 if the RTC_HourFormat_24 is selected */

  uint8_t Minutes;          /*!< Specifies the RTC Time Minutes.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 59 */

  uint8_t Seconds;          /*!< Specifies the RTC Time Seconds.
                                 This parameter must be a number between Min_Data = 0 and Max_Data = 59 */

  uint8_t WeekDay;  /*!< Specifies the RTC Date WeekDay.
                         This parameter can be a value of @ref RTC_WeekDay_Definitions */

  uint8_t Month;    /*!< Specifies the RTC Date Month (in BCD format).
                         This parameter can be a value of @ref RTC_Month_Date_Definitions */

  uint8_t Date;     /*!< Specifies the RTC Date.
                         This parameter must be a number between Min_Data = 1 and Max_Data = 31 */

  uint8_t Year;     /*!< Specifies the RTC Date Year.
                         This parameter must be a number between Min_Data = 0 and Max_Data = 99 */
}TimeType;



void      RTC_SetTime_old (uint8_t Hours, uint8_t Minutes, uint8_t Seconds, uint8_t WeekDay, uint8_t Month, uint8_t Date,uint8_t Year );
void      RTC_GetTime_old (uint8_t *Hours, uint8_t *Minutes, uint8_t *Seconds, uint8_t *WeekDay, uint8_t *Month, uint8_t *Date,uint8_t *Year );
TimeType  RTC_GetTime(void);
void      RTC_SetTime (TimeType * Time);
struct tm TimeType2tm(TimeType t);
TimeType  tm2TimeType(struct tm tm);
void      normalizeTimeType(TimeType * t);
float     get_TimeDiffInSeconds(TimeType startTime, TimeType stopTime);
void 	  RTC_Set12hMode(bool set);
bool 	  RTC_is12hMode(void);
void      RTC_SetmmddMode(bool set);
bool      RTC_ismmddMode(void);




#endif /* RTC_RTC_H_ */
