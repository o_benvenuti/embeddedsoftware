/*
 * common.h
 *
 *  Created on: 7 d�c. 2017 *      Author: Benjamin
 */

#ifndef COMMON_H_
#define COMMON_H_
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdint.h>
#include <stdbool.h>

#include <User_RTC.h>

#include "serenityConfig.h"
#include <cmsis_os.h>

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ICE_NAME_LENGTH         15
#define ICE_PHONE_LENGTH        19
#define ICE_NATIO_LENGTH        4

// IMPORTANT keep a log of previous magic value
// 10.01.2020: 0xBADC0DED
#define SERENITY_CONTEXT_MAGIC  0xDEADBEEF

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef enum serenity_dive_mode {
    Mode_DecoAlgo 	    = 1,
}serenity_dive_mode_t;

typedef struct serenity_dive_stop {
    bool          occuring ;
    bool          allowed  ;
    TimeType      startTime;
    float         StopDepth;
    uint16_t      StopDuration;
    uint8_t       Ascent_speed;
}serenity_dive_stop_t;

typedef struct serenity_free_param {
    uint8_t                 lap_count;
    float                   depth_max;
    uint16_t                lap_duration;
    uint16_t                rest_duration;
    uint16_t                lap_duration_max;
    uint16_t                rest_time_to_end; /* in seconds */
}serenity_free_param_t;

typedef struct serenity_deco_param {
    bool                    safety_stop;
    float                   o2_ratio;
    float                   he_ratio;
    float                   ppo2;
    float                   safety_factor;
    uint8_t                 max_speed;
    float                   depth_max;
    uint16_t                dive_duration; /* in minutes */
    serenity_dive_stop_t    dive_stop;
}serenity_deco_param_t;

typedef struct serenity_depth_param {
    float depth_max;
    uint8_t  stop_nb;
    uint16_t  dive_duration; /* in minutes */
    serenity_dive_stop_t dive_stops[NB_PALIERS_MAX];
}serenity_depth_param_t;

typedef struct serenity_context {
    int      language;
    uint32_t option_code;
    uint8_t  water_salinity;
    uint8_t  channel;
    uint32_t compass_declination;
    uint8_t  dive_mode;
    uint8_t  Brightness;
    uint8_t  flick_sensitivity;
    char     FirstName  [14];
    char     LastName   [14];
    char     PhoneNumber[20];
    char     Nationality[3];
    bool     h12 ;
    bool     mmdd;
    bool     metric_system;
    TimeType last_dive_date;
    serenity_free_param_t  free_params;
    serenity_deco_param_t  deco_params;
    serenity_depth_param_t depth_params;
}serenity_context_t;

typedef enum _serenity_state_t {
    serenity_state_default,
    serenity_state_charging,
    serenity_state_dive_config,
    serenity_state_watch_config,
    serenity_state_ble,
    serenity_state_dive,
}serenity_state_t;

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
/* Depth sensor Global variables */
extern int32_t gStartDiveLevelPressure;

/* RTC time global variables */
//extern TimeType gsTime;
extern TimeType gsTimeSettings;
extern TimeType gsLastActivityTime;

/* global owner infos */
extern char gFirstName[ICE_NAME_LENGTH];
extern char gLastName[ICE_NAME_LENGTH];
extern char gPhoneNumber[ICE_PHONE_LENGTH];
extern char gNationality[ICE_NATIO_LENGTH];

/* poc poc variables */
extern uint8_t poc_sensitivity;

extern bool     gWakedUpFromStandby;
extern SemaphoreHandle_t flick_flag;

#ifdef DEBUG
extern void initialise_monitor_handles(void);
#endif /* DEBUG */

void user_Init(void);
bool ChargeError(void);
uint8_t getstate_STWLC33_INT(void);
void shutDownPeripherals(void);
void serenity_standby_with_acc(void);
void serenity_standby_with_rtc(int sec); /* sec need to be from 1 to 30 secs */
void displayDebugScreen(void);
void displayDiveDebugScreen(void);
void DEBUG_display_mA(void);
void displayMagCalScreen(void);
int serenity_save_status(serenity_context_t * ctx);
int serenity_retrieve_status(serenity_context_t * ctx);
void serenity_factory_reset(void);
int serenity_get_brigthness(void);
void serenity_set_default(serenity_context_t * context);
void vApplicationStackOverflowHook( TaskHandle_t xTask, signed char *pcTaskName );

#endif /* COMMON_H_ */
