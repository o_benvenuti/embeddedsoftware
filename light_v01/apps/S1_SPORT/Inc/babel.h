#ifndef BABEL_H_
#define BABEL_H_
#include <languages.h>

enum BABEL_LIST
{
	TXT_WEEKDAY_SUNDAY = 0,              //  0
	TXT_WEEKDAY_MONDAY,                  //  1
	TXT_WEEKDAY_TUESDAY,                 //  2
	TXT_WEEKDAY_WEDNESDAY,               //  3
	TXT_WEEKDAY_THURSDAY,                //  4
	TXT_WEEKDAY_FRIDAY,                  //  5
	TXT_WEEKDAY_SATURDAY,                //  6
	TXT_NO_DIVE,                         //  7
	TXT_PARAM_MODE,                      //  8
	TXT_PARAM_STOPS,                     //  9
	TXT_PARAM_MAX_DEPTH,                 // 10
	TXT_PARAM_DIVE_TIME,                 // 11
	TXT_PARAM_STOP_DEPTH,                // 12
	TXT_PARAM_STOP_DURATION,             // 13
	TXT_PARAM_STOP_SPEED,                // 14
	TXT_DECO_PARAM_OXYGEN,               // 15
	TXT_DECO_PARAM_PP02,                 // 16
	TXT_DECO_PARAM_SAFETY,               // 17
	TXT_DECO_PARAM_MAX_DEPTH,            // 18
	TXT_DECO_PARAM_CONSERVATIVE,         // 19
	TXT_DECO_PARAM_STANDARD,             // 20
	TXT_DECO_PARAM_PROGRESSIVE,          // 21
    TXT_SPEED_UNIT_METRIC,               // 22
    TXT_SPEED_UNIT_FEET,                 // 23
    TXT_M_UNIT,                          // 24
    TXT_F_UNIT,                          // 25
    TXT_SECONDS_UNIT,                    // 26
	TXT_MINUTES_UNIT,                    // 27
	TXT_HOURS_UNIT,                      // 28
	TXT_FLICK_SENSITIVITY_L1,            // 29
	TXT_FLICK_SENSITIVITY_L2,            // 30
	TXT_BRIGHTNESS,                      // 31
	TXT_COMPASS_CALIBRATION_L1,          // 32
	TXT_COMPASS_CALIBRATION_L2,          // 33
	TXT_START_COMPASS_CALIBRATION,       // 34
	TXT_NO,              		         // 35
	TXT_YES,             		         // 36
	TXT_FACTORY_RESET_BUTTON_L1,         // 37
	TXT_FACTORY_RESET_BUTTON_L2,         // 38
	TXT_FACTORY_RESET_WARNING,           // 39
	TXT_FACTORY_RESET_L1,                // 40
	TXT_FACTORY_RESET_L2,                // 41
	TXT_FACTORY_RESET_L3,                // 42
	TXT_FACTORY_RESET_L4,                // 43
	TXT_MAGCAL_TITLE,                    // 44
	TXT_MAGCAL_8_L1,                     // 45
	TXT_MAGCAL_8_L2,                     // 46
	TXT_MAGCAL_8_L3,                     // 47
	TXT_MAGCAL_READY_O_TITLE,            // 48
	TXT_MAGCAL_READY_O_L1,               // 49
	TXT_MAGCAL_READY_O_L2,               // 50
	TXT_MAGCAL_READY_O_L3,               // 51
	TXT_MAGCAL_READY_O_L4,               // 52
	TXT_MAGCAL_READY_O_BUTTON,           // 53
	TXT_MAGCAL_O_L1,    		         // 54
	TXT_MAGCAL_O_L2,                     // 55
	TXT_BLE_CONNECTED,                   // 56
	TXT_BLE_PHONE,                       // 57
	TXT_BLE_SLAVE,                       // 58
	TXT_BLE_MASTER,                      // 59
	TXT_BLE_NAME,                        // 60
	TXT_BLE_DOWNLOADING,                 // 61
	TXT_BLE_SCAN_UPDATED_L1,             // 62
	TXT_BLE_SCAN_UPDATED_L2,             // 63
	TXT_STOP_LETTER,                     // 64
	TXT_DIVE_TIME,                       // 65
	TXT_DIVE_STOP,                       // 66
	TXT_STOP_SAFETY,                     // 67
	TXT_STOP_DEEP,                       // 68
	TXT_TOO_FAST,                        // 69
	TXT_SLOW_DOWN,                       // 70
	TXT_FREE_DIVE_LAPS,                  // 71
	TXT_FREE_DIVE_MAX_LAP,               // 72
	TXT_FREE_DIVE_TOTAL_TIME,            // 73
	TXT_FREE_DIVE_LAP,                   // 74
	TXT_FREE_DIVE_REST_TIME,             // 75
	TXT_PARAM_FREE_DIVE_IDLE_TIME_L1,    // 76
	TXT_PARAM_FREE_DIVE_IDLE_TIME_L2,    // 77
    TXT_TOO_DEEP,                        // 78
    TXT_GO_UP,                           // 79
    TXT_BLE_WAIT_CONN,                   // 80
    TXT_METER_SYSTEM,
    TXT_FEET_SYSTEM,
    TXT_UNIT_SYSTEM_L1,
    TXT_UNIT_SYSTEM_L2,
};

#endif /* BABEL_H_ */
