//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_tools.h>
#include <DIALOG.h>
#include <stdio.h>
#include <common.h>
#include <GUI_serenity.h>
#include <ms5837_driver.h>
#include "babel.h"
#include <trace.h>

#include "feature_dive_config.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_BUTTON_0              (GUI_ID_USER + 0x01)
#define ID_BUTTON_1              (GUI_ID_USER + 0x02)
#define ID_BUTTON_2              (GUI_ID_USER + 0x03)
#define ID_BUTTON_3              (GUI_ID_USER + 0x04)
#define ID_BUTTON_4              (GUI_ID_USER + 0x05)
#define ID_BUTTON_5              (GUI_ID_USER + 0x06)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDiveMenuBackground(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
bool Oxy_Button_Clicked = false;
bool PPO2_Button_Clicked = false;
bool Safety_Button_Clicked = false;
bool Up_Button_Clicked;
bool Down_Button_Clicked;

static serenity_context_t * _global_ctx = NULL;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "01_12_0_Param_Algo_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Oxy_Button", ID_BUTTON_0, 0, 14, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "PPO2_Button", ID_BUTTON_1, 0, 52, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Safety_Button", ID_BUTTON_2, 0, 90, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus_Button", ID_BUTTON_3, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button", ID_BUTTON_4, 85, 88, 75, 40, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialization of 'Param_Plongee_01_Window'
            //
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));

            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_4),
                           GUI_transparent_button_cb);

            Oxy_Button_Clicked = true;
            PPO2_Button_Clicked = false;
            Safety_Button_Clicked = false;
            Up_Button_Clicked = false;
            Down_Button_Clicked = false;

            _global_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_CONFIG_SHARE_CTX:
        {
            _global_ctx = &(((feat_dive_conf_priv_ctx_t *)pMsg->Data.p)->global_ctx);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbDiveMenuBackground);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;

            if ( ! _global_ctx )
            {
                debug_printf("context not received yet\n");
                break;
            }

            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'Oxy_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Oxy_Button_Clicked = true;
                        PPO2_Button_Clicked = false;
                        Safety_Button_Clicked = false;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'PPO2_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Oxy_Button_Clicked = false;
                        PPO2_Button_Clicked = true;
                        Safety_Button_Clicked = false;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_2: // Notifications sent by 'Safety_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Oxy_Button_Clicked = false;
                        PPO2_Button_Clicked = false;
                        Safety_Button_Clicked = true;
                        WM_Invalidate(pMsg->hWin);

                    }
                    break;
                }
                case ID_BUTTON_3: // Notifications sent by 'Plus_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        Up_Button_Clicked = true;
                    }
                    else if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Up_Button_Clicked = false;
                        if ( Oxy_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.o2_ratio < O2_RATIO_MAX-0.009 )
                            {
                                _global_ctx->deco_params.o2_ratio += 0.010;
                            }
                        }
                        if ( PPO2_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.ppo2 < PPO2_MAX-0.09 )
                            {
                                _global_ctx->deco_params.ppo2         += 0.10;
                            }
                        }
                        if ( Safety_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.safety_factor < SAFETY_FACTOR_MAX-0.19 )
                            {
                                _global_ctx->deco_params.safety_factor += 0.20;
                            }
                        }
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_4: // Notifications sent by 'Moins_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        Down_Button_Clicked = true;
                    }
                    else if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Down_Button_Clicked = false;
                        if ( Oxy_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.o2_ratio > O2_RATIO_MIN+0.009 )
                            {
                                _global_ctx->deco_params.o2_ratio -= 0.010;
                            }
                        }
                        if ( PPO2_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.ppo2 > PPO2_MIN+0.09 )
                            {
                                _global_ctx->deco_params.ppo2         -= 0.10;
                            }
                        }
                        if ( Safety_Button_Clicked )
                        {
                            if ( _global_ctx->deco_params.safety_factor > SAFETY_FACTOR_MIN+0.19 )
                            {
                                _global_ctx->deco_params.safety_factor -= 0.20;
                            }
                        }
                        WM_Invalidate(pMsg->hWin);
                    }

                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}


//----------------------------------------------------------------------
WM_HWIN Create_01_12_0_Param_Algo_Window(void)
{
    return GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
}

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbDiveMenuBackground(WM_MESSAGE * pMsg)
{
    char Pstr[15];
    int HI = 0;
    int LO = 0;

    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            if ( ! _global_ctx )
            {
                debug_printf("context not received yet\n");
                break;
            }

            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw arrows */
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 19);

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            /* draw dots */
            GUI_Draw_MenuBar_Dots(3,2);

            /* draw buttons */
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            if(Oxy_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 15, 80, 52);
            GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_OXYGEN),4,18);

            if(PPO2_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 53, 80, 90);
            GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_PP02),4,59);

            if(Safety_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 91, 80, 128);
            GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_SAFETY),4,95);

            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
            if((Oxy_Button_Clicked)||(PPO2_Button_Clicked))
            {
                if(Oxy_Button_Clicked)
                {
                    sprintf(Pstr, "%d%%",(int)((_global_ctx->deco_params.o2_ratio+0.005)*100.0));
                }
                else if(PPO2_Button_Clicked)
                {
                    HI = ((_global_ctx->deco_params.ppo2+0.05) * 10)/10;
                    LO = ((_global_ctx->deco_params.ppo2+0.05) * 10) - (HI*10);
                    sprintf(Pstr, "%d.%d",HI,LO);
                }
                GUI_DispStringAt(Pstr,152,56);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
                GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_MAX_DEPTH),152,78);
                _global_ctx->deco_params.depth_max =
                        mbarToDepth((_global_ctx->deco_params.ppo2/_global_ctx->deco_params.o2_ratio)*1000.0,
                                    gStartDiveLevelPressure);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                HI = ((_global_ctx->deco_params.depth_max) * 10)/10;
                LO = ((_global_ctx->deco_params.depth_max) * 10) - (HI*10);
                if ( _global_ctx->metric_system )
                {
                    sprintf(Pstr, "%d.%1d%s",
                            HI,LO,
                            languages_get_string(TXT_M_UNIT));
                }
                else
                {
                    sprintf(Pstr, "%d.%1d %s",
                            (int)al_tools_to_feet(HI),
                            (int)al_tools_to_feet(LO),
                            languages_get_string(TXT_F_UNIT));
                }
                GUI_DispStringAt(Pstr,152,97);
            }
            else if(Safety_Button_Clicked)
            {
                if (_global_ctx->deco_params.safety_factor < 0.79)
                {
                    GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_CONSERVATIVE),152,71);
                }
                else if(_global_ctx->deco_params.safety_factor > 0.81)
                {
                    GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_PROGRESSIVE),152,71);
                }
                else
                {
                    GUI_DispStringAt(languages_get_string(TXT_DECO_PARAM_STANDARD),152,71);
                }
            }
            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}
