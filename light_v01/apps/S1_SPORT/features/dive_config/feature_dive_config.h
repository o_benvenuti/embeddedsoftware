#ifndef _FEATURE_DIVE_CONFIG_H_
#define _FEATURE_DIVE_CONFIG_H_
/**
 * @file:  feature_dive_config.h
 *
 * @brief
 *
 * @date   26/03/2019
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define FEATURE_DIVE_CONFIG_SHARE_CTX  WM_USER + 0x06

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
enum feature_dive_config_state {
    dive_config_state_deco,
    dive_config_state_deco_sub,
};

typedef struct feat_dive_conf_priv_ctx {
    WM_HWIN current_window;
    serenity_context_t global_ctx;
    serenity_state_t parent_state;
    int current_state;
}feat_dive_conf_priv_ctx_t;


//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN Create_01_12_0_Param_Algo_Window(void);
WM_HWIN Create_01_12_1_Param_Algo_2_Window(void);

#endif  // _FEATURE_DIVE_CONFIG_H_

