//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
// standard
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

// private
#include <algapi.h>
#include <al_database.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <serenityConfig.h>
#include <stm32l4xx_hal.h>
#include <trace.h>

#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static int dive_size_in_pixel = 0;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int compare_stops_depth(const void *a, const void *b);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
void graph_data_management(feat_dive_priv_ctx_t * ctx)
{
    int i;
    uint16_t expected_count, dive_duration;

    // get planned dive time regarding dive mode
    switch ( ctx->dive_mode )
    {
        case Mode_DecoAlgo:
        {
            dive_duration = ctx->deco_params.dive_duration;
            break;
        }
        default:
        {
            debug_printf("Unknown dive mode\n");
            return;
        }
    }

    // compute expected point number
    ctx->current_nb_point++;
    expected_count = dive_duration * 60 * 1000 / SAMPLE_PERIOD;

    // next pixel available ?
    if ( (ctx->current_nb_point == 1)   ||
         (ctx->current_nb_point*144/expected_count > dive_size_in_pixel) )
    {

        dive_size_in_pixel = ctx->current_nb_point*144/expected_count;

        // if depth is lower than expected max - we need to rescale all data
        if ( ctx->graph_max_depth < ctx->current_depth )
        {
            /* rescale all data from existing graph */
            for ( i = 0 ;
                  i < (ctx->graph_data_count<144?ctx->graph_data_count:144);
                  i++)
            {
                ctx->graph_data[i] = (float)ctx->graph_data[i] * ctx->graph_max_depth;
                ctx->graph_data[i] = ctx->graph_data[i] / ctx->current_depth;
            }

            ctx->graph_max_depth = ctx->current_depth;
        }

        // if array is full, shift all data to the left
        if ( ctx->graph_data_count > (FEATURE_DIVE_GRAPH_SIZE-1) )
        {
            for(i = 0 ; i < FEATURE_DIVE_GRAPH_SIZE-1; i++)
            {
                ctx->graph_data[i] = ctx->graph_data[i+1];
            }

            ctx->graph_data_count = (FEATURE_DIVE_GRAPH_SIZE-1);
        }

        /* add new data point to tab */
        /* get correct Y position of data point depending of DepthMax */
        int depthPixels = (ctx->current_depth*43.0) / ctx->current_max_depth;
        /* add the new value to graph */
        ctx->graph_data[ctx->graph_data_count] = depthPixels;
        /* increment index to next pixel do draw */
        ctx->graph_data_count++;
    }
}

//----------------------------------------------------------------------
int start_new_dive(feat_dive_priv_ctx_t * priv, serenity_context_t * glob)
{
    int rc;
    al_database_diverec_config_t config;

    // sanity check
    if ( ! priv || ! glob )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // initialise local context
    priv->current_depth = 0;
    priv->curr_dive_duration = 0;
    priv->rising_speed = 0;
    priv->current_stop = -1;
    priv->too_fast = false;
    priv->ndl_passed = false;
    priv->current_nb_point = 0;
    priv->graph_data_count = 0;
    priv->min_temperature = 259;
    priv->dive_start = RTC_GetTime();
    priv->dive_mode = glob->dive_mode;

    // local variable
    dive_size_in_pixel = 0;

    memset(priv->speedTab, 0, ASC_SPEED_AVG_TAB_SIZE*sizeof(float));

    // copy context from user config
    memcpy(&(priv->deco_params),
           &(glob->deco_params),
           sizeof(serenity_deco_param_t));

    // no safety stop till 10 meter deep
    priv->deco_params.safety_stop = false;
    priv->deco_params.dive_stop.allowed = false;
    priv->deco_params.dive_stop.occuring = false;
    priv->stop_paused = false;
    priv->current_max_depth = priv->deco_params.depth_max;
    priv->graph_max_depth = priv->deco_params.depth_max;

    // set algo configuration
    priv->deco_info.oxygen_ratio  = priv->deco_params.o2_ratio;
    priv->deco_info.helium_ratio  = priv->deco_params.he_ratio;
    priv->deco_info.safety_factor = priv->deco_params.safety_factor;

    DECO_init_algo(&(priv->deco_info), gStartDiveLevelPressure);

    // init new dive record session
    rc = al_database_diverec_start();
    if ( rc )
    {
        debug_printf("unable to start dive recording\n");
    }

    // update dive record header
    config.data.sample = SAMPLE_FREQUENCY;
    config.type = AL_DATABASE_DIVEREC_TYPE_SAMPLE;
    rc = al_database_diverec_set(&config);
    if ( rc )
    {
        debug_printf("unable to update dive record parameters\n");
    }

    // Write header being careful to not write the data that
    // needs to be written after the dive
    config.data.params.dive_mode = priv->dive_mode;

    // no stop number for DecoAlgo
    config.data.params.stop_number = 0;

    config.type = AL_DATABASE_DIVEREC_TYPE_PARAMS;
    rc = al_database_diverec_set(&config);
    if ( rc )
    {
        debug_printf("unable to update dive record parameters\n");
    }

    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn); /* disable touchscreen */
    HAL_NVIC_EnableIRQ (int1_3D_EXTI_IRQn);   /* enable flick */

    return 0;
}

//----------------------------------------------------------------------
int manage_stops(feat_dive_priv_ctx_t * ctx)
{
    int i, stop_found;
    TimeType Time = RTC_GetTime();
    static TimeType PauseTime = {0};
    static TimeType StartTimeSave = {0};
    struct tm t = { 0 };
    float upper_limit = 1.0;
    int stop_number = 0;
    serenity_dive_stop_t * dive_stop = NULL;

    // make sure the stops calculated by the
    // deco algorithm are not over 2 meters
    if ( ctx->deco_params.dive_stop.StopDepth < 1.5 )
    {
        ctx->deco_params.dive_stop.StopDepth = 1.5;
    }

    if ( ctx->deco_params.safety_stop || ctx->ndl_passed )
    {
        stop_number = 1;
        dive_stop = &(ctx->deco_params.dive_stop);
        ctx->stop_paused = false;
    }
    else
    {
        // paranoia
        stop_number = 0;
    }

    // check if stop is allowed, if a stop happens
    // and if a stop is finished
    for(i = 0, stop_found = -1; i < stop_number; i++)
    {
        // check if a stop can be allowed
        // if we are 1 meter below stop depth or a deco algo stop is mandatory */
        if( ((ctx->current_depth > dive_stop[i].StopDepth + 1.0) ||
           (*(ctx->deco_info.deco_status) == DecoStop)) && (*(ctx->deco_info.ceiling_stop) > 30))
        {
            dive_stop[i].allowed = true;
        }

        /* check if an undisplayed & allowed stop need to be displayed */
        if( (dive_stop[i].allowed == true)                      &&
            (ctx->current_depth <= dive_stop[i].StopDepth + 0.5)   &&
            (dive_stop[i].occuring == false)                    )
        {
            // Avoid to handle many stops at the same
            // time: start with the deepest one first
            if ( stop_found < 0 )
            {
                stop_found = i;
            }
            else if ( dive_stop[stop_found].StopDepth < dive_stop[i].StopDepth )
            {
                stop_found = i;
            }
        }

        /* check if an occurring stop has reached its end */
        if(dive_stop[i].occuring == true)
        {
            int stopCurrentDuration = (int)get_TimeDiffInSeconds(dive_stop[i].startTime,Time);

            if((stopCurrentDuration > (dive_stop[i].StopDuration))  &&
              (ctx->deco_params.safety_stop))
            {
                dive_stop[i].occuring = false;
                dive_stop[i].allowed = false;
            }
        }

        // Update start time while we are not in the safety stop
        if( ((ctx->dive_mode == Mode_DecoAlgo)&&!(ctx->deco_params.safety_stop)) &&
        	(dive_stop[i].occuring == true))
        {
            dive_stop[i].startTime = Time;
        }

        // check if occurring stop needs to be paused
        // only for depth meter mode & Deco mode when NDL is not reached
        if( dive_stop[i].occuring == true )
        {
            if (ctx->deco_params.safety_stop)
            {
                upper_limit = 3.0;

                /* if we are out of bonds, we need to 'pause' the timer */
                if((ctx->current_depth < dive_stop[i].StopDepth - upper_limit)  ||
                   (ctx->current_depth > dive_stop[i].StopDepth + 1.0))
                {
                    /* if pause not started */
                    if(!ctx->stop_paused)
                    {
                        ctx->stop_paused = true;
                        PauseTime = RTC_GetTime();
                        StartTimeSave = dive_stop[i].startTime;
                    }
                    else /* if pause already started */
                    {
                        int secsToAdd = (int)get_TimeDiffInSeconds(PauseTime,Time);
                        /* read start time as temporary tm structure */
                        t = TimeType2tm(StartTimeSave);
                        /* edit seconds */
                        t.tm_sec += secsToAdd; /* time to add */
                        /* Normalise time if secs outside of bounds */
                        mktime(&t);
                        /* force the new start time */
                        dive_stop[i].startTime = tm2TimeType(t);
                    }
                }
                else if(ctx->stop_paused) /* if we are in bounds and paused */
                {
                    ctx->stop_paused = false;
                }
            }
            // if its a deco calculated stop
            else
            {
                /* if we are out of bonds, we need to hide the stop screen */
                if((ctx->current_depth < dive_stop[i].StopDepth - 1.0)  ||
                   (ctx->current_depth > dive_stop[i].StopDepth + 1.0))
                {
                	ctx->stop_paused = true;
                }
                else if(ctx->stop_paused) /* if we are in bounds and paused */
                {
                	ctx->stop_paused = false;
                }
            }
        }
    }

    // if a new stop has been reached, discard others
    for (i = 0; (stop_found > -1) && (i < stop_number); i++)
    {
        if(dive_stop[i].occuring == true && i != stop_found)
        {
            dive_stop[i].occuring = false;
            dive_stop[i].allowed = false;
       }
    }

    // activate the stop
    if ( stop_found > -1)
    {
        dive_stop[stop_found].occuring = true;
        dive_stop[stop_found].startTime = Time;
        if(ctx->stop_paused == true)
        {
        	return -1;
        }
        else
        {
        	return stop_found;
        }
    }

	// if a previously started stop still occurs
    for ( i = 0; (i < stop_number) && (i < NB_PALIERS_MAX); ++i)
    {
        if ( dive_stop[i].occuring == true )
        {
            if(ctx->stop_paused == true)
            {
            	return -1;
            }
            else
            {
            	return i;
            }
        }
    }



    return -1;
}

//----------------------------------------------------------------------
bool manage_speed(float depth, feat_dive_priv_ctx_t * ctx)
{
    (void)depth;

    //-------------------------------------------
    // DECO ALGO
    //-------------------------------------------
    return (ctx->rising_speed > ctx->deco_params.max_speed);
}

//----------------------------------------------------------------------
bool end_of_dive(feat_dive_priv_ctx_t * ctx)
{
    static int surfaceTimer = 0;

    // paranoia
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return true;
    }

    /* stop current dive if depth inferior than 80 cm for more than 1 minutes */
    if( (ctx->current_depth < 0.80) && (ctx->curr_dive_duration > 60) )
    {
        surfaceTimer++;
    } else {
        surfaceTimer = 0;
    }

    // is dive ended
    if( (surfaceTimer > DECO_BOTTOM_END_TIMEOUT) || (ctx->curr_dive_duration > 18000) )
    {
        surfaceTimer = 0;
        gStartDiveLevelPressure = SEALEVELPRESSURE;


        if ( ctx->dive_mode == Mode_DecoAlgo )
        {
            DECO_saveTissuePressureInFlash();
        }

        // stop recording current dive
        al_database_diverec_end(ctx->min_temperature);

        return true;
    }

    return false;
}

//----------------------------------------------------------------------
void refresh_screen(feat_dive_priv_ctx_t * ctx)
{
    // Most important too-fast first
    if ( ctx->too_fast == true )
    {
        if ( ctx->current_window != ctx->toofast_window )
        {
            GUI_Switch_Screens(ctx->current_window, ctx->toofast_window);
            ctx->current_window = ctx->toofast_window;
        }

        return;
    }
    else if ( ctx->current_stop > -1)
    {
        if ( (ctx->current_window != ctx->stop_window) &&
                (ctx->base_window == ctx->dive_window)     )
        {
            GUI_Switch_Screens(ctx->current_window, ctx->stop_window);
            ctx->current_window = ctx->stop_window;
        }

        return;
    }

    // for all mode: return from special case window to dive window if needed
    if ( ctx->current_window != ctx->base_window )
    {
        GUI_Switch_Screens(ctx->current_window, ctx->base_window);
        ctx->current_window = ctx->base_window;
    }
}

//----------------------------------------------------------------------
int compare_stops_depth(const void *a, const void *b)
{
  const serenity_dive_stop_t *da = (const serenity_dive_stop_t *) a;
  const serenity_dive_stop_t *db = (const serenity_dive_stop_t *) b;

  return (da->StopDepth < db->StopDepth) - (da->StopDepth > db->StopDepth);
}

#ifdef DEBUG_DIVE
//----------------------------------------------------------------------
float _fake_depth2(feat_dive_priv_ctx_t * dive_ctx)
{
    static float medor;
    static bool down = true;
    static bool stopUp = true;
    static bool sw = false;

    // bottom depth
    if ( medor >  35 )
    {
        down = false;
    }

    // "zero" level
    if ( medor <  0.1 )
    {
        if ( dive_ctx->curr_dive_duration < 10 )
        {
            down = true;
        }
        else
        {
            return medor;
        }
    }
    if ( down )
    {
      medor +=  0.82;
    }
    else /* up */
    {
        if ( dive_ctx->curr_dive_duration < (13*60) )
        {
            return medor;
        }
        for(int i = 0 ; i < dive_ctx->depth_params.stop_nb; i++)
        {
            if ( dive_ctx->depth_params.dive_stops[i].occuring == true )
            {
                return medor; /* unchanged */
            }
        }
        medor -=  0.20;
    }
    return medor;
}

//----------------------------------------------------------------------
float _fake_depth(void)
{
////  float current_depth = medor ;
  static float medor = 0;
//  float TimeToClimb = Diving_duration;
//  float YouAreLate = Diving_duration;
//  int next_stop = Stop_nb;
//  float time_diff = 0;
//  int nxt_asc_spd,nxt_depth,depth_diff, i;
//  int occuring_stop = -1;
//  static bool stopUp = true;
//  int StopTabSortedIndex[NB_PALIERS_MAX] = {0};
//  static bool down = true;
//
//  /** get the stops ordered by depth **/
//  /* initialize the sorting array */
//  for(i = 0 ; i < NB_PALIERS_MAX ; i++)
//  {
//      StopTabSortedIndex[i] = i;
//  }
//  for (int ii = 0; ii < Stop_nb; ii++)                      /* for each array element */
//  {
//      for (int jj = 0; jj < Stop_nb; jj++)                  /* compare with other values */
//      {
//          if (gStopTab[StopTabSortedIndex[jj]].StopDepth <
//                  gStopTab[StopTabSortedIndex[ii]].StopDepth) /* Comparing other array elements */
//          {
//              int tmp = StopTabSortedIndex[ii];             /* Using temporary variable for storing last index */
//              StopTabSortedIndex[ii] = StopTabSortedIndex[jj];/* replacing index */
//              StopTabSortedIndex[jj] = tmp;                 /* storing last index */
//          }
//      }
//  }
//  /* now the index to draw is not "i" but "StopTabSortedIndex[i]" */
//
//  for(i = Stop_nb ; i > 0 ; i--)
//  {
//      /* if we are below */
//      if(medor > gStopTab[StopTabSortedIndex[i-1]].StopDepth) next_stop = i-1;
//  }
//  for(i = Stop_nb ; i > next_stop ; i--)
//  {
//      if(i == Stop_nb)
//      {
//          nxt_asc_spd = gStopTab[StopTabSortedIndex[i-1]].Ascent_speed;
//          nxt_depth = 0;
//      }
//      else
//      {
//          nxt_asc_spd = gStopTab[StopTabSortedIndex[i]].Ascent_speed;
//          nxt_depth = gStopTab[StopTabSortedIndex[i]].StopDepth;
//      }
//      depth_diff = nxt_depth - gStopTab[StopTabSortedIndex[i-1]].StopDepth; /* (m) = (m) - (m) */
//      /* time of next swim up */
//      time_diff = time_diff + ((float)-depth_diff / (float)nxt_asc_spd);    /* (min) = (m) / (m/min) */
//      /* time of next stop */
//      time_diff = time_diff + ((float)gStopTab[StopTabSortedIndex[i-1]].StopDuration/60.0);
//  }
//  YouAreLate = Diving_duration - time_diff;
//  /* from here, time_diff = ascent time after stops + stop times */
//  depth_diff = gStopTab[StopTabSortedIndex[next_stop]].StopDepth - medor; /* (m) = (m) - (m) */
//  /* here, time_diff = the moment when the diver should start rising according to the plan */
//  time_diff = time_diff + ((float)-depth_diff / (float)gStopTab[StopTabSortedIndex[next_stop]].Ascent_speed);    /* (min) = (m) / (m/min) */
//  TimeToClimb = Diving_duration - time_diff;
//
//  if (down)
//  {
//      medor +=  0.9;
//  }
//  else /* up */
//  {
//      /* flat at bottom */
//      if((gCurrentDivingDurationSec < (TimeToClimb*60.0)) && ((occuring_stop == -1)))
//      {
//          medor +=  ((rand() % 20)-10.0)/20.0; /* between -0.1 & +0.1 */
//          if(medor<(Depth_max-4.0)) medor+=  ((rand() % 10))/10.0; /* between 0 & +0.1 */
//          if(medor>(Depth_max+4.0)) medor-=  ((rand() % 10))/10.0; /* between 0 & +0.1 */
//      }
//      else /* time to climb */
//      {
//          for(i=0; i<Stop_nb ; i++)
//          {
//              if(gStopTab[StopTabSortedIndex[i]].occuring)
//              {
//                  occuring_stop = i;
//              }
//          }
//          if(occuring_stop == -1) /* pas dans un palier */
//          {
//              if(medor > gStopTab[StopTabSortedIndex[next_stop]].StopDepth + 8) /* if 8 meter below next stop */
//              {
//                  medor -=  ((rand() % 5))/50.0; /* between 0 & +0.1 */
//                  medor -= 0.34; /* 20m/min -> too fast */
//                  //medor -= 0.20;
//              }
//              else
//              {
//                  medor +=  ((rand() % 5))/20.0; /* between 0 & +0.25 */
//                  medor -= ((float)gStopTab[StopTabSortedIndex[next_stop]].Ascent_speed-1.0) / 60.0; /* slower than max asc speed */
//                  //medor -= 0.20;
//              }
//          }
//          else /* dans un palier */
//          {
//              if(stopUp) medor -= 0.20;
//              else       medor += 0.20;
//
//              if (medor > gStopTab[StopTabSortedIndex[occuring_stop]].StopDepth+2.0) /* below limit */
//              {
//                  stopUp = true;
//              }
//              if (medor < gStopTab[StopTabSortedIndex[occuring_stop]].StopDepth-2.0) /* more than limit */
//              {
//                  stopUp = false;
//              }
//          }
//
//      }
//  }
//
//  if (medor >  Depth_max)
//  {
//      down = false; /* bottom depth */
//  }
//  if (medor <  0)
//  {
//      down = true; /* "zero" level */
//  }

  return medor;
}

#endif  // DEBUG_DIVE
