//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <al_tools.h>
#include <algapi.h>
#include <DIALOG.h>
#include <common.h>
#include <GUI_serenity.h>
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_PROGBAR_0   (GUI_ID_USER + 0x01)
#define ID_PROGBAR_1   (GUI_ID_USER + 0x02)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "too_fast_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldReduced16pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedRegularReduced18pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiCondNumbers36pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flcheASC_milieubas;

//----------------------------------------------------------------------
WM_HWIN CreateTOOFAST_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    char tmp_str[16];
    int timeLeft, xPos, yPos;
    int timeLeftPixels;
    int RisingSpeedPixels;
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* Top section */
            if(TicTac)
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
                GUI_SetColor(GUI_WHITE);
            }
            else
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B));/* RED */
            }
            GUI_ClearRect(8,0,151,31);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = LCD_GetXSize() / 2;
            yPos = (LCD_GetYSize() / 6) - 5;
            GUI_DispStringAt(languages_get_string(TXT_TOO_FAST), xPos ,yPos);

            /* Middle section */
            xPos =  11;
            yPos = (LCD_GetYSize() / 2);
            yPos = yPos - bmcran_Serenity_13a_flcheASC_milieubas.YSize/2;
            GUI_DrawBitmap(&bmcran_Serenity_13a_flcheASC_milieubas, xPos, yPos);
            xPos =  xPos + bmcran_Serenity_13a_flcheASC_milieubas.YSize + 20;
            yPos = LCD_GetYSize() / 2;
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiCondNumbers36pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_GotoXY(xPos ,yPos);
            snprintf(&tmp_str[0], sizeof(tmp_str), "%d",
                (dive_ctx->metric_system ?
                            (int)(dive_ctx->rising_speed) :
                            (int)al_tools_to_feet(dive_ctx->rising_speed)
                )
                    );
            GUI_DispString(tmp_str);
            GUI_SetFont(&GUI_FontSairaSemiCondensedRegularReduced18pts);
            xPos =  GUI_GetDispPosX() + 2;
            yPos = LCD_GetYSize() / 2 - 8;
            GUI_SetTextAlign(GUI_TA_VCENTER);
            GUI_GotoXY(xPos ,yPos);
            GUI_DispString((dive_ctx->metric_system ?
                            languages_get_string(TXT_M_UNIT) :
                            languages_get_string(TXT_F_UNIT) ));
            yPos = LCD_GetYSize() / 2 + 8;
            GUI_SetTextAlign(GUI_TA_VCENTER);
            GUI_GotoXY(xPos ,yPos);
            GUI_DispString(languages_get_string(TXT_MINUTES_UNIT));


            /* Bottom section */
            if(!TicTac)
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
                GUI_SetColor(GUI_WHITE);
            }
            else
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B));/* RED */
            }
            GUI_ClearRect(8,97,151,128);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = LCD_GetXSize() / 2;
            yPos = LCD_GetYSize() - ((LCD_GetYSize() / 6) - 5);
            GUI_DispStringAt(languages_get_string(TXT_SLOW_DOWN), xPos ,yPos);

            /****** left progbar - Time Left + NDL ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00002134));
            GUI_ClearRect(0, 0, 7, LCD_GetYSize());
            /* Draw bar foreground */
            timeLeft = (dive_ctx->deco_params.dive_duration*60);
            timeLeft = timeLeft - dive_ctx->curr_dive_duration;
            timeLeftPixels = (((timeLeft)>0?timeLeft:0)*LCD_GetYSize());
            timeLeftPixels /= (dive_ctx->deco_params.dive_duration * 60);

            GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_ClearRect(0, LCD_GetYSize()-timeLeftPixels, 7, LCD_GetYSize());

            /****** right progbar - speed ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00353537));
            GUI_ClearRect(152, 0, 160, LCD_GetYSize());
            /* Draw bar foreground (scale goes from 9 to 18*/
            RisingSpeedPixels = ((dive_ctx->rising_speed-9.0)*LCD_GetYSize())/9.0;
            GUI_SetBkColor(GUI_WHITE);
            GUI_ClearRect(152, LCD_GetYSize()-RisingSpeedPixels, 160, LCD_GetYSize());

            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}
