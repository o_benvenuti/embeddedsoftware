//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------

#include <al_tools.h>
#include <stdint.h>
#include <DIALOG.h>
#include <algapi.h>
#include <common.h>
#include <GUI_serenity.h>
#include <ms5837_driver.h>
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)

///----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Dive_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_iconprofmax;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced24pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced18pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmTimeLeftDive;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            /* Initialization of 'Dive_Window' */
            WM_SetCallback(pMsg->hWin, _cbRepaint);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateDive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Tstr[20] = {0};
    int sec, hr, min;
    int xPos = 0;
    int yPos = 0;
    int timeLeft, timeLeftPixels;
    int RisingSpeedPixels;
    float stopDepth = 0.0;
    int deco_stop_duration = 0;

    int xSize = 0;
    int ySize = 0;
    int nxt_asc_spd = 0;
    int nxt_depth = 0;
    int depth_diff = 0;
    int stop_size_px = 0;
    int pos_in_graph = 0;
    float time_diff = 0;
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();


            //*****************************************************************
            //********************** UPPER SECTION ****************************
            //*****************************************************************
            //**************** LEFT SECTION *******************
            // print current depth numerical value
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = X_1_3;
            yPos = Y_1_6;
            GUI_GotoXY(xPos ,yPos-1);
            if ( dive_ctx->metric_system )
            {
                GUI_DispFloatFix(dive_ctx->current_depth,
                                 dive_ctx->current_depth < 100 ?
                                 dive_ctx->current_depth < 10 ?3:4:5,1);
            }
            else
            {
                GUI_DispFloatFix(al_tools_to_feet(dive_ctx->current_depth),
                        al_tools_to_feet(dive_ctx->current_depth) < 100 ?
                        al_tools_to_feet(dive_ctx->current_depth) < 10 ?
                        3 : 4 : 5 ,1);
            }
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_GotoY(Y_1_3 - 13);
            GUI_DispString(" ");
            GUI_DispString((dive_ctx->metric_system ?
                    languages_get_string(TXT_M_UNIT) :
                    languages_get_string(TXT_F_UNIT) ));

            //**************** RIGHT SECTION *******************
            // if too deep
            if ( dive_ctx->current_depth >= dive_ctx->current_max_depth )
            {
                xPos = X_2_3;
                yPos = Y_1_6 - (bmcran_Serenity_13B_iconprofmax.YSize/2);
                GUI_DrawBitmap(&bmcran_Serenity_13B_iconprofmax, xPos ,yPos);
            }
            // else show stop depth
            else if(((*(dive_ctx->deco_info.deco_status) == Buff)     ||
                    (*(dive_ctx->deco_info.deco_status) == BuffStop) ||
                    (*(dive_ctx->deco_info.deco_status) == DecoCal)  ||
                    (*(dive_ctx->deco_info.deco_status) == DecoMgr)  ||
                    (*(dive_ctx->deco_info.deco_status) == DecoStop) ||
					((  dive_ctx->deco_params.safety_stop == true)))  &&
					   (dive_ctx->deco_params.dive_stop.allowed  ))
            {
                stopDepth = dive_ctx->deco_params.dive_stop.StopDepth;
                /* display text */
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetColor(GUI_WHITE);
                xPos = X_5_6;
                yPos = 9;
                GUI_DispStringAt(languages_get_string(TXT_DIVE_STOP), xPos +2,yPos);
                yPos = Y_1_6 + 8;
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
                GUI_GotoXY(xPos ,yPos);
                if ( dive_ctx->metric_system )
                {
                    GUI_DispFloatFix(stopDepth, ((stopDepth > 9.0) ? 4 : 3), 1);
                }
                else
                {
                    GUI_DispFloatFix(al_tools_to_feet(stopDepth),
                            ((al_tools_to_feet(stopDepth) > 9.0) ? 4 : 3), 1);
                }
            }

            //*****************************************************************
            //********************** MIDDLE SECTION ***************************
            //*****************************************************************
            /* draw graph background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_ClearRect(8, 41, LCD_GetXSize()-9, LCD_GetYSize() - LCD_GetYSize()/3);
            GUI_SetColor(GUI_BLACK);
            GUI_DrawVLine(56, 41, LCD_GetYSize() - LCD_GetYSize()/3);
            GUI_DrawVLine(104, 41, LCD_GetYSize() - LCD_GetYSize()/3);

            /****** draw stops  ******/
            GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
            if(dive_ctx->dive_mode == Mode_DecoAlgo)
            {
                if ( (dive_ctx->ndl_passed)                     ||
                     (dive_ctx->deco_params.safety_stop == true) )
                {
                    yPos = (int)(dive_ctx->deco_params.dive_stop.StopDepth * 43.0);
                    yPos = (int)(yPos / (dive_ctx->current_max_depth > 0 ? dive_ctx->current_max_depth : 1));
                    yPos = yPos + 41;

                    GUI_DrawHLine(yPos, 0, LCD_GetXSize());
                }
            }
            else
            {
                xPos = 160-8; /* screen size - bargraph width */

                /* gDiveProfileGraphIndex will be 144 max according to the management in freertos.c */
                uint nb_max_points = dive_ctx->depth_params.dive_duration*60*1000/SAMPLE_PERIOD;
                pos_in_graph = dive_ctx->current_nb_point*144/nb_max_points;

                /* get position to draw stops even if graph started to shift left */
                if(pos_in_graph > 144)
                {
                    xPos -= (pos_in_graph-144);
                }

                // NOTE dive has been sorted by the feature
                // dive init. function. Then draw rects
                for(int i = dive_ctx->depth_params.stop_nb; i > 0 ; --i)
                {
                    if ( i == dive_ctx->depth_params.stop_nb ) /* last stop */
                    {
                        nxt_asc_spd = dive_ctx->depth_params.dive_stops[i-1].Ascent_speed;
                        nxt_depth = 0;
                    }
                    else /* other stops */
                    {
                        nxt_asc_spd = dive_ctx->depth_params.dive_stops[i].Ascent_speed;
                        nxt_depth = (int)dive_ctx->depth_params.dive_stops[i].StopDepth;
                    }

                    depth_diff = nxt_depth - (int)dive_ctx->depth_params.dive_stops[i-1].StopDepth; /* (m) = (m) - (m) */
                    time_diff = (float)-depth_diff / (float)nxt_asc_spd;    /* (min) = (m) / (m/min) */
                    stop_size_px = (int)((float)((float)(dive_ctx->depth_params.dive_stops[i-1].StopDuration) / (float)(dive_ctx->depth_params.dive_duration*60)) * 144.0);

                    /* xPos = previous position - Size in pixel of time_diff - size of stop */
                    xPos = xPos - ((int)((float)((float)(time_diff*60) / (float)(dive_ctx->depth_params.dive_duration*60)) * 144.0)) - stop_size_px;
                    /* yPos = Vertical depth in px  */
                    yPos = (int)((dive_ctx->depth_params.dive_stops[i-1].StopDepth*43.0)/(dive_ctx->depth_params.depth_max>0?dive_ctx->depth_params.depth_max:1)) + 39;
                    xSize = xPos + stop_size_px;
                    ySize = yPos + 4;
                    GUI_DrawRect(xPos,yPos,xSize,ySize); /* draw rectangle */
                    GUI_DrawRect(xPos-1,yPos-1,xSize+1,ySize+1); /* draw slightly bigger rectangle */
                }
            }

            // draw graph
            GUI_SetColor(GUI_WHITE);
            GUI_DrawGraph(dive_ctx->graph_data, dive_ctx->graph_data_count, 8, 41);


            //*****************************************************************
            //********************** LOWER SECTION ****************************
            //*****************************************************************

            // display low Section Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767));
            GUI_ClearRect(0,LCD_GetYSize()-LCD_GetYSize()/3 ,LCD_GetXSize(),LCD_GetYSize());
            /* print DiveTime */
            GUI_SetColor(GUI_WHITE);
            min = dive_ctx->curr_dive_duration/60;
            sec = dive_ctx->curr_dive_duration%60;
            hr = min/60;
            min = min%60;

            //**************** LEFT SECTION *******************
        	/* display title text */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            xPos = 7+2;
            yPos = LCD_GetYSize() - (LCD_GetYSize()/6) - 12;
            GUI_DispStringAt(languages_get_string(TXT_DIVE_TIME), xPos +5,yPos);

            /* print DiveTime */
            yPos = LCD_GetYSize() - (LCD_GetYSize()/6) + 8;
            /* if more than 1h -> format H:MM */
            if(dive_ctx->curr_dive_duration>3600) sprintf(Tstr, "%1d:%02d",hr,min);
            /* if less than 1h -> format MM:SS */
            else 								 sprintf(Tstr, "%02d:%02d",min,sec);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced24pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, xPos ,yPos);

            //**************** RIGHT SECTION *******************
            /* disp NDL */
            if((*(dive_ctx->deco_info.deco_status) == Normal)     ||
               (*(dive_ctx->deco_info.deco_status) == NormalStop) )
            {
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                yPos = LCD_GetYSize() - (LCD_GetYSize()/6) - 12;
                xPos = LCD_GetXSize()/2 + 20;
                GUI_DispStringAt("NDL", xPos +10,yPos);
                if ( *(dive_ctx->deco_info.ndl) < 60 )
                {
                    yPos = LCD_GetYSize() - (LCD_GetYSize()/6) + 8;
                    sprintf(Tstr, "%d",(int)*(dive_ctx->deco_info.ndl));
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
                    GUI_DispStringAt(Tstr, xPos -2 ,yPos);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                    GUI_DispString(" ");
                    GUI_DispString(languages_get_string(TXT_SECONDS_UNIT));
                }
                else if ( *(dive_ctx->deco_info.ndl) < 3600 )
                {
                    sec = (int)*(dive_ctx->deco_info.ndl) % 60;
                    min = ((int)*(dive_ctx->deco_info.ndl) - sec) / 60;
                    yPos = LCD_GetYSize() - (LCD_GetYSize()/6) + 8;
                    sprintf(Tstr, "%d",min);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
                    GUI_DispStringAt(Tstr, xPos -2 ,yPos);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                    GUI_DispString(" ");
                    GUI_DispString(languages_get_string(TXT_MINUTES_UNIT));
                }
                else
                {
                    yPos = LCD_GetYSize() - (LCD_GetYSize()/6) + 8;
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
                    GUI_DispStringAt("--", xPos ,yPos);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                    GUI_DispString(" ");
                    GUI_DispString(languages_get_string(TXT_MINUTES_UNIT));
                }
            }
			else if(((*(dive_ctx->deco_info.deco_status) == Buff)     ||
                    (*(dive_ctx->deco_info.deco_status) == BuffStop) ||
                    (*(dive_ctx->deco_info.deco_status) == DecoCal)  ||
                    (*(dive_ctx->deco_info.deco_status) == DecoMgr)  ||
                    (*(dive_ctx->deco_info.deco_status) == DecoStop) ||
					((  dive_ctx->deco_params.safety_stop == true)))  &&
					   (dive_ctx->deco_params.dive_stop.allowed  ))
            {
            	deco_stop_duration = (int)dive_ctx->deco_params.dive_stop.StopDuration ;
            	sec =  deco_stop_duration%60;
            	min = (deco_stop_duration-sec)/60;
                /* display text */
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetColor(GUI_WHITE);
                xPos = X_5_6;
                yPos = Y_5_6 - 12;
                GUI_DispStringAt(languages_get_string(TXT_DIVE_STOP), xPos +2,yPos);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
                xPos = LCD_GetXSize()/2 + 14;
                yPos = Y_5_6 + 10;
                sprintf(Tstr, "%02d:%02d",min,sec);
                GUI_DispStringAt(Tstr, xPos -2 ,yPos);
            }


            /****** left progbar - Time Left + NDL ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00002134));
            GUI_ClearRect(0, 0, 7, LCD_GetYSize());
            /* Draw bar foreground */
            timeLeft = (dive_ctx->deco_params.dive_duration*60);
            timeLeft = timeLeft - dive_ctx->curr_dive_duration;
            timeLeftPixels = (((timeLeft)>0?timeLeft:0)*LCD_GetYSize());
            timeLeftPixels /= (dive_ctx->deco_params.dive_duration * 60);

            GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_ClearRect(0, LCD_GetYSize()-timeLeftPixels, 7, LCD_GetYSize());

            /****** right progbar - speed ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00353537));
            GUI_ClearRect(152, 0, 160, LCD_GetYSize());
            /* Draw bar foreground (scale goes from 9 to 18*/
            RisingSpeedPixels = ((dive_ctx->rising_speed-9.0)*LCD_GetYSize())/9.0;
            GUI_SetBkColor(GUI_WHITE);
            GUI_ClearRect(152, LCD_GetYSize()-RisingSpeedPixels, 160, LCD_GetYSize());

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
        }
    }
}

