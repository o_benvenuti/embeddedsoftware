//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <common.h>
#include <GUI_serenity.h>
#include <al_tools.h>
#include <algapi.h>
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_PROGBAR_0   (GUI_ID_USER + 0x01)
#define ID_PROGBAR_1   (GUI_ID_USER + 0x02)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "STOP_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldReduced16pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced24pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced26pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_milieuhaute;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_milieubas;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_milieuOK;
extern GUI_CONST_STORAGE GUI_BITMAP bmstopwatch;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateSTOP_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    int timeLeftPixels;
    char Tstr[20] = {0};
    int RisingSpeedPixels;
    float upper_limit = 0.5;
    int timeLeft, xPos, yPos, min, sec;
    serenity_dive_stop_t * curr_stop_ptr;
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* Top section */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B));
            GUI_ClearRect(8,0,151,31);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = LCD_GetXSize() / 2;
            yPos = (LCD_GetYSize() / 6) - 5;

            /* get highest stop position */
            GUI_DispStringAt(languages_get_string(TXT_STOP_SAFETY), xPos ,yPos);


            /* Middle section */
            if( (dive_ctx->dive_mode == Mode_DecoAlgo) &&
                (dive_ctx->ndl_passed == false) )
            {
                upper_limit = 2.5;
            }
            else
            {
                upper_limit = 0.5;
            }

            curr_stop_ptr = &(dive_ctx->deco_params.dive_stop);
            GUI_SetBkColor(GUI_BLACK);
            GUI_ClearRect(8,33,151,78);

            if (dive_ctx->current_depth > curr_stop_ptr->StopDepth+0.5) /* below limit */
            {
                xPos = 7 + 3;
                yPos = 37;
                GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_milieuhaute, xPos, yPos);
            }
            else if (dive_ctx->current_depth < curr_stop_ptr->StopDepth-upper_limit) /* more than limit */
            {
                xPos = 7 + 3;
                yPos = 37;
                GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_milieubas, xPos, yPos);
            }
            else if ((dive_ctx->current_depth > curr_stop_ptr->StopDepth-upper_limit) &&
                    (dive_ctx->current_depth < curr_stop_ptr->StopDepth+0.5)) /* inside limit */
            {
                xPos = 7 + 6;
                yPos = 39;
                GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_milieuOK, xPos, yPos);
            }
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced24pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            if ( dive_ctx->metric_system )
            {
                GUI_GotoXY((LCD_GetXSize() / 2), 56);
                snprintf(Tstr, sizeof(Tstr), "%.1f %s",
                         dive_ctx->current_depth,
                         languages_get_string(TXT_M_UNIT));
            }
            else
            {
                GUI_GotoXY((LCD_GetXSize() / 2) + 10, 56);
                snprintf(Tstr, sizeof(Tstr), "%.1f %s",
                        al_tools_to_feet(dive_ctx->current_depth),
                        languages_get_string(TXT_F_UNIT));
            }
            GUI_DispString(Tstr);

            /* Bottom section */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767));
            GUI_ClearRect(8,80,151,128);
            int stopTimeLeft = 0;

            if ( dive_ctx->dive_mode == Mode_DecoAlgo )
            {
                if (! (dive_ctx->deco_params.safety_stop) )
                {
                    stopTimeLeft = dive_ctx->deco_params.dive_stop.StopDuration;
                }
                else
                {
                    stopTimeLeft = dive_ctx->deco_params.dive_stop.StopDuration -
                            (int)get_TimeDiffInSeconds(curr_stop_ptr->startTime,RTC_GetTime());
                }
            }
            else
            {
                int pos = dive_ctx->current_stop;
                curr_stop_ptr = &(dive_ctx->depth_params.dive_stops[pos]);
                stopTimeLeft = curr_stop_ptr->StopDuration - (int)get_TimeDiffInSeconds(curr_stop_ptr->startTime,RTC_GetTime());
            }

            sec = stopTimeLeft % 60;
            min = (stopTimeLeft - sec) / 60;
            if(sec<0) sprintf(Tstr, "%1d:%02d",00,00);
            else sprintf(Tstr, "%1d:%02d",min,sec);
            xPos =  (LCD_GetXSize() / 2);
            yPos = 104;
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced26pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, xPos ,yPos);
            xPos = 7 + 6;
            yPos = 91;
            GUI_DrawBitmap(&bmstopwatch, xPos, yPos);

            /****** left progbar - Time Left + NDL ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00002134));
            GUI_ClearRect(0, 0, 7, LCD_GetYSize());
            /* Draw bar foreground */
            timeLeft = (dive_ctx->deco_params.dive_duration*60);
            timeLeft = timeLeft - dive_ctx->curr_dive_duration;
            timeLeftPixels = (((timeLeft)>0?timeLeft:0)*LCD_GetYSize());
            timeLeftPixels /= (dive_ctx->deco_params.dive_duration * 60);

            GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_ClearRect(0, LCD_GetYSize()-timeLeftPixels, 7, LCD_GetYSize());

            /****** right progbar - speed ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00353537));
            GUI_ClearRect(152, 0, 160, LCD_GetYSize());
            /* Draw bar foreground (scale goes from 9 to 18*/
            RisingSpeedPixels = ((dive_ctx->rising_speed-9.0)*LCD_GetYSize())/9.0;
            GUI_SetBkColor(GUI_WHITE);
            GUI_ClearRect(152, LCD_GetYSize()-RisingSpeedPixels, 160, LCD_GetYSize());

            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
        }
    }
}
