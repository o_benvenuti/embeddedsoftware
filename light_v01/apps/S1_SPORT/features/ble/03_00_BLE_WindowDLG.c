//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdbool.h>

#include <al_feature.h>
#include <common.h>
#include <DIALOG.h>
#include <GUI_serenity.h>
#include <trace.h>
#include <stm32l4xx_hal.h>
#include <babel.h>

#include "feature_ble.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_bluetoothpetit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "BLE_Window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
        },
};

// vector for icon upper arrow
static GUI_POINT upper_arrow[] = {
        {  0, 13},
        { 32, 13},
        { 32,  0},
        { 64, 21},
        { 32, 41},
        { 32, 23},
        { 23, 28},
        {  0, 28},
};

// vector for icon lower arrow
static GUI_POINT lower_arrow[] = {
        {  1, 42},
        {  6, 42},
        { 33, 59},
        { 33, 49},
        { 64, 49},
        { 64, 51},
        { 35, 51},
        { 35, 64},
};

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;

    static ble_private_ctx_t * ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialization of 'BLE_Window'
            //
            ctx = NULL;

            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            break;
        }
        case FEATURE_BLE_SHARE_CTX:
        {
            ctx = (ble_private_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! ctx )
            {
                debug_printf("ble context not received yet\n");
                break;
            }

            /* draw background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_Draw_MenuBar_Dots(1,1);
            /* draw bluetooth logo */
            GUI_DrawBitmap(&bmcran_Serenity_13a_bluetoothpetit, 6, 0);

            /* draw button text */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);

            GUI_DispStringAt(languages_get_string(TXT_BLE_NAME), 17, 16);
            GUI_DispHex(HAL_GetUIDw0(), 8);

            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetTextAlign(GUI_TA_HCENTER);
            if ( ctx->current_state == ble_state_listen )
            {
                GUI_DispStringAt(languages_get_string(TXT_BLE_WAIT_CONN),
                                 (LCD_GetXSize() / 2), 32);
            }
            else
            {
                GUI_DispStringAt(languages_get_string(TXT_BLE_CONNECTED),
                                 (LCD_GetXSize() / 2), 32);
            }

            GUI_SetColor(GUI_WHITE);
            GUI_AA_FillPolygon(upper_arrow, countof(upper_arrow), 48, 58);
            GUI_AA_FillPolygon(lower_arrow, countof(lower_arrow), 48, 58);

            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateBLE_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);

    return hWin;
}

