/**
 * @file:  feature_ble.c
 *
 * @brief
 *
 * @date   25/09/2018
 *
 * <b>Description:</b>\n
 *    BLE functionnality
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <string.h>

#include <common.h>
#include <al_feature.h>
#include <al_ble.h>
#include <al_database.h>
#include <al_upgrade.h>
#include <al_tools.h>

#include <trace.h>
#include <ds2782_driver.h>
#include <GUI_Serenity.h>
#include <touchscreen.h>
#include <crc.h>
#include <stm32l4xx_hal.h>
#include <languages.h>

#include "feature_ble.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define BLE_SEND_PACKET_MAX_RETRY   16
#define BLE_RETRIEVE_PACKET_RETRY_MAX  16

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef feature_ble_states_t state_func_t(ble_private_ctx_t * ctx);

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _ble_init_func(void ** priv_data, int parent);
int _ble_release_func(void * priv_data);
int _ble_timer_func(void * priv_data, int time);
int _ble_button_func(void * priv_data, uint32_t);
int _ble_gest_func(void * priv_data, gestures_t gesture);
int _ble_event_func(void * priv_data, al_feature_cmd_t cmd);

// helpers
static inline int _send_unique_id(void);
static int _send_dive_record(uint8_t continuity);
static int _prepare_next_state(ble_private_ctx_t * ctx);
static int _retrieve_dive_param(uint8_t * config, size_t len);
static int _send_response(uint16_t header, uint8_t countinuity, bool error);

// states
feature_ble_states_t _do_state_listen(ble_private_ctx_t * ctx);
feature_ble_states_t _do_state_app(ble_private_ctx_t * ctx);
feature_ble_states_t _do_state_upgrade(ble_private_ctx_t * ctx);

// state array
state_func_t* const _state_table[ ] = {
        NULL, _do_state_listen, _do_state_app, _do_state_upgrade,
        NULL, NULL, NULL
};

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_ble_ctx = {
        .name       = "BLE",
        .ini_func   = _ble_init_func,
        .rls_func   = _ble_release_func,
        .usr_func   = _ble_gest_func,
        .evt_func   = _ble_event_func,
        .tmr_func   = _ble_timer_func,
        .gui_func   = _ble_button_func,
};

static ble_private_ctx_t _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _ble_init_func(void ** priv_data, int parent)
{
    WM_MESSAGE pMsg;
    al_feature_msg_t feat_msg;

    // set initial state
    _private_ctx.firmware_size = 0;
    _private_ctx.upgrade_flash_addr = 0;
    _private_ctx.parent_state = parent;
    _private_ctx.current_state = ble_state_listen;
    _private_ctx.next_state = ble_state_listen;

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    // create and display BLE window
    _private_ctx.current_window = CreateBLE_Window();

    // notify GUI
    pMsg.MsgId = FEATURE_BLE_SHARE_CTX;
    pMsg.Data.v = (int)&_private_ctx;
    WM_SendMessage(_private_ctx.current_window, &pMsg);
    WM_Invalidate(_private_ctx.current_window);

    // initialise state machine
    _prepare_next_state(&_private_ctx);

    // set feature timer/timeout to 1second
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 1;
    al_feature_send_to_tsk(&feat_msg);

    // return context
    *priv_data = &_private_ctx;

    return 0;
}

int _ble_release_func(void * priv_data)
{
    (void)priv_data;

	/* power down BLE */
	al_ble_deinit();

    return 0;
}

//----------------------------------------------------------------------
int _ble_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    ble_private_ctx_t * ctx = (ble_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    // process command
    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            // do not exit if we upgrade the watch
            if ( ctx->current_state != ble_state_upgrade )
            {
                msg.cmd = CMD_END_STATE;
                msg.data.value = serenity_state_charging;
                al_feature_send_to_tsk(&msg);
            }
            break;
        }
        case CMD_DVE_START:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("unknown event\n");
            return -1;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _ble_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc;
    ble_private_ctx_t * ctx = (ble_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    // update to new state
    if ( ctx->next_state != ctx->current_state )
    {
        rc = _prepare_next_state(ctx);
        if ( rc )
        {
            debug_printf("unable to prepare state\n");
            ctx->next_state = ctx->current_state;
        }
    }

    // switch state
    ctx->current_state = ctx->next_state;

    // notify GUI
    WM_Invalidate(ctx->current_window);

    // run state function
    if ( _state_table[ctx->current_state] )
    {
        ctx->next_state = _state_table[ctx->current_state](ctx);
    }

    return 0;
}

//----------------------------------------------------------------------
int _ble_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t msg;
    ble_private_ctx_t * ctx = (ble_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( gesture )
    {
        case swipe_right:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("gesture not managed\n");
            break;
        }
    }

    return 0;
}


//----------------------------------------------------------------------
int _ble_button_func(void * priv_data, uint32_t button)
{
    ble_private_ctx_t * ctx = (ble_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( button )
    {
        default:
        {
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
static int _retrieve_packet(uint8_t * input, size_t input_len, size_t size,
                            uint8_t * output, size_t * output_len)
{
    uint8_t type;
    int attempt = 0, rc = 0;
    uint16_t packet_size, cursor;

    if ( ! input || ! output || ! output_len )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // compute remaining size to read (add crc size)
    packet_size = (uint16_t)(input[4] << 8) | (uint16_t)(input[5] & 0xff);
    packet_size = packet_size + 4;

    // check that output buffer is big enough
    if ( packet_size > *output_len )
    {
        debug_printf("output buffer too small need %d\n", packet_size);
        return -1;
    }

    // remove already read buffer appart from
    // header, handle, continuity and size
    cursor = input_len - 2 - 2 - 1 - 1;

    // copy input buffer in output buffer and discard
    // header, handle, continuity counter, packet size
    memcpy(&output[0], &input[6], cursor);

    // read remaining parts of packet
    while ( cursor < packet_size )
    {
        // whatever append we continue till we
        // get expected size (or exceed max attempts)
        rc = al_ble_get_notification(&type, &input[0], &size);
        if ( rc )
        {
            attempt++;
            if ( attempt > BLE_RETRIEVE_PACKET_RETRY_MAX )
            {
                debug_printf("unable to retrieve complete packet\n");
                return -1;
            }

            HAL_Delay(10);
            continue;
        }

        // copy buffer discarding handle
        memcpy(&output[cursor], &input[1], (size - 1));
        cursor = cursor + size - 1;
        attempt = 0;
    }

    // update output buffer size
    *output_len = cursor;

    return 0;
}

//----------------------------------------------------------------------
static int _retrieve_dive_param(uint8_t * config, size_t len)
{
    int rc, idx = 0;
    serenity_context_t context;

    // sanity check
    if ( ! config || ! len )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // get context from flash
    rc = serenity_retrieve_status(&context);
    if ( rc )
    {
        debug_printf("unable to retrieve context\n");
        return -1;
    }

    //--------------------------------------------
    // parse configuration
    //--------------------------------------------
    // channel
    idx++;
    // water_salinity
    context.water_salinity = config[idx++];
    // compass_declination
    context.compass_declination = (uint32_t)(config[idx++] << 24);
    context.compass_declination |= (uint32_t)(config[idx++] << 16);
    context.compass_declination |= (uint32_t)(config[idx++] << 8);
    context.compass_declination |= (uint32_t)(config[idx++] & 0xff);
    // longitude
    idx += 4;
    // latitude
    idx += 4;
    context.deco_params.o2_ratio= (float)((float)config[idx++] / 100);

    // convert safety factor
    switch ( config[idx++] )
    {
    case 0:
        context.deco_params.safety_factor = SAFETY_FACTOR_MIN;
        break;
    case 1:
        context.deco_params.safety_factor = SAFETY_FACTOR_DEFAULT;
        break;
    case 2:
        context.deco_params.safety_factor = SAFETY_FACTOR_MAX;
        break;
    default:
        context.deco_params.safety_factor = SAFETY_FACTOR_DEFAULT;
        break;
    }

    context.deco_params.max_speed = config[idx++];
    context.deco_params.ppo2 = (float)((float)config[idx++] / 10);
    context.deco_params.dive_duration = config[idx++];
    context.depth_params.dive_duration = context.deco_params.dive_duration;
    context.depth_params.depth_max = (float)config[idx++];

    context.depth_params.stop_nb = config[idx++];
    if ( context.depth_params.stop_nb > NB_PALIERS_MAX )
    {
        debug_printf("received inconsistent stop number: discard\n");
        context.depth_params.stop_nb = NB_PALIERS_MAX;
    }

    // configure new stops
    for ( int i = 0; i < context.depth_params.stop_nb; ++i)
    {
        context.depth_params.dive_stops[i].StopDepth = config[idx++];
        context.depth_params.dive_stops[i].StopDuration = config[idx++] * 60;
        context.depth_params.dive_stops[i].Ascent_speed = config[idx++];

    }

    // save the context in flash
    rc = serenity_save_status(&context);
    if ( rc )
    {
        debug_printf("unable to save context\n");
        return -1;
    }

    return 0;
}
//----------------------------------------------------------------------
static int _retrieve_dev_config(uint8_t * packet, size_t len)
{
    int rc = 0;
    uint8_t size = 0;
    serenity_context_t context;
    uint16_t pos = 0, year_tmp;
    uint32_t tmp_opcode;

    // sanity check
    if ( ! packet || ! len )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    rc = serenity_retrieve_status(&context);
    if ( rc )
    {
        debug_printf("unable to retrieve context\n");
        return -1;
    }

    // retrieve but discard on S1_SPORT
    tmp_opcode  = (uint32_t)(packet[pos++] << 24);
    tmp_opcode |= (uint32_t)(packet[pos++] << 16);
    tmp_opcode |= (uint32_t)(packet[pos++] << 8);
    tmp_opcode |= (uint32_t)(packet[pos++] & 0xff);

    // language
    context.language = packet[pos++];
    languages_set_language(context.language);

    // retrieve first name (less one for '\0')
    for ( int i = 0; i < (ICE_NAME_LENGTH-1);)
    {
        // copy only if there is something
        if ( packet[pos] != 0 )
        {
            gFirstName[size++] = packet[pos];
        }

        // increase packet position
        i++;
        pos++;
    }

    // add ending zero
    if ( size < ICE_NAME_LENGTH )
    {
        gFirstName[size] = '\0';
    }

    // retrieve family name (less one for '\0')
    size = 0;
    for ( int i = 0; i < (ICE_NAME_LENGTH-1); ++i)
    {
        if ( packet[pos] != 0 )
        {
            gLastName[size++] = packet[pos];
        }

        // increase packet position
        pos++;
    }

    // add ending zero
    if ( size < ICE_NAME_LENGTH )
    {
        gLastName[size] = '\0';
    }

    // get nationality
    gNationality[0] = packet[pos++];
    gNationality[1] = packet[pos++];
    gNationality[2] = packet[pos++];
    gNationality[3] = '\0';

    // discard connection handle

    size = packet[pos++];
    if ( size > ICE_PHONE_LENGTH )
    {
        gPhoneNumber[0] = '\0';
        pos += ICE_PHONE_LENGTH - 1;

        debug_printf("invalid phone number length\n");
    }
    else
    {
        // copy number
        pos += ICE_PHONE_LENGTH - 1 - size;
        memcpy(&gPhoneNumber[0], &packet[pos], size);

        // update cursor and end of string character
        pos = pos + size;
        gPhoneNumber[size] = '\0';
    }


    // date of last dive received
    pos += 4;

    // time of last dive received
    pos += 4;

    year_tmp = (uint16_t)(packet[pos++] << 8);
    year_tmp |= (uint16_t)(packet[pos++] & 0xff);   // 61
    gsTimeSettings.Year = int2bcd((uint8_t)(year_tmp % 2000)); //sorry

    // discard connection handle
    gsTimeSettings.Month = int2bcd(packet[pos++]);
    gsTimeSettings.Date = int2bcd(packet[pos++]);

    // unused byte
    pos++;
    gsTimeSettings.Hours = int2bcd(packet[pos++]);
    gsTimeSettings.Minutes = int2bcd(packet[pos++]);
    gsTimeSettings.Seconds = int2bcd(packet[pos++]);    // 68

    // update internal RTC device
    normalizeTimeType(&gsTimeSettings);
    RTC_SetTime(&gsTimeSettings);

    // save context
    rc = serenity_save_status(&context);
    if ( rc )
    {
        debug_printf("unable to save context\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
static int _send_dive_record(uint8_t continuity)
{
    uint32_t crc;
    TimeType start;
    uint16_t packet_size, entry_count;
    int rc, idx = 0, count;
    uint8_t response[32], sample;
    al_database_diverec_config_t config;
    al_database_diverec_entry_t entry_buf;

    // respond
    response[idx++] = 0xb4;
    response[idx++] = 0xc4;
    response[idx++] = continuity;
    response[idx++] = 0x0;
    response[idx++] = 0x5;
    response[idx++] = (uint8_t)(PROJECT_VERSION >> 8);
    response[idx++] = (uint8_t)(PROJECT_VERSION & 0xff);
    response[idx++] = 0x0e;
    BAT_LoadValue(&response[idx++]);

    count = al_database_diverec_get_count();
    if ( count < 0 )
    {
        count = 0;
    }

    response[idx++] = count;
    crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)&response[0], idx);
    response[idx++] = crc >> 24;
    response[idx++] = crc >> 16;
    response[idx++] = crc >> 8;
    response[idx++] = crc;

    rc = al_ble_set_notification(0x0052, &response[0], idx);
    if ( rc )
    {
        debug_printf("error while responding\n");
    }

    if ( count == 0 )
    {
        debug_printf("no dive\n");
        return 0;
    }

    rc = al_database_diverec_oldest(&start);
    if ( rc )
    {
        debug_printf("last dive not found\n");
        return -1;
    }

    rc = al_database_diverec_open(start);
    if ( rc )
    {
        debug_printf("cannot open last dive\n");
        return -1;
    }

    // retrieve data/entry count
    config.type = AL_DATABASE_DIVEREC_TYPE_DCOUNT;
    rc = al_database_diverec_get(&config);
    if ( rc )
    {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    // data, time, sample, temp min, stop count, dive mode
    entry_count = config.data.entry_count;
    packet_size = 14;
    packet_size += entry_count * 4;

    // retrieve sample frequency
    config.type = AL_DATABASE_DIVEREC_TYPE_SAMPLE;
    rc = al_database_diverec_get(&config);
    if ( rc )
    {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    // save for future use
    sample = config.data.sample;
    config.type = AL_DATABASE_DIVEREC_TYPE_PARAMS;
    rc = al_database_diverec_get(&config);
    if ( rc )
    {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    packet_size += config.data.params.stop_number * 3;

    __HAL_CRC_DR_RESET(&hcrc);

    idx = 0;
    // respond
    response[idx++] = 0xb4;
    response[idx++] = 0xc4;
    response[idx++] = continuity;
    response[idx++] = (uint8_t)(packet_size >> 8);
    response[idx++] = (uint8_t)(packet_size & 0xff);

    response[idx++] = 0x20; // 20
    response[idx++] = start.Year;
    response[idx++] = start.Month;
    response[idx++] = start.Date;
    response[idx++] = 0x00;
    response[idx++] = start.Hours;
    response[idx++] = start.Minutes;
    response[idx++] = start.Seconds;

    response[idx++] = sample;
    response[idx++] = config.data.params.dive_mode;
    response[idx++] = config.data.params.stop_number;

    // send via transparent UART
    rc = al_ble_set_notification(0x0052, &response[0], idx);
    if ( rc )
    {
        debug_printf("error while responding\n");
        al_database_diverec_close();
        return -1;
    }

    HAL_CRC_Accumulate(&hcrc, (uint32_t*)&response[0], idx);

    idx = 0;
    for ( int i = 0; i < config.data.params.stop_number; ++i)
    {
        response[idx++] = config.data.params.dive_stops[i].stop_depth;
        response[idx++] = config.data.params.dive_stops[i].stop_duration;
        response[idx++] = config.data.params.dive_stops[i].ascent_speed;
    }

    config.type = AL_DATABASE_DIVEREC_TYPE_MINTMP;
    rc = al_database_diverec_get(&config);
    if ( rc )
    {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    // water min to implement
    response[idx++] = config.data.temperature;
    response[idx++] = (uint8_t)(entry_count >> 8);
    response[idx++] = (uint8_t)(entry_count & 0xFF);
    rc = al_ble_set_notification(0x0052, &response[0], idx);
    if ( rc )
    {
        debug_printf("error while responding\n");
        al_database_diverec_close();
        return -1;
    }

    HAL_CRC_Accumulate(&hcrc, (uint32_t*)&response[0], idx);

    idx = 0;
    for (int i = 0; i < entry_count; ++i)
    {
        size_t entry_len = sizeof(al_database_diverec_entry_t);

        rc = al_database_diverec_read(&entry_buf, &entry_len);
        if ( rc || ! entry_len )
        {
            debug_printf("packet too small\n");
            continue;
        }

        uint16_t fixed_12_5 = (uint16_t)(entry_buf.depth * (1 << 4));
        response[idx++] = (uint8_t)(fixed_12_5 >> 8);
        response[idx++] = (uint8_t)(fixed_12_5 & 0xff);
        response[idx++] = (uint8_t)(entry_buf.direction >> 8);
        response[idx++] = (uint8_t)(entry_buf.direction & 0xff);

        if ( (idx == 20) || (entry_count-1 == i) )
        {
            // in case the peer did not receive the packet
            // retry few time and abort if it never succeed
            int attempts = 0;
            do
            {
                if ( attempts++ > BLE_SEND_PACKET_MAX_RETRY )
                {
                    // close file and return
                    al_database_diverec_close();
                    return -1;
                }

                rc = al_ble_set_notification(0x0052, &response[0], idx);
                if ( rc )
                {
                    HAL_Delay(20);
                }
            } while ( rc );

            // we noticed that minimum time between packet transfer
            // is 1.5ms then delay whatever happens
            HAL_Delay(2);

            // add to crc current value
            HAL_CRC_Accumulate(&hcrc, (uint32_t*)&response[0], idx);
            idx = 0;
        }
    }

    idx = 0;
    crc = hcrc.Instance->DR;
    response[idx++] = crc >> 24;
    response[idx++] = crc >> 16;
    response[idx++] = crc >> 8;
    response[idx++] = crc;

    rc = al_ble_set_notification(0x0052, &response[0], idx);
    if ( rc )
    {
        debug_printf("error while responding\n");
        al_database_diverec_close();
        return -1;
    }

    al_database_diverec_close();
    rc = al_database_diverec_delete_oldest();
    return rc;
}

//----------------------------------------------------------------------
static inline int _send_unique_id(void)
{
    int rc = 0;
    uint32_t crc;
    uint64_t uid[2];
    uint8_t temp[24];

    // header
    temp[0] = 0xB3;
    temp[1] = 0xC3;

    // counter and size
    temp[2] = 0x0;
    temp[3] = 0x0;
    temp[4] = 0xC;

    // actual data
    al_tools_read_uid(&uid[0]);
    temp[5] = (uint8_t)(uid[0] & 0xff);
    temp[6] = (uint8_t)(uid[0] >> 8);
    temp[7] = (uint8_t)(uid[0] >> 16);
    temp[8] = (uint8_t)(uid[0] >> 24);
    temp[9] = (uint8_t)(uid[0] >> 32);
    temp[10] = (uint8_t)(uid[0] >> 40);
    temp[11] = (uint8_t)(uid[0] >> 48);
    temp[12] = (uint8_t)(uid[0] >> 56);
    temp[13] = (uint8_t)(uid[1] & 0xff);
    temp[14] = (uint8_t)(uid[1] >> 8);
    temp[15] = (uint8_t)(uid[1] >> 16);
    temp[16] = (uint8_t)(uid[1] >> 24);

    rc = al_ble_set_notification(0x0052, &temp[0], 17);
    if ( rc )
    {
        return -1;
    }

    // checksum
    crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)&temp[0], 17);
    temp[0]  = (uint8_t)(crc >> 24);
    temp[1]  = (uint8_t)(crc >> 16);
    temp[2] = (uint8_t)(crc >> 8);
    temp[3] = (uint8_t)(crc & 0xff);
    return al_ble_set_notification(0x0052, &temp[0], 4);
}

//----------------------------------------------------------------------
static int _send_response(uint16_t header, uint8_t countinuity, bool error)
{
    int32_t rc, crc;
    int16_t pos = 0;
    uint8_t response[16];


    // respond
    response[pos++] = (uint8_t)(header >> 8);
    response[pos++] = (uint8_t)(header & 0xff);
    response[pos++] = countinuity;
    response[pos++] = 0x0;  // packet size
    response[pos++] = 0x4;
    response[pos++] = (uint8_t)(PROJECT_VERSION >> 8);  // firmware version
    response[pos++] = (uint8_t)(PROJECT_VERSION & 0xff);

    // get error code regarding request type
    if ( error )
    {
        if ( header == AL_BLE_CHAR_HANDLE_APP_CONFIG )
        {
            response[pos++] = 0x6;
        }
        else if ( header == AL_BLE_CHAR_HANDLE_APP_RECORD )
        {
            response[pos++] = 0xC;
        }
        else if ( header == AL_BLE_CHAR_HANDLE_FWOTA_NOTIFY_2 )
        {
            response[pos++] = 0xA;
        }
        else    // CRC
        {
            response[pos++] = 0xF;
        }
    }
    else
    {
        response[pos++] = 0xE;
    }

    BAT_LoadValue(&response[pos++]);

    crc = HAL_CRC_Calculate(&hcrc, (uint32_t*)&response[0], pos);
    response[pos++] = crc >> 24;
    response[pos++] = crc >> 16;
    response[pos++] = crc >> 8;
    response[pos++] = crc;

    // TODO send on different channel for upgrade
    rc = al_ble_set_notification(0x0052, &response[0], pos);
    if ( rc )
    {
        debug_printf("error while responding\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
// States
//----------------------------------------------------------------------
feature_ble_states_t _do_state_listen(ble_private_ctx_t * ctx)
{
    (void)ctx;
    int rc;
    uint8_t type, buffer[32];
    size_t length;

    length = sizeof(buffer);
    rc = al_ble_get_notification(&type, &buffer[0], &length);
    if ( rc )
    {
        return ble_state_listen;
    }

    if ( type == 0x81 && buffer[0] == 0x0C )
    {
        return ble_state_listen;
    }

    // handle configuration messages
    if ( type == 0x71 && buffer[0] == 0x00 )
    {
        debug_printf("received connected notification\n");
        return ble_state_app;
    }

    return ble_state_listen;
}

//----------------------------------------------------------------------
feature_ble_states_t _do_state_app(ble_private_ctx_t * ctx)
{
    (void)ctx;
    int rc;
    uint8_t type, continuity;
    size_t length, packet_len;
    uint8_t buffer[32], packet[128];
    uint16_t characteristic;

    length = sizeof(buffer);
    rc = al_ble_get_notification(&type, &buffer[0], &length);
    if ( rc )
    {
        return ble_state_app;
    }

    // transparent UART enabled
    if ( type == 0x81 )
    {
        debug_printf("transparent uart enabled\n");
        return ble_state_app;
    }

    // disconnection event
    if ( type == 0x72 )
    {
        debug_printf("received disconnect notification\n");
        return ble_state_listen;
    }

    // handle configuration messages
    if ( type == 0x73 )
    {
        debug_printf("updated connection parameters\n");
        return ble_state_app;
    }

    // translate header characteristic
    continuity = buffer[3];
    characteristic = ((uint16_t)buffer[1] << 8) | buffer[2];

    // registration to a service
    if ( type == 0x98 )
    {
        /**
         * Those subscription will possibly help
         * to display a new window eventually
        **/
        if ( characteristic == AL_BLE_CHAR_HANDLE_FWOTA_NOTIFY_1 )
        {
            // keep waiting for second subscription
            return ble_state_app;
        }

        if ( characteristic == AL_BLE_CHAR_HANDLE_FWOTA_NOTIFY_2 )
        {
            // send firmware version and battery level
            buffer[0] = 0x00;
            BAT_LoadValue(&buffer[1]);
            buffer[2] = (uint8_t)(PROJECT_VERSION >> 8);
            buffer[3] = (uint8_t)(PROJECT_VERSION & 0xff);

            // Make sure the phone is be ready to get the message
            HAL_Delay(1000);

            // TODO see why there is no acknowledge here
            al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_FW_VERSION,
                                    &buffer[0], 4);
            return ble_state_app;
        }

        // the phone ask if we are ready
        if ( characteristic == AL_BLE_CHAR_HANDLE_FWOTA_TYPE )
        {
            // init the flash
            rc = al_upgrade_flash_init(&(ctx->upgrade_flash_addr));
            if ( rc )
            {
                return -1;
            }

            // send ready message
            memset(&buffer[0], 0x00, 15);
            buffer[15] = 0x52;
            buffer[16] = 0x45;
            buffer[17] = 0x41;
            buffer[18] = 0x44;
            buffer[19] = 0x59;

            al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX,
                                    &buffer[0], 20);

            return ble_state_app;
        }

        if ( characteristic == AL_BLE_CHAR_HANDLE_FWOTA_SIZE )
        {
            ctx->firmware_size = (buffer[3] << 8) | buffer[4];

            return ble_state_upgrade;
        }

        if ( characteristic == AL_BLE_CHAR_HANDLE_TRANSP_UART )
        {
            debug_printf("received registration for application\n");
            return ble_state_app;
        }
    }

    // transparent uart request
    if ( type == 0x9a )
    {
        // compute request ID
        if ( characteristic == AL_BLE_CHAR_HANDLE_APP_UUID )
        {
            rc = _send_unique_id();
            if ( rc )
            {
                debug_printf("unable to send UUID\n");
            }

            return ble_state_app;
        }

        if ( characteristic == AL_BLE_CHAR_HANDLE_APP_CONFIG )
        {
            // read packet
            packet_len = sizeof(packet);
            rc = _retrieve_packet(&buffer[0], length, sizeof(buffer),
                                  &packet[0], &packet_len);
            if ( rc )
            {
                _send_response(AL_BLE_CHAR_HANDLE_APP_CONFIG, continuity, true);
                return ble_state_app;
            }

            rc = _retrieve_dive_param(&packet[0], packet_len);
            if ( rc )
            {
                _send_response(AL_BLE_CHAR_HANDLE_APP_CONFIG, continuity, true);
                debug_printf("unable to configure the watch\n");
            }

            _send_response(AL_BLE_CHAR_HANDLE_APP_CONFIG, continuity, false);
            return ble_state_app;
        }

        if ( characteristic == AL_BLE_CHAR_HANDLE_APP_RECORD )
        {
            // read packet
            packet_len = sizeof(packet);
            rc = _retrieve_packet(&buffer[0], length, sizeof(buffer),
                                  &packet[0], &packet_len);
            if ( rc )
            {
                _send_response(AL_BLE_CHAR_HANDLE_APP_RECORD, continuity, true);
                return ble_state_app;
            }

            // send starting at 1 to discard comm. handle
            rc = _retrieve_dev_config(&packet[0], packet_len);
            if ( rc )
            {
                debug_printf("input configuration error\n");
                _send_response(AL_BLE_CHAR_HANDLE_APP_RECORD, continuity, true);
                return ble_state_app;
            }

            rc = _send_dive_record(continuity);
            if ( rc )
            {
                debug_printf("unable to send dives to the phone\n");
                _send_response(AL_BLE_CHAR_HANDLE_APP_RECORD, continuity, true);
            }

            return ble_state_app;
        }
    }

    return ble_state_app;
}

//----------------------------------------------------------------------
static uint64_t u8tou64(uint8_t const u8[static 8])
{
  uint64_t u64;
  memcpy(&u64, u8, sizeof u64);
  return u64;
}

//----------------------------------------------------------------------
feature_ble_states_t _do_state_upgrade(ble_private_ctx_t * ctx)
{
    int rc = 0;
    size_t size;
    uint8_t buffer[32], type;
    uint16_t packet_received_lsb, packet_received_msb;

    // NOTE those variables must be reset on error
    static int buffer_pos = 0;
    static uint16_t packet_received = 0;
    static uint8_t buffer_to_write[16] = { 0 };

    // read from source (BLE/UART) and write to other bank
    size = sizeof(buffer);
    rc = al_ble_get_notification(&type, &buffer[0], &size);
    if ( rc )
    {
        HAL_Delay(100);
        return ble_state_upgrade;
    }

    // characteristic update
    if ( type != AL_BLE_NOTIF_TYPE_REQUEST_CHAR )
    {
        debug_printf("unexpected message\n");
        return ble_state_upgrade;
    }

    if ( (buffer[0] != AL_BLE_CONNECTION_HANDLE) ||
         (buffer[1] != 0x80) || (buffer[2] != 0x07) )
    {
        debug_printf("unexpected message\n");
        return ble_state_upgrade;
    }

    packet_received++;
    packet_received_msb = packet_received >> 8;
    packet_received_lsb = packet_received & 0x00FF;

    // validate packet number
    if ((buffer[size-2] != packet_received_msb) ||
        (buffer[size-1] != packet_received_lsb)  )
    {
        // _send_NOK()
        buffer[0] = 0x4B;
        buffer[1] = 0x4F;
        al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &buffer[0], 2);

        debug_printf("invalid packet number %d expected %d\n",
                (uint16_t)(buffer[size-2] << 8 | buffer[size-1]),
                packet_received);

        buffer_pos = 0;
        packet_received = 0;
        memset(buffer_to_write, 0, sizeof(buffer_to_write));
        return ble_state_end;
    }

    // remove packet number
    size -= 2;

    // write every 20 bytes (protocol packet size)
    for ( unsigned int i = 3; (i < size) ; ++i)
    {
        buffer_to_write[buffer_pos] = buffer[i];
        buffer_pos++;

        if ( buffer_pos == 8 )
        {
            // unlock the Flash
            HAL_FLASH_Unlock();

            //operation will be done by word
            rc = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,
                                   ctx->upgrade_flash_addr,
                                   u8tou64(&buffer_to_write[0]));
            if ( rc != HAL_OK )
            {
                HAL_FLASH_Lock();
                debug_printf("unable to write to flash\n");
                buffer_pos = 0;
                packet_received = 0;
                memset(buffer_to_write, 0, sizeof(buffer_to_write));
                return ble_state_end;
            }

            // lock the Flash to protect against possible unwanted operation
            HAL_FLASH_Lock();

            buffer_pos = 0;
            ctx->upgrade_flash_addr += 8;
        }
    }

    // for the last data check if full buffer can be written
    if ( buffer_pos && (packet_received == ctx->firmware_size) )
    {
        // fill with zero to write doubleword
        memset(&buffer_to_write[buffer_pos], 0, (8-buffer_pos));

        // unlock the Flash
        HAL_FLASH_Unlock();

        //operation will be done by word
        rc = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,
                               ctx->upgrade_flash_addr,
                               u8tou64(&buffer_to_write[0]));
        if ( rc != HAL_OK )
        {
            HAL_FLASH_Lock();

            debug_printf("unable to write to flash\n");
            buffer_pos = 0;
            packet_received = 0;
            memset(buffer_to_write, 0, sizeof(buffer_to_write));
            return ble_state_end;
        }

        // lock the Flash to protect against possible unwanted operation
        HAL_FLASH_Lock();

        buffer_pos = 0;
        ctx->upgrade_flash_addr += 8;
    }

    if ( packet_received < ctx->firmware_size )
    {
        return ble_state_upgrade;
    }


    // send number of received packet
    buffer[0] = packet_received >> 8;
    buffer[1] = packet_received;

    // send received size
    rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &buffer[0], 2);
    if ( rc )
    {
        debug_printf("cannot send success notification\n");
    }

    // turn device down
    al_ble_deinit();

    if ( packet_received == ctx->firmware_size )
    {
        // switch bank to boot on
        al_upgrade_bank_switch();
        NVIC_SystemReset();
    }

    return ble_state_end;
}

//----------------------------------------------------------------------
// Transitions
//----------------------------------------------------------------------
static int _prepare_next_state(ble_private_ctx_t * ctx)
{
    int rc;
    al_feature_msg_t msg;

    if ( ctx->next_state == ble_state_listen )
    {
        // reset if coming from upper state
        al_ble_deinit();


        // Initialise ble interface
        rc = al_ble_init();
        if ( rc ) {
            debug_printf("unable to initialise ble\n");

            // exit to previous feature if we cannot enable advertisement
            msg.cmd = CMD_END_STATE;
            msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&msg);
            return -1;
        }

        // start listening for client
        rc = al_ble_listen();
        if ( rc ) {
            debug_printf("error while starting advertisement\n");

            // exit to previous feature if we cannot enable advertisement
            msg.cmd = CMD_END_STATE;
            msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&msg);
            return -1;
        }
    }

    return 0;
}

