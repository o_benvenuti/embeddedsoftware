#ifndef _FEATURE_BLE_H_
#define _FEATURE_BLE_H_
/**
 * @file:  feature_ble.h
 *
 * @brief
 *
 * @date   24/09/2018
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FEATURE_BLE_SHARE_CTX        WM_USER + 0x07

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef enum _feature_ble_states_t {
    ble_state_idle,
    ble_state_listen,
    ble_state_app,
    ble_state_upgrade,
    ble_state_end,
}feature_ble_states_t;

typedef struct ble_private_ctx {
    size_t firmware_size;
    WM_HWIN current_window;
    uint32_t upgrade_flash_addr;
    serenity_state_t parent_state;
    feature_ble_states_t current_state;
    feature_ble_states_t next_state;
}ble_private_ctx_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateBLE_Window(void);

#endif  /* _FEATURE_BLE_H_ */
