#ifndef _FEATURE_WATCH_CONFIG_H_
#define _FEATURE_WATCH_CONFIG_H_
/**
 * @file:  feature_watch_config.h
 *
 * @brief
 *
 * @date   26/03/2019
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define FEATURE_WATCH_CONFIG_SHARE_CTX  WM_USER + 0x06

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
enum feature_watch_config_state {
    watch_config_state_time,
    watch_config_state_watch,
    watch_config_state_reset,
};

typedef struct feat_watch_conf_priv_ctx {
    WM_HWIN current_window;
    serenity_context_t global_ctx;
    serenity_state_t parent_state;
    int current_state;
}feat_watch_conf_priv_ctx_t;


//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateWatchSettings_Window(void);
WM_HWIN CreateTimeSetting_Window(void);
WM_HWIN Create01_23_Factory_Reset_Window(void);

#endif  // _FEATURE_WATCH_CONFIG_H_

