//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------

#include <common.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <string.h>
#include <touchscreen.h>
#include <trace.h>

#include "feature_watch_config.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _watch_config_init_func(void ** priv_data, int parent);
int _watch_config_button_func(void * priv_data, uint32_t button);
int _watch_config_gest_func(void * priv_data, gestures_t gesture);
int _watch_config_event_func(void * priv_data, al_feature_cmd_t cmd);
int _watch_config_release_func(void * priv_data);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_watch_config_ctx = {
        .name       = "WATCH_CONFIG",
        .usr_func   = _watch_config_gest_func,
        .ini_func   = _watch_config_init_func,
        .gui_func   = _watch_config_button_func,
        .evt_func   = _watch_config_event_func,
        .rls_func   = _watch_config_release_func,
};

/**
 * NOTE static context allocation to be used only in init function,
 * then use pointer provided by al_feature library to callbacks
**/
static feat_watch_conf_priv_ctx_t _watch_conf_local_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _watch_config_init_func(void ** priv_data, int parent)
{
    int rc;
    WM_MESSAGE window_msg;

    // set initial state
    _watch_conf_local_ctx.parent_state = parent;
    _watch_conf_local_ctx.current_state = watch_config_state_watch;

    // background is parent window of all screen
    // delete previous screen if needed
    _watch_conf_local_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _watch_conf_local_ctx.current_window )
    {
        WM_DeleteWindow(_watch_conf_local_ctx.current_window);
    }

    // retrieve context from flash
    rc = serenity_retrieve_status(&(_watch_conf_local_ctx.global_ctx));
    if ( rc )
    {
        debug_printf("unable to get context from flash\n");
        return -1;
    }

    // create and display BLE window
    _watch_conf_local_ctx.current_window = CreateWatchSettings_Window();

    // send local context to windows
    window_msg.MsgId = FEATURE_WATCH_CONFIG_SHARE_CTX;
    window_msg.Data.p = &_watch_conf_local_ctx;
    WM_SendMessage(_watch_conf_local_ctx.current_window, &window_msg);

    // return context
    *priv_data = &_watch_conf_local_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _watch_config_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    feat_watch_conf_priv_ctx_t * ctx = (feat_watch_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_charging;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case CMD_DVE_START:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("invalid command\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
// processing
//----------------------------------------------------------------------
int _watch_config_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t msg;
    WM_MESSAGE window_msg;
    feat_watch_conf_priv_ctx_t * ctx = (feat_watch_conf_priv_ctx_t *)priv_data;

    switch ( gesture )
    {
        case swipe_right:
        {

            if ( ctx->current_state == watch_config_state_watch )
            {
                msg.cmd = CMD_END_STATE;
                msg.data.value = ctx->parent_state;
                al_feature_send_to_tsk(&msg);
            }
            else if ( ctx->current_state == watch_config_state_time )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateWatchSettings_Window();
                ctx->current_state = watch_config_state_watch;

                // send local context to windows
                window_msg.MsgId = FEATURE_WATCH_CONFIG_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            else if ( ctx->current_state == watch_config_state_reset )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateTimeSetting_Window();
                ctx->current_state = watch_config_state_time;
            }
            break;
        }
        case swipe_left:
        {
            if ( ctx->current_state == watch_config_state_reset )
            {
                msg.cmd = CMD_END_STATE;
                msg.data.value = ctx->parent_state;
                al_feature_send_to_tsk(&msg);
            }
            else if ( ctx->current_state == watch_config_state_watch )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateTimeSetting_Window();
                ctx->current_state = watch_config_state_time;
            }
            else if ( ctx->current_state == watch_config_state_time )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create01_23_Factory_Reset_Window();
                ctx->current_state = watch_config_state_reset;
            }
            break;
        }
        default:
        {
            debug_printf("unknown gesture\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _watch_config_button_func(void * priv_data, uint32_t button)
{
    feat_watch_conf_priv_ctx_t * ctx = (feat_watch_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( button )
    {
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _watch_config_release_func(void * priv_data)
{
    int rc;
    feat_watch_conf_priv_ctx_t * ctx = (feat_watch_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // save new context to flash
    rc = serenity_save_status(&(ctx->global_ctx));
    if ( rc )
    {
        debug_printf("unable to save context to flash\n");
        return -1;
    }

    return 0;
}

