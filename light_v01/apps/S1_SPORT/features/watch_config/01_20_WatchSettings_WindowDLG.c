//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <stdio.h>
#include <common.h>
#include <GUI_serenity.h>
#include <User_RTC.h>
#include <ILI9163_driver.h>
#include <trace.h>
#include <babel.h>
#include <al_feature.h>

#include "feature_watch_config.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x02)
#define ID_BUTTON_1    (GUI_ID_USER + 0x03)
#define ID_BUTTON_2    (GUI_ID_USER + 0x04)
#define ID_BUTTON_3    (GUI_ID_USER + 0x05)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static bool _brightness_clicked = true;
static bool _metric_system_clicked = false;
static serenity_context_t * _global_ctx = NULL;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldReduced16pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "WatchSettings_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "brightness selection", ID_BUTTON_0, 0, 15, 80, 38, 0, 0x0, 0  },
        { BUTTON_CreateIndirect, "metric selection"    , ID_BUTTON_1, 0, 52, 80, 38, 0, 0x0, 0  },
        { BUTTON_CreateIndirect, "Plus_Button"         , ID_BUTTON_2, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button"        , ID_BUTTON_3, 85, 88, 75, 40, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialisation of 'WatchSettings_Window'
            //
            _global_ctx = NULL;
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cbRepaint);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            break;
        }
        case FEATURE_WATCH_CONFIG_SHARE_CTX:
        {
            _global_ctx = &(((feat_watch_conf_priv_ctx_t *)pMsg->Data.p)->global_ctx);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            if ( ! _global_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                // Notifications sent by brightness button
                case ID_BUTTON_0:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        _brightness_clicked = true;
                        _metric_system_clicked = false;
                    }
                    break;
                }
                // Notifications sent by metric system button
                case ID_BUTTON_1:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        _brightness_clicked = false;
                        _metric_system_clicked = true;
                    }
                    break;
                }
                // Notifications sent by plus button
                case ID_BUTTON_2:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if ( _brightness_clicked )
                        {
                            _global_ctx->Brightness =
                                    (_global_ctx->Brightness == 100) ?
                                    100 : (_global_ctx->Brightness + 10);
                            change_BaklightIntensity(_global_ctx->Brightness);
                        }
                        else if ( _metric_system_clicked )
                        {
                            _global_ctx->metric_system = ! _global_ctx->metric_system;
                        }
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_3: // Notifications sent by 'Moins_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if ( _brightness_clicked )
                        {
                            _global_ctx->Brightness =
                                    (_global_ctx->Brightness == 10) ?
                                    10 : (_global_ctx->Brightness - 10);
                            change_BaklightIntensity(_global_ctx->Brightness);
                        }
                        else if ( _metric_system_clicked )
                        {
                            _global_ctx->metric_system = ! _global_ctx->metric_system;
                        }
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
            }
            WM_Invalidate(pMsg->hWin);
            break; // WM_NOTIFY_PARENT
        }
        default:
            WM_DefaultProc(pMsg);
            break;
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateWatchSettings_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Bstr[8];
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            GUI_Draw_MenuBar_Dots(4,2);

            /* draw arrows */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 20);
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);

            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);

            // display brightness button regarding current selection
            if( _brightness_clicked )
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }

            GUI_ClearRect(0, 16, 80, 90);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_BRIGHTNESS), 4, 22);

            // display metric system button regarding current selection
            if( _metric_system_clicked )
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }

            GUI_ClearRect(0, 53, 80, 90);
            GUI_SetTextMode(GUI_TM_TRANS);

            // special case for German that takes one line only
            if ( 2 == _global_ctx->language )
            {
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L1),4,59);
            }
            else
            {
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L1),4,52);
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L2),4,66);
            }

            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);

            if ( _brightness_clicked )
            {
                sprintf(Bstr, "%d%%", _global_ctx->Brightness);
                GUI_DispStringAt(Bstr,152,71);
            }
            else if ( _metric_system_clicked )
            {
                if ( _global_ctx->metric_system )
                {
                    GUI_DispStringAt(
                            languages_get_string(TXT_METER_SYSTEM), 152,71);
                }
                else
                {
                    GUI_DispStringAt(
                            languages_get_string(TXT_FEET_SYSTEM), 152,71);
                }
            }

            break;
        default:
            WINDOW_Callback(pMsg);
    }
}

