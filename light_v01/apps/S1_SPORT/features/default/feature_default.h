#ifndef _FEATURE_DEFAULT_H_
#define _FEATURE_DEFAULT_H_
/**
 * @file:  feature_default.h
 *
 * @brief
 *
 * @date        25/09/2018
 *
 * @description
 *
 * @platform    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <common.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FEATURE_DEFAULT_SET_GRAPH   WM_USER + 0x07
#define FEATURE_DEFAULT_SET_NOFLY   WM_USER + 0x08

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
enum _feature_default_button_value
{
    BUTTON_ICE = 0,
    BUTTON_MENU,
    BUTTON_WTCH,
    BUTTON_HIST,
    BUTTON_BLUE,
    BUTTON_DIVE,
};

typedef struct feature_default_graph_info
{
    float max_depth;
    uint8_t lap_count;
    bool metric_system;
    TimeType dive_start;
    uint16_t dive_duration;
    uint16_t lap_max_duration;
    int16_t  dive_profile_graph[144];
    int16_t  dive_profile_graph_idx;
    serenity_dive_mode_t last_dive_mode;
}feature_default_graph_info_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateICE_WINDOW(void);
WM_HWIN CreateMainMenu_1(void);
WM_HWIN Create_00_01_HourScreen(void);
WM_HWIN CreateShowLastDive_Window(void);

#endif  // _FEATURE_DEFAULT_H_

