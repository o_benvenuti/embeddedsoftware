//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <al_tools.h>
#include <common.h>
#include <GUI_serenity.h>
#include <babel.h>
#include <trace.h>

#include "feature_default.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_GRAPH_0              (GUI_ID_USER + 0x06)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "ShowLastDive_Window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
        },
};

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_iconprofmax;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_icontempstotal;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_diver_small;

static feature_default_graph_info_t * graph_info = NULL;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    char Tstr[32];
    int hr, min;
    int x_pos, y_pos;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            // Initialization
            graph_info = NULL;
            break;
        }
        case FEATURE_DEFAULT_SET_GRAPH:
        {
            graph_info = (feature_default_graph_info_t *)pMsg->Data.v;
            break;
        }
        case WM_PAINT:
        {
            // wait for context to be received
            if ( ! graph_info )
            {
                break;
            }

            // if last dive not found
            if ( ! graph_info->dive_duration )
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_Clear();
                GUI_DrawBitmap(&bmcran_Serenity_13a_diver_small, 60, 20);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER);
                GUI_DispStringAt(languages_get_string(TXT_NO_DIVE), 80 , 80);
                break;
            }

            //*****************************************************************
            //*********************** BACKGROUND ******************************
            //*****************************************************************
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            //*****************************************************************
            //********************** UPPER SECTION ****************************
            //*****************************************************************
            bool mmdd = RTC_ismmddMode();
            sprintf(Tstr, "%d.%d.20%02d %02d:%02d",
                    (mmdd ? bcd2int(graph_info->dive_start.Month) :
                            bcd2int(graph_info->dive_start.Date)),
                    (mmdd ? bcd2int(graph_info->dive_start.Date) :
                            bcd2int(graph_info->dive_start.Month)),
                    bcd2int(graph_info->dive_start.Year),
                    bcd2int(graph_info->dive_start.Hours),
                    bcd2int(graph_info->dive_start.Minutes));
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            x_pos = (LCD_GetXSize() / 2);
            y_pos = 8;
            GUI_DispStringAt(Tstr, x_pos ,y_pos);


            /* draw MaxDeep Arrow */
            x_pos = 6;
            y_pos = Y_1_6 + 7 - (bmcran_Serenity_13B_iconprofmax.YSize/2);
            GUI_DrawBitmap(&bmcran_Serenity_13B_iconprofmax, x_pos ,y_pos);

            if ( graph_info->metric_system )
            {
                snprintf(Tstr, sizeof(Tstr), "%.1f %s",
                         graph_info->max_depth,
                         languages_get_string(TXT_M_UNIT));
            }
            else
            {
                snprintf(Tstr, sizeof(Tstr), "%.1f %s",
                         al_tools_to_feet(graph_info->max_depth),
                         languages_get_string(TXT_F_UNIT));
            }
            x_pos = ((LCD_GetXSize() - (6+bmcran_Serenity_13B_iconprofmax.XSize)) / 2) + (6+bmcran_Serenity_13B_iconprofmax.XSize);
            y_pos = Y_1_6 + 6;
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E5));
            GUI_ClearRect(0,Y_2_3+ 1,LCD_GetXSize(),LCD_GetYSize());
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, x_pos ,y_pos);


            //*****************************************************************
            //********************** MIDDLE SECTION ***************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); // dark blue
            GUI_ClearRect(0,Y_1_3 ,LCD_GetXSize(),Y_2_3);
            // draw vertical lines
            GUI_SetColor(GUI_BLACK);
            GUI_DrawVLine(X_1_4, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_2_4, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_3_4, Y_1_3, Y_2_3);
            // draw graph
            GUI_SetColor(GUI_WHITE);
            GUI_DrawGraph(graph_info->dive_profile_graph,
                    graph_info->dive_profile_graph_idx, 0, 42);

            //*****************************************************************
            //********************** LOWER SECTION ****************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767)); // normal blue
            GUI_ClearRect(0,Y_2_3 ,LCD_GetXSize(),LCD_GetYSize());
            hr = graph_info->dive_duration / (60*60);
            min = (graph_info->dive_duration / 60 ) % 60;

            /* print DiveTime */
            sprintf(Tstr, "%02d:%02d",hr,min);
            x_pos = ((LCD_GetXSize() - (6+bmcran_Serenity_13B_icontempstotal.XSize)) / 2) + (6+bmcran_Serenity_13B_icontempstotal.XSize);
            y_pos = Y_5_6;
            GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced26pts);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, x_pos ,y_pos);

            /* draw total Time Icon */
            x_pos = 6;
            y_pos = Y_5_6 - (bmcran_Serenity_13B_icontempstotal.YSize/2);
            GUI_DrawBitmap(&bmcran_Serenity_13B_icontempstotal, x_pos, y_pos);

            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateShowLastDive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
            _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}


