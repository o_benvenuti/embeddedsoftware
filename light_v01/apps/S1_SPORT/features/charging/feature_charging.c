/*
 * feature_charging.C
 *
 *  Created on: 28 sept. 2018
 *      Author: user
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <main.h>
#include <common.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <touchscreen.h>
#include <ds2782_driver.h>
#include <trace.h>
#include <stm32l4xx_hal.h>

#include "feature_charging.h"

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
struct private_charging_ctx {
    WM_HWIN current_window;
    serenity_state_t parent_state;
    uint8_t prev_battery;
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _charging_init_func(void ** priv_data, int parent);
int _charging_timer_func(void * priv_data, int time);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_charging_ctx = {
        .name       = "CHARGING",
        .ini_func   = _charging_init_func,
        .usr_func   = NULL,
        .evt_func   = NULL,
        .tmr_func   = _charging_timer_func,
        .gui_func   = NULL,
};

static struct private_charging_ctx _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _charging_init_func(void ** priv_data, int parent)
{
    int rc;
    uint8_t battery = 0;
    WM_MESSAGE window_msg;
    al_feature_msg_t feat_msg;

    // set initial state
    _private_ctx.prev_battery = 0;
    _private_ctx.parent_state = parent;

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    // create and display BLE window
    _private_ctx.current_window = CreateCharging_Window();

    // get current battery value
    rc = BAT_LoadValue(&battery);
    if ( rc )
    {
        debug_printf("unable to receive data from battery\n");
    }

    // init screen with battery value
    window_msg.MsgId = FEATURE_BAT_UPDATE;
    window_msg.Data.v = battery;
    WM_SendMessage(_private_ctx.current_window, &window_msg);
    WM_Invalidate(_private_ctx.current_window);

    // set feature timer/timeout to 1second
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 999; //ms
    al_feature_send_to_tsk(&feat_msg);

    // return context
    *priv_data = &_private_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _charging_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc;
    uint8_t battery;
    float current = 0.0;
    WM_MESSAGE window_msg;
    al_feature_msg_t feat_msg;
    struct private_charging_ctx * ctx = (struct private_charging_ctx*)priv_data;

    // read current
    rc = BAT_readCurrent(&current);
    if ( rc )
    {
        current = 0.0;
    }

    // if negative current, we are not charging anymore
    if ( current < 0.0 )
    {
        // if not charging anymore
        feat_msg.cmd = CMD_END_STATE;
        feat_msg.data.value = ctx->parent_state;
        al_feature_send_to_tsk(&feat_msg);
        return 0;
    }

    // get current battery value
    rc = BAT_LoadValue(&battery);
    if ( rc )
    {
        debug_printf("unable to receive data from battery\n");
    }

    // redraw only if changed
    if ( ctx->prev_battery != battery )
    {
        window_msg.MsgId = FEATURE_BAT_UPDATE;
        window_msg.Data.v = battery;
        WM_SendMessage(ctx->current_window, &window_msg);
        WM_Invalidate(ctx->current_window);
    }

    // update battery previous value
    ctx->prev_battery = battery;

    return 0;
}

