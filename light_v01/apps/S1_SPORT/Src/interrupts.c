#include <common.h>
#include <main.h>
#include <tim.h>
#include <stm32l4xx_hal.h>
#include <touchscreen.h>
#include <trace.h>

/**
  * @brief  EXTI line detection callback.
  * @param  GPIO_Pin: Specifies the port pin connected to corresponding EXTI line.
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    int rc = 0;
	BaseType_t high_prio_task = pdFALSE;

	if(GPIO_Pin == GPIO_PIN_5) /* if interrupt is from touchscreen */
	{
	    // Was done only when x and y were not 0
        HAL_TIM_Base_Stop_IT(&htim6);
        __HAL_TIM_SET_COUNTER(&htim6, 0);
        HAL_TIM_Base_Start_IT(&htim6);

	    rc = touchscreen_get_xy_coordinates();
		if( rc )
		{
            xSemaphoreGiveFromISR(touchscreen_flag, &high_prio_task);
            HAL_TIM_Base_Stop_IT(&htim6);

            /* disable touchscreen interrupt */
            HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
		}
	}

	if(GPIO_Pin == int1_3D_Pin) /* if interrupt is from Gyroscope */
	{
		HAL_NVIC_DisableIRQ(int1_3D_EXTI_IRQn);
	    xSemaphoreGiveFromISR(flick_flag, &high_prio_task);
	}

	portYIELD_FROM_ISR(high_prio_task);
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM2) {
        HAL_IncTick();
    }

    if (htim->Instance == TIM6) {
        BaseType_t high_prio_task = pdFALSE;

        HAL_TIM_Base_Stop_IT(&htim6);
        xSemaphoreGiveFromISR(touchscreen_flag, &high_prio_task);

        portYIELD_FROM_ISR(high_prio_task);
    }
}
/**
  * @brief  EXTI line detection callback.
  * @param  GPIO_Pin: Specifies the port pin connected to corresponding EXTI line.
  * @retval None
  */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
    (void)hrtc;
}
