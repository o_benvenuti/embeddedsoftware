/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <string.h>

#include <FreeRTOS.h>
#include <task.h>
#include <cmsis_os.h>

#include <accel_lib.h>
#include <common.h>
#include <al_database.h>
#include <al_upgrade.h>
#include <al_feature.h>
#include <ds2782_driver.h>
#include <GUI_serenity.h>
#include <ILI9163_driver.h>
#include <languages.h>
#include <ms5837_driver.h>
#include <stm32l4xx_hal.h>
#include <trace.h>
#include <touchscreen.h>
#include <al_tools.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define MANAGER_TASK_STACK  512
#define GUIUPDT_TASK_STACK  1536

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern al_feature_ctx_t feature_default_ctx;
extern al_feature_ctx_t feature_charging_ctx;
extern al_feature_ctx_t feature_dive_config_ctx;
extern al_feature_ctx_t feature_watch_config_ctx;
extern al_feature_ctx_t feature_ble_ctx;
extern al_feature_ctx_t feature_dive_ctx;

// /!\ array and enum in al_feature's header must be in sync
al_feature_ctx_t * serenity_features[] = {
        &feature_default_ctx,
        &feature_charging_ctx,
        &feature_dive_config_ctx,
        &feature_watch_config_ctx,
        &feature_ble_ctx,
        &feature_dive_ctx,
};

osThreadId ManagerTaskHandle;
StackType_t  ManagerTaskBuffer[MANAGER_TASK_STACK];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId GUIScreenUpdateHandle;
StackType_t  GUIScreenUpdateBuffer[GUIUPDT_TASK_STACK];
osStaticThreadDef_t GUIScreenUpdateControlBlock;

// global because needed by time configuration
// can be improved
TimeType gsLastActivityTime;

bool gSerenityGUIIsInit = false;
static int32_t pressureTab[PRESSURE_TAB_SIZE] = {0};

// freertos static allocation stuff
static StaticTimer_t _standby_timer_hdl;
static StaticTimer_t _FlickRebound_timer_hdl;
osStaticMessageQDef_t _manager_queue_set_handle;
static void * _manager_queue_set_buffer[4];

extern char * serenity_language_pack[][LANGUAGES_MAX_LANGUAGES];

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
// TASKS
void StartManagerTask(void const * argument);
void StartuGUIScreenUpdateTask(void const * argument);

// HELPERS
static void _need_standby(TimerHandle_t xTimer);
static bool _is_dive_started(void);
static bool _is_battery_charging(void);
static void _change_state(int * current, int next, TimerHandle_t stdby_tmr);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */
static void flickReboundTimerCallback(TimerHandle_t xTimer);

__attribute__((weak)) void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
__attribute__((weak)) void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

WM_HWIN CreateIntro_Window(void);

//----------------------------------------------------------------------
// Tasks definitions
//----------------------------------------------------------------------
void StartManagerTask(void const * argument)
{
    (void)argument;
    int rc = 0;
    al_feature_msg_t feat_msg;
    TimerHandle_t stdby_tmr;
    TimerHandle_t flickRebound_tmr;
    QueueSetHandle_t queue_set;
    QueueHandle_t available_queue = NULL, feature_queue = NULL;
    uint32_t tick_last = 0;
    uint32_t time_to_wait = 0;
    int current_state, next_state;
    serenity_context_t context = { 0 };

    // try to reload previous status
    rc = serenity_retrieve_status(&context);
    if ( rc )
    {
        debug_printf("unable to retrieve status\n");
    }

    // init database of dive record
    rc = al_database_diverec_init();
    if ( rc ) {
        debug_printf("unable to initialize dive recording\n");
    }

    // set default values
    gsLastActivityTime = RTC_GetTime();
    current_state = serenity_state_default;
    next_state = serenity_state_default;

    rc = languages_load_package(serenity_language_pack);
    if ( rc )
    {
        debug_printf("unable to load language pack\n");
    }

    rc = languages_set_language(context.language);
    if ( rc )
    {
        debug_printf("unable to set language to %d\n", context.language);
    }

    // wait for GUI to be initialized
    while( gSerenityGUIIsInit == false )
    {
        osDelay(10);	// we'd better add timeout
    }

    uint8_t lum = serenity_get_brigthness();

    // display first time screen
    if( gWakedUpFromStandby == false )
    {
        // this window is deleted by the next feature
        CreateIntro_Window();

        GUI_Delay(10); /* delay to force window draw */
        for(int i = 0 ; i <= (lum > 10 ? lum : 10) ; i++)
        {
            change_BaklightIntensity(i);
            HAL_Delay(10);
        }
        HAL_Delay(2000);
    }
    else
    {
        change_BaklightIntensity(lum > 10 ? lum : 10);
    }

    // create timer that check for standby
    stdby_tmr = xTimerCreateStatic("Standby", STDBY_CHECK_PERIOD, pdTRUE,
    								&current_state, _need_standby, &_standby_timer_hdl);
    if ( ! stdby_tmr ) {
        debug_printf("error while creating timer\n");
    }

    // start standby timer
    if ( pdPASS != xTimerStart(stdby_tmr, 0) ) {
        debug_printf("error while starting standby timer\n");
    }

    // set user configured flick sensitivity
    agm_set_flick_sensitivity(context.flick_sensitivity);

    // create a timer to prevent flick rebound
    flickRebound_tmr = xTimerCreateStatic("flick Rebound", 500, pdFALSE,
                                   0, flickReboundTimerCallback,
								   &_FlickRebound_timer_hdl);
    if ( ! flickRebound_tmr )
    {
        debug_printf("error while creating flick Rebound timer\n");
    }

    // create the communication object
    queue_set = xQueueCreateSetStatic(3, (uint8_t*)&_manager_queue_set_buffer[0],
                                      &_manager_queue_set_handle);

    xQueueAddToSet(flick_flag, queue_set );
    rc = xQueueAddToSet(touchscreen_flag, queue_set );
    if ( rc != pdPASS )
    {
        xQueueReset(touchscreen_flag);
        xQueueAddToSet(touchscreen_flag, queue_set );
    }

    // Initialise feature library
    rc = al_feature_init();
    if(rc)
    {
        debug_printf("feature init failed\n");
    }

    // retrieve feature queue
    rc = al_feature_get_queue(&feature_queue);
    if(rc)
    {
        debug_printf("cannot retrieve feature queue\n");
    }

    xQueueAddToSet(feature_queue, queue_set );

    // start default feature
    rc = al_feature_start(current_state, current_state);
    if(rc)
    {
        debug_printf("unable to start feature\n");
    }

    // update last timeout loop timestamp
    tick_last = HAL_GetTick();

    // main loop
    while(1)
    {
    	// check how much time we have to wait (still)
    	if ( DeltaT_Calculation(tick_last) > 1000 )
    	{
    		time_to_wait = 0;
    	}
    	else
    	{
    		time_to_wait = 1000 - DeltaT_Calculation(tick_last);
    	}

        // wait for user input or system event
        available_queue = (QueueHandle_t) xQueueSelectFromSet(queue_set, time_to_wait);

        /* touchscreen stuff */
        if( available_queue == touchscreen_flag ) {
            // get touchscreen info
            touch_info_t touch_info = get_TouchInfo();
            if ( touch_info.gesture == touch )
            {
                // send position to graphical interface
                GUI_TOUCH_StoreState(touch_info.xy_coord.x,
                                     touch_info.xy_coord.y);

                // tell the GUI we are done
                GUI_TOUCH_StoreState(-1,-1);
            }
            else
            {
                // send user input to features
                feat_msg.cmd = CMD_USR_INPUT;
                feat_msg.data.gesture = touch_info.gesture;
                al_feature_send_to_tsk(&feat_msg);
            }

            // update last activity to prevent standby
            gsLastActivityTime = RTC_GetTime();
            continue;
        }

        // process accelerometer
        if ( available_queue == flick_flag ) {
            feat_msg.cmd = CMD_RCV_FLICK;
            al_feature_send_to_tsk(&feat_msg);

            xSemaphoreTake(flick_flag, 0);
            if ( pdPASS != xTimerStart(flickRebound_tmr, 0) )
            {
                debug_printf("error while starting flick Rebound timer\n");
            }

            continue;
        }

        // check for task change state request
        if ( available_queue == feature_queue )
        {
            next_state = al_feature_get_return_state();
            if ( next_state < 0 )
            {
                next_state = serenity_state_default;
            }

            _change_state(&current_state, next_state, stdby_tmr);
            continue;
        }

        // Does not check for stand-by, charge screen
        // or new dive if we are currently diving
        if ( current_state == serenity_state_dive )
        {
           tick_last = HAL_GetTick();
           continue;
        }

        // timeout event
        if ( ! available_queue )
        {
            // update last timeout loop timestamp
            tick_last = HAL_GetTick();

#ifndef DEBUG_NO_CHARGE_SCREEN
            if ( _is_battery_charging() )
            {
                feat_msg.cmd = CMD_BAT_CHRGE;
                al_feature_send_to_tsk(&feat_msg);

                // paranoia reset in case of inconsistent pressure when charging
                memset(&pressureTab[0], 0, sizeof(pressureTab));
                continue;
            }
#endif  // DEBUG_NO_CHARGE_SCREEN
#ifdef DEBUG_DIVE
            static bool already_done = false;
            if ( ! already_done )
            {
                already_done = true;
                feat_msg.cmd = CMD_DVE_START;
                al_feature_send_to_tsk(&feat_msg);
                continue;
            }
#else
            if ( _is_dive_started() )
            {
                feat_msg.cmd = CMD_DVE_START;
                al_feature_send_to_tsk(&feat_msg);
                continue;
            }
#endif  // DEBUG_DIVE
        }
    }
}

//----------------------------------------------------------------------
void StartuGUIScreenUpdateTask(void const * argument)
{
    (void)argument;


    gSerenityGUIIsInit = GUI_init();
    if ( gSerenityGUIIsInit == false )
    {
        NVIC_SystemReset();
    }

    /* Infinite loop */
    for(;;)
    {
        /* exec GUI tasks */
        GUI_Delay(100); /*ms*/
    }
}

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
__attribute__((weak)) void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
    /* Called by the kernel before it places the MCU into a sleep mode because
       configPRE_SLEEP_PROCESSING() is #defined to PreSleepProcessing().

     NOTE:  Additional actions can be taken here to get the power consumption
      even lower.  For example, peripherals can be turned off here, and then back
      on again in the post sleep processing function.  For maximum power saving
      ensure all unused pins are in their lowest power state. */

    /*
    (*ulExpectedIdleTime) is set to 0 to indicate that PreSleepProcessing contains
    its own wait for interrupt or wait for event instruction and so the kernel vPortSuppressTicksAndSleep
    function does not need to execute the wfi instruction
     */
    *ulExpectedIdleTime = 0;

    HAL_SuspendTick();
    /*Enter to sleep Mode using the HAL function HAL_PWR_EnterSLEEPMode with WFI instruction*/
    HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

//----------------------------------------------------------------------
__attribute__((weak)) void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
    /* Called by the kernel when the MCU exits a sleep mode because
  configPOST_SLEEP_PROCESSING is #defined to PostSleepProcessing(). */

    /* Avoid compiler warnings about the unused parameter. */
    (void) ulExpectedIdleTime;
    HAL_ResumeTick();
}

//----------------------------------------------------------------------
void MX_FREERTOS_Init(void)
{
    /* Create the thread(s) */
    /* definition and creation of ManagerTask */
    osThreadStaticDef(ManagerTask, StartManagerTask,
                      osPriorityAboveNormal, 0, MANAGER_TASK_STACK,
                      ManagerTaskBuffer, &defaultTaskControlBlock);
    ManagerTaskHandle = osThreadCreate(osThread(ManagerTask), NULL);

    /* definition and creation of GUIScreenUpdate */
    osThreadStaticDef(GUIScreenUpdate, StartuGUIScreenUpdateTask,
                      osPriorityNormal, 0, GUIUPDT_TASK_STACK,
                      GUIScreenUpdateBuffer, &GUIScreenUpdateControlBlock);
    GUIScreenUpdateHandle = osThreadCreate(osThread(GUIScreenUpdate), NULL);

}

//----------------------------------------------------------------------
static void _need_standby(TimerHandle_t xTimer)
{
    int * current_state;
    uint8_t rc = 0;
    TimeType curr = RTC_GetTime();

    // get context through timer API
    current_state = (int*)pvTimerGetTimerID(xTimer);
    if ( ! current_state )
    {
        debug_printf("invalid argument\n");
        return;
    }

    debug_printf("CHECK FOR STANDBY\n");

    // don't sleep if charging
    rc = _is_battery_charging();
    if ( rc == true )
    {
        return;
    }

    // if not enough battery , go to bed
    if ( FlagBatteryIsAlmostEmpty() )
    {
        debug_printf("standby for battery\n");
        serenity_standby_with_rtc(30); /* 30 secs rtc delay */
    }

    // if current state is BLE, do not go to sleep
    if( * current_state == serenity_state_ble)
    {
    	return;
    }

    // not diving, inactive for more than 5min and not charging
    if ( get_TimeDiffInSeconds(gsLastActivityTime, curr) > INACTIVITY_TIME_BEFORE_SLEEP )
    {
        debug_printf("standby for inactivity\n");
        serenity_standby_with_acc();
    }
}

//----------------------------------------------------------------------
static bool _is_dive_started(void)
{
    int rc;
    int32_t pressure, temperature;
    uint32_t max = 0, min = 0;

    /* Get depth meter information */
    rc = MS5837_GetPressionAndTemp(&pressure, &temperature);
    if ( rc )
    {
        return false;
    }

    /* if tab is empty, fill it with the same value */
    if ( pressureTab[0] == 0 )
    {
        for(int i = 0 ; i<PRESSURE_TAB_SIZE ; i++)
        {
            pressureTab[i] = pressure;
        }
    }

    /* move all pressure values one step to the left in array */
    for(int i = 0 ; i < PRESSURE_TAB_SIZE-1 ; ++i)
    {
        pressureTab[i] = pressureTab[i+1];
    }

    /* update last one */
    pressureTab[PRESSURE_TAB_SIZE-1] = pressure;

    for(int i = 0 ; i < PRESSURE_TAB_SIZE ; ++i)
    {
        if(pressureTab[i] > pressureTab[max]) max=i;
        if(pressureTab[i] < pressureTab[min]) min=i;
    }

    // if difference between min & max is more than
    // 100mBar (100cm underwater): Immersion detected
    if( ((pressureTab[max] - pressureTab[min]) > 100) && (max > min) )
    {
		// set all tab to the same value as a reference for diving
    	for(uint8_t i=0;i<COUNTOF(pressureTab);i++)
		{
			pressureTab[i] = gStartDiveLevelPressure/100;
		}
        return true;
    }

    // if difference between min & max is less than 10mBar (100m outside water)
    // that means we are outside the water and can update ambiant pressure
    if ( (pressureTab[max] - pressureTab[min]) < 10 )
    {
        gStartDiveLevelPressure = pressure*100;
    }

    return false;
}

//----------------------------------------------------------------------
static bool _is_battery_charging(void)
{
    uint8_t rc = 0;
    float current = 0.0;

    // return not charging on error
    rc = BAT_readCurrent(&current);
    if ( rc )
    {
        return false;
    }

    // if positive current, we are charging
    if ( current > 0.0 )
    {
        return true;
    }

    return false;
}


//----------------------------------------------------------------------
static void _change_state(int * current, int next, TimerHandle_t stdby_tmr)
{
    int rc = 0;
    serenity_state_t return_state;

    // do we need to check ?
    if ( *current == next )
    {
        return;
    }

    // restart activity timer when we get out of dive or charging
    // IMPORTANT do not set dive or charging state as previous state
    if ( (*current == serenity_state_dive) ||
         (*current == serenity_state_charging) )
    {
        gsLastActivityTime = RTC_GetTime();

        // restart standby timer
        if ( pdPASS != xTimerStart(stdby_tmr, 0) ) {
            debug_printf("error while starting standby timer\n");
        }

        return_state = serenity_state_default;
    }
    else
    {
        return_state = *current;
    }

    // stop standby timer when diving or charging
    if ( (next == serenity_state_dive) || (next == serenity_state_charging) )
    {
        xTimerStop(stdby_tmr, 0);
    }

    // stop previous feature
    al_feature_stop();

    // start the new feature (return state as parameter)
    rc = al_feature_start(return_state, next);
    if ( rc ) {
        NVIC_SystemReset();
    }

    // update status
    *current = next;
}

//----------------------------------------------------------------------
static void flickReboundTimerCallback(TimerHandle_t xTimer)
{
    (void)xTimer;

	__HAL_GPIO_EXTI_CLEAR_IT(int1_3D_Pin);
	HAL_NVIC_EnableIRQ(int1_3D_EXTI_IRQn);
}
