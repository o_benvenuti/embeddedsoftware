//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <common.h>

#include <al_ble.h>
#include <al_database.h>
#include <ds2782_driver.h>
#include <GUI_serenity.h>
#include <ILI9163_driver.h>
#include <LSM9DS1_Includes.h>
#include <ms5837_driver.h>
#include <main.h>
#include <rtc.h>
#include <stm32l4xx_hal.h>
#include <trace.h>
#include <touchscreen.h>

#include <FreeRTOSConfig.h>

//----------------------------------------------------------------------
//         GLOBAL VARIABLES COMMON TO ALL FILES
//----------------------------------------------------------------------
/* Depth sensor Global variables */
int32_t gStartDiveLevelPressure = SEALEVELPRESSURE;

/* RTC time global variables */
TimeType gsTimeSettings;

/* global owner infos */
char gFirstName[ICE_NAME_LENGTH] = "First Name";
char gLastName[ICE_NAME_LENGTH] = "LAST NAME";
char gPhoneNumber[ICE_PHONE_LENGTH] = "01 23 45 67 89";
char gNationality[ICE_NATIO_LENGTH] = "FRA";

bool     gWakedUpFromStandby = false;
SemaphoreHandle_t flick_flag = 0;

// idle task static stack and handle
static StaticTask_t _xIdleTaskTCBBuffer;
static StackType_t _xIdleStack[configMINIMAL_STACK_SIZE];

static StaticTask_t _xTimerTaskTCBBuffer;
static StackType_t _xTimerStack[configTIMER_TASK_STACK_DEPTH];

//----------------------------------------------------------------------
// Functions
//----------------------------------------------------------------------

/** Function getstate_STWLC33_INT
 *  This function reads the STWLC33_INT pin state to detect if a loading on going
 *  If STWLC33_INT = '1' then no loading detected else a loading on going
 * returns: Pin state 0 or 1
 */
uint8_t getstate_STWLC33_INT(void)
{
    uint8_t rc = 0;
    rc = HAL_GPIO_ReadPin(STWLC33_INT_GPIO_Port, STWLC33_INT_Pin);
    return rc;
}

//----------------------------------------------------------------------
void shutDownPeripherals(void)
{
    /* Power down LCD */
    LCD_OFF();
    HAL_Delay(1);
    change_BaklightIntensity(0);
    /* Power down AGM */
    LSM9DS1_PowerDown();
    /* power down BLE */
    al_ble_deinit();

    touchscreen_PowerDown();

    //------------ SET PINS PULL UP/PULL DOWN -------------------
    HAL_PWREx_EnablePullUpPullDownConfig();
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_4);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_5);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_A, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_A, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_14);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_15);

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_4);     // CS AG
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_5);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_14);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_15);

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_4);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_5); // INT DISP
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_9);  // RST Disp
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_10); // CS LCD
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_11); // CS Flash
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_12); // CS M
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_13); // INT1 AGM
    /**
     * @WARNING DO NOT set status for pins:
     *           * PC14: RTC crystal
     *           * PC15: RTC crystal
    **/

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_D, PWR_GPIO_BIT_2); // EN Bluetooth

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_14); // enable charger
}

//----------------------------------------------------------------------
void serenity_standby_with_acc(void)
{
    int rc = 0;

    // power down magnetometer and gyroscope
    LSM9DS1_PowerDownMag();
    LSM9DS1_PowerDownGyro();
    LSM9DS1_initAccel();

    //-------------------------------------------
    // Enable accelerometer wake up interrupt
    //-------------------------------------------
    // low threshold to wake up easily
    LSM9DS1_configAccelThs(25, 0, 1, false);
    LSM9DS1_configAccelThs(25, 1, 1, false);
    LSM9DS1_configAccelThs(250, 2, 1, false);

    // interrupt on two axis (as the watch flat has 1G on z-axis)
    rc = LSM9DS1_configAccelInt((XHIE_XL | YHIE_XL) ,false);
    if ( rc < 0 )
    {
        return;
    }

    // actual activation of interrupt
    LSM9DS1_configInt(XG_INT1, INT_IG_XL, INT_ACTIVE_LOW, INT_PUSH_PULL);

    //-------------------------------------------
    // Power down all other devices
    //-------------------------------------------
    // Power down LCD
    LCD_OFF();
    HAL_Delay(1);
    change_BaklightIntensity(0);

    // power down BLE
    al_ble_deinit();

    // and finally touchscreen
    touchscreen_PowerDown();

    //-------------------------------------------
    // set GPIO pins status
    //-------------------------------------------
    HAL_PWREx_EnablePullUpPullDownConfig();
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_4);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_5);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_A, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_A, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_14);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_A, PWR_GPIO_BIT_15);

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullUp  (PWR_GPIO_B, PWR_GPIO_BIT_4);     // CS AG
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_5);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_B, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_14);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_B, PWR_GPIO_BIT_15);

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_0);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_1);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_2);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_3);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_4);
    HAL_PWREx_EnableGPIOPullUp  (PWR_GPIO_C, PWR_GPIO_BIT_5); // INT DISP
    HAL_PWREx_EnableGPIOPullUp(PWR_GPIO_C, PWR_GPIO_BIT_6);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_7);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_C, PWR_GPIO_BIT_8);
    HAL_PWREx_EnableGPIOPullDown  (PWR_GPIO_C, PWR_GPIO_BIT_9);  // RST Disp
    HAL_PWREx_EnableGPIOPullUp  (PWR_GPIO_C, PWR_GPIO_BIT_10); // CS LCD
    HAL_PWREx_EnableGPIOPullUp  (PWR_GPIO_C, PWR_GPIO_BIT_11); // CS Flash
    HAL_PWREx_EnableGPIOPullUp  (PWR_GPIO_C, PWR_GPIO_BIT_12); // CS M
    /**
     * @WARNING DO NOT set status for pins:
     *           * PB13: INT1 AGM
     *           * PB14: RTC crystal
     *           * PB15: RTC crystal
    **/

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_D, PWR_GPIO_BIT_2); // EN Bluetooth

    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_9);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_10);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_11);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_12);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_13);
    HAL_PWREx_EnableGPIOPullDown(PWR_GPIO_G, PWR_GPIO_BIT_14); // enable charger

    // enable wake up from accelerometer interrupt pin
    HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN2_LOW);

    // make sure the interrupt flag is cleared
    __HAL_GPIO_EXTI_CLEAR_IT(int1_3D_Pin);
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

    // go to power saving mode
    HAL_PWREx_DisableSRAM2ContentRetention();
    HAL_PWR_EnterSTANDBYMode();
}

//----------------------------------------------------------------------
void serenity_standby_with_rtc(int sec)
{
    // power down all devices and set GPIO's status
    shutDownPeripherals();

    /* 0xffff - 65535 - 30 secs */
    uint16_t WakeUpCounter;
    if ( sec > 30 )
    {
        WakeUpCounter = 0xFFFF;
    }
    else if ( sec < 1 )
    {
        WakeUpCounter = 2;
    }
    else
    {
        WakeUpCounter = (sec*0xFFFF)/30;
    }

    // wake up with the RTC
    HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, WakeUpCounter,
                                RTC_WAKEUPCLOCK_RTCCLK_DIV16);

    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    HAL_PWREx_DisableSRAM2ContentRetention();
    HAL_PWR_EnterSTANDBYMode();
}

//----------------------------------------------------------------------
void displayDebugScreen(void)
{
    float depth;
    uint32_t x0 = 0;
    uint32_t y0 = 0;
    uint32_t x1 = 0;
    uint32_t y1 = 0;
    uint8_t count;
    int32_t pressure, temperature;

    /* bright up Screen, clear it and print title */
    change_BaklightIntensity(100);

    get_PressureTempDepth(&pressure,&temperature,&depth,
            gStartDiveLevelPressure);

    /* Get time from RTC */
    TimeType Time = RTC_GetTime();
    GUI_SetColor(GUI_MAKE_COLOR(0x00C0C0C0));
    GUI_DispHexAt(Time.Hours,8,(0*8),2);
    GUI_DispChar(':');
    GUI_DispHex(Time.Minutes,2);
    GUI_DispChar(':');
    GUI_DispHex(Time.Seconds,2);
    switch(Time.WeekDay)
    {
        case 1  : GUI_DispString(" Lun ");break;
        case 2  : GUI_DispString(" Mar ");break;
        case 3  : GUI_DispString(" Mer ");break;
        case 4  : GUI_DispString(" Jeu ");break;
        case 5  : GUI_DispString(" Ven ");break;
        case 6  : GUI_DispString(" Sam ");break;
        case 7  : GUI_DispString(" Dim ");break;
        default : GUI_DispString(" ERR ");break;
    }
    GUI_DispHex(Time.Date,2);
    GUI_DispChar('/');
    GUI_DispHex(Time.Month,2);
    GUI_DispString("/20");
    GUI_DispHex(Time.Year,2);

    /* Display depth value */
    GUI_SetColor(GUI_MAKE_COLOR(0x00FF3333));
    GUI_DispStringAt("Profondeur : ",8, (1*8));
    //	GUI_DispFloatFix(gDepth_value, 6, 2);
    GUI_DispString("m");

    /* Display pressure value */
    GUI_SetColor(GUI_MAKE_COLOR(0x00FF9933));
    GUI_DispStringAt("Pression   : ",8, (2*8));
    GUI_DispDec(pressure,4);
    GUI_DispString("mBar");

    /* Display temperature value */
    GUI_SetColor(GUI_MAKE_COLOR(0x00FFFF33));
    GUI_DispStringAt("Temp 1     : ",8, (3*8));
    GUI_DispDec(temperature,2);
    GUI_DispString("�C");

    float BatTemperatureValue = 0.0f;

    BAT_readTemperature(&BatTemperatureValue);

    GUI_SetColor(GUI_MAKE_COLOR(0x0099FF33));
    GUI_DispStringAt("Temp 2     : ",8, (4*8));
    GUI_DispDec(BatTemperatureValue,2);
    GUI_DispString("�C");

    /* touchscreen coordinates */
    touchscreen_get_coordinates(&count, &x0, &y0, &x1, &y1);
    GUI_SetColor(GUI_MAKE_COLOR(0x0033FF99));
    GUI_DispStringAt("Touch : X0 : ",8,(6*8));
    GUI_DispDec(x0,3);
    GUI_DispString(" Y0 : ");
    GUI_DispDec(y0,3);

    float VoltageValue = 0.0f;
    float CurrentValue = 0.0f;

    BAT_readVoltage(&VoltageValue);
    BAT_readCurrent(&CurrentValue);

    GUI_SetColor(GUI_MAKE_COLOR(0x006666FF));
    GUI_DispStringAt("V  :",8, (10*8));
    GUI_DispFloatFix(VoltageValue, 4, 2);
    GUI_DispStringAt(" | I:",50, (10*8));
    GUI_DispFloatFix(CurrentValue, 6, 1);
    GUI_DispString("mA");
    GUI_SetColor(GUI_MAKE_COLOR(0x00B266FF));

    uint8_t BatteryLoadValue;
    BAT_LoadValue(&BatteryLoadValue);
    GUI_DispStringAt("RSRC:", 95, (13*8));
    GUI_DispDec(BatteryLoadValue,3);
    GUI_DispString("%");

    /* estimated battery level */
    GUI_SetColor(GUI_WHITE);
    GUI_DispStringAt("ACR:", 8, (14*8));
    GUI_DispHex(BAT_readAcrReg(),4);

    /* Booted bank */
    GUI_SetColor(GUI_WHITE);
    GUI_DispStringAt("Boot bank : ",8, (15*8));
    GUI_DispDec((READ_BIT(SYSCFG->MEMRMP, SYSCFG_MEMRMP_FB_MODE) ? 2 : 1), 1);
}

//----------------------------------------------------------------------
void displayFilesInFlash(void)
{
    GUI_DispStringAt("Files in flash is currently out of service :(",8, 16);
}

//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
void displayDiveDebugScreen(void)
{
    int x, y;
    float depth;
    uint32_t x0 = 0;
    uint32_t y0 = 0;
    uint32_t x1 = 0;
    uint32_t y1 = 0;
    int32_t pressure, temperature;
    float BatTemperatureValue = 0.0f;

    GUI_SetColor(GUI_WHITE);
    GUI_SetTextMode(GUI_TM_TRANS);
    change_BaklightIntensity(100);

    /* pressure */
    get_PressureTempDepth(&pressure,&temperature,&depth,
            gStartDiveLevelPressure);

    x = 8;
    y = (0*25) - 9;
    GUI_GotoXY(x,y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispDecSpace(pressure,4);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoY(y+13);
    GUI_DispString("mBar");

    /* temperature from depth meter */
    x = 130;
    GUI_GotoXY(x,y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispDec(temperature,2);
    GUI_GotoY(y+13);
    GUI_SetFont(GUI_FONT_13HB_1);
    GUI_DispString("�");
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_DispString("C p");

    /* depth */
    x = 8;
    y = (1*25) - 9;
    GUI_GotoXY(x,y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispFloatFix(depth, 6, 2);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoY(y+13);
    GUI_DispString("m");

    /* temp fuel gauge */
    BAT_readTemperature(&BatTemperatureValue);
    x = 130;
    GUI_GotoXY(x,y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispFloatFix(BatTemperatureValue,3,1);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoY(y+13);
    GUI_SetFont(GUI_FONT_13HB_1);
    GUI_DispString("�");
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_DispString("C b");

    /* vit remont�e */
    x = 8;
    y = (2*25) - 9;
    GUI_GotoXY(x,y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    //	GUI_DispFloatFix(gRising_Speed, 4, 1);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoY(y+13);
    GUI_DispString("m/min");

    /* boussole */
    x = 110;
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_GotoXY(x,y);
    uint16_t heading = COMPASS_get_heading();
    GUI_DispDecSpace(heading,3);
    GUI_GotoY(y+13);
    GUI_SetFont(GUI_FONT_13HB_1);
    GUI_DispString("�");

    GUI_SetPenSize(1);
    GUI_SetColor(GUI_WHITE);
    GUI_DrawLine(0, 77, LCD_GetXSize(), 77);
    GUI_DrawLine(101, 0, 101, 77);

    /* tactile */
    touchscreen_get_coordinates(NULL, &x0, &y0, &x1, &y1);
    x = 25;
    y = (3*25) - 8;
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoXY(x,y+13);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispString("X0 ");
    GUI_GotoY(y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispDecSpace(x0,3);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoXY(100,y+13);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispString("Y0 ");
    GUI_GotoY(y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispDecSpace(y0,3);
    x = 25;
    y = (4*25) - 8;
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoXY(x,y+13);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispString("X1 ");
    GUI_GotoY(y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispDecSpace(x1,3);
    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoXY(100,y+13);
    GUI_SetTextAlign(GUI_TA_RIGHT);
    GUI_DispString("Y1 ");
    GUI_GotoY(y);
    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
    GUI_DispDecSpace(y1,3);
    GUI_SetColor(GUI_MAKE_COLOR(0x00FF0000));
    if((x0!=0)&&(y0!=0))GUI_FillCircle(x0,y0,5);
    if((x1!=0)&&(y1!=0))GUI_FillCircle(x1,y1,5);



}

void DEBUG_display_mA(void)
{
    float Current = 0.0f;
    GUI_SetColor(GUI_GREEN);
    GUI_SetTextMode(GUI_TEXTMODE_NORMAL);
    GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);

    BAT_readCurrent(&Current);

    GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
    GUI_GotoXY(X_1_2-10,Y_1_2);
    GUI_DispFloatFix(Current, 5, 1);
    GUI_DispString("mA");
}

//----------------------------------------------------------------------
int serenity_save_status(serenity_context_t * ctx)
{
    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid parameter\n");
        return -1;
    }

    /**
     * So far we overwrite with global variables
     * TODO replace global variables
     **/
    // fill the context struct
    memcpy(ctx->FirstName,           gFirstName, sizeof(gFirstName));
    memcpy(ctx->LastName,            gLastName, sizeof(gLastName ));
    memcpy(ctx->PhoneNumber,         gPhoneNumber, sizeof(gPhoneNumber));
    memcpy(ctx->Nationality ,        gNationality, sizeof(gNationality));
    ctx->h12                       = RTC_is12hMode();
    ctx->mmdd                      = RTC_ismmddMode();

    return al_database_set_context(SERENITY_CONTEXT_MAGIC, ctx,
                                   sizeof(serenity_context_t));
}

//----------------------------------------------------------------------
int serenity_retrieve_status(serenity_context_t * ctx)
{
    int rc;
    uint32_t magic;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid parameter\n");
        return -1;
    }


    // read back the context from the flash
    rc = al_database_get_context(&magic, ctx, sizeof(serenity_context_t));

    // if unable to read context or invalid magic
    if ( rc || (SERENITY_CONTEXT_MAGIC != magic) )
    {
        debug_printf("unable to reload previous status: set default\n");
        serenity_set_default(ctx);

        // save into flash
        rc = serenity_save_status(ctx);
        if ( rc )
        {
            debug_printf("unable to initialise and save context\n");
        }
    }

    // set corresponding variables
    RTC_Set12hMode (               ctx->h12);
    RTC_SetmmddMode(               ctx->mmdd);

    memcpy(gFirstName,             ctx->FirstName, sizeof(gFirstName));
    memcpy(gLastName,              ctx->LastName, sizeof(gLastName)) ;
    memcpy(gPhoneNumber,           ctx->PhoneNumber, sizeof(gPhoneNumber));
    memcpy(gNationality,           ctx->Nationality,  sizeof(gNationality));

    return 0;
}

//----------------------------------------------------------------------
int serenity_get_brigthness(void)
{
    int rc;
    uint32_t magic;
    serenity_context_t tmp;

    // open dedicated file
    rc = al_database_get_context(&magic, &tmp, sizeof(serenity_context_t));
    if ( rc || (SERENITY_CONTEXT_MAGIC != magic) )
    {
        debug_printf("context not found\n");
        // return medium brightness if invalid
        return 100;
    }


    return tmp.Brightness;
}

//----------------------------------------------------------------------
void serenity_set_default(serenity_context_t * context)
{
    /*
     * Depth meter
     */
    context->depth_params.stop_nb   = NB_PALIERS_DEFAULT;
    context->depth_params.depth_max = MAX_DEPTH_DEFAULT;
    context->depth_params.dive_duration = DIVE_TIME_DEFAULT;

    // stops configuration
    for( int i = 0;i < NB_PALIERS_MAX; i++ )
    {
        context->depth_params.dive_stops[i].StopDepth    = STOP_DEPTH_DEFAULT;
        context->depth_params.dive_stops[i].StopDuration = STOP_TIME_DEFAULT;
        context->depth_params.dive_stops[i].Ascent_speed = ASC_SPEED_DEFAULT;
    }

    /*
     * Deco algo
     */
    context->deco_params.max_speed      = ASC_SPEED_DEFAULT;
    context->deco_params.depth_max      = MAX_DEPTH_DEFAULT;
    context->deco_params.dive_duration  = DIVE_TIME_DEFAULT;
    context->deco_params.o2_ratio       = O2_RATIO_DEFAULT;
    context->deco_params.he_ratio       = HE_RATIO_MIN;
    context->deco_params.ppo2           = PPO2_DEFAULT;
    context->deco_params.safety_factor  = SAFETY_FACTOR_DEFAULT;

    /*
     * Free dive
     */
    context->free_params.depth_max              = MAX_DEPTH_DEFAULT;
    context->free_params.rest_time_to_end       = IDLE_TIME_DEFAULT;

    /*
     * System
     */
    context->metric_system       = SERENITY_METRIC_DEFAULT;
    context->language            = SERENITY_LANGUAGE_DEFAULT;
    context->option_code         = SERENITY_OPTION_CODE_DEFAULT;
    context->compass_declination = SERENITY_DECLINATION_DEFAULT;
    context->Brightness          = SERENITY_BRIGTHNESS_DEFAULT;
    context->flick_sensitivity   = DEFAULT_FLICK_SENSITIVITY;
    context->dive_mode           = Mode_DecoAlgo;
    memset(&context->last_dive_date, 0, sizeof(context->last_dive_date));

}

//----------------------------------------------------------------------
void serenity_factory_reset(void)
{
    int rc = 0;
    serenity_context_t context;

    // try to get context or reset it on failure
    rc = serenity_retrieve_status(&context);
    if ( rc )
    {
        memset(&context, 0, sizeof(serenity_context_t));
    }

    // set default values
    serenity_set_default(&context);

    // write default values in flash
    rc = serenity_save_status(&context);
    if ( rc )
    {
        debug_printf("cannot save factory context\n");
    }

    // erase all dive record
    rc = al_database_diverec_reset();
    if ( rc )
    {
        debug_printf("unable to reset diverec database\n");
    }

    // once all data have been reset: reboot the chip
    NVIC_SystemReset();
}

//----------------------------------------------------------------------
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName )
{
    (void)xTask;
    (void)pcTaskName;

    NVIC_SystemReset();
}


//----------------------------------------------------------------------
void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
        StackType_t **ppxIdleTaskStackBuffer,
        uint32_t *pulIdleTaskStackSize )
{
    *ppxIdleTaskTCBBuffer = &_xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &_xIdleStack[0];
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

//----------------------------------------------------------------------
void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer,
        StackType_t **ppxTimerTaskStackBuffer,
        uint32_t *pulTimerTaskStackSize )
{
    *ppxTimerTaskTCBBuffer = &_xTimerTaskTCBBuffer;
    *ppxTimerTaskStackBuffer = &_xTimerStack[0];
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

