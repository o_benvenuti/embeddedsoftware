
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include <common.h>
#include <al_upgrade.h>
#include <ds2782_driver.h>
#include <GUI_serenity.h>
#include <LSM9DS1_Includes.h>
#include <ms5837_driver.h>
#include <eflash.h>
#include <touchscreen.h>
#include <trace.h>

#include <stm32l4xx_hal.h>
#include <dma.h>
#include <cmsis_os.h>
#include <crc.h>
#include <i2c.h>
#include <rtc.h>
#include <spi.h>
#include <tim.h>
#include <usart.h>
#include <gpio.h>

/* USER CODE END Includes */


/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
/* Pressure and temperature parameters */
/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
/*  */
/* -------------------------------------------------------------- */

/* USER CODE END PV */
static StaticSemaphore_t _flick_semaphore_hdl;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void)
{
    /* USER CODE BEGIN 1 */
    // must absolutely be called first as if
    // we are using double bank functionality
    // we need to play with vector table
    al_upgrade_init();

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */
    /* @TODO For now, all STM32 is cadenced at 80MHz, needs to be reduced in the future.
     * however, LCD needs high frequency for fluid display. Maybe we should change the frequency
     * dynamically in the future.
     */
    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */
    /* the "#if 0" is used to isolate the generated lines by CubeMX
     * and edit them in the "#else if" inside the "USER CODE BEGIN/END
     * so the edited code is not wiped out when code is regenerated by CubeMX
     * It's done here to init the interrupts after user_init and FreeRTOS_Init*/
#if 0
    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_CRC_Init();
    MX_I2C1_Init();
    MX_I2C2_Init();
    MX_RTC_Init();
    MX_SPI2_Init();
    MX_TIM3_Init();
    MX_TIM6_Init();
    MX_USART1_UART_Init();

    /* Initialize interrupts */
    MX_NVIC_Init();
    /* USER CODE BEGIN 2 */
#else
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DMA_Init();
    HAL_Delay(1000); /* ms */
    MX_I2C1_Init();
    MX_I2C2_Init();
    MX_SPI2_Init();
    MX_USART1_UART_Init();
    MX_RTC_Init();
    MX_TIM3_Init();
    MX_TIM6_Init();
    MX_CRC_Init();
#endif

#ifdef DEBUG
    initialise_monitor_handles();
    debug_printf("SERENITY - Start debug session\n");
    debug_printf("PLATFORMID : %d\tPROJECT NAME : %s\n",PLATFORMID,PROJECT_NAME);
#endif /* DEBUG */

    user_Init();

#if 0
    /* USER CODE END 2 */

    /* Call init function for freertos objects (in freertos.c) */
    MX_FREERTOS_Init();

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
#else

    /* Call init function for freertos objects (in freertos.c) */
    MX_FREERTOS_Init();


    /* Init the STemWin GUI Library */
    debug_printf("STemWin init Start\n");
    /* Set create flag to use automatically memory devices, this is one way to avoid flickering. */
    WM_SetCreateFlags(WM_CF_MEMDEV);
    GUI_Init();


    if ( ! GUI_IsInitialized() )
    {
        debug_printf("STemWin init error! \n");
    }
    else
    {
        debug_printf("STemWin init done! \n" );
    }

    //while(1){ displayDebugScreen(); HAL_Delay(100);} /* uncomment for debug screen without freertos */

    /* Initialize interrupts */
    MX_NVIC_Init();
    /* EXTI interrupt init*/

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
#endif
    while (1)
    {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
        /* We should never get here as control is now taken by the scheduler */

    }
    /* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure LSE Drive Capability
     */
    HAL_PWR_EnableBkUpAccess();

    __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 1;
    RCC_OscInitStruct.PLL.PLLN = 10;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
    RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Initializes the CPU, AHB and APB busses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
            |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
            |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C2;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the main internal regulator output voltage
     */
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the Systick interrupt time
     */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
     */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/**
 * @brief NVIC Configuration.
 * @retval None
 */
static void MX_NVIC_Init(void)
{
    /* FLICK interrupt configuration */
    __HAL_GPIO_EXTI_CLEAR_IT(int1_3D_Pin);
    HAL_NVIC_SetPriority(int1_3D_EXTI_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(int1_3D_EXTI_IRQn);
    /* EXTI9_5_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    /* RTC_WKUP_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
}

/* USER CODE BEGIN 4 */
/* user functions for drivers & peripheral init */
void user_Init(void)
{
    uint8_t rc = 0;
    float current = 0.0;
    gWakedUpFromStandby = false;

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    // initialize mutex used to protect
    // battery and temp/pressure sensors
    flick_flag = xSemaphoreCreateBinaryStatic(&_flick_semaphore_hdl);

    // initialize flick interrupt primitive
    xSemaphoreTake(flick_flag, 0);

    /* Init Fuel Gauge */
    if (BAT_Init(&hi2c1) != 0)
    {
        debug_printf("Fuel Gauge - Failed to communicate with DS2782\n");
    }

    // check we have enough battery to boot
    rc = BAT_readCurrent(&current);
    if ( rc )
    {
        // set to 0 on error to prevent booting
        current = 0.0;
    }

    // If Battery is almost empty (Vbat <= 3V) and we are not charging
    if ( FlagBatteryIsAlmostEmpty() && ( current <= 0) )
    {
        serenity_standby_with_rtc(30); /* seconds */
    }

    /* Check and handle if the system was resumed from StandBy mode */
    if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)
    {
        /* clear Standby flag */
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

        /* Clear all related wakeup flags */
        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

        // set the flag to notify app that we wakeup from reset
        gWakedUpFromStandby = true;
    }

    HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN5_LOW);
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
    /* Clear all related wakeup flags */
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

    /* init touchscreen */
    touchscreen_Init(); /* i2c2 */

    /* Flash Init (SPI2)*/
    eflash_discover();

    /* init AGM */
    agm_init(); /* spi2 */
    LSM9DS1_PowerDown();

    /* start PLL for backlight */
    HAL_TIM_Base_Start(&htim3);
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);

    /* Init PPressure and temperature sensor */
    MS5837_Init(&hi2c1);
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
    (void)file;
    (void)line;

    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    // printf("errror occured\n\tFile : %s\n\tLine : %d\n\n",file,line);
    while(1)
    {
        NVIC_SystemReset();
    }
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 
    (void)file;
    (void)line;

    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
    ex: // printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
