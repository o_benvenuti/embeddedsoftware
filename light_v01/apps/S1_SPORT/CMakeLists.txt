# TO BE REWORKED
ADD_SUBDIRECTORY (features)

include_freertos()
include_drivers(ACC LCD DEPTH_METER RTC TOUCHSCREEN FUEL_GAUGE EFLASH)
include_libraries(BLE GUI STemWin FEATURE DATABASE UPGRADE TRACE TOOLS LANGUAGES)

# FEATURE INCLUDE DIRECTORIES (WILL BE INTEGRATED)
INCLUDE_DIRECTORIES (${CMAKE_CURRENT_SOURCE_DIR}/Inc
                     ${CMAKE_SOURCE_DIR}/libraries/STemWin/Inc
                     ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config)

# SEARCH FOR PRECOMPILED LIBRARIES
FIND_LIBRARY(libdecolib NAMES libdecolib.a HINTS ${CMAKE_SOURCE_DIR}/libraries/DECO/)
FIND_LIBRARY(libstemwin NAMES STemWin540_CM4_OS_GCC.a HINTS ${CMAKE_SOURCE_DIR}/libraries/STemWin/Lib)

SET_PROPERTY (SOURCE ${PLATFORM_SOURCE_FILES}*.s PROPERTY LANGUAGE C)

# COMPILE PROJECT MAIN SOURCES
ADD_EXECUTABLE (S1_SPORT Src/main.c
                         Src/common.c
                         Src/freertos.c
                         Src/interrupts.c
                         Src/babel.c
                         Src/00_00_Intro_WindowDLG.c
                         ${CMAKE_SOURCE_DIR}/libraries/STemWin/OS/GUI_X.c
                         ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config/GUIConf.c
                         ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config/LCDConf.c
                         ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config/GUIDRV.c
                         ${PLATFORM_SOURCE_FILES})
        
# LINK LIBRARIES (CREATE DEPENDENCIES)
TARGET_LINK_LIBRARIES (S1_SPORT SPORT_FEATURE_DIVE 
                                SPORT_FEATURE_DEFAULT
                                SPORT_FEATURE_DIVE_CONFIG
                                SPORT_FEATURE_WATCH_CONFIG
                                SPORT_FEATURE_CHARGING
                                SPORT_FEATURE_BLE
                                LANGUAGES
                                freertos
                                ACC
                                DEPTH_METER
                                EFLASH
                                FUEL_GAUGE
                                RTC
                                TOUCHSCREEN
                                BLE
                                DATABASE
                                DECO
                                FEATURE
                                GUI
                                ${libstemwin}
                                ${libdecolib}
                                LCD
                                TOOLS
                                UPGRADE
                                MAGCAL)

# SET LINKER OPTIONS
SET_TARGET_PROPERTIES (S1_SPORT
                       PROPERTIES 
                       LINK_FLAGS "-T${CMAKE_SOURCE_DIR}/STM32L486JGYx_FLASH.ld -Wl,--Map -Wl,serenity_test.map -Wl,--gc-sections -lc -lrdimon")

# CONVERT ELF FILE TO BINARY FORMAT
ADD_CUSTOM_COMMAND (TARGET S1_SPORT POST_BUILD
                    COMMAND ${objcopy} 
                    ARGS -O binary 
                    S1_SPORT S1_SPORT.bin
                    COMMENT "Converting ELF to BIN" VERBATIM)

# DISPLAY BINARY SIZE
ADD_CUSTOM_COMMAND (TARGET S1_SPORT POST_BUILD
                    COMMAND ${objsize} 
                    ARGS S1_SPORT)
                    