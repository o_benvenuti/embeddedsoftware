include_freertos()
include_drivers(DEPTH_METER FUEL_GAUGE EFLASH ACC TOUCHSCREEN RTC LCD)
include_libraries(TRACE BLE TOOLS)

SET_PROPERTY (SOURCE ${PLATFORM_SOURCE_FILES}*.s PROPERTY LANGUAGE C)

ADD_EXECUTABLE (UNIT_TEST Src/main.c
                          ${PLATFORM_SOURCE_FILES})

TARGET_LINK_LIBRARIES (UNIT_TEST RTC                                 
                                 freertos
                                 ACC
                                 DEPTH_METER
                                 EFLASH
                                 FUEL_GAUGE
                                 TOUCHSCREEN
                                 MAGCAL
                                 DATABASE
                                 BLE
                                 TOOLS
                                 LCD)
                     
SET_TARGET_PROPERTIES (UNIT_TEST
                       PROPERTIES 
                       LINK_FLAGS "-T${CMAKE_SOURCE_DIR}/STM32L486JGYx_FLASH.ld -Wl,--Map -Wl,UNIT_TEST.map -Wl,--gc-sections -lc -lrdimon")

ADD_CUSTOM_COMMAND (TARGET UNIT_TEST POST_BUILD
                    COMMAND ${objcopy} 
                    ARGS -O binary 
                    UNIT_TEST UNIT_TEST.bin
                    COMMENT "Converting ELF to BIN" VERBATIM)

ADD_CUSTOM_COMMAND (TARGET UNIT_TEST POST_BUILD
                    COMMAND ${objsize} 
                    ARGS UNIT_TEST)
                    