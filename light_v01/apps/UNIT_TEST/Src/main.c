/**
 * @file:  main.c
 *
 * @brief
 *
 * @date   28/01/2019
 *
 * <b>Description:</b>\n
 *    Main routine that initialize the MCU
 *    And run all different module test
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <cmsis_os.h>
#include <stm32l4xx_hal.h>
#include <crc.h>
#include <i2c.h>
#include <rtc.h>
#include <spi.h>
#include <tim.h>
#include <usart.h>
#include <gpio.h>
#include <trace.h>
#include "ms5837_driver.h"
#include "ds2782_driver.h"
#include "LSM9DS1.h"
#include "touchscreen.h"
#include "al_ble.h"
#include "ILI9163_driver.h"

#include <eflash.h>
#include <al_tools.h>

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
void SystemClock_Config(void);
#ifdef DEBUG
	extern void initialise_monitor_handles(void);
#endif
int testcapteurms5837(void);
int testfuelgauge(void);
int progfuelgauge(void);
int testexternalflash(void);
int testtouchscreen(void);
int testble(void);
int testagm(void);

void cfLocalEnterAppMode(void);
void cfLocalEnterTestMode(void);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int main(void)
{
	volatile uint64_t UID[2] = {0};
    char buff[16] = {0};
	uint8_t error_counter = 0;


	// Reset of all peripherals
    // Initializes the Flash interface and the Systick.
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals minus usart */
	MX_GPIO_Init();
	MX_I2C1_Init();
	MX_I2C2_Init();
	MX_SPI2_Init();
	MX_RTC_Init();
	MX_TIM3_Init();
	MX_TIM6_Init();
	MX_CRC_Init();

#ifdef DEBUG
	initialise_monitor_handles();
#endif /* DEBUG */

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* Clear all related wakeup flags */
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

	// user notification
#ifdef DEBUG
	printf("Test Software Initialization    : OK\n");
	printf("PLATFORM ID                     : %d\n",PLATFORMID);
	printf("UNIT TEST                       : START\n");
#endif /*DEBUG*/

	//******************************* UID *************************************
	// when done, edit bool to 0 to continue code
write_uid:
	if(al_tools_write_uid(&UID[0]))
	{
#ifdef DEBUG
		printf("PROG UID                        : ERROR\n");
#endif /*DEBUG*/
		error_counter++;
	}
	else
	{
#ifdef DEBUG
		printf("PROG UID                        : OK\n");
#endif /*DEBUG*/
	}
	al_tools_read_uid(&UID[0]);
	snprintf(buff, 13, "%s", (char *)&UID[0]);
#ifdef DEBUG
	printf("READ UID                        : %s\n",buff);
#endif /*DEBUG*/
	//*************************************************************************

	//****************************** BLE **************************************
	// Reset module and put into test mode to allow uart programming
	cfLocalEnterTestMode();
	// when done, edit bool to 0 to continue code
flash_ble:
	cfLocalEnterAppMode();
	MX_USART1_UART_Init();
#ifdef DEBUG
	printf("PROG BLE                        : OK\n");
#endif /*DEBUG*/
	//*************************************************************************

	//****************************** UNIT TEST ********************************
    if(testexternalflash()){error_counter++;}
    if(testfuelgauge())    {error_counter++;}
    if(progfuelgauge())    {error_counter++;}
    if(testagm())          {error_counter++;}
    if(testcapteurms5837()){error_counter++;}
    if(testtouchscreen())  {error_counter++;}
    if(testble())          {error_counter++;}

    if(error_counter)
    {
#ifdef DEBUG
    	printf("\nUNIT TEST RESULTS               : %d ERROR(S)\n",error_counter);
#endif /*DEBUG*/
    }
    else
    {
#ifdef DEBUG
    	printf("\nUNIT TEST RESULTS               : OK\n");
#endif /*DEBUG*/
    }
	//*************************************************************************

    HAL_Delay(1000);

test_end:
	while(1)
	{
	}
}

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------

int testagm(void)
{
	int rc=-1;

	rc = agm_init();
	if( rc != 0 )
	{
#ifdef DEBUG
		printf("TEST AGM (COMMUNICATION)        : ERROR\n");
		printf("PROG UID                        : DONE\n");
#endif /*DEBUG*/
		return rc;
	}
#ifdef DEBUG
	printf("TEST AGM (COMMUNICATION)        : OK\n");
	printf("TEST AGM                        : OK\n");
#endif /*DEBUG*/
	return rc;
}

//----------------------------------------------------------------------
int testble(void)
{
	uint8_t rc;

	rc=al_ble_init();
	if( rc != 0 )
	{
#ifdef DEBUG
		printf("TEST BLE (COMMUNICATION)        : ERROR\n");
#endif /*DEBUG*/
		return rc;
	}

#ifdef DEBUG
	printf("TEST BLE (COMMUNICATION)        : OK\n");
	printf("TEST BLE                        : OK\n");
#endif /*DEBUG*/

	return rc;
}

//----------------------------------------------------------------------
int testtouchscreen(void)
{
	uint8_t value;

	if( touchscreen_Init()==-1 )
	{
#ifdef DEBUG
		printf("TEST TOUCHSCREEN (COMMUNICATION): ERROR\n");
#endif /*DEBUG*/
		return -1;
	}

	touchscreen_ReadReg(TOUCHSCREEN_REG_T_TO_IDLE, &value);

	if( value != 0x00 )
	{
#ifdef DEBUG
		printf("TEST TOUCHSCREEN (COMMUNICATION): ERROR\n");
#endif /*DEBUG*/
		return -1;
	}

#ifdef DEBUG
	printf("TEST TOUCHSCREEN (COMMUNICATION): OK\n");
	printf("TEST TOUCHSCREEN                : OK\n");
#endif /*DEBUG*/

	return 0;
}

//----------------------------------------------------------------------
int testexternalflash(void)
{
    int rc = 0;
    int8_t wbuffer[8] = { 0xA5, 0xA5, 0x00, 0x00, 0xBE, 0xEF, 0x5A, 0x5A };
    uint8_t rbuffer[8];

    rc = eflash_discover();
    if ( rc )
    {
#ifdef DEBUG
        printf("TEST FLASH (DISCOVER)           : ERROR\n");
        return -1;
#endif /*DEBUG*/
   }
    else
    {
#ifdef DEBUG
        printf("TEST FLASH (DISCOVER)           : OK\n");
#endif /*DEBUG*/
    }

    rc = eflash_device_availables();
#ifdef DEBUG
    printf("TEST FLASH FOUND DEVICES        : %02d \n", rc);
#endif /*DEBUG*/

    //--------------------------------------------
    // /!\ So far, we only have one device
    //     per project then always use the first
    //--------------------------------------------

    // Attempt to erase the whole chip
    rc = eflash_erase_sector(0, 0x10000);
    if ( rc )
    {
#ifdef DEBUG
        printf("TEST FLASH (ERASE CHIP)         : ERROR\n");
#endif /*DEBUG*/
        return -1;
    }
    else
    {
#ifdef DEBUG
        printf("TEST FLASH (ERASE CHIP)         : OK\n");
#endif /*DEBUG*/
    }

    // attempt to program 8 bytes
    rc = eflash_prog(0, 0x10080, &wbuffer[0], 8);
    if ( rc )
    {
#ifdef DEBUG
        printf("TEST FLASH (WRITE)              : ERROR\n");
#endif /*DEBUG*/
        return -1;
    }
    else
    {
#ifdef DEBUG
        printf("TEST FLASH (WRITE)              : OK\n");
#endif /*DEBUG*/
    }

    memset(&rbuffer[0], 0 ,sizeof(rbuffer));

    // attempt to read the same area
    rc = eflash_read(0, 0x10080, &rbuffer[0], 8);
    if ( rc )
    {
#ifdef DEBUG
        printf("TEST FLASH (READ)               : ERROR\n");
#endif /*DEBUG*/
        return -1;
    }
    else
    {
#ifdef DEBUG
        printf("TEST FLASH (READ)               : OK\n");
#endif /*DEBUG*/
    }

    // compare data
    if( memcmp(wbuffer, rbuffer, sizeof(wbuffer)) )
    {
#ifdef DEBUG
        printf("TEST FLASH (DATA CHECK)         : ERROR\n");
#endif /*DEBUG*/
        return -1;
    }
    else
    {
#ifdef DEBUG
        printf("TEST FLASH (DATA CHECK)         : OK\n");
#endif /*DEBUG*/
   }

#ifdef DEBUG
    printf("TEST FLASH                      : OK\n");
#endif /*DEBUG*/

    return 0;
}

int testfuelgauge(void)
{
	float current=0.0;
	float voltage=0.0;

	//Fuel gauge initialization
	if(BAT_Init(&hi2c1)==-1)
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (COMMUNICATION) : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (COMMUNICATION) : OK\n");
#endif /*DEBUG*/
	}


	//Battery current read and value test
	BAT_readCurrent(&current);
	if( (current<-100) || (current>800) )
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (CURRENT VALUE) : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (CURRENT VALUE) : OK\n");
#endif /*DEBUG*/
	}

	//Battery Voltage read
	BAT_readVoltage(&voltage);
	if( (voltage<2.8) || (voltage>4.3) )
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (VOLTAGE VALUE) : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST FUEL GAUGE (VOLTAGE VALUE) : OK\n");
#endif /*DEBUG*/
	}


#ifdef DEBUG
	printf("TEST FUEL GAUGE                 : OK\n");
#endif /*DEBUG*/
	return 0;
}

int progfuelgauge(void)
{
	// Fuel gauge Hardware Init
	if(BAT_hwInit())
	{
#ifdef DEBUG
		printf("PROG FUEL GAUGE HARDWARE_INIT   : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
#ifdef DEBUG
	printf("PROG FUEL GAUGE HARDWARE_INIT   : OK\n");
#endif /*DEBUG*/
	//from now, battery need to be fully discharged to start learning
	return 0;
}

int testcapteurms5837(void)
{
	int32_t pressure=0;
	int32_t temperature=0;

	//Initialization Test
	if(MS5837_Init(&hi2c1)==-1) //Connection and initialization of ms5837
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (COMM)     : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}

	//Get Pressure and Temp Test
	if(MS5837_GetPressionAndTemp(&pressure, &temperature)==-1)
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (COMM)     : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (COMM)     : OK\n");
#endif /*DEBUG*/
	}

	//Pressure value test
	if( (pressure<800) || (pressure>1200) )
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (PRESSURE) : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (PRESSURE) : OK\n");
#endif /*DEBUG*/
	}

	//Temperature value test
	if( (temperature<-10) || (temperature>70) )
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (TEMP)     : ERROR\n");
#endif /*DEBUG*/
		return -1;
	}
	else
	{
#ifdef DEBUG
		printf("TEST PRESSURE SENSOR (TEMP)     : OK\n");
#endif /*DEBUG*/
	}

#ifdef DEBUG
	printf("TEST PRESSURE SENSOR            : OK\n");
#endif /*DEBUG*/
	return 0;
}
/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Configure LSE Drive Capability
	 */
	HAL_PWR_EnableBkUpAccess();

	__HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
			|RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C2;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
	PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	(void) file;
	(void) line;
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	// printf("errror occured\n\tFile : %s\n\tLine : %d\n\n",file,line);
	while(1)
	{
		NVIC_SystemReset();
	}
	/* USER CODE END Error_Handler_Debug */
}

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	(void) file;
	(void) line;
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: // printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}

//----------------------------------------------------------------------
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName )
{
    (void)xTask;
    (void)pcTaskName;

    NVIC_SystemReset();
}

//----------------------------------------------------------------------
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM2) {
        HAL_IncTick();
    }
}

