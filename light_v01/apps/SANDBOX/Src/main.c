#include <stm32l4xx_hal.h>
#include <cmsis_os.h>
#include <crc.h>
#include <i2c.h>
#include <ILI9163_driver.h>
#include <rtc.h>
#include <spi.h>
#include <tim.h>
#include <usart.h>
#include <gpio.h>

#include <GUI_serenity.h>
#include <touchscreen.h>
#include <trace.h>

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void SystemClock_Config(void);
static void interrupt_init(void);
static void device_init(void);

void create_tasks(void);

#ifdef DEBUG
extern void initialise_monitor_handles(void);
#endif /* DEBUG */

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int main(void)
{
    // first initialise abstraction layer
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialise all configured peripherals */
    MX_GPIO_Init();
    HAL_Delay(1000); /* ms */
    MX_I2C1_Init();
    MX_I2C2_Init();
    MX_SPI2_Init();
    MX_USART1_UART_Init();
    MX_RTC_Init();
    MX_TIM3_Init();
    MX_TIM6_Init();
    MX_CRC_Init();

#ifdef DEBUG
    initialise_monitor_handles();
    debug_printf("SERENITY - Start debug session\n");
#endif /* DEBUG */

    // initialise base devices
    device_init();

    // create unique task to test features
    create_tasks();

    /* Init the STemWin GUI Library */
    debug_printf("STemWin init Start\n");

    // Set create flag to use automatically memory devices,
    // this is one way to avoid flickering. */
    WM_SetCreateFlags(WM_CF_MEMDEV);
    GUI_Init();
    if ( ! GUI_IsInitialized() )
    {
        debug_printf("STemWin init error! \n");
    }
    else
    {
        debug_printf("STemWin init done! \n" );
    }

    /* Initialise interrupts */
    interrupt_init();

    // set backlight to 70percent
    change_BaklightIntensity(70);

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    while (1)
    {
        // do nothing
    }

}

//----------------------------------------------------------------------
static void interrupt_init(void)
{
    // enable interrupt from touchscreen
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

//----------------------------------------------------------------------
static void device_init(void)
{
    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* init touchscreen */
    touchscreen_Init(); /* i2c2 */

    /* start PLL for backlight */
    HAL_TIM_Base_Start(&htim3);
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);

}

//----------------------------------------------------------------------
void SystemClock_Config(void)
{

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Configure LSE Drive Capability
     */
    HAL_PWR_EnableBkUpAccess();

    __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 1;
    RCC_OscInitStruct.PLL.PLLN = 10;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
    RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Initializes the CPU, AHB and APB busses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
            |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USART1
            |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C2;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the main internal regulator output voltage
     */
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the Systick interrupt time
     */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
     */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

//----------------------------------------------------------------------
void _Error_Handler(char *file, int line)
{
    (void)file;
    (void)line;

    /* User can add his own implementation to report the HAL error return state */
    while(1)
    {
        NVIC_SystemReset();
    }
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    debug_printf("assertion failed %s@%ld", (char*)file, line);
}
#endif /* USE_FULL_ASSERT */
