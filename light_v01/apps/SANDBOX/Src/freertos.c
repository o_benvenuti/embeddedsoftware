/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <cmsis_os.h>

#include <GUI_serenity.h>
#include <stm32l4xx_hal.h>
#include <trace.h>
#include <touchscreen.h>
#include <al_tools.h>
#include <illbeback.c>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define SANDBOX_TASK_STACK  2048

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
osThreadId sandbox_task_handle;
StackType_t  sandbox_task_buffer[SANDBOX_TASK_STACK];
osStaticThreadDef_t sandbox_task_controlblock;

// idle task static stack and handle
static StaticTask_t _xIdleTaskTCBBuffer;
static StackType_t _xIdleStack[configMINIMAL_STACK_SIZE];

static StaticTask_t _xTimerTaskTCBBuffer;
static StackType_t _xTimerStack[configTIMER_TASK_STACK_DEPTH];

osStaticMessageQDef_t _sandbox_queueset_handle;
static void * _sandbox_queueset_buf[1];

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
// Tasks
void sandbox_task(void const * argument);

// Helpers

// Systems
__attribute__((weak)) void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
__attribute__((weak)) void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

//----------------------------------------------------------------------
// Tasks definitions
//----------------------------------------------------------------------
void sandbox_task(void const * argument)
{
    (void)argument;
    int rc = 0;

    QueueSetHandle_t queue_set;
    QueueHandle_t available_queue = NULL;

    // create the communication object
    queue_set = xQueueCreateSetStatic(1, (uint8_t*)&_sandbox_queueset_buf[0],
                                      &_sandbox_queueset_handle);

    rc = xQueueAddToSet(touchscreen_flag, queue_set );
    if ( rc != pdPASS )
    {
        xQueueReset(touchscreen_flag);
        xQueueAddToSet(touchscreen_flag, queue_set );
    }

    while (1)
    {
        // wait for user input or system event
        available_queue = (QueueHandle_t) xQueueSelectFromSet(queue_set, 10);

        /* touchscreen stuff */
        if( available_queue == touchscreen_flag )
        {
            // get touchscreen info
            touch_info_t touch_info = get_TouchInfo();
            if ( touch_info.gesture == touch )
            {
                // send position to graphical interface
                GUI_TOUCH_StoreState(touch_info.xy_coord.x,
                                     touch_info.xy_coord.y);

                // tell the GUI we are done
                GUI_TOUCH_StoreState(-1,-1);
            }
            else
            {
                // send user input to features
                debug_printf("swipe detected\n");
            }
        }

        if( available_queue == NULL )
        {
        	//--------- DO STUFF HERE ------------
        	int ibb_feed(float * acc_in, float * gyr_in, float * mag_in,
        	             float dt_sec, uint16_t heading)
        	//------------------------------------
    }
}
}

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
__attribute__((weak)) void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
    /* Called by the kernel before it places the MCU into a sleep mode because
       configPRE_SLEEP_PROCESSING() is #defined to PreSleepProcessing().

     NOTE:  Additional actions can be taken here to get the power consumption
      even lower.  For example, peripherals can be turned off here, and then back
      on again in the post sleep processing function.  For maximum power saving
      ensure all unused pins are in their lowest power state. */

    /*
    (*ulExpectedIdleTime) is set to 0 to indicate that PreSleepProcessing contains
    its own wait for interrupt or wait for event instruction and so the kernel vPortSuppressTicksAndSleep
    function does not need to execute the wfi instruction
     */
    *ulExpectedIdleTime = 0;

    HAL_SuspendTick();
    /*Enter to sleep Mode using the HAL function HAL_PWR_EnterSLEEPMode with WFI instruction*/
    HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

//----------------------------------------------------------------------
__attribute__((weak)) void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
    /* Called by the kernel when the MCU exits a sleep mode because
  configPOST_SLEEP_PROCESSING is #defined to PostSleepProcessing(). */

    /* Avoid compiler warnings about the unused parameter. */
    (void) ulExpectedIdleTime;
    HAL_ResumeTick();
}

//----------------------------------------------------------------------
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName )
{
    (void)xTask;
    (void)pcTaskName;

    NVIC_SystemReset();
}


//----------------------------------------------------------------------
void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
                                   StackType_t **ppxIdleTaskStackBuffer,
                                   uint32_t *pulIdleTaskStackSize )
{
    *ppxIdleTaskTCBBuffer = &_xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &_xIdleStack[0];
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

//----------------------------------------------------------------------
void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer,
                                    StackType_t **ppxTimerTaskStackBuffer,
                                    uint32_t *pulTimerTaskStackSize )
{
    *ppxTimerTaskTCBBuffer = &_xTimerTaskTCBBuffer;
    *ppxTimerTaskStackBuffer = &_xTimerStack[0];
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}

//----------------------------------------------------------------------
void create_tasks(void)
{
    /* Create the thread(s) */
    /* definition and creation of ManagerTask */
    osThreadStaticDef(sandbox_task, sandbox_task,
                      osPriorityRealtime, 0, SANDBOX_TASK_STACK,
                      sandbox_task_buffer, &sandbox_task_controlblock);
    sandbox_task_handle = osThreadCreate(osThread(sandbox_task), NULL);

}

