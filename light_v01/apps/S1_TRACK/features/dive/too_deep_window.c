//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <al_tools.h>
#include <algapi.h>
#include <common.h>
#include <DIALOG.h>
#include <GUI_Serenity.h>
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_PROGBAR_0   (GUI_ID_USER + 0x01)
#define ID_PROGBAR_1   (GUI_ID_USER + 0x02)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cb_dialog(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _adialog_create[] = {
        {
                WINDOW_CreateIndirect, "too_deep_window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0
        },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldReduced16pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedRegularReduced18pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiCondNumbers36pts;

//----------------------------------------------------------------------
WM_HWIN create_too_deep_window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_adialog_create,
                               GUI_COUNTOF(_adialog_create),
                               _cb_dialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cb_dialog(WM_MESSAGE * pMsg)
{
    int xPos, yPos;
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* Top section */
            if ( TicTac )
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
                GUI_SetColor(GUI_WHITE);
            }
            else
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B));/* RED */
            }
            GUI_ClearRect(0,0,160,31);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = LCD_GetXSize() / 2;
            yPos = (LCD_GetYSize() / 6) - 5;
            GUI_DispStringAt(languages_get_string(TXT_TOO_DEEP), xPos ,yPos);

            /* Middle section */
            xPos = LCD_GetXSize() / 6;
            yPos = LCD_GetYSize() / 2;
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_LEFT | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiCondNumbers36pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_GotoXY(xPos ,yPos);
            if ( dive_ctx->metric_system )
            {
                GUI_DispFloatFix(dive_ctx->current_depth,
                                 (dive_ctx->current_depth < 100 ? 4 : 5), 1);
            }
            else
            {
                GUI_DispFloatFix(al_tools_to_feet(dive_ctx->current_depth),
                     (al_tools_to_feet(dive_ctx->current_depth) < 100 ? 4 : 5),
                     1);
            }
            GUI_SetFont(&GUI_FontSairaSemiCondensedRegularReduced18pts);
            xPos =  GUI_GetDispPosX() + 8;
            yPos = LCD_GetYSize() / 2 - 8;
            GUI_SetTextAlign(GUI_TA_VCENTER);
            GUI_GotoXY(xPos ,yPos);
            GUI_DispString((dive_ctx->metric_system ?
                            languages_get_string(TXT_M_UNIT) :
                            languages_get_string(TXT_F_UNIT) ));

            /* Bottom section */
            if(!TicTac)
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
                GUI_SetColor(GUI_WHITE);
            }
            else
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B));/* RED */
            }
            GUI_ClearRect(0,97,160,128);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            xPos = LCD_GetXSize() / 2;
            yPos = LCD_GetYSize() - ((LCD_GetYSize() / 6) - 5);
            GUI_DispStringAt(languages_get_string(TXT_GO_UP), xPos ,yPos);

            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}
