#ifndef _FEATURE_DIVE_PRIVATE_H_
#define _FEATURE_DIVE_PRIVATE_H_
/**
 * @file:  feature_dive_private.h
 *
 * @brief
 *
 * @date   13/02/2013
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define FEATURE_DIVE_GRAPH_SIZE 144

#define FREEDIVE_ALLOW_FLICK_END        (5 * 60)   // in second
#define DECO_BOTTOM_END_TIMEOUT         (1 * 60)    // in second
#ifdef DEBUG_DIVE
extern float DEBUG_DIVE_MANUAL_SPEED;
#endif

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct feat_dive_priv_ctx {
    int parent;
    uint32_t last_tick;
    int time_keeper;                /**< used to split actions when timer is raised   */
    int current_stop;               /**< occurring stop [DECO|BOTTOM], -1 = no stop   */
    int current_nb_point;           /**< number of samples, used to scale the graph   */
    uint8_t dive_mode;              /**< local value of configured dive mode          */
    bool too_fast;                  /**< processed in manage_too_fast() [DECO|BOTTOM] */
    bool too_deep;                  /**< processed in manage_too_deep() [FREE]        */
    bool stop_paused;               /**< pause timer on stop depth's margin exceeded  */
    bool metric_system;             /**< display depth in feet or meters              */
    DiveInfo deco_info;             /**< deco algo internal configuration             */
    uint16_t curr_dive_duration;    /**< total dive duration in seconds               */
    TimeType dive_start;            /**< dive start time used to compute duration     */
    int graph_data_count;           /**< amount of data in graph buffer               */
    float current_depth;            /**< current depth retrieved every sample period  */
    float rising_speed;             /**< average rising speed                         */
    float current_max_depth;        /**< planned max depth and if occurs current max  */
    float graph_max_depth;          /**< used to rescale graph if needed              */
    float speedTab[ASC_SPEED_AVG_TAB_SIZE];         /**< used to compute rising speed */
    int16_t graph_data[FEATURE_DIVE_GRAPH_SIZE];    /**< graph data for dive window   */
    int32_t min_temperature;        /**< to be saved at the end of dive               */
    TimerHandle_t compass_tmr;      /**< handle for compass screen refresh            */
    WM_HWIN base_window, current_window;        /**< windows handles for every screen */
    WM_HWIN dive_window, compass_window;        /**< TODO use states machine to have  */
    WM_HWIN stop_window, toofast_window;        /**< only window displayed at a time  */
    WM_HWIN toodeep_window;
    union {                         /**< union of struct containing data per dive mode*/
        struct {                    /**< Free dive parameters                         */
            TimeType lap_start;
            TimeType rest_start;
            serenity_free_param_t   free_params;
        };
        struct {                    /**< Deco Algo parameters                         */
            bool ndl_passed;
            serenity_deco_param_t   deco_params;
        };
        serenity_depth_param_t  depth_params;   /**< Bottom timer rule by itself      */
    };
}feat_dive_priv_ctx_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
// Helpers
#ifdef DEBUG_DIVE
float _fake_depth(void);
float _fake_depth2(feat_dive_priv_ctx_t * dive_ctx);
float _fake_depth3(feat_dive_priv_ctx_t * dive_ctx);
#endif  // DEBUG_DIVE

/**
  * @brief      Check ascent rate regarding configuration and return true or
  *             false depending if this is in allowed range.
  *
  * @param[in]  Feature context for configuration and current depth and speed
  *
  * @retval     true if going too fast else false
  **/
bool manage_speed(feat_dive_priv_ctx_t * ctx);

/**
  * @brief      Check if end of dive conditions are fulfilled
  *
  * @param[in]  Feature context for configuration and current depth
  *
  * @retval     true if dive is finished, false otherwise
  **/
bool end_of_dive(feat_dive_priv_ctx_t * ctx);

/**
  * @brief      Fill depth graph arraywith scaled values
  *
  * @param[in]  Feature context
  *
  * @retval     None
  **/
void graph_data_management(feat_dive_priv_ctx_t * ctx);

/**
  * @brief      Fill local context with configuration and start required modules
  *
  * @param[in]  Local feature context and global context from flash
  *
  * @retval     0 or system error code
  **/
int start_new_dive(feat_dive_priv_ctx_t * priv, serenity_context_t * glob);

/**
  * @brief      Depending on configuration, check if we reach a stop depth
  *
  * @param[in]  Local feature context
  *
  * @retval     Stop number if reached, -1 if not
  **/
int manage_stops(feat_dive_priv_ctx_t * ctx);

#endif  // _FEATURE_DIVE_PRIVATE_H_


