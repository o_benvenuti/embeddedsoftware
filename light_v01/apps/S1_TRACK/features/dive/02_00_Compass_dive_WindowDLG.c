//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <algapi.h>
#include <DIALOG.h>
#include <common.h>
#include <al_tools.h>
#include <GUI_Serenity.h>
#include <spi.h>
#include <LSM9DS1.h>
#include <compass.h>
#include <illbeback.h>
#include <magcal.h>
#include <trace.h>
#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_PROGBAR_0             (GUI_ID_USER + 0x01)
#define ID_PROGBAR_1             (GUI_ID_USER + 0x02)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_topbasse;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_tophaute;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_topOK;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced20pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
    {
        WINDOW_CreateIndirect, "Compass_S1_Window",
        ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
    },
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateCompass_dive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    int RisingSpeedPixels;
    int timeLeft, timeLeftPixels;
    static int16_t cHeading = 0;
    static int bHeading = 0;
    static int16_t lHeading = 0; /* local heading value for comparison */
    static int16_t lbHeading = 0; /* local heading value for comparison */
    static feat_dive_priv_ctx_t * dive_ctx = NULL;
    float upper_limit = 0.0;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case WM_PRE_PAINT:
        {
            cHeading = COMPASS_get_heading();
            bHeading = ibb_get_origin_heading();
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* High resolution settings for Anti-aliasing */
            GUI_AA_SetFactor(HIGHRES_FACTOR);
#if HIGHRES_FACTOR > 1
            GUI_AA_EnableHiRes();
#endif
            // draw 'toward' arrow
            GUI_AA_FillPolygon(GUI_toward_arrow, countof(GUI_toward_arrow),
                               80 * HIGHRES_FACTOR , 5 * HIGHRES_FACTOR);

            /* get smallest difference between the two angles actual heading & compass reading */
            int diff = cHeading - lHeading;
            diff += (diff>180) ? -360 : (diff<-180) ? 360 : 0;
            /* calculate the amount of degrees to move : faster if far away, slower if closer */
            if	   ((diff>-20)&&(diff<-6 )) lHeading -= 2; /* -20 to -6  */
            else if((diff>=-6)&&(diff<-1 )) lHeading -= 1; /* -6  to -1  */
            else if((diff>=-1)&&(diff<=1 ))	lHeading += 0; /* -1  to  1  */
            else if((diff> 1 )&&(diff<=6 )) lHeading += 1; /*  1  to  6  */
            else if((diff> 6 )&&(diff<20 )) lHeading += 2; /*  6  to  20 */
            else  							lHeading += diff /10;
            /* modify range between 0 & 360 */
            while (lHeading>360)  {lHeading -= 360;}
            while (lHeading<0)    {lHeading += 360;}

            /* get smallest difference between the two angles actual heading & compass reading */
            int diff2 = bHeading - lbHeading;
            diff2 += (diff2>180) ? -360 : (diff2<-180) ? 360 : 0;
            /* calculate the amount of degrees to move */
            if	   ((diff2>-20)&&(diff2<-6 )) lbHeading -= 2; /* -20 to -6  */
            else if((diff2>=-6)&&(diff2<-1 )) lbHeading -= 1; /* -6  to -1  */
            else if((diff2>=-1)&&(diff2<=1 ))	lbHeading += 0; /* -1  to  1  */
            else if((diff2> 1 )&&(diff2<=6 )) lbHeading += 1; /*  1  to  6  */
            else if((diff2> 6 )&&(diff2<20 )) lbHeading += 2; /*  6  to  20 */
            else  							lbHeading += diff2 /10;
            /* modify range between 0 & 360 */
            while (lbHeading>360)  lbHeading -= 360;
            while (lbHeading<0)    lbHeading += 360;

            /* print compass value */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_SetColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_DispDecAt(lHeading,11,-9,lHeading>99?3:lHeading>9?2:1);
            GUI_DispChar('�');

            /* print compass text */
            char * DirectionsTab[] = {"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW","N"};
            char * CompassDir = DirectionsTab[(int)round(((lHeading*10)+112 % 3600) / 225)];
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(CompassDir,11,17);

            GUI_SetColor(GUI_WHITE);

            // to break if dive mode is unknown
            do
            {
                // no current stop to display
                if ( dive_ctx->current_stop < 0 )
                {
                    break;
                }

                serenity_dive_stop_t * curr_stop_ptr;
                if ( dive_ctx->dive_mode == Mode_BottomTimer )
                {
                    int pos = dive_ctx->current_stop;
                    curr_stop_ptr = &(dive_ctx->depth_params.dive_stops[pos]);
                }
                else if ( dive_ctx->dive_mode == Mode_DecoAlgo )
                {
                    curr_stop_ptr = &(dive_ctx->deco_params.dive_stop);
                }
                else
                {
                    debug_printf("unknown dive mode: cannot display stop\n");
                    break;
                }

                if( (dive_ctx->dive_mode == Mode_DecoAlgo) &&
                    (dive_ctx->ndl_passed == false) )
                {
                    upper_limit = 2.5;
                }
                else
                {
                    upper_limit = 0.5;
                }

                int xPos = 127, yPos = 27;
                if (dive_ctx->current_depth > curr_stop_ptr->StopDepth+0.5) /* below limit */
                {
                    GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_tophaute, xPos, yPos);
                }
                else if (dive_ctx->current_depth < curr_stop_ptr->StopDepth-upper_limit) /* more than limit */
                {
                    GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_topbasse, xPos, yPos);
                }
                else if ((dive_ctx->current_depth > curr_stop_ptr->StopDepth-upper_limit) &&
                         (dive_ctx->current_depth < curr_stop_ptr->StopDepth+0.5)  ) /* inside limit */
                {
                    GUI_DrawBitmap(&bmcran_Serenity_13a_flchepalier_topOK, xPos, yPos);
                }

                GUI_SetBkColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */

            }while(0);


            GUI_ClearRect(90,0,160,25);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_GotoXY((dive_ctx->current_depth>=10?100:dive_ctx->current_depth<0?132:114),-9);
            GUI_DispFloat(dive_ctx->current_depth,dive_ctx->current_depth>9.99?4:dive_ctx->current_depth<0?1:3);

            GUI_DrawCompass(lHeading, lbHeading, ibb_are_we_arrived(),
                            ibb_get_status(), TicTac);

            /****** left progbar - Time Left + NDL ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00002134));
            GUI_ClearRect(0, 0, 7, LCD_GetYSize());
            /* Draw bar foreground */
            if ( dive_ctx->dive_mode == Mode_BottomTimer )
            {
                timeLeft = (dive_ctx->depth_params.dive_duration*60);
                timeLeft = timeLeft - dive_ctx->curr_dive_duration;
                timeLeftPixels = (((timeLeft)>0?timeLeft:0)*LCD_GetYSize());
                timeLeftPixels /= (dive_ctx->depth_params.dive_duration * 60);
            }
            else
            {
                timeLeft = (dive_ctx->deco_params.dive_duration*60);
                timeLeft = timeLeft - dive_ctx->curr_dive_duration;
                timeLeftPixels = (((timeLeft)>0?timeLeft:0)*LCD_GetYSize());
                timeLeftPixels /= (dive_ctx->deco_params.dive_duration * 60);
            }
            GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_ClearRect(0, LCD_GetYSize()-timeLeftPixels, 7, LCD_GetYSize());

            /****** right progbar - speed ******/
            /* draw bar background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00353537));
            GUI_ClearRect(152, 0, 160, LCD_GetYSize());
            /* Draw bar foreground (scale goes from 9 to 18*/
            RisingSpeedPixels = ((dive_ctx->rising_speed-9.0)*LCD_GetYSize())/9.0;
            GUI_SetBkColor(GUI_WHITE);
            GUI_ClearRect(152, LCD_GetYSize()-RisingSpeedPixels, 160, LCD_GetYSize());
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}
