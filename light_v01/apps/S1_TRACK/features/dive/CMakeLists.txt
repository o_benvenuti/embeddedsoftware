include_drivers(ACC RTC TOUCHSCREEN DEPTH_METER)        # RTC for common.h
include_libraries(DECO DATABASE TRACE GUI FEATURE STemWin TOOLS LANGUAGES ILLBEBACK COMPASS MAGCAL)
include_freertos()

INCLUDE_DIRECTORIES (${CMAKE_CURRENT_SOURCE_DIR}/../../Inc
                     ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config)
                     
ADD_LIBRARY (TRACK_FEATURE_DIVE feature_dive.c
                                feature_dive_private.c
                                02_00_Compass_dive_WindowDLG.c
                                DiveScreen_WindowDLG.c
                                FreeDive_WindowDLG.c 
                                STOP_WindowDLG.c
                                too_deep_window.c
                                TOOFAST_WindowDLG.c)