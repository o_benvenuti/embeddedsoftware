//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <algapi.h>     // for private context
#include <DIALOG.h>
#include <stdio.h>
#include <GUI_Serenity.h>
#include "common.h"
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
    {
        WINDOW_CreateIndirect, "FreeDive_Window",
        ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
    },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced20pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            WM_SetCallback(pMsg->hWin, _cbRepaint);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateFreeDive_Window(void)
{
    return GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Tstr[20] = {0};
    uint8_t x_pos, y_pos, min, sec;
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            // draw Background
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            //*****************************************************************
            //********************** UPPER SECTION ****************************
            //*****************************************************************
            //**************** LEFT SECTION *******************
            // print current depth numerical value
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            x_pos = X_1_3;
            y_pos = Y_1_6;
            GUI_GotoXY(x_pos ,y_pos);
            GUI_DispFloatFix(dive_ctx->current_depth,
                             dive_ctx->current_depth<100?dive_ctx->current_depth<10?3:4:5, 1);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_GotoY(Y_1_3 - 13);
            GUI_DispString(" ");
            GUI_DispString((dive_ctx->metric_system ?
                            languages_get_string(TXT_M_UNIT) :
                            languages_get_string(TXT_F_UNIT) ));

            //**************** RIGHT SECTION *******************
            // print lap count text
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetColor(GUI_WHITE);
            x_pos = X_5_6+5;
            y_pos = 9;
            GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_LAPS), x_pos,y_pos);
            // print lap counter value
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            x_pos = X_5_6 + 5;
            y_pos = Y_1_6 + 8;
            GUI_DispDecAt(dive_ctx->free_params.lap_count, x_pos, y_pos,
                          (dive_ctx->free_params.lap_count > 9) ? 2 : 1);

            //*****************************************************************
            //********************** MIDDLE SECTION ***************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); // dark blue
            GUI_ClearRect(0,Y_1_3 ,LCD_GetXSize(),Y_2_3);
            // draw vertical lines
            GUI_SetColor(GUI_BLACK);
            GUI_DrawVLine(X_1_3, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_2_3, Y_1_3, Y_2_3);
            // draw graph
            GUI_SetColor(GUI_WHITE);
            GUI_DrawGraph(dive_ctx->graph_data, dive_ctx->graph_data_count, 0, 41);

            //*****************************************************************
            //********************** LOWER SECTION ****************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767)); // normal blue
            GUI_ClearRect(0,Y_2_3 ,LCD_GetXSize(),LCD_GetYSize());

            //**************** LEFT SECTION *******************
            // print current lap time text
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetColor(GUI_WHITE);
            x_pos = X_1_4;
            y_pos = Y_2_3 + 10;
            GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_LAP), x_pos,y_pos);
            // print current lap time
            min = dive_ctx->free_params.lap_duration/60;
            sec = dive_ctx->free_params.lap_duration%60;
            sprintf(Tstr, "%02d:%02d",min,sec);
            y_pos = Y_5_6 + 10;
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, x_pos ,y_pos);

            //**************** RIGHT SECTION *******************
            // print rest time text
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetColor(GUI_WHITE);
            x_pos = X_3_4;
            y_pos = Y_2_3 + 10;
            GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_REST_TIME), x_pos,y_pos);
            // print current lap time
            min = dive_ctx->free_params.rest_duration/60;
            sec = dive_ctx->free_params.rest_duration%60;
            sprintf(Tstr, "%02d:%02d",min,sec);
            y_pos = Y_5_6 + 10;
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, x_pos ,y_pos);
            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
        }
    }
}
