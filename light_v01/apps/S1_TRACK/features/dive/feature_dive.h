#ifndef _FEATURE_DIVE_H_
#define _FEATURE_DIVE_H_
/**
 * @file:  feature_dive.h
 *
 * @brief
 *
 * @date   25/09/2018
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FEATURE_DIVE_SHARE_CTX  WM_USER + 0x06

//----------------------------------------------------------------------
// Shared variables
//----------------------------------------------------------------------
extern bool TicTac;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateTOOFAST_Window(void);
WM_HWIN CreateSTOP_Window(void);
WM_HWIN CreateDive_Window(void);
WM_HWIN CreateCompass_dive_Window(void);
WM_HWIN CreateFreeDive_Window(void);
WM_HWIN create_too_deep_window(void);

#endif  // _FEATURE_DIVE_H_
