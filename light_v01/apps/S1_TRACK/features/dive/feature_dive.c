/*
 * feature_dive.c
 *
 *  Created on: 28 sept. 2018
 *      Author: user
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdlib.h> // rand

#include <common.h>
#include <accel_lib.h>
#include <algapi.h>
#include <al_database.h>
#include <al_feature.h>
#include <al_tools.h>
#include <LSM9DS1_Includes.h>
#include <ms5837_driver.h>
#include <touchscreen.h>
#include <trace.h>
#include <GUI_Serenity.h>
#include <illbeback.h>
#include <compass.h>
#include <magcal.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
// Public
int _dive_init_func(void ** priv_data, int parent);
int _dive_event_func(void * priv_data, al_feature_cmd_t cmd);
int _dive_timer_func(void * priv_data, int time);
int _dive_release_func(void * priv_data);

// private
static void _compass_timer(TimerHandle_t xTimer);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_dive_ctx = {
        .name       = "DIVE",
        .ini_func   = _dive_init_func,
        .rls_func   = _dive_release_func,
        .evt_func   = _dive_event_func,
        .tmr_func   = _dive_timer_func,
};

static StaticTimer_t _compass_timer_hdl;

/**
 * NOTE static context allocation to be used only in init function,
 * then use pointer provided by al_feature library to callbacks
**/
static feat_dive_priv_ctx_t _dive_local_ctx;

bool TicTac = false;

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _dive_init_func(void ** priv_data, int parent)
{
    int rc;
    WM_MESSAGE window_msg;
    al_feature_msg_t feat_msg;
    serenity_context_t global_ctx;

    // get global configuration from flash
    rc = serenity_retrieve_status(&global_ctx);
    if ( rc )
    {
        debug_printf("unable to retrieve global context\n");
        return -1;
    }

    // turn devices on
    LSM9DS1_PowerUp();
    ibb_init(global_ctx.watch_stance_left);

#ifndef DEBUG_DIVE
    // power down Touchscreen Device
    touchscreen_PowerDown();
#endif

    // set display metric system preferences
    _dive_local_ctx.metric_system = global_ctx.metric_system;

    // configure the dive with global configuration
    start_new_dive(&_dive_local_ctx, &global_ctx);

    // create timer that retrieve compass data & blink the screen
    _dive_local_ctx.compass_tmr = xTimerCreateStatic("Wink screen", 50, pdTRUE,
                        &_dive_local_ctx, _compass_timer, &_compass_timer_hdl);
    if ( ! _dive_local_ctx.compass_tmr )
    {
        debug_printf("error while creating timer\n");
    }

    // start blink timer
    if ( pdPASS != xTimerStart(_dive_local_ctx.compass_tmr, 0) )
    {
        debug_printf("error while starting standby timer\n");
    }

    // background is parent window of all screen
    // delete previous screen if needed
    _dive_local_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _dive_local_ctx.current_window )
    {
        WM_DeleteWindow(_dive_local_ctx.current_window);
    }

    // start by dive window (till flick)
    if( global_ctx.dive_mode == Mode_FreeDive )
    {
        _dive_local_ctx.dive_window = CreateFreeDive_Window();

        _dive_local_ctx.toodeep_window = create_too_deep_window();
        WM_HideWindow(_dive_local_ctx.toodeep_window);
    }
    else
    {
        _dive_local_ctx.dive_window = CreateDive_Window();

        _dive_local_ctx.compass_window = CreateCompass_dive_Window();
        WM_HideWindow(_dive_local_ctx.compass_window);

        _dive_local_ctx.toofast_window = CreateTOOFAST_Window();
        WM_HideWindow(_dive_local_ctx.toofast_window);

        _dive_local_ctx.stop_window = CreateSTOP_Window();
        WM_HideWindow(_dive_local_ctx.stop_window);
    }

    // save info from parent
    _dive_local_ctx.parent = parent;

    _dive_local_ctx.base_window =  _dive_local_ctx.dive_window;
    _dive_local_ctx.current_window =  _dive_local_ctx.dive_window;

    // send local context to windows
    window_msg.MsgId = FEATURE_DIVE_SHARE_CTX;
    window_msg.Data.p = &_dive_local_ctx;
    WM_SendMessage(_dive_local_ctx.dive_window, &window_msg);

    if( global_ctx.dive_mode == Mode_FreeDive )
    {
        WM_SendMessage(_dive_local_ctx.toodeep_window, &window_msg);
    }
    else
    {
        WM_SendMessage(_dive_local_ctx.stop_window, &window_msg);
        WM_SendMessage(_dive_local_ctx.compass_window, &window_msg);
        WM_SendMessage(_dive_local_ctx.toofast_window, &window_msg);
    }

    //edit timer delay
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 50;
    al_feature_send_to_tsk(&feat_msg);

    // send private configuration to feature manager
    *priv_data = &_dive_local_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _dive_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    feat_dive_priv_ctx_t * ctx = (feat_dive_priv_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( cmd  )
    {
        case CMD_RCV_FLICK:
        {
            // in free dive mode, flick is used to end dive only (no compass)
            if ( ctx->dive_mode != Mode_FreeDive )
            {
                // discard flick if going up too fast
                if ( ctx->current_window == ctx->toofast_window )
                {
                    break;
                }
                else if ( ctx->current_window == ctx->dive_window )
                {
                    GUI_Switch_Screens(ctx->current_window,
                                       ctx->compass_window);
                    ctx->current_window = ctx->compass_window;
                    ctx->base_window = ctx->compass_window;
                    break;
                }
                else if ( ctx->current_window == ctx->compass_window )
                {
                    if ( ctx->current_stop > -1)
                    {
                        GUI_Switch_Screens(ctx->current_window,
                                           ctx->stop_window);
                        ctx->current_window = ctx->stop_window;
                    }
                    else
                    {
                        GUI_Switch_Screens(ctx->current_window,
                                           ctx->dive_window);
                        ctx->current_window = ctx->dive_window;
                        ctx->base_window = ctx->dive_window;
                    }
                    break;
                }
                else if ( ctx->current_window == ctx->stop_window )
                {
                    GUI_Switch_Screens(ctx->current_window,
                                       ctx->compass_window);
                    ctx->current_window = ctx->compass_window;
                    ctx->base_window = ctx->compass_window;
                    break;
                }
            }
            else
            {
                if ( ctx->free_params.rest_duration > FREEDIVE_ALLOW_FLICK_END )
                {
                    // send end message
                    msg.cmd = CMD_END_STATE;
                    msg.data.value = ctx->parent;
                    al_feature_send_to_tsk(&msg);
                }
            }

            break;
        }
        default:
        {
            debug_printf("unknown command\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _dive_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc = 0;
    GUI_RECT pRect;
    al_feature_msg_t msg;
    bool dive_end = false;
    float speedSum = 0, prev_depth;
    float acc[3] = {0.0};
    float gyr[3] = {0.0};
    float mag[3] = {0.0};
    float dTsec  =  0.0;
    // NOTE those values are kept static in case
    // that the sensor request fails. Then we keep
    // the previous value as a reference
    static int32_t pressure = 0, temperature = 0;

    feat_dive_priv_ctx_t * dive_ctx = (feat_dive_priv_ctx_t *)priv_data;
    if ( ! dive_ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    //******************** DATA ACQUISITION ***********************
    // update ibb with AGM

    do
    {
        // ACCELEROMETER
        if(LSM9DS1_GetAccelerometerValuesG(&acc[0],&acc[1],&acc[2])<0)
        {
            debug_printf("error getting Accelerometer values\n");
            break;
        }

        // GYROSCOPE
        if(agm_gyro_compensated_dps(&gyr[0])<0)
        {
            debug_printf("error getting Gyroscope values\n");
            break;
        }

        // MAGNETOMETER
        if(LSM9DS1_GetMagnetometerValuesGs(&mag[0],&mag[1],&mag[2])<0)
        {
            debug_printf("error getting Magnetometer values\n");
            break;
        }

        // compensate magnetometer with calibration matrix
        magcal_compensate_mag(mag);
        COMPASS_update_heading(mag, acc);

    	// calculate the delay between now and the last acquisition
    	dTsec = DeltaT_Calculation(dive_ctx->last_tick)/1000.0;
    	dive_ctx->last_tick = HAL_GetTick();

        uint16_t heading = COMPASS_get_heading();

        rc = ibb_feed(acc, gyr, mag, dTsec, heading);
        if ( rc )
        {
            debug_printf("failed to feed ibb algorithm\n");
        }
    }while (0);
    //*************************************************************

#ifdef DEBUG_FASTER_READ_SENSOR_TASK
    if ( ++dive_ctx->time_keeper % 2 )
    {
        return 0;
    }
#else
    // and do everything else every seconds
    if ( ++dive_ctx->time_keeper % 20 )
    {
        return 0;
    }
#endif

    /* store the current values in local variables */
    prev_depth = dive_ctx->current_depth;

#ifdef DEBUG_DIVE
    /* generate fake data for debug */
    dive_ctx->current_depth = _fake_depth3(dive_ctx);
    pressure = (dive_ctx->current_depth * 100) + 1000;
#else
    /* Get depth meter information */
    rc = get_PressureTempDepth(&pressure, &temperature,
                               &dive_ctx->current_depth,
                               gStartDiveLevelPressure);
    if ( rc )
    {
        debug_printf("pressure, temperature and depth skipped\n");
    }
#endif  // DEBUG_DIVE

#ifdef DEBUG_FASTER_READ_SENSOR_TASK
    dive_ctx->curr_dive_duration++;
#else
    dive_ctx->curr_dive_duration = get_TimeDiffInSeconds(dive_ctx->dive_start,
                                                RTC_GetTime());
#endif  // DEBUG_FASTER_READ_SENSOR_TASK

    // update max depth value
    if( dive_ctx->current_depth > dive_ctx->current_max_depth )
    {
        dive_ctx->current_max_depth = dive_ctx->current_depth;
    }

    // update min temperature
    if ( temperature < dive_ctx->min_temperature )
    {
        dive_ctx->min_temperature = temperature;
    }

    /* calculate average rising/diving speed */
    /* move all values one step to the left in array */
    for(int i = 0 ; i<ASC_SPEED_AVG_TAB_SIZE-1 ; i++)
    {
        dive_ctx->speedTab[i] = dive_ctx->speedTab[i+1];
    }

    /* add new value to the end */   /* m/min = m/60sec */
    dive_ctx->speedTab[ASC_SPEED_AVG_TAB_SIZE-1] = - (dive_ctx->current_depth - prev_depth);
    dive_ctx->speedTab[ASC_SPEED_AVG_TAB_SIZE-1] *= (float)(SAMPLE_PERIOD/1000*60.0);

    /* sum all values in tab */
    speedSum = 0;
    for(int i = 0; i<ASC_SPEED_AVG_TAB_SIZE ; i++)
    {
        speedSum += dive_ctx->speedTab[i];
    }

    dive_ctx->rising_speed = speedSum/ASC_SPEED_AVG_TAB_SIZE;

    /*******************************************/
    /*             DIVE RECORD                 */
    /*******************************************/
    // record last position (convert float to uint16_t)
    al_database_diverec_entry(dive_ctx->current_depth, 0);

    /*******************************************/
    /*                  DECO ALGO              */
    /*******************************************/
    if ( dive_ctx->dive_mode == Mode_DecoAlgo )
    {
        update_info_api(dive_ctx->deco_info, pressure, (float)(SAMPLE_PERIOD/1000));

        /* if NDL is passed */
        if( ((dive_ctx->ndl_passed) || (*(dive_ctx->deco_info.ndl) <= 0))                           &&
        	//filter to avoid stop flickering when stop is finished (1sec left)
            !((*(dive_ctx->deco_info.deco_status) == DecoCal)&&(*(dive_ctx->deco_info.ndl)==60000)) &&
			(*(dive_ctx->deco_info.ceiling_stop)>1.0)                                               &&
			// do only algo stops deeper than 4.5m
			(mbarToDepth(*(dive_ctx->deco_info.ceiling_depth),gStartDiveLevelPressure)>4.5)         &&
			// do only algo stops longer than 3 min
			(*(dive_ctx->deco_info.ceiling_stop) >= 180) )
        {
            dive_ctx->ndl_passed = true;
            dive_ctx->deco_params.safety_stop = false;
            dive_ctx->deco_params.dive_stop.StopDepth =
                            mbarToDepth(*(dive_ctx->deco_info.ceiling_depth),
                                        gStartDiveLevelPressure);
            dive_ctx->deco_params.dive_stop.StopDuration =
                            *(dive_ctx->deco_info.ceiling_stop);
        }
        else if ((( dive_ctx->current_depth > 10.0 ) && (!dive_ctx->ndl_passed)) ||
        		 (*(dive_ctx->deco_info.ceiling_stop) < 180))
        {
            // if NDL is not reached, we do only
            // the default safety stop if we went more than 10m
            dive_ctx->deco_params.safety_stop = true;
            dive_ctx->deco_params.dive_stop.StopDepth    = STOP_DEPTH_DEFAULT;
            dive_ctx->deco_params.dive_stop.StopDuration = STOP_TIME_DEFAULT;
        }
    }

    /* manage the GRAPH */
    graph_data_management(dive_ctx);

    /*******************************************/
    /*              STOP & SPEED               */
    /*******************************************/
    if ( (dive_ctx->dive_mode == Mode_BottomTimer)  ||
         (dive_ctx->dive_mode == Mode_DecoAlgo)     )
    {
        rc = manage_stops(dive_ctx);
        if ( dive_ctx->current_stop != rc )
        {
            // first update status
            dive_ctx->current_stop = rc;

            // and switch to corresponding window
            if ( dive_ctx->current_stop > -1 )
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->stop_window);
                dive_ctx->current_window = dive_ctx->stop_window;
            }
            else
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->base_window);
                dive_ctx->current_window = dive_ctx->base_window;
            }
        }

        /* manage too fast window */
        rc = manage_speed(dive_ctx);
        if ( dive_ctx->too_fast != rc )
        {
            // first, update status
            dive_ctx->too_fast = rc;

            // and switch to corresponding window
            if ( dive_ctx->too_fast == true )
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->toofast_window);
                dive_ctx->current_window = dive_ctx->toofast_window;
            }
            else
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->base_window);
                dive_ctx->current_window = dive_ctx->base_window;
            }
        }
    }

    /*******************************************/
    /*          FREE DIVE TOO DEEP             */
    /*******************************************/
    if ( dive_ctx->dive_mode == Mode_FreeDive )
    {
        if ( dive_ctx->current_depth > dive_ctx->free_params.depth_max )
        {
            rc = true;
        }
        else
        {
            rc = false;
        }

        // verify if the screen has to be changed
        if ( dive_ctx->too_deep != rc )
        {
            // change the status
            dive_ctx->too_deep = rc;

            if ( dive_ctx->too_deep )
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->toodeep_window);
                dive_ctx->current_window = dive_ctx->toodeep_window;
            }
            else
            {
                GUI_Switch_Screens(dive_ctx->current_window,
                                   dive_ctx->base_window);
                dive_ctx->current_window = dive_ctx->base_window;            }
        }
    }

    /*******************************************/
    /*               END OF DIVE               */
    /*******************************************/
    dive_end = end_of_dive(dive_ctx);
    if ( dive_end == true )
    {
        msg.cmd = CMD_END_STATE;
        msg.data.value = dive_ctx->parent;
        al_feature_send_to_tsk(&msg);
    }

    // refresh whole screen in free dive mode
    if ( dive_ctx->dive_mode == Mode_FreeDive )
    {
        WM_InvalidateWindow(dive_ctx->current_window);
    }
    else
    {
        /* if we are in a stop & paused : do not invalidate time */
        if ( (dive_ctx->current_window == dive_ctx->stop_window) && (dive_ctx->stop_paused) )
        {
            pRect.x0 = 7;
            pRect.y0 = 0;
            pRect.x1 = 152;
            pRect.y1 = 80;
            WM_InvalidateRect(dive_ctx->current_window, &pRect);
        }
        else
        {
            WM_InvalidateWindow(dive_ctx->current_window);
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _dive_release_func(void * priv_data)
{
    int rc = 0;
    serenity_context_t global_ctx;
    feat_dive_priv_ctx_t * ctx = (feat_dive_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // get global configuration from flash
    rc = serenity_retrieve_status(&global_ctx);
    if ( rc )
    {
        debug_printf("unable to retrieve global context\n");
    }

    // save last dive time in global context for nofly notification.
    global_ctx.last_dive_date = RTC_GetTime();

    // save global configuration to flash
    rc = serenity_save_status(&global_ctx);
    if ( rc )
    {
        debug_printf("unable to save global context\n");
    }

    // reset sea level pressure
    gStartDiveLevelPressure = SEALEVELPRESSURE;

    // save pressure in tissue for next dive
    if ( ctx->dive_mode == Mode_DecoAlgo )
    {
        DECO_saveTissuePressureInFlash();
    }

    // stop recording current dive
    al_database_diverec_end(ctx->min_temperature);

    // stop timer in case
    xTimerStop(ctx->compass_tmr, 0);
    xTimerDelete(ctx->compass_tmr, 0);

    // turn devices off
    LSM9DS1_PowerDown();

    // start touchscreen
	touchscreen_Init();

    WM_DeleteWindow(ctx->dive_window);

    if ( ctx->dive_mode == Mode_FreeDive )
    {
        WM_DeleteWindow(ctx->toodeep_window);
    }
    else
    {
        WM_DeleteWindow(ctx->compass_window);
        WM_DeleteWindow(ctx->toofast_window);
        WM_DeleteWindow(ctx->stop_window);
    }

    // enable touchscreen interrupt, disable pocpoc
    HAL_NVIC_EnableIRQ (EXTI9_5_IRQn); /* enable touchscreen */
    HAL_NVIC_DisableIRQ(int1_3D_EXTI_IRQn);   /* disable flick */

    return 0;
}

//----------------------------------------------------------------------
static uint8_t TicTacIncrement = 0;
static void _compass_timer(TimerHandle_t xTimer)
{
    feat_dive_priv_ctx_t * ctx;

    // get feature context through timer API
    ctx = (feat_dive_priv_ctx_t*)pvTimerGetTimerID(xTimer);
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return;
    }

    if(!(TicTacIncrement++ % 10)) /* every 10 timer iteration (10*50ms)*/
    {
        TicTac = ! TicTac;
        // refresh to make stop and too-fast windows blink
        if ( ctx->current_window == ctx->stop_window)
        {
            if(!ctx->stop_paused)
            {
                WM_InvalidateWindow(ctx->current_window);
            }
        }
        else if ( ctx->current_window != ctx->compass_window  )
        {
            WM_InvalidateWindow(ctx->current_window);
        }
    }
    if ((!(TicTacIncrement % 2)) && (ctx->current_window == ctx->compass_window)  ) /* every 2 timer iteration */
    {
        WM_InvalidateWindow(ctx->current_window);
    }
}
