/*
 * feature_compass calibration.C
// *
// *  Created on: 24 jan. 2019
// *      Author: BGE
// */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <common.h>
#include <accel_lib.h>
#include <al_tools.h>
#include <illbeback.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <stm32l4xx_hal.h>
#include <touchscreen.h>
#include <trace.h>
#include <LSM9DS1.h>
#include <ds2782_driver.h>
#include <magcal.h>
#include "feature_compass_calibration.h"
#include <al_database.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define MAGCAL_FEATURE_LOOP_PERIOD          80 //ms
#define MAGCAL_FEATURE_OVERDRAFT_THRESHOLD  0.02
#define MAGCAL_FEATURE_SAMPLE_COUNT         512

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct compcalib_private_ctx {
    WM_HWIN current_window;
    serenity_state_t parent_state;
    uint32_t gyro_data_count;
    float gyro_cumulated_values[3];
    float gyro_max_norm;
    float gyro_min_norm;
    feature_compass_calibration_states_t current_state;
}compcalib_private_ctx_t;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _compass_calibration_init_func(void ** priv_data, int parent);
int _compass_calibration_timer_func(void * priv_data, int time);
int _compass_calibration_user_func(void * priv_data, gestures_t gesture);
int _compass_calibration_button_func(void * priv_data, uint32_t button);

//----------------------------------------------------------------------
// state machine
//----------------------------------------------------------------------
// states
static int _do_state_8(compcalib_private_ctx_t* ctx);
static int _do_state_O(compcalib_private_ctx_t* ctx);
static feature_compass_calibration_states_t _do_state_gyro(compcalib_private_ctx_t* ctx);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
bool exit_cal_on_swipe = false;

al_feature_ctx_t feature_compass_calibration_ctx = {
        .name       = "COMPASS_CALIBRATION",
        .ini_func   = _compass_calibration_init_func,
        .tmr_func   = _compass_calibration_timer_func,
        .usr_func   = _compass_calibration_user_func,
        .gui_func   = _compass_calibration_button_func,
};
static compcalib_private_ctx_t _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _compass_calibration_init_func(void ** priv_data, int parent)
{
    al_feature_msg_t feat_msg;

    // set initial state
    _private_ctx.gyro_data_count = 0;
    memset(&_private_ctx.gyro_cumulated_values[0], 0,
           sizeof(_private_ctx.gyro_cumulated_values));
    _private_ctx.gyro_max_norm = 0.0;
    _private_ctx.gyro_min_norm = 10.0;

    _private_ctx.parent_state = parent;
    _private_ctx.current_state = compass_calibration_8;

    // start IMU
    LSM9DS1_PowerUp();

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    _private_ctx.current_window = CreateCompass_calibration_8_Window();

    // set feature timer/timeout to 1second
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 20; //ms
    al_feature_send_to_tsk(&feat_msg);

    // return private data
    *priv_data = &_private_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _compass_calibration_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc = 0;
    WM_MESSAGE window_msg;
    al_feature_msg_t feat_msg;

    // retrieve current status
    compcalib_private_ctx_t * ctx = (compcalib_private_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // process current state
    switch (ctx->current_state)
    {
        case compass_calibration_idle:
        {
            // wait for user input
            break;
        }
        case compass_calibration_8:
        {
            // perform calibration. first step
            rc = _do_state_8(ctx);
            if ( rc )
            {
                // on error, return to parent app. state
                feat_msg.cmd = CMD_END_STATE;
                feat_msg.data.value = ctx->parent_state;
                al_feature_send_to_tsk(&feat_msg);
            }

            // perform gyro calibration and  wait for user input
            ctx->current_state = compass_calibration_gyro;
            break;
        }
        case compass_calibration_O:
        {
            // perform calibration. second step
            rc = _do_state_O(ctx);

            // success or not, return to parent app. state
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        case compass_calibration_gyro:
        {
            ctx->current_state = _do_state_gyro(ctx);

            // if we return to idle, we are ready for
            // magnetometer calibration second step
            if ( ctx->current_state == compass_calibration_idle )
            {
                // send ready message to window
                window_msg.MsgId = FEATURE_COMPASS_CALIBRATION_READY;
                WM_SendMessage(ctx->current_window, &window_msg);
                WM_Invalidate(ctx->current_window);
            }
            break;
        }
        default:
        {
            // unknown state, return to parent app. state
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_calibration_user_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t feat_msg;
    compcalib_private_ctx_t * ctx = (compcalib_private_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( gesture )
    {
        case swipe_right:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        default:
        {
            break;
        }
    }

    return 0;
}


//----------------------------------------------------------------------
int _compass_calibration_button_func(void * priv_data, uint32_t button)
{
    compcalib_private_ctx_t * ctx = (compcalib_private_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( button )
    {
        case BUTTON_RDY:
        {
            ctx->current_state = compass_calibration_O;
            break;
        }
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_calibration_release_func(void * priv_data)
{
    (void)priv_data;

    /* turn off the component */
    LSM9DS1_PowerDown();

    return 0;
}

//----------------------------------------------------------------------
// Sate machine function
//----------------------------------------------------------------------
static int _do_state_8(compcalib_private_ctx_t * ctx)
{
    int8_t ret;

    // delete previous window if needed
    if ( ctx->current_window )
    {
        WM_DeleteWindow(ctx->current_window);
    }

    // create calibration window
    ctx->current_window = CreateCompass_calibration_8_Window();

    // process hard iron step
    ret = magcal_hard_soft_iron();
    if ( ret < 0 )
    {
        // Retrieve previous calibration data from FLASH
        magcal_get_calibration_from_flash();
        debug_printf("error occurred during magcal 8\n");
        return -1;
    }

    WM_DeleteWindow(ctx->current_window);
    ctx->current_window = CreateCompass_calibration_ready_O_Window();

    return 0;
}

//----------------------------------------------------------------------
static int _do_state_O(compcalib_private_ctx_t * ctx)
{
    int8_t ret;

    // delete previous window if needed
    if ( ctx->current_window )
    {
        WM_DeleteWindow(ctx->current_window);
    }

	// create second step calibration window
    ctx->current_window = CreateCompass_calibration_O_Window();

    ret = magcal_tilt_post_hard_iron();
    if ( ret  < 0 )
    {
        // Retrieve previous calibration data from FLASH
        magcal_get_calibration_from_flash();
        debug_printf("error occured during magcal O\n");
    }
    return 0;
}

//----------------------------------------------------------------------
static feature_compass_calibration_states_t _do_state_gyro(compcalib_private_ctx_t * ctx)
{
    int8_t ret;
    float euc_norm, acc_axes[3], gyro_axes[3];

    //-------------------------------------------
    // Using the accelerometer we verify that
    // the watch do not move during calibration.
    //-------------------------------------------
    ret = LSM9DS1_GetAccelerometerValuesG(&acc_axes[0],
                                          &acc_axes[1],
                                          &acc_axes[2]);
    if ( ret )
    {
        debug_printf("invalid data from accelerometer\n");
        return compass_calibration_gyro;
    }

    // compute the norm which should be 1.0 when we are not moving
    euc_norm = euclidian_norm(COUNTOF(acc_axes),&acc_axes[0]);

    // save the biggest value we got
    if ( euc_norm > ctx->gyro_max_norm )
    {
        ctx->gyro_max_norm = euc_norm;
    }

    // and the minimal value as well
    if ( euc_norm < ctx->gyro_min_norm )
    {
        ctx->gyro_min_norm = euc_norm;
    }

    // if the gap between these is more than just noise
    // we consider that the watch is moving. Then restart the process
    if ( (ctx->gyro_max_norm - ctx->gyro_min_norm) > MAGCAL_FEATURE_OVERDRAFT_THRESHOLD )
    {
        ctx->gyro_max_norm      = 0.0;
        ctx->gyro_min_norm      = 10.0;
        ctx->gyro_data_count    = 0;
        memset(&ctx->gyro_cumulated_values[0], 0,
               sizeof(ctx->gyro_cumulated_values));
        return compass_calibration_gyro;
    }

    //-------------------------------------------
    // Process accelerometer now
    //-------------------------------------------
    ret =LSM9DS1_GetGyroscopeValuesDPS(&gyro_axes[0],
                                       &gyro_axes[1],
                                       &gyro_axes[2]);
    if ( ret )
    {
        debug_printf("invalid data from gyroscope\n");
        return compass_calibration_gyro;
    }

    ctx->gyro_data_count++;
    ctx->gyro_cumulated_values[0] += gyro_axes[0];
    ctx->gyro_cumulated_values[1] += gyro_axes[1];
    ctx->gyro_cumulated_values[2] += gyro_axes[2];

    if ( ctx->gyro_data_count == MAGCAL_FEATURE_SAMPLE_COUNT )
    {
        gyro_axes[0] = ctx->gyro_cumulated_values[0] / ctx->gyro_data_count;
        gyro_axes[1] = ctx->gyro_cumulated_values[1] / ctx->gyro_data_count;
        gyro_axes[2] = ctx->gyro_cumulated_values[2] / ctx->gyro_data_count;
        ret = agm_gyro_update_bias(&gyro_axes[0]);
        if ( ret )
        {
            debug_printf("error while updating gyro bias\n");
        }

        return compass_calibration_idle;
    }

    return compass_calibration_gyro;
}

