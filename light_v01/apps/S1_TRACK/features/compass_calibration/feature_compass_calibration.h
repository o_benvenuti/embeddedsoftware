#ifndef _FEATURE_COMPASS_CALIBRATION_H_
#define _FEATURE_COMPASS_CALIBRATION_H_
/**
 * @file:  feature_compass_calibration.h
 *
 * @brief
 *
 * @date   24/01/2019
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define FEATURE_COMPASS_CALIBRATION_READY  WM_USER + 0x06

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
enum _feature_compass_calibration_button_value {
    BUTTON_RDY = 0,
};

typedef enum _feature_compass_calibration_states_t {
    compass_calibration_idle,
    compass_calibration_gyro,
    compass_calibration_8,
    compass_calibration_O,
}feature_compass_calibration_states_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateCompass_calibration_8_Window(void);
WM_HWIN CreateCompass_calibration_ready_O_Window(void);
WM_HWIN CreateCompass_calibration_O_Window(void);

#endif  // _FEATURE_COMPASS_CALIBRATION_H_
