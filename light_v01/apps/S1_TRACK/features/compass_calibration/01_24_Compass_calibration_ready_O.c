//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_feature.h>
#include <DIALOG.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <queue.h>
#include <trace.h>
#include <babel.h>

#include "feature_compass_calibration.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x01)

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "CompassCalibration_Window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "ready",
                ID_BUTTON_0, 13, 76, 133, 51, 0, 0x0, 0
        },
};

bool gyro_calibration_done = false;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced9pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;
    al_feature_msg_t feat_msg;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            hItem = pMsg->hWin;
            gyro_calibration_done = false;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x005B91AB));
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(hItem, _cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            if ( Id == ID_BUTTON_0 ) // Notifications sent by 'ready'
            {
                if ( NCode == WM_NOTIFICATION_RELEASED )
                {
                    if ( gyro_calibration_done == true )
                    {
                        feat_msg.cmd = CMD_GUI_INPUT;
                        feat_msg.data.value = BUTTON_RDY;
                        al_feature_send_to_tsk(&feat_msg);
                    }
                }
            }
            WM_Invalidate(pMsg->hWin);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateCompass_calibration_ready_O_Window(void)
{
    return GUI_CreateDialogBox(_aDialogCreate,
                               GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case FEATURE_COMPASS_CALIBRATION_READY:
        {
            gyro_calibration_done = true;
            break;
        }
        case WM_PAINT:
        {
            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); /* normal blue */
            GUI_Clear();

            /* fill background */
            GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
            GUI_AA_FillRoundedRect(8, 10, 152, 122,5);

            /* text */
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_TITLE),80,25);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
            GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_L1),80,26+(1*14));
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_L2),80,26+(2*14));
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_L3),80,26+(3*14));
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_L4),80,26+(4*14));

            /* 'yes' button */
            if ( gyro_calibration_done == true )
            {
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect(13, 96, 146, 117,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_BUTTON),80,107);
            }
            else
            {
                GUI_SetColor(GUI_DARKGRAY);
                GUI_AA_FillRoundedRect(13, 96, 146, 117,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_LIGHTGRAY);
                GUI_DispStringAt(languages_get_string(TXT_MAGCAL_READY_O_BUTTON),80,107);
            }

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}

