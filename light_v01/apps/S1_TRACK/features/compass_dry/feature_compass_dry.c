/*
 * feature_compass_dry.C
 *
 *  Created on: 12 feb. 2019
 *      Author: Aix Sonic
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <common.h>
#include <accel_lib.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <stm32l4xx_hal.h>
#include <touchscreen.h>
#include <trace.h>
#include <LSM9DS1.h>
#include <compass.h>
#include <magcal.h>
#include <math.h>

#include "feature_compass_dry.h"

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct compass_dry_private_ctx {
    uint32_t start;
    uint16_t prev_heading;
    uint16_t prev_start_heading;
    WM_HWIN current_window;
    serenity_state_t parent_state;
}compass_dry_private_ctx_t;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static int _compass_dry_init_func(void ** priv_data, int parent);
static int _compass_dry_gest_func(void * priv_data, gestures_t gesture);
static int _compass_dry_event_func(void * priv_data, al_feature_cmd_t cmd);
static int _compass_dry_timer_func(void * priv_data, int time);
static int _compass_dry_button_func(void * priv_data, uint32_t button);
static int _compass_dry_release_func(void * priv_data);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_compass_dry_ctx = {
        .name       = "COMPASS DRY",
        .ini_func   = _compass_dry_init_func,
        .usr_func   = _compass_dry_gest_func,
        .evt_func   = _compass_dry_event_func,
        .tmr_func   = _compass_dry_timer_func,
        .gui_func   = _compass_dry_button_func,
        .rls_func   = _compass_dry_release_func,
};

static compass_dry_private_ctx_t _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _compass_dry_init_func(void ** priv_data, int parent)
{
    al_feature_msg_t feat_msg;

    // save parent feature
    _private_ctx.prev_heading = 0;
    _private_ctx.parent_state = parent;
    _private_ctx.start = HAL_GetTick();

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    // create screen
    _private_ctx.current_window = CreateCompass_dry_Window();

    /* activate the component */
    if ( LSM9DS1_PowerUp() < 0 )
    {
        debug_printf("Compass dry feature - impossibility to start component\n");
    }

    // set feature timer/timeout to 50ms
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 50;
    al_feature_send_to_tsk(&feat_msg);

    // return private context
    *priv_data = &_private_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_timer_func(void * priv_data, int time)
{
    (void)time;
    uint16_t heading;
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

	//******************** DATA ACQUISITION ***********************
    // update ibb with AGM
	float acc[3] = {0};
	float mag[3] = {0};

	// ACCELEROMETER
	if(LSM9DS1_GetAccelerometerValuesG(&acc[0],&acc[1],&acc[2])<0)
	{
		debug_printf("error getting Accelerometer values\n");
		return -1;
	}
	// MAGNETOMETER
	if(LSM9DS1_GetMagnetometerValuesGs(&mag[0],&mag[1],&mag[2])<0)
	{
		debug_printf("error getting Magnetometer values\n");
		return -1;
	}
	// compensate magnetometer with calibration matrix
	magcal_compensate_mag(mag);

	COMPASS_update_heading(mag,acc);

	//*************************************************************
    heading = COMPASS_get_heading();


    /* if the value changed, update compass window */
    if ( heading != ctx->prev_heading )
    {
        WM_Invalidate(ctx->current_window);
        ctx->prev_heading = heading;
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t feat_msg;
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = serenity_state_charging;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        case CMD_DVE_START:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        case CMD_RCV_FLICK:
        {
            // return to previous state
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        default:
        {
            debug_printf("Unknown button\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_release_func(void * priv_data)
{
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    /* turn off the component */
    LSM9DS1_PowerDown();
    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t feat_msg;
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    switch ( gesture )
    {
        case swipe_right:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        default:
        {
            debug_printf("unknown gesture\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_button_func(void * priv_data, uint32_t button)
{
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( button )
    {
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}

