#ifndef _FEATURE_CHARGING_H_
#define _FEATURE_CHARGING_H_
/**
 * @file:  feature_charging.h
 *
 * @brief
 *
 * @date   28/09/2018
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */

//----------------------------------------------------------------------
// Constant (for UI)
//----------------------------------------------------------------------
#define FEATURE_BAT_UPDATE             WM_USER + 0x06

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateCharging_Window(void);

#endif  // _FEATURE_CHARGING_H_
