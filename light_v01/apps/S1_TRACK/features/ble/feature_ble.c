/**
 * @file:  feature_ble.c
 *
 * @brief
 *
 * @date   25/09/2018
 *
 * <b>Description:</b>\n
 *    BLE functionnality
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <string.h>

#include <common.h>
#include <al_feature.h>
#include <al_ble.h>

#include <trace.h>
#include <GUI_Serenity.h>
#include <touchscreen.h>

#include "feature_ble.h"
#include "feature_ble_private.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _ble_init_func(void ** priv_data, int parent);
int _ble_release_func(void * priv_data);
int _ble_timer_func(void * priv_data, int time);
int _ble_button_func(void * priv_data, uint32_t);
int _ble_gest_func(void * priv_data, gestures_t gesture);
int _ble_event_func(void * priv_data, al_feature_cmd_t cmd);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_ble_ctx = {
        .name       = "BLE",
        .ini_func   = _ble_init_func,
        .rls_func   = _ble_release_func,
        .usr_func   = _ble_gest_func,
        .evt_func   = _ble_event_func,
        .tmr_func   = _ble_timer_func,
        .gui_func   = _ble_button_func,
};

static feat_ble_priv_ctx_t _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _ble_init_func(void ** priv_data, int parent)
{
    al_feature_msg_t feat_msg;

    // set initial state
    _private_ctx.firmware_size = 0;
    _private_ctx.scan_dev_count = 0;
    _private_ctx.upgrade_flash_addr = 0;
    _private_ctx.selected_broadcast = -1;
    _private_ctx.parent_state = parent;
    _private_ctx.current_state = ble_state_idle;
    _private_ctx.next_state = ble_state_idle;
    memset(_private_ctx.scan_data, 0, sizeof(_private_ctx.scan_data));

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    // create and display BLE window
    _private_ctx.current_window = CreateBLE_Window();

    // set feature timer/timeout to 1second
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 1;
    al_feature_send_to_tsk(&feat_msg);

    // return context
    *priv_data = &_private_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _ble_release_func(void * priv_data)
{
    (void)priv_data;

    /* power down BLE */
    al_ble_deinit();

    return 0;
}

//----------------------------------------------------------------------
int _ble_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    feat_ble_priv_ctx_t * ctx = (feat_ble_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    // process command
    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            // do not exit if we upgrade the watch
            if ( ctx->current_state != ble_state_upgrade )
            {
                msg.cmd = CMD_END_STATE;
                msg.data.value = serenity_state_charging;
                al_feature_send_to_tsk(&msg);
            }
            break;
        }
        case CMD_DVE_START:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("unknown event\n");
            return -1;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _ble_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc;
    feat_ble_priv_ctx_t * ctx = (feat_ble_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    // update to new state
    if ( ctx->next_state != ctx->current_state )
    {
        rc = _prepare_next_state(ctx);
        if ( rc )
        {
            debug_printf("unable to prepare state\n");
            ctx->next_state = ctx->current_state;
        }
    }

    // switch state
    ctx->current_state = ctx->next_state;

    // run state function
    if ( _state_table[ctx->current_state] )
    {
        ctx->next_state = _state_table[ctx->current_state](ctx);
    }

    return 0;
}

//----------------------------------------------------------------------
int _ble_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t msg;
    feat_ble_priv_ctx_t * ctx = (feat_ble_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( gesture )
    {
        case swipe_right:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("gesture not managed\n");
            break;
        }
    }

    return 0;
}


//----------------------------------------------------------------------
int _ble_button_func(void * priv_data, uint32_t button)
{
    int rc = 0;
    WM_HWIN tmp_win;
    WM_MESSAGE pMsg;
    serenity_context_t context;
    feat_ble_priv_ctx_t * ctx = (feat_ble_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( button )
    {
        case BUTTON_APP:
        {
            if ( (ctx->current_state == ble_state_app)      ||
                 (ctx->current_state == ble_state_listen)   ||
                 (ctx->current_state == ble_state_upgrade)   )
            {
                ctx->next_state = ble_state_idle;
            }
            else
            {
                ctx->next_state = ble_state_listen;
            }
            break;
        }
        case BUTTON_RCV:
        {
            if ( ctx->current_state == ble_state_scanning )
            {
                ctx->next_state = ble_state_idle;
            }
            else
            {
                ctx->scan_dev_count = 0;
                ctx->selected_broadcast = -1;
                ctx->next_state = ble_state_scanning;
            }
            break;
        }
        case BUTTON_BRD:
        {
            if ( ctx->current_state == ble_state_broadcast)
            {
                ctx->next_state = ble_state_idle;
            }
            else
            {
                ctx->next_state = ble_state_broadcast;
            }
            break;
        }
        case BUTTON_SAVE_SCN:
        {
            // sanity check
            if ( ctx->selected_broadcast < 0 || ctx->selected_broadcast > ctx->scan_dev_count )
            {
                debug_printf("invalid selected broadcast\n");
                break;
            }

            // get context from flash
            rc = serenity_retrieve_status(&context);
            if ( rc )
            {
                debug_printf("unable to retrieve context\n");
            }

            uint8_t * p = &ctx->scan_data[ctx->selected_broadcast].data[0];
            context.deco_params.o2_ratio = (float)((float)*p++ / 100);
            context.deco_params.ppo2 = (float)((float)*p++ / 100);
            context.deco_params.max_speed = *p++;
            context.deco_params.safety_factor = (float)((float)*p++ / 100);
            context.depth_params.depth_max = (float)*p++;
            context.depth_params.dive_duration = *p++;
            context.depth_params.stop_nb = *p++;
            if ( context.depth_params.stop_nb > NB_PALIERS_MAX )
            {
                context.depth_params.stop_nb = 0;
            }

            for ( int i = 0; i < context.depth_params.stop_nb; ++i)
            {
                context.depth_params.dive_stops[i].StopDepth = (float)*p++;
                context.depth_params.dive_stops[i].StopDuration = (uint16_t)(*p++ * 60);
                context.depth_params.dive_stops[i].Ascent_speed = *p++;
            }


            // save new context to the flash
            rc = serenity_save_status(&context);
            if ( rc )
            {
                debug_printf("unable to save context\n");
            }

            ctx->next_state = ble_state_idle;

            // create and display BLE window
            tmp_win = CreateBLE_Window();
            GUI_Switch_Screens(ctx->current_window, tmp_win);

            // and delete previous
            WM_DeleteWindow(ctx->current_window);
            ctx->current_window = tmp_win;

            // and send it local context
            pMsg.MsgId = FEATURE_UPDATE_STATE;
            pMsg.Data.v = (int)ctx->next_state;
            WM_SendMessage(ctx->current_window, &pMsg);
            WM_Invalidate(ctx->current_window);

            break;
        }
        default:
        {
            break;
        }
    }

    return 0;
}

