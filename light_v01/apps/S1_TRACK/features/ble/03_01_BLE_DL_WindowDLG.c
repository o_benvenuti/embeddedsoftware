//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include "feature_ble.h"
#include "common.h"
#include <GUI_Serenity.h>
#include <babel.h>

//----------------------------------------------------------------------
// defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)

//----------------------------------------------------------------------
// External data
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_bluetoothpetit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

//----------------------------------------------------------------------
// Static data
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
  { WINDOW_CreateIndirect, "BLE_Upgrade_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

static void _cbBLEMenuBackground(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg) {
    WM_HWIN hItem;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cbBLEMenuBackground);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
static void _cbBLEMenuBackground(WM_MESSAGE * pMsg) {
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            /* draw background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_Draw_MenuBar_Dots(1,1);
            /* draw bluetooth logo */
            GUI_DrawBitmap(&bmcran_Serenity_13a_bluetoothpetit, 6, 0);

            /* draw button text */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_ClearRect(0, 38, 53, 128);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_LEFT);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_BLE_DOWNLOADING),30,60);

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
// public API
//----------------------------------------------------------------------
WM_HWIN CreateBLE_Upgrade_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}


