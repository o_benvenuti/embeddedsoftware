//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_feature.h>
#include <DIALOG.h>
#include <ds2782_driver.h>
#include <stdio.h>
#include <al_tools.h>
#include "GUI_Serenity.h"
#include <User_RTC.h>
#include <trace.h>
#include <languages.h>
#include <babel.h>

#include "feature_default.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ID_WINDOW_0        (GUI_ID_USER + 0x00)
#define ID_BUTTON_0        (GUI_ID_USER + 0x01)
#define ID_BUTTON_1        (GUI_ID_USER + 0x02)
#define ID_BUTTON_2        (GUI_ID_USER + 0x03)
#define ID_BUTTON_3        (GUI_ID_USER + 0x04)

//----------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------
static int sequence = 0;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "HourScreen",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "0",
                ID_BUTTON_0, 0, 0, 160, 128, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "1",
                ID_BUTTON_1, 55, 0, 50, 50, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "2",
                ID_BUTTON_2, 0, 57, 50, 50, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "3",
                ID_BUTTON_3, 110, 80, 50, 50, 0, 0x0, 0
        },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldNumbers48pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedRegularNumbers48pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battrouge;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battorange;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battverte;
extern GUI_CONST_STORAGE GUI_BITMAP bmlock;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg);
static void _cbHourScreenBackground(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
WM_HWIN Create_00_01_HourScreen(void)
{
    return GUI_CreateDialogBox(_aDialogCreate,
                               GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
}

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    int     NCode;
    int     Id;
    al_feature_msg_t feat_msg;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialisation of 'HourScreen'
            //
            WINDOW_SetBkColor(pMsg->hWin, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(pMsg->hWin,_cbHourScreenBackground);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                    GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                    GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                    GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                    GUI_transparent_button_cb);

            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_BUTTON_0:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        /*button clicked */
                        sequence = 0;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_1:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        /*button clicked */
                        if(sequence == 0) sequence = 1;
                        else sequence = 0;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_2:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        /*button clicked */
                        if(sequence == 1) sequence = 2;
                        else sequence = 0;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_3:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        /*button clicked */
                        if ( sequence == 2 )
                        {
                            sequence = 0;
                            feat_msg.cmd = CMD_GUI_INPUT;
                            feat_msg.data.value = BUTTON_MENU;
                            al_feature_send_to_tsk(&feat_msg);
                        }
                        else
                        {
                            sequence = 0;
                            WM_Invalidate(pMsg->hWin);
                        }
                    }
                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
static void _cbHourScreenBackground(WM_MESSAGE * pMsg)
{
    char Tstr[50];
    uint8_t rc;
    uint8_t battery_state = 0;
    TimeType Time = RTC_GetTime();
    normalizeTimeType(&Time);
    bool h12 =  RTC_is12hMode();
    bool mmdd = RTC_ismmddMode();
    bool pm = false;
    static bool nofly = false;

    if((bcd2int(Time.Hours)>12)&&(h12))
    {
        pm = true;
        Time.Hours -= int2bcd(12);
    }

    switch (pMsg->MsgId)
    {
        case FEATURE_DEFAULT_SET_NOFLY :
        {
            nofly = pMsg->Data.v;
            break;
        }
        case WM_PAINT:
        {
            // background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); // normal blue
            GUI_Clear();

            // Display Hours
            GUI_SetColor(GUI_WHITE);
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldNumbers48pts);
            GUI_SetTextAlign(GUI_TA_RIGHT|GUI_TA_VCENTER);
            sprintf(Tstr, "%02d",bcd2int(Time.Hours));
            GUI_DispStringAt(Tstr,X_1_2-5,Y_1_2+5);

            // Display Minutes
            GUI_SetTextAlign(GUI_TA_LEFT|GUI_TA_VCENTER);
            GUI_SetFont(&GUI_FontSairaSemiCondensedRegularNumbers48pts);
            sprintf(Tstr, ":%02d",bcd2int(Time.Minutes));
            GUI_DispStringAt(Tstr,X_1_2-5,Y_1_2+5);

            // AM/PM text
            GUI_SetTextAlign(GUI_TA_TOP);
            GUI_SetColor(GUI_WHITE);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);

            if ( h12 )
            {
                GUI_SetTextAlign(GUI_TA_RIGHT);
                if(pm){GUI_DispStringAt("PM", 160-8, 5);}
                else  {GUI_DispStringAt("AM", 160-8, 5);}
            }


            sprintf(Tstr, "%s %d.%d.20%02d",
                    languages_get_string(bcd2int(Time.WeekDay)),
                    (mmdd ? bcd2int(Time.Month)  : bcd2int(Time.Date)),
                    (mmdd ? bcd2int(Time.Date) : bcd2int(Time.Month)),
                    bcd2int(Time.Year));

            GUI_SetTextAlign(GUI_TA_HCENTER);
            GUI_DispStringAt(Tstr,X_1_2,Y_SIZE-25);

            // Display Battery Value
            rc = BAT_LoadValue(&battery_state);
            if ( rc )
            {
                debug_printf("error while reading battery state\n");
            }

            // display battery icon
            if ( battery_state >= 30 )
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battverte, 3, 5);
            }
            else if ( ISBETWEEN(battery_state, 10, 29) )
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battorange, 3, 5);
            }
            else
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battrouge, 3, 5);
            }

            // display battery percentage
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_TOP);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_DispDecAt(battery_state, 18, 5,
                    (battery_state > 99 ? 3 : battery_state > 9 ? 2 : 1));
            GUI_DispString("%");

            // display no fly notification
            if ( nofly )
            {
                GUI_DrawBitmap(&bmicon_nofly, X_SIZE - (h12 ? 60 : 30), 5);
            }

            // display the menu bar
            GUI_SetColor(GUI_BLACK);
            GUI_SetPenSize(1);
            GUI_DrawLine(0, 33, X_SIZE, 33);

            // display lock buttons
            if ( sequence == 0 )
            {
                GUI_DrawBitmap(&bmlock, 65, 2 );
            }
            else if( sequence == 1 )
            {
                GUI_DrawBitmap(&bmlock, 3, 67 );
            }
            else if ( sequence == 2 )
            {
                GUI_DrawBitmap(&bmlock, 130, 98);
            }

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}

