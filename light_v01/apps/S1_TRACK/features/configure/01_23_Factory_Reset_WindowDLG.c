//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <stdio.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <ILI9163_driver.h>
#include <eflash.h>
#include <stm32l4xx_hal.h>
#include <User_RTC.h>
#include <babel.h>

#include <inttypes.h>
#include <al_tools.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x01)
#define ID_BUTTON_1    (GUI_ID_USER + 0x02)
#define ID_BUTTON_2    (GUI_ID_USER + 0x03)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "WatchSettings_Window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "FactoryReset",
                ID_BUTTON_0, 10, 28, 140, 57, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "no",
                ID_BUTTON_1, 13, 96, 62, 21, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "yes", ID_BUTTON_2,
                85, 96, 62, 21, 0, 0x0, 0
        },
};

bool FactoryResetButtonPressed = false;
bool YesButtonPressed = false;
bool ReadyToReset = false;

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced9pts;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cbRepaint);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'FactoryReset'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        FactoryResetButtonPressed = true;
                        WM_InvalidateWindow(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'NO'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        FactoryResetButtonPressed = false;
                        WM_InvalidateWindow(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_2: // Notifications sent by 'YES'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(FactoryResetButtonPressed)
                        {
                            YesButtonPressed = true;
                        }
                        WM_InvalidateWindow(pMsg->hWin);
                    }
                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN Create01_23_Factory_Reset_Window(void)
{
    return GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
            _cbDialog, WM_HBKWIN, 0, 0);
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char buff[32];
    uint64_t unique_id[2];

    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); /* normal blue */
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);
            GUI_Draw_MenuBar_Dots(4,4);

            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            if(!FactoryResetButtonPressed)
            {
                /* draw background button & text */
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect  (20, 28, 140, 85,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652)); /* normal blue */
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_BUTTON_L1),80,51);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_BUTTON_L2),80,51+14);

                GUI_SetColor(GUI_WHITE);
                GUI_SetTextAlign(GUI_TA_LEFT | GUI_TA_VCENTER);
                GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
                GUI_DispStringAt("UID : ",20,100);

                al_tools_read_uid(&unique_id[0]);

                // copy to buffer
                snprintf(buff, 13, "%s", (char *)&unique_id[0]);
                GUI_DispString(buff);

                snprintf(buff, sizeof(buff), "SW : %s v%d", PROJECT_NAME,
                                                 PROJECT_FIRMWARE_VERSION);
                GUI_DispStringAt(buff,20,114);
            }
            else
            {
                /* fill background */
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_AA_FillRoundedRect(8, 21, 152, 122,5);

                /* text */
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00E3001B)); /* RED */
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_WARNING),80,30);
                GUI_SetColor(GUI_WHITE);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_L1),80,30+(1*14));
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_L2),80,30+(2*14));
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_L3),80,30+(3*14));
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_FACTORY_RESET_L4),80,30+(4*14));

                /* 'no' button */
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect(13, 96, 75, 117,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_DispStringAt(languages_get_string(TXT_NO),44,107);

                /* 'yes' button */
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect(85, 96, 146, 117,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_DispStringAt(languages_get_string(TXT_YES),116,107);
            }
            if((FactoryResetButtonPressed)&&(YesButtonPressed))
            {
                /* fade out */
                for(int i = get_BaklightIntensity() ; i >=0 ; i--)
                {
                    change_BaklightIntensity(i);
                    HAL_Delay(10);
                }

                serenity_factory_reset();
            }
            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}

