#ifndef _FEATURE_CONFIGURE_H_
#define _FEATURE_CONFIGURE_H_
/**
 * @file:  feature_configure.h
 *
 * @brief
 *
 * @date   07/02/2019
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define FEATURE_CONFIGURE_SHARE_CTX  WM_USER + 0x06

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
enum _feature_configure_button_value {
    BUTTON_DEPTH = 0,
    BUTTON_WATCH,
    BUTTON_DECO,
    BUTTON_FREE,
	BUTTON_MAGCAL
};

//not really needed outside but needed for context
enum feature_configure_state {
    configure_state_mode,
    configure_state_deco,
    configure_state_free,
    configure_state_stop,
    configure_state_time,
    configure_state_depth,
    configure_state_watch,
    configure_state_reset,
    configure_state_deco_sub,
    configure_state_watch_sub,
};

typedef struct feat_conf_priv_ctx {
    uint8_t current_stop;
    WM_HWIN current_window;
    serenity_context_t global_ctx;
    serenity_state_t parent_state;
    enum feature_configure_state current_state;
}feat_conf_priv_ctx_t;


//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN Create01_10_ModeSelection_Window(void);
WM_HWIN CreateParam_Plongee_01_Window(void);
WM_HWIN CreateParam_Palier_Window(void);
WM_HWIN Create_01_12_0_Param_Algo_Window(void);
WM_HWIN Create_01_12_1_Param_Algo_2_Window(void);
WM_HWIN CreateWatchSettings_Window_0(void);
WM_HWIN CreateWatchSettings_Window_1(void);
WM_HWIN CreateTimeSetting_Window(void);
WM_HWIN Create01_23_Factory_Reset_Window(void);
WM_HWIN CreateParam_Free_Dive_Window(void);

#endif  // _FEATURE_CONFIGURE_H_

