//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <common.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <languages.h>
#include <babel.h>
#include <trace.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x01)
#define ID_BUTTON_1    (GUI_ID_USER + 0x02)
#define ID_BUTTON_2    (GUI_ID_USER + 0x03)
#define ID_BUTTON_3    (GUI_ID_USER + 0x04)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "ParamPlongeeMenu_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "ModeDive_Button", ID_BUTTON_0, 0, 16, 80, 56, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "ModeBuhl_Button", ID_BUTTON_1, 80, 17, 80, 56, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "ModeFree_Button", ID_BUTTON_2, 0, 73, 80, 56, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Watch_Settings_Button", ID_BUTTON_3, 80, 73, 80, 56, 0, 0x0, 0 },
};

static void _cbDiveMenuBackground(WM_MESSAGE * pMsg);
extern GUI_CONST_STORAGE GUI_BITMAP bmIconSettings;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictodiver_small;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictodiver_small_blue;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictofreediving_small;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictofreediving_small_blue;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictogauge_small;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictogauge_small_blue;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced9pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmsmalltick12x12;


//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;
    al_feature_msg_t feat_msg;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialization of 'ParamPlongeeMenu_Window'
            //
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));

            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(pMsg->hWin,_cbDiveMenuBackground);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'DepthMeter'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        feat_msg.cmd = CMD_GUI_INPUT;
                        feat_msg.data.value = BUTTON_DEPTH;
                        al_feature_send_to_tsk(&feat_msg);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'Deco'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        feat_msg.cmd = CMD_GUI_INPUT;
                        feat_msg.data.value = BUTTON_DECO;
                        al_feature_send_to_tsk(&feat_msg);
                    }
                    break;
                }
                case ID_BUTTON_2: // Notifications sent by 'FreeDive'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        	feat_msg.cmd = CMD_GUI_INPUT;
                        	feat_msg.data.value = BUTTON_FREE;
                        	al_feature_send_to_tsk(&feat_msg);
                    }
                    break;
                }
                case ID_BUTTON_3: // Notifications sent by 'Parameters'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        feat_msg.cmd = CMD_GUI_INPUT;
                        feat_msg.data.value = BUTTON_WATCH;
                        al_feature_send_to_tsk(&feat_msg);
                    }
                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN Create01_10_ModeSelection_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbDiveMenuBackground(WM_MESSAGE * pMsg)
{
    static feat_conf_priv_ctx_t * configure_ctx = NULL;
    GUI_CONST_STORAGE GUI_BITMAP * imgptr = NULL;
    int8_t x_pos = 0;
    int8_t y_pos = 0;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            configure_ctx = NULL;
            break;
        }
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            configure_ctx = (feat_conf_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            if ( ! configure_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw separation lines */
            GUI_SetPenSize(1);
            GUI_SetColor(GUI_BLACK);
            GUI_AA_DrawLine(X_1_2, 0, X_1_2, Y_SIZE);
            GUI_AA_DrawLine(0, ((LCD_GetYSize()-14)/2)+14, LCD_GetXSize(), ((LCD_GetYSize()-14)/2)+14);

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, LCD_GetXSize(), 14);

            /* draw dots */
            GUI_Draw_MenuBar_Dots(1,1);

            GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_MODE),4,-2);


            /* draw buttons */


			// top left - depth meter mode
            if((configure_ctx->global_ctx.option_code == DEPT__________) ||
               (configure_ctx->global_ctx.option_code == DEPT______FREE) ||
               (configure_ctx->global_ctx.option_code == DEPT_DECO_____) ||
           	   (configure_ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
            	imgptr = &bmcran_Serenity_16B_pictogauge_small;
            }
            else
            {
            	imgptr = &bmcran_Serenity_16B_pictogauge_small_blue;
            }
            x_pos =  LCD_GetXSize()/4     - imgptr->XSize/2;
            y_pos = (LCD_GetYSize()-14)/4 - imgptr->YSize/2 + 14;
            GUI_DrawBitmap(imgptr,x_pos,y_pos);

            // top right - decompression algo mode
            if((configure_ctx->global_ctx.option_code == _____DECO_____) ||
               (configure_ctx->global_ctx.option_code == _____DECO_FREE) ||
               (configure_ctx->global_ctx.option_code == DEPT_DECO_____) ||
           	   (configure_ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
            	imgptr = &bmcran_Serenity_16B_pictodiver_small;
            }
            else
            {
            	imgptr = &bmcran_Serenity_16B_pictodiver_small_blue;
            }
            x_pos =  LCD_GetXSize()-LCD_GetXSize()/4 - imgptr->XSize/2;
            y_pos = (LCD_GetYSize()-14)/4            - imgptr->YSize/2 + 14;
            GUI_DrawBitmap(imgptr,x_pos,y_pos);

            // bottom left - free dive mode
            if((configure_ctx->global_ctx.option_code == __________FREE) ||
               (configure_ctx->global_ctx.option_code == _____DECO_FREE) ||
               (configure_ctx->global_ctx.option_code == DEPT______FREE) ||
		       (configure_ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
            	imgptr = &bmcran_Serenity_16B_pictofreediving_small;
            }
            else
            {
            	imgptr = &bmcran_Serenity_16B_pictofreediving_small_blue;
            }
            x_pos = LCD_GetXSize()/4                     - imgptr->XSize/2;
            y_pos = LCD_GetYSize()-(LCD_GetYSize()-14)/4 - imgptr->YSize/2;
            GUI_DrawBitmap(imgptr,x_pos,y_pos);


            // bottom right - settings
            GUI_DrawBitmap(&bmIconSettings,
            		LCD_GetXSize()-LCD_GetXSize()/4 - bmIconSettings.XSize/2,
					LCD_GetYSize()-(LCD_GetYSize()-14)/4 - bmIconSettings.YSize/2);

            // draw tick to indicate selected & active dive mode
            switch ( configure_ctx->global_ctx.dive_mode )
            {
            	case Mode_BottomTimer:
            	{
            		GUI_DrawBitmap(&bmsmalltick12x12,(LCD_GetXSize()/2) - 18,((LCD_GetYSize()/2)+7) - 18);
            		break;
            	}
            	case Mode_DecoAlgo:
            	{
            		GUI_DrawBitmap(&bmsmalltick12x12,(LCD_GetXSize()) - 18,((LCD_GetYSize()/2)+7) - 18);
            		break;
            	}
            	case Mode_FreeDive:
            	{
            		GUI_DrawBitmap(&bmsmalltick12x12,(LCD_GetXSize()/2) - 18,(LCD_GetYSize() - 18));
            		break;
            	}
                default:
                	break;
            }

          break;
      default:
          WINDOW_Callback(pMsg);
      }
    }
}
