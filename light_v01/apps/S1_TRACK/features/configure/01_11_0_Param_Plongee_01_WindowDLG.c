//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <common.h>
#include <stdio.h>
#include <GUI_Serenity.h>
#include <languages.h>
#include <babel.h>
#include <trace.h>
#include <al_tools.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_BUTTON_0              (GUI_ID_USER + 0x01)
#define ID_BUTTON_1              (GUI_ID_USER + 0x02)
#define ID_BUTTON_2              (GUI_ID_USER + 0x03)
#define ID_BUTTON_3              (GUI_ID_USER + 0x04)
#define ID_BUTTON_4              (GUI_ID_USER + 0x05)
#define ID_BUTTON_5              (GUI_ID_USER + 0x06)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDiveMenuBackground(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
bool Palier_Button_Clicked = false;
bool Profondeur_Button_Clicked = false;
bool Temps_Button_Clicked = false;
bool Up_Button_Clicked;
bool Down_Button_Clicked;
static serenity_context_t * _global_ctx = NULL;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Param_Plongee_01_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Palier_Button", ID_BUTTON_0, 0, 14, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Profondeur_Button", ID_BUTTON_1, 0, 52, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Temps_Button", ID_BUTTON_2, 0, 90, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus_Button", ID_BUTTON_3, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button", ID_BUTTON_4, 85, 88, 75, 40, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialization of 'Param_Plongee_01_Window'
            //
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_4),
                           GUI_transparent_button_cb);
            Palier_Button_Clicked = true;
            Profondeur_Button_Clicked = false;
            Temps_Button_Clicked = false;
            Up_Button_Clicked = false;
            Down_Button_Clicked = false;
            _global_ctx = NULL;
            break;
        }
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            _global_ctx = &(((feat_conf_priv_ctx_t *)pMsg->Data.p)->global_ctx);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbDiveMenuBackground);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;

            if ( ! _global_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'Palier_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Button_Clicked = true;
                        Profondeur_Button_Clicked = false;
                        Temps_Button_Clicked = false;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'Profondeur_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Button_Clicked = false;
                        Profondeur_Button_Clicked = true;
                        Temps_Button_Clicked = false;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_2: // Notifications sent by 'Temps_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Button_Clicked = false;
                        Profondeur_Button_Clicked = false;
                        Temps_Button_Clicked = true;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_3: // Notifications sent by 'Plus_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        Up_Button_Clicked = true;
                    }

                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Up_Button_Clicked = false;
                        if(Palier_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.stop_nb < NB_PALIERS_MAX )
                            {
                                _global_ctx->depth_params.stop_nb++;
                            }
                        }
                        if(Profondeur_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.depth_max < MAX_DEPTH_MAX )
                            {
                                _global_ctx->depth_params.depth_max++;
                            }
                        }
                        if(Temps_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.dive_duration < DIVE_TIME_MAX )
                            {
                                _global_ctx->depth_params.dive_duration+=5;
                            }
                            if ( _global_ctx->depth_params.dive_duration  > DIVE_TIME_MAX-5 )
                            {
                                _global_ctx->depth_params.dive_duration= DIVE_TIME_MAX;
                            }
                        }
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_4: // Notifications sent by 'Moins_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        Down_Button_Clicked = true;
                    }

                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Down_Button_Clicked = false;
                        if(Palier_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.stop_nb > NB_PALIERS_MIN )
                            {
                                _global_ctx->depth_params.stop_nb--;
                            }
                        }
                        if(Profondeur_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.depth_max > MAX_DEPTH_MIN )
                            {
                                _global_ctx->depth_params.depth_max--;
                            }
                        }
                        if(Temps_Button_Clicked)
                        {
                            if ( _global_ctx->depth_params.dive_duration > DIVE_TIME_MIN )
                            {
                                _global_ctx->depth_params.dive_duration-=5;
                            }
                            if ( _global_ctx->depth_params.dive_duration < DIVE_TIME_MIN+5 )
                            {
                                _global_ctx->depth_params.dive_duration = DIVE_TIME_MIN;
                            }
                        }
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateParam_Plongee_01_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

// USER START (Optionally insert additional public code)
static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};

static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbDiveMenuBackground(WM_MESSAGE * pMsg)
{
    uint8_t min, hr;
    char Tstr[8];

    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw arrows */
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 19);

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            /* draw dots */
            GUI_Draw_MenuBar_Dots(_global_ctx->depth_params.stop_nb+2,2);

            /* draw buttons */
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            if(Palier_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 15, 80, 52);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_STOPS),4,18);

            if(Profondeur_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 53, 80, 90);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_MAX_DEPTH),4,59);

            if(Temps_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 91, 80, 128);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_DIVE_TIME),4,95);

            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
            if(Palier_Button_Clicked)
            {
                GUI_DispDecAt(_global_ctx->depth_params.stop_nb,152,71,(_global_ctx->depth_params.stop_nb>9?2:1));
            }
            else if(Profondeur_Button_Clicked)
            {
                if ( _global_ctx->metric_system )
                {
                    sprintf(Tstr, "%d%s",
                            (int)_global_ctx->depth_params.depth_max,
                            languages_get_string(TXT_M_UNIT));
                }
                else
                {
                    sprintf(Tstr, "%d %s",
                        (int)al_tools_to_feet(_global_ctx->depth_params.depth_max),
                        languages_get_string(TXT_F_UNIT));
                }
                GUI_DispStringAt(Tstr,152,71);
            }
            else if(Temps_Button_Clicked)
            {
                min = _global_ctx->depth_params.dive_duration % 60;
                hr = (_global_ctx->depth_params.dive_duration - min) / 60;
                snprintf(Tstr, sizeof(Tstr), "%1d:%02d",hr,min);
                GUI_DispStringAt(Tstr,152,71);
            }
            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
        }
    }
}
