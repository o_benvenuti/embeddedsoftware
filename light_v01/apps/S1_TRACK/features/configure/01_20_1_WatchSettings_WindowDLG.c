//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <stdio.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <User_RTC.h>
#include <ILI9163_driver.h>
#include <LSM9DS1_Includes.h>
#include <trace.h>
#include <babel.h>
#include <al_feature.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x02)
#define ID_BUTTON_1    (GUI_ID_USER + 0x03)
#define ID_BUTTON_2    (GUI_ID_USER + 0x04)
#define ID_BUTTON_3    (GUI_ID_USER + 0x05)
#define ID_BUTTON_4    (GUI_ID_USER + 0x06)
#define ID_BUTTON_5    (GUI_ID_USER + 0x07)
#define ID_BUTTON_6    (GUI_ID_USER + 0x08)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static bool stance_clicked = true;
static bool metric_system_clicked = false;
static serenity_context_t * global_ctx = NULL;
static feat_conf_priv_ctx_t * conf_ctx = NULL;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedBoldReduced16pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "WatchSettings_Window_1", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "stance selection"      , ID_BUTTON_0, 0, 15, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "metric selection"      , ID_BUTTON_1, 0, 52, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus button"           , ID_BUTTON_2, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Minus button"          , ID_BUTTON_3, 85, 88, 75, 40, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialisation of 'WatchSettings_Window'
            //
            conf_ctx = NULL;
            global_ctx = NULL;
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cbRepaint);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            break;
        }
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            conf_ctx = (feat_conf_priv_ctx_t *) pMsg->Data.p;
            if ( ! conf_ctx )
            {
                debug_printf("invalid configuration context address\n");
                break;
            }

            global_ctx = &(conf_ctx->global_ctx);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            if ( ! global_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;

            switch(Id)
            {
                // Notifications sent by stance button
                case ID_BUTTON_0:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        stance_clicked = true;
                        metric_system_clicked = false;
                    }
                    break;
                }
                // Notifications sent by metric sytem button
                case ID_BUTTON_1:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        stance_clicked = false;
                        metric_system_clicked = true;
                    }
                    break;
                }
                /**
                  * Treat both plus and minus buttons the same way as
                  * values to be changed are booleans
                  **/
                case ID_BUTTON_2:
                case ID_BUTTON_3:
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if( stance_clicked )
                        {
                            global_ctx->watch_stance_left =
                                    ! global_ctx->watch_stance_left;
                        }
                        else
                        {
                            global_ctx->metric_system =
                                    ! global_ctx->metric_system;
                        }
                    }
                    break;
                }
            }
            WM_Invalidate(pMsg->hWin);
            break; // WM_NOTIFY_PARENT
        }
        default:
            WM_DefaultProc(pMsg);
            break;
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateWatchSettings_Window_1(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            // validate we already received context
            if ( ! global_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            GUI_Draw_MenuBar_Dots(4,2);

            /* draw arrows */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 20);
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);

            //---------------------------------------------
            // First, display buttons
            //---------------------------------------------
            // set stance's button color regarding selected line
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            if( stance_clicked )
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }

            // display button's text
            GUI_ClearRect(0, 15, 80, 52);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_STANCE),4,21);

            // set metric system's button color regarding selected line
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            if( metric_system_clicked )
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }

            // display button's text
            GUI_ClearRect(0, 53, 80, 90);
            GUI_SetTextMode(GUI_TM_TRANS);

            // special case for German that takes one line only
            if ( 2 == global_ctx->language )
            {
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L1),4,59);
            }
            else
            {
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L1),4,52);
                GUI_DispStringAt(languages_get_string(TXT_UNIT_SYSTEM_L2),4,66);
            }

            //---------------------------------------------
            // Secondly, display value
            //---------------------------------------------
            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaSemiCondensedBoldReduced16pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);

            // and then current value
            if( stance_clicked )
            {
                if ( global_ctx->watch_stance_left )
                {
                    GUI_DispStringAt(languages_get_string(TXT_STANCE_LEFT), 152,71);
                }
                else
                {
                    GUI_DispStringAt(languages_get_string(TXT_STANCE_RIGHT), 152,71);
                }
            }
            else
            {
                if ( global_ctx->metric_system )
                {
                    GUI_DispStringAt(languages_get_string(TXT_METER_SYSTEM), 152,71);
                }
                else
                {
                    GUI_DispStringAt(languages_get_string(TXT_FEET_SYSTEM), 152,71);
                }
            }



            break;
        }
        default:
            WINDOW_Callback(pMsg);
    }
}

