//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <common.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <string.h>
#include <touchscreen.h>
#include <trace.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _configure_init_func(void ** priv_data, int parent);
int _configure_button_func(void * priv_data, uint32_t button);
int _configure_gest_func(void * priv_data, gestures_t gesture);
int _configure_event_func(void * priv_data, al_feature_cmd_t cmd);
int _configure_release_func(void * priv_data);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_configure_ctx = {
        .name       = "CONFIGURE",
        .ini_func   = _configure_init_func,
        .usr_func   = _configure_gest_func,
        .gui_func   = _configure_button_func,
        .evt_func   = _configure_event_func,
        .rls_func   = _configure_release_func,
};

/**
 * NOTE static context allocation to be used only in init function,
 * then use pointer provided by al_feature library to callbacks
**/
static feat_conf_priv_ctx_t _conf_local_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _configure_init_func(void ** priv_data, int parent)
{
    int rc;
    WM_MESSAGE window_msg;

    // set initial state
    _conf_local_ctx.current_stop = 0;
    _conf_local_ctx.parent_state = parent;
    _conf_local_ctx.current_state = configure_state_mode;

    // background is parent window of all screen
    // delete previous screen if needed
    _conf_local_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _conf_local_ctx.current_window )
    {
        WM_DeleteWindow(_conf_local_ctx.current_window);
    }

    // retrieve context from flash
    rc = serenity_retrieve_status(&(_conf_local_ctx.global_ctx));
    if ( rc )
    {
        debug_printf("unable to get context from flash\n");
        return -1;
    }

    // create and display BLE window
    _conf_local_ctx.current_window = Create01_10_ModeSelection_Window();

    // send local context to windows
    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
    window_msg.Data.p = &_conf_local_ctx;
    WM_SendMessage(_conf_local_ctx.current_window, &window_msg);

    // return context
    *priv_data = &_conf_local_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _configure_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    feat_conf_priv_ctx_t * ctx = (feat_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_charging;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case CMD_DVE_START:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case CMD_RCV_FLICK:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_compass_dry;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("invalid command\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
// processing
//----------------------------------------------------------------------
int _configure_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t msg;
    WM_MESSAGE window_msg;
    feat_conf_priv_ctx_t * ctx = (feat_conf_priv_ctx_t *)priv_data;

    switch ( gesture )
    {
        case swipe_right:
        {
            switch ( ctx->current_state )
            {
                case configure_state_mode:
                {
                    msg.cmd = CMD_END_STATE;
                    msg.data.value = serenity_state_default;
                    al_feature_send_to_tsk(&msg);
                    break;
                }
                case configure_state_watch_sub:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = CreateWatchSettings_Window_0();
                    ctx->current_state = configure_state_watch;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                    break;
                }
                case configure_state_depth:
                case configure_state_deco:
                case configure_state_watch:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = Create01_10_ModeSelection_Window();
                    ctx->current_state = configure_state_mode;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                    break;
                }
                case configure_state_stop:
                {
                    if ( ctx->current_stop == 0 )
                    {
                        WM_DeleteWindow(ctx->current_window);
                        ctx->current_window = CreateParam_Plongee_01_Window();
                        ctx->current_state = configure_state_depth;

                        // send local context to windows
                        window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                        window_msg.Data.p = ctx;
                        WM_SendMessage(ctx->current_window, &window_msg);
                    }
                    else
                    {
                        // decrement and notify window value has changed
                        --ctx->current_stop;
                        WM_Invalidate(ctx->current_window);
                    }

                    break;
                }
                case configure_state_deco_sub:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = Create_01_12_0_Param_Algo_Window();
                    ctx->current_state = configure_state_deco;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                    break;
                }
                case configure_state_time:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = CreateWatchSettings_Window_1();
                    ctx->current_state = configure_state_watch;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);

                    break;
                }
                case configure_state_reset:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = CreateTimeSetting_Window();
                    ctx->current_state = configure_state_time;
                    break;
                }
                case configure_state_free:
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = Create01_10_ModeSelection_Window();
                    ctx->current_state = configure_state_mode;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);

                    break;
                }
            }
            break;
        }
        case swipe_left:
        {
            if ( ctx->current_state == configure_state_depth )
            {
                if ( ctx->global_ctx.depth_params.stop_nb > 0 )
                {
                    ctx->current_stop = 0;
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = CreateParam_Palier_Window();
                    ctx->current_state = configure_state_stop;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                }
                else
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = Create01_10_ModeSelection_Window();
                    ctx->current_state = configure_state_mode;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                }
            }
            else if ( ctx->current_state == configure_state_deco )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create_01_12_1_Param_Algo_2_Window();
                ctx->current_state = configure_state_deco_sub;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            else if ( ctx->current_state == configure_state_stop )
            {
                if ( (ctx->current_stop+1) >= ctx->global_ctx.depth_params.stop_nb )
                {
                    WM_DeleteWindow(ctx->current_window);
                    ctx->current_window = Create01_10_ModeSelection_Window();
                    ctx->current_state = configure_state_mode;

                    // send local context to windows
                    window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                    window_msg.Data.p = ctx;
                    WM_SendMessage(ctx->current_window, &window_msg);
                }
                else
                {
                    // increment and notify window value has changed
                    ++ctx->current_stop;
                    WM_Invalidate(ctx->current_window);
                }
            }
            else if ( ctx->current_state == configure_state_deco_sub ||
                      ctx->current_state == configure_state_reset     )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create01_10_ModeSelection_Window();
                ctx->current_state = configure_state_mode;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            else if ( ctx->current_state == configure_state_watch )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateWatchSettings_Window_1();
                ctx->current_state = configure_state_watch_sub;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            else if ( ctx->current_state == configure_state_watch_sub )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateTimeSetting_Window();
                ctx->current_state = configure_state_time;
            }
            else if ( ctx->current_state == configure_state_time )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create01_23_Factory_Reset_Window();
                ctx->current_state = configure_state_reset;
            }
            break;
        }
        default:
        {
            debug_printf("unknown gesture\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _configure_button_func(void * priv_data, uint32_t button)
{
    al_feature_msg_t msg;
    WM_MESSAGE window_msg;
    feat_conf_priv_ctx_t * ctx = (feat_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( button )
    {
        case BUTTON_DEPTH:
        {
            if((ctx->global_ctx.option_code == DEPT__________) ||
               (ctx->global_ctx.option_code == DEPT______FREE) ||
               (ctx->global_ctx.option_code == DEPT_DECO_____) ||
           	   (ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
                ctx->global_ctx.dive_mode = Mode_BottomTimer;
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateParam_Plongee_01_Window();
                ctx->current_state = configure_state_depth;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            break;
        }
        case BUTTON_DECO:
        {
            if((ctx->global_ctx.option_code == _____DECO_____) ||
               (ctx->global_ctx.option_code == _____DECO_FREE) ||
               (ctx->global_ctx.option_code == DEPT_DECO_____) ||
           	   (ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
                ctx->global_ctx.dive_mode = Mode_DecoAlgo;
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create_01_12_0_Param_Algo_Window();
                ctx->current_state = configure_state_deco;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            break;
        }
        case BUTTON_FREE:
        {
            if((ctx->global_ctx.option_code == __________FREE) ||
               (ctx->global_ctx.option_code == _____DECO_FREE) ||
               (ctx->global_ctx.option_code == DEPT______FREE) ||
               (ctx->global_ctx.option_code == DEPT_DECO_FREE))
            {
                ctx->global_ctx.dive_mode = Mode_FreeDive;
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateParam_Free_Dive_Window();
                ctx->current_state = configure_state_free;

                // send local context to windows
                window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
                window_msg.Data.p = ctx;
                WM_SendMessage(ctx->current_window, &window_msg);
            }
            break;
        }
        case BUTTON_WATCH:
        {
            WM_DeleteWindow(ctx->current_window);
            ctx->current_window = CreateWatchSettings_Window_0();
            ctx->current_state = configure_state_watch;

            // send local context to windows
            window_msg.MsgId = FEATURE_CONFIGURE_SHARE_CTX;
            window_msg.Data.p = ctx;
            WM_SendMessage(ctx->current_window, &window_msg);
            break;
        }
        case BUTTON_MAGCAL:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_compass_calibration;
            al_feature_send_to_tsk(&msg);
            break;
        }
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _configure_release_func(void * priv_data)
{
    int rc;
    feat_conf_priv_ctx_t * ctx = (feat_conf_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // save new context to flash
    rc = serenity_save_status(&(ctx->global_ctx));
    if ( rc )
    {
        debug_printf("unable to save context to flash\n");
        return -1;
    }

    return 0;
}

