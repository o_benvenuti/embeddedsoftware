//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_tools.h>
#include <DIALOG.h>
#include <stdio.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <babel.h>
#include <trace.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_BUTTON_0              (GUI_ID_USER + 0x01)
#define ID_BUTTON_1              (GUI_ID_USER + 0x02)
#define ID_BUTTON_2              (GUI_ID_USER + 0x03)
#define ID_BUTTON_3              (GUI_ID_USER + 0x04)
#define ID_BUTTON_4              (GUI_ID_USER + 0x05)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Param_Palier_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Profondeur_Button", ID_BUTTON_0, 0, 15, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Vitesse_Button", ID_BUTTON_1, 0, 90, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Temps_Button", ID_BUTTON_2, 0, 53, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus_Button", ID_BUTTON_3, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button", ID_BUTTON_4, 85, 88, 75, 40, 0, 0x0, 0 },
};

extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced9pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced16pts;
bool Palier_Profondeur_Button_Clicked = false;
bool Palier_Vitesse_Button_Clicked = false;
bool Palier_Temps_Button_Clicked = false ;

static feat_conf_priv_ctx_t * ctx = NULL;
static serenity_dive_stop_t * stops = NULL;
static serenity_depth_param_t * params = NULL;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    int NCode;
    int Id;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialization of 'Param_Palier_Window'
            //
            ctx = NULL;
            stops = NULL;
            params = NULL;
            Palier_Profondeur_Button_Clicked = true;
            Palier_Vitesse_Button_Clicked = false;
            Palier_Temps_Button_Clicked = false ;
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_4),
                           GUI_transparent_button_cb);
            break;
        }
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            ctx = (feat_conf_priv_ctx_t *)pMsg->Data.p;
            params = &(ctx->global_ctx.depth_params);
            stops = params->dive_stops;

            // has soon as we have handles we can paint window
            WM_SetCallback(pMsg->hWin, _cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            if ( ! params )
            {
                debug_printf("context not received\n");
                break;
            }

            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'Profondeur_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Profondeur_Button_Clicked = true;
                        Palier_Vitesse_Button_Clicked = false;
                        Palier_Temps_Button_Clicked = false ;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'Vitesse_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Profondeur_Button_Clicked = false;
                        Palier_Vitesse_Button_Clicked = true;
                        Palier_Temps_Button_Clicked = false ;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                break;
                case ID_BUTTON_2: // Notifications sent by 'Temps_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        Palier_Profondeur_Button_Clicked = false;
                        Palier_Vitesse_Button_Clicked = false;
                        Palier_Temps_Button_Clicked = true ;
                        WM_Invalidate(pMsg->hWin);
                    }
                    break;
                }
                break;
                case ID_BUTTON_3: // Notifications sent by 'Plus_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        if ( Palier_Profondeur_Button_Clicked )
                        {
                            if ( stops[ctx->current_stop].StopDepth < params->depth_max )
                            {
                                stops[ctx->current_stop].StopDepth += 1.0f;
                            }
                        }
                        if(Palier_Vitesse_Button_Clicked)
                        {
                            if ( stops[ctx->current_stop].Ascent_speed < ASC_SPEED_MAX )
                            {
                                stops[ctx->current_stop].Ascent_speed++;
                            }
                        }
                        if(Palier_Temps_Button_Clicked)
                        {
                            if( (stops[ctx->current_stop].StopDuration < params->dive_duration*60) &&
                                (stops[ctx->current_stop].StopDuration < STOP_TIME_MAX-59))
                            {
                                stops[ctx->current_stop].StopDuration+=60;
                            }
                        }
                        WM_InvalidateWindow(pMsg->hWin);
                    }
                    break;
                }
                case ID_BUTTON_4: // Notifications sent by 'Moins_Button'
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        if(Palier_Profondeur_Button_Clicked)
                        {
                            if(stops[ctx->current_stop].StopDepth > MAX_DEPTH_MIN)
                            {
                                stops[ctx->current_stop].StopDepth -= 1.0f;
                            }
                        }
                        if(Palier_Vitesse_Button_Clicked)
                        {
                            if(stops[ctx->current_stop].Ascent_speed > ASC_SPEED_MIN)
                            {
                                stops[ctx->current_stop].Ascent_speed--;
                            }
                        }
                        if(Palier_Temps_Button_Clicked)
                        {
                            if(stops[ctx->current_stop].StopDuration > STOP_TIME_MIN)
                            {
                                stops[ctx->current_stop].StopDuration-=60;
                            }
                        }
                        WM_InvalidateWindow(pMsg->hWin);
                        break;
                    }
                    break;

                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateParam_Palier_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Tstr[10];
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextMode(GUI_TM_TRANS);
            sprintf(Tstr, "%s%d",languages_get_string(TXT_STOP_LETTER),
                    ctx->current_stop+1);
            GUI_DispStringAt(Tstr,4,-2);

            /* draw dots */
            GUI_Draw_MenuBar_Dots(params->stop_nb+2,
                                  ctx->current_stop+3);

            /* draw arrows */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 19);
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);

            /* draw buttons */
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            if(Palier_Profondeur_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 15, 80, 52);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_STOP_DEPTH),4,18);

            if(Palier_Temps_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 53, 80, 90);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_STOP_DURATION),4,59);

            if(Palier_Vitesse_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 91, 80, 128);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_STOP_SPEED),4,95);

            /* display numeric value */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
            if(Palier_Vitesse_Button_Clicked)
            {
                if ( ctx->global_ctx.metric_system )
                {
                    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispDecAt(stops[ctx->current_stop].Ascent_speed,
                          152,71-12,
                          (stops[ctx->current_stop].Ascent_speed > 99) ? 3 : 2);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced16pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispStringAt(
                            languages_get_string(TXT_SPEED_UNIT_METRIC),
                            152,71+12);
                }
                else
                {
                    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispDecAt(
                        (int)al_tools_to_feet(stops[ctx->current_stop].Ascent_speed),
                        152 , (71 - 12),
                        (al_tools_to_feet(stops[ctx->current_stop].Ascent_speed) > 99) ? 3 : 2);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced16pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispStringAt(
                            languages_get_string(TXT_SPEED_UNIT_FEET),
                            152,71+12);
                }
            }
            else if(Palier_Profondeur_Button_Clicked)
            {
                if ( ctx->global_ctx.metric_system )
                {
                    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispDecAt((int)stops[ctx->current_stop].StopDepth,
                              152, (71 - 12),
                              (stops[ctx->current_stop].StopDepth > 99) ? 3 : 2);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced16pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispStringAt(languages_get_string(TXT_M_UNIT),152,71+12);
                }
                else
                {
                    GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
                    GUI_DispDecAt(
                        (int)al_tools_to_feet(stops[ctx->current_stop].StopDepth),
                        152,71-12,
                        (al_tools_to_feet(stops[ctx->current_stop].StopDepth) > 99) ? 3 : 2);
                    GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced16pts);
                    GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                    GUI_DispStringAt(languages_get_string(TXT_F_UNIT),152,71+12);
                }
            }
            else if(Palier_Temps_Button_Clicked)
            {
                GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
                GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                GUI_DispDecAt(stops[ctx->current_stop].StopDuration/60,152,71-12,2);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced16pts);
                GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_MINUTES_UNIT),152,71+12);

            }
            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
        }
    }
}

