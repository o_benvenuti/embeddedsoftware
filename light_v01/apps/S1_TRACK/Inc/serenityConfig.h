/*
 * serenityConfig.h
 *
 *  Created on: 22 f�vr. 2018 *      Author: AixSonic
 */

#ifndef SERENITYCONFIG_H_
#define SERENITYCONFIG_H_

/* -------------------------------------------------------------- */
#define I2C_TIMEOUT 500       /* ticks */

//----------------------------------------------------------------------
#define STDBY_CHECK_PERIOD              pdMS_TO_TICKS(120000) /*ms*/
#define INACTIVITY_TIME_BEFORE_SLEEP    300                   /* seconds */
/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
				/* DEBUG OPTIONS */
//#define DEBUG_SCREEN
//#define DEBUG_DIVE
//#define DEBUG_FASTER_READ_SENSOR_TASK
//#define DEBUG_NO_CHARGE_SCREEN

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
/* Serenity name and firmware version number */
#define PROJECT_MODEL 0x1 // 4 bits - 0xF max

#if PROJECT_MODEL == 0x0
	#define PROJECT_NAME        "S1 SPORT"
#elif PROJECT_MODEL == 0x1
	#define PROJECT_NAME        "S1 TRACK"
#elif PROJECT_MODEL == 0x2
	#define PROJECT_NAME        "S2 SONAR"
#elif PROJECT_MODEL == 0x3
	#define PROJECT_NAME        "S3 APNEA"
#elif PROJECT_MODEL == 0x4
	#define PROJECT_NAME        "S4 PHYSIO"
#endif

// this one must be incremented whatever the project
#define PROJECT_FIRMWARE_VERSION    0x04
#define PROJECT_VERSION             ((uint16_t)((PROJECT_MODEL<<12) |       \
                                                (PROJECT_FIRMWARE_VERSION)))
#define PROJECT_VSN_TXT             "4.0"
#define SAMPLE_FREQUENCY            0x02
#define SAMPLE_PERIOD               (500*SAMPLE_FREQUENCY) /* 500 ms x 2 (frequency) */

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
/* Screen parameters */
/* Horizontal size of screen */
#define H_SIZE 160
/* Vertical size of screen */
#define V_SIZE 128
/* swipe sensitivity */
#define SWIPE_SENSITIVITY 60 /* pixels */

#define DEFAULT_BRIGHTNESS 50
#define DEFAULT_FLICK_SENSITIVITY 3
/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
#define NB_PALIERS_MAX     3
#define NB_PALIERS_MIN     0
#define NB_PALIERS_DEFAULT 1

#define DIVE_TIME_MAX      240 /* minutes */
#define DIVE_TIME_MIN      5
#define DIVE_TIME_DEFAULT  45

#define STOP_TIME_MAX      3600 /* secondes */
#define STOP_TIME_MIN      60
#define STOP_TIME_DEFAULT  180

#define MAX_DEPTH_MIN      3.0
#define MAX_DEPTH_MAX      100.0 /* meters */
#define MAX_DEPTH_DEFAULT  20.0

#define STOP_DEPTH_DEFAULT 5.0

#define ASC_SPEED_MIN      1
#define ASC_SPEED_MAX      20 /* m/min */
#define ASC_SPEED_DEFAULT  12

#define IDLE_TIME_MAX      (60 * 60) /* seconds */
#define IDLE_TIME_MIN      (1  * 60)
#define IDLE_TIME_DEFAULT  (10 * 60)

/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
/* algo deco parameters */
#define O2_RATIO_MAX          (0.40)     /* 0 ~ 1  ( 0% -> 100% ) */
#define O2_RATIO_MIN          (0.21)     /* 0 ~ 1  ( 0% -> 100% ) */
#define O2_RATIO_DEFAULT      (0.21)

#define HE_RATIO_MAX          (1.0)     /* 0 ~ 1  ( 0% -> 100% ) */
#define HE_RATIO_MIN          (0.0)     /* 0 ~ 1  ( 0% -> 100% ) */

#define PPO2_MAX              (1.6)
#define PPO2_MIN              (1.2)
#define PPO2_DEFAULT          (1.4)

#define SAFETY_FACTOR_MAX     (1.0)     /* 0 ~ 1  ( 0% -> 100% ) */
#define SAFETY_FACTOR_MIN     (0.6)     /* 0 ~ 1  ( 0% -> 100% ) */
#define SAFETY_FACTOR_DEFAULT (0.8)
/* -------------------------------------------------------------- */

/* -------------------------------------------------------------- */
#define SERENITY_METRIC_DEFAULT true
#define SERENITY_STANCE_DEFAULT true
#define ASC_SPEED_AVG_TAB_SIZE  8
#define NB_SEC_DIVE_DETECT      60  /* 60 secs */
#define PRESSURE_TAB_SIZE       (NB_SEC_DIVE_DETECT*1000/SAMPLE_PERIOD) /* 60 secs */
#define SEALEVELPRESSURE        101300

#define SERENITY_LANGUAGE_DEFAULT       0
#define SERENITY_OPTION_CODE_DEFAULT    0x54f0e977 // Bottom Timer Only
#define SERENITY_DECLINATION_DEFAULT    1.3
#define SERENITY_BRIGTHNESS_DEFAULT     50

/* -------------------------------------------------------------- */

#endif /* SERENITYCONFIG_H_ */
