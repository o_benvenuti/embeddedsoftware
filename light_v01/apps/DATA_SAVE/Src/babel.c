/*
 * babel.c
 *
 *  Created on: 13 f�vr. 2019
 *      Author: user
 */
#include <languages.h>

char * serenity_language_pack[][LANGUAGES_MAX_LANGUAGES] = {
		//FRENCH                          ENGLISH                          GERMAN                           SPANISH						   ITALIAN
		{"Dimanche\0"                  , "Sunday\0"                     , "Sonntag\0"                    , "Domingo\0"                  , "Domenica\0"                  }, //  0
		{"Lundi\0"                     , "Monday\0"                     , "Montag\0"                     , "Lunes\0"                    , "Lunedi\0"                    }, //  1
		{"Mardi\0"                     , "Tuesday\0"                    , "Dienstag\0"                   , "Martes\0"                   , "Martedi\0"                   }, //  2
        {"Mercredi\0"                  , "Wednesday\0"                  , "Mittwoch\0"                   , "Miercoles\0"                , "Mercoledi\0"                 }, //  3
        {"Jeudi\0"                     , "Thursday\0"                   , "Donnerstag\0"                 , "Jueves\0"                   , "Giovedi\0"                   }, //  4
        {"Vendredi\0"                  , "Friday\0"                     , "Freitag\0"                    , "Viernes\0"                  , "Venerdi\0"                   }, //  5
        {"Samedi\0"                    , "Saturday\0"                   , "Samstag\0"                    , "Sabado\0"                   , "Sabato\0"                    }, //  6
        {"Aucune plongee...\0"         , "No dive found\0"              , "Kein Tauchgang...\0"          , "Ninguna inmersion...\0"     , "No Immersioni\0"             }, //  7
        {"MODE\0"                      , "MODE\0"                       , "MODUS\0"                      , "MODO\0"                     , "Modalita\0"                  }, //  8
        {"PALIERS\0"                   , "STOPS\0"                      , "PHASEN\0"                     , "PARADAS\0"                  , "SOSTA\0"                     }, //  9 -  9 char max
        {"PROF. MAX\0"                 , "MAX DEPTH\0"                  , "TIEFE MAX\0"                  , "PROF. MAX\0"                , "PROF.MAX\0"                  }, // 10 -  9 char max
        {"TEMPS\0"                     , "TIME\0"                       , "ZEIT\0"                       , "TIEMPO\0"                   , "TEMPO\0"                     }, // 11 -  9 char max
        {"PROF.\0"                     , "DEPTH\0"                      , "TIEFE\0"                      , "PROF.\0"                    , "PROF.\0"                     }, // 12 -  9 char max
        {"DUREE\0"                     , "DURATION\0"                   , "DAUER\0"                      , "DURACION\0"                 , "DURATA\0"                    }, // 13 -  9 char max
        {"VITESSE\0"                   , "SPEED\0"                      , "GESCHWIN.\0"                  , "VELOCIDAD\0"                , "VELOCITA\0"                  }, // 14 -  9 char max
        {"OXYGENE\0"                   , "OXYGEN\0"                     , "SAUERST.\0"                   , "OXIGENO\0"                  , "OSSIGENO\0"                  }, // 15 -  9 char max
        {"PP02\0"                      , "PP02\0"                       , "PP02\0"                       , "PP02\0"                     , "PP02\0"                      }, // 16 -  9 char max
        {"SECURITE\0"                  , "SAFETY\0"                     , "SICHERH.\0"                   , "SEGURIDAD\0"                , "SICUREZZA\0"                 }, // 17 -  9 char max
        {"Prof. max\n\0"               , "Max depth\n\0"                , "Max Tiefe\n\0"                , "Pro max\n\0"                , "Prof. max\n\0"               }, // 18 - 11 char max
        {"CONS\0"                      , "CONS\0"                       , "KONS\0"                       , "CONS\0"                     , "CONS\0"                      }, // 19 -  4 char max
        {"NORM\0"                      , "NORM\0"                       , "NORM\0"                       , "NORM\0"                     , "NORM\0"                      }, // 20 -  4 char max
        {"PROG\0"                      , "PROG\0"                       , "PROG\0"                       , "PROG\0"                     , "PROG\0"                      }, // 21 -  4 char max
        {"m/min\0"                     , "m/min\0"                      , "m/min\0"                      , "m/min.\0"                   , "m/min\0"                     }, // 22
        {"m\0"                         , "m\0"                          , "m\0"                          , "m\0"                        , "m\0"                         }, // 23 - 1 char max
        {"sec\0"                       , "sec\0"                        , "Sek.\0"                       , "s.\0"                       , "sec\0"                       }, // 24
        {"min\0"                       , "min\0"                        , "Min.\0"                       , "min.\0"                     , "min\0"                       }, // 25
        {"h\0"                         , "h\0"                          , "Std.\0"                       , "h\0"                        , "h\0"                         }, // 26
        {"Sensibilite\0"               , "Flick\0"                      , "Empfindlichk\0"               , "Sensibilidad\0"             , "Sensibilita\0"               }, // 27 - 12 char max
        {"Flick\0"                     , "Sensitivity\0"                , "Film\0"                       , "Flick\0"                    , "Flick\0"                     }, // 28 - 12 char max
        {"Luminosite\0"                , "Brightness\0"                 , "Helligkeit\0"                 , "Luminosidad\0"              , "Luminosita\0"                }, // 29 - 12 char max
        {"Calibration\0"               , "Compass\0"                    , "Kalibrierung\0"               , "Calibrado\0"                , "Calibrazione\0"              }, // 30 - 12 char max
        {"Boussole\0"                  , "Calibration\0"                , "Kompass\0"                    , "Brujula\0"                  , "Bussola\0"                   }, // 31 - 12 char max
        {"Commencer la calibration ?\0", "Start calibration\0"          , "Mit Kalibrierung beginnen?\0" , "Iniciar el calibrado?\0"    , "Inizia Calibrazione ?\0"     }, // 32 - 26 char max
        {"NON\0"                       , "NO\0"                         , "NEIN\0"                       , "NO\0"                       , "NO\0"                        }, // 33 -  6 char max
        {"OUI\0"                       , "YES\0"                        , "JA\0"                         , "SI\0"                       , "SI\0"                        }, // 34 -  6 char max
        {"Reinitialisation\0"          , "Factory\0"                    , "Auf Werkseinst.\0"            , "Reinicializacion\0"         , "Resetta\0"                   }, // 35 - 16 char max
        {"d'usine\0"                   , "reset\0"                      , "zurucksetzen\0"               , "de fabrica\0"               , "strumento\0"                 }, // 36 - 16 char max
        {"ATTENTION\0"                 , "WARNING\0"                    , "ACHTUNG\0"                    , "ATENCION\0"                 , "ATTENZIONE\0"                }, // 37
        {"\0"                          , "\0"                           , "\0"                           , "\0"                         , "\0"                          }, // 38 - 23 char max
        {"Tous les parametres et\0"    , "All data and parameters\0"    , "Alle Einstellungen und\0"     , "Se borraran todas las\0"    , "Tutti i dati e paramentri\0" }, // 39 - 23 char max
        {"donnees seront effaces.\0"   , "will be erased.\0"            , "Daten werden geloscht.\0"     , "configuraciones y datos\0"  , "andanno persi\0"             }, // 40 - 23 char max
        {"\0"                          , "\0"                           , "\0"                           , "\0"                         , "\0"                          }, // 41 - 23 char max
        {"CALIBRATION BOUSSOLE\0"      , "COMPASS CALIBRATION\0"        , "KOMPASSKALIBRIERUNG\0"        , "CALIBRADO BRUJULA\0"        , "CALIBRAZIONE BUSSOLA\0"      }, // 42
        {"Tournez et inclinez\0"       , "Wave your device\0"           , "Drehen und neigen Sie\0"      , "Gire e incline\0"           , "Muovi lo strumento \0"       }, // 43 - 25 char max
        {"la montre en\0"              , "around in a\0"                , "die Uhr so, dass Sie\0"       , "el reloj\0"                 , "realizzando un numero\0"     }, // 44 - 25 char max
        {"effectuant des huit\0"       , "eight-figure motion\0"        , "Achter bilden.\0"             , "trazando ochos\0"           , "otto come in figura\0"       }, // 45 - 25 char max
        {"ETAPE 2\0"                   , "STEP 2\0"                     , "SCHRITT 2\0"                  , "ETAPA 2\0"                  , "PASSO 1\0"                   }, // 46
        {"\0"                          , "\0"                           , "\0"                           , "\0"                         , "\0"                          }, // 47 - 27 char max
        {"Veuillez laisser la montre\0", "Please keep the watch\0"      , "Bitte halten sie den\0"       , "Por favor, deje el reloj\0" , "Si prega di lasciare\0"      }, // 48 - 27 char max
        {"immobile 10 secondes\0"      , "still for 10 seconds\0"       , "Computer fur 10 Sekunden\0"   , "inmovil durante 10\0"       , "l'orologio immobile per 10\0"}, // 49 - 27 char max
        {"\0"                          , "\0"                           , "still\0"                      , "segundos\0"                 , "secondi senza toccarla\0"    }, // 50 - 27 char max
        {"PRET\0"                      , "READY\0"                      , "BEREIT\0"                     , "LISTO\0"                    , "PRONTA\0"                    }, // 51
        {"Posez la montre a plat\0"    , "Place watch on a flat\0"      , "Legen Sie die Uhr flach hin\0", "Ponga el reloj horizontal\0", "Posiziona superficie piana\0"}, // 52 - 26 char max
        {"et faites un tour complet\0" , "surface and make full turn\0" , "und drehen Sie diese um\0"    , "y haga un giro completo\0"  , "fai completa rotazione\0"    }, // 53 - 26 char max
        {"Connecte au telephone\0"     , "Connected to phone\0"         , "Mit Telefon verbunden\0"      , "Conectado al telefono\0"    , "Connetti al telefono\0"      }, // 54 - 23 char max
        {"APPLI\0"                     , "PHONE\0"                      , "ANWEN\0"                      , "APLI.\0"                    , "TELEF\0"                     }, // 55 -  5 char max
        {"RECEP\0"                     , "GET\0"                        , "EMPF\0"                       , "RECEP\0"                    , "RICEV\0"                     }, // 56 -  5 char max
        {"EMISS\0"                     , "SHARE\0"                      , "UBTR\0"                       , "EMIS.\0"                    , "SHARE\0"                     }, // 57 -  5 char max
        {"Nom : S1_\0"                 , "Name: S1_\0"                  , "Name: S1_\0"                  , "Nom.: S1_\0"                , "Nome: S1_\0"                 }, // 58
        {"Telechargement...\0"         , "Downloading...\0"             , "Download...\0"                , "Descargando...\0"           , "Sto scaricando\0"            }, // 60
        {"Parametres\0"                , "Parameters\0"                 , "Einstellungen\0"              , "Configuraciones\0"          , "Paramentri\0"                }, // 61
        {"mis a jour.\0"               , "updated.\0"                   , "aktualisiert.\0"              , "actualizado.\0"             , "aggiornati\0"                }, // 62
        {"P\0"                         , "S\0"                          , "S\0"                          , "P\0"                        , "S\0"                         }, // 63 -  1 char max
        {"Temps\0"                     , "Time\0"                       , "Zeit\0"                       , "Tiemp\0"                    , "Tempo\0"                     }, // 64 -  5 char max
        {"Palier\0"                    , "Stop\0"                       , "Phase\0"                      , "Parada\0"                   , "Sosta\0"                     }, // 65 -  6 char max
        {"PALIER SECU\0"               , "SAFETY STOP\0"                , "SICH. STOPP\0"                , "PARADA SEG.\0"              , "SAFETY STOP\0"               }, // 66 - 12 char max
        {"PALIER PROF\0"               , "DEEP STOP\0"                  , "TIEFENSTOPP\0"                , "PARADA PROF.\0"             , "DEEP STOP\0"                 }, // 67 - 12 char max
        {"TROP RAPIDE\0"               , "TOO FAST\0"                   , "ZU SCHNELL\0"                 , "EXCESIV RAP\0"              , "TROPP VELOCE\0"              }, // 68 - 12 char max
        {"RALENTIR\0"                  , "SLOW DOWN\0"                  , "LANGSAMER\0"                  , "RALENTIZAR\0"               , "RALLENTA\0"                  }, // 69 - 12 char max
        {"Cycles\0"                    , "Laps\0"                       , "Zyklen\0"                     , "Ciclos\0"                   , "Cicli\0"                     }, // 70 -  6 char max
        {"Cycle max\0"                 , "Max lap\0"                    , "Zyklus max.\0"                , "Ciclo max.\0"               , "Cicli Max\0"                 }, // 71 - 11 char max
        {"Total\0"                     , "Total\0"                      , "Gesamt\0"                     , "Total\0"                    , "Totale\0"                    }, // 72 - 11 char max
        {"Cycle\0"                     , "Lap\0"                        , "Zyklus\0"                     , "Ciclo\0"                    , "Ciclo\0"                     }, // 73 - 11 char max
        {"Repos\0"                     , "Rest\0"                       , "Ruhe\0"                       , "Reposo\0"                   , "Rimanenza\0"                 }, // 74 - 11 char max
        {"TEMPS\0"                     , "IDLE\0"                       , "WARTE-\0"                     , "TIEMPO\0"                   , "TEMPO\0"                     }, // 75 - 10 char max
        {"INACTIVITE\0"                , "TIME\0"                       , "ZEIT\0"                       , "INACTIVID.\0"               , "INATTIVO\0"                  }, // 76 - 10 char max
        {"TROP PROFOND\0"              , "TOO DEEP\0"                   , "TOO DEEP\0"                   , "TOO DEEP\0"                 , "TOO DEEP\0"                  }, // 77 - 12 char max
        {"REMONTEZ\0"                  , "GO UP\0"                      , "GO UP\0"                      , "GO UP\0"                    , "GO UP\0"                     }, // 78 - 12 char max
};

