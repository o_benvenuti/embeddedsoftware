#ifndef BABEL_H_
#define BABEL_H_
#include <languages.h>

enum BABEL_LIST
{
	TXT_WEEKDAY_SUNDAY = 0,              //  0
	TXT_WEEKDAY_MONDAY,                  //  1
	TXT_WEEKDAY_TUESDAY,                 //  2
	TXT_WEEKDAY_WEDNESDAY,               //  3
	TXT_WEEKDAY_THURSDAY,                //  4
	TXT_WEEKDAY_FRIDAY,                  //  5
	TXT_WEEKDAY_SATURDAY,                //  6
	TXT_NO_DIVE,                         //  7
	TXT_PARAM_MODE,                      //  8
	TXT_PARAM_STOPS,                     //  9
	TXT_PARAM_MAX_DEPTH,                 // 10
	TXT_PARAM_DIVE_TIME,                 // 11
	TXT_PARAM_STOP_DEPTH,                // 12
	TXT_PARAM_STOP_DURATION,             // 13
	TXT_PARAM_STOP_SPEED,                // 14
	TXT_DECO_PARAM_OXYGEN,               // 15
	TXT_DECO_PARAM_PP02,                 // 16
	TXT_DECO_PARAM_SAFETY,               // 17
	TXT_DECO_PARAM_MAX_DEPTH,            // 18
	TXT_DECO_PARAM_CONSERVATIVE,         // 19
	TXT_DECO_PARAM_STANDARD,             // 20
	TXT_DECO_PARAM_PROGRESSIVE,          // 21
	TXT_SPEED_UNIT,                      // 22
	TXT_M_UNIT,                          // 23
	TXT_SECONDS_UNIT,                    // 24
	TXT_MINUTES_UNIT,                    // 25
	TXT_HOURS_UNIT,                      // 26
	TXT_FLICK_SENSITIVITY_L1,            // 27
	TXT_FLICK_SENSITIVITY_L2,            // 28
	TXT_BRIGHTNESS,                      // 29
	TXT_COMPASS_CALIBRATION_L1,          // 30
	TXT_COMPASS_CALIBRATION_L2,          // 31
	TXT_START_COMPASS_CALIBRATION,       // 32
	TXT_NO,              		         // 33
	TXT_YES,             		         // 34
	TXT_FACTORY_RESET_BUTTON_L1,         // 35
	TXT_FACTORY_RESET_BUTTON_L2,         // 36
	TXT_FACTORY_RESET_WARNING,           // 37
	TXT_FACTORY_RESET_L1,                // 38
	TXT_FACTORY_RESET_L2,                // 39
	TXT_FACTORY_RESET_L3,                // 40
	TXT_FACTORY_RESET_L4,                // 41
	TXT_MAGCAL_TITLE,                    // 42
	TXT_MAGCAL_8_L1,                     // 43
	TXT_MAGCAL_8_L2,                     // 44
	TXT_MAGCAL_8_L3,                     // 45
	TXT_MAGCAL_READY_O_TITLE,            // 46
	TXT_MAGCAL_READY_O_L1,               // 47
	TXT_MAGCAL_READY_O_L2,               // 48
	TXT_MAGCAL_READY_O_L3,               // 49
	TXT_MAGCAL_READY_O_L4,               // 50
	TXT_MAGCAL_READY_O_BUTTON,           // 51
	TXT_MAGCAL_O_L1,    		         // 52
	TXT_MAGCAL_O_L2,                     // 53
	TXT_BLE_CONNECTED,                   // 54
	TXT_BLE_PHONE,                       // 55
	TXT_BLE_SLAVE,                       // 56
	TXT_BLE_MASTER,                      // 57
	TXT_BLE_NAME,                        // 58
	TXT_BLE_DOWNLOADING,                 // 60
	TXT_BLE_SCAN_UPDATED_L1,             // 61
	TXT_BLE_SCAN_UPDATED_L2,             // 62
	TXT_STOP_LETTER,                     // 63
	TXT_DIVE_TIME,                       // 64
	TXT_DIVE_STOP,                       // 65
	TXT_STOP_SAFETY,                     // 66
	TXT_STOP_DEEP,                       // 67
	TXT_TOO_FAST,                        // 68
	TXT_SLOW_DOWN,                       // 69
	TXT_FREE_DIVE_LAPS,                  // 70
	TXT_FREE_DIVE_MAX_LAP,               // 71
	TXT_FREE_DIVE_TOTAL_TIME,            // 72
	TXT_FREE_DIVE_LAP,                   // 73
	TXT_FREE_DIVE_REST_TIME,             // 74
	TXT_PARAM_FREE_DIVE_IDLE_TIME_L1,    // 75
	TXT_PARAM_FREE_DIVE_IDLE_TIME_L2,    // 76
    TXT_TOO_DEEP,                        // 77
    TXT_GO_UP,                           // 78
};

#endif /* BABEL_H_ */
