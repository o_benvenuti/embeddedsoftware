//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <al_tools.h>
#include <common.h>
#include <GUI_serenity.h>
#include <babel.h>
#include <trace.h>

#include "feature_default.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_GRAPH_0              (GUI_ID_USER + 0x06)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "ShowLastDive_Window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
        },
};

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_iconprofmax;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_icontempstotal;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced20pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_diver_small;

static feature_default_graph_info_t * graph_info = NULL;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    char Tstr[32];
    int hr, min, sec;
    int x_pos, y_pos;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            // Initialization
            graph_info = NULL;
            break;
        }
        case FEATURE_DEFAULT_SET_GRAPH:
        {
            graph_info = (feature_default_graph_info_t *)pMsg->Data.v;
            break;
        }
        case WM_PAINT:
        {
            // wait for context to be received
            if ( ! graph_info )
            {
                break;
            }

            // if last dive not found
            if ( ! graph_info->dive_duration )
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_Clear();
                GUI_DrawBitmap(&bmcran_Serenity_13a_diver_small, 60, 20);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER);
                GUI_DispStringAt(languages_get_string(TXT_NO_DIVE), 80 , 80);
                break;
            }

            //*****************************************************************
            //*********************** BACKGROUND ******************************
            //*****************************************************************
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            //*****************************************************************
            //********************** UPPER SECTION ****************************
            //*****************************************************************
            bool mmdd = RTC_ismmddMode();
            sprintf(Tstr, "%d.%d.20%02d %02d:%02d",
                    (mmdd ? bcd2int(graph_info->dive_start.Month) :
                            bcd2int(graph_info->dive_start.Date)),
                    (mmdd ? bcd2int(graph_info->dive_start.Date) :
                            bcd2int(graph_info->dive_start.Month)),
                    bcd2int(graph_info->dive_start.Year),
                    bcd2int(graph_info->dive_start.Hours),
                    bcd2int(graph_info->dive_start.Minutes));
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            x_pos = (LCD_GetXSize() / 2);
            y_pos = 8;
            GUI_DispStringAt(Tstr, x_pos ,y_pos);

            if ( graph_info->last_dive_mode == Mode_FreeDive )
            {
                //**************** LEFT SECTION *******************
                // print current depth numerical value
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
                GUI_SetColor(GUI_WHITE);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                x_pos = X_1_6 + 5;
                y_pos = Y_1_6 + 6;
                GUI_GotoXY(x_pos ,y_pos);
                GUI_DispFloatFix(graph_info->max_depth,
                        graph_info->max_depth<100?graph_info->max_depth<10?3:4:5, 1);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_GotoY(Y_1_6 + 6);
                GUI_DispString(" ");
                GUI_DispString(languages_get_string(TXT_M_UNIT));

                //**************** RIGHT SECTION *******************
                // print lap count text
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetColor(GUI_WHITE);
                x_pos = X_4_6;
                y_pos = Y_1_6 + 6;
                GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_LAPS), x_pos,y_pos);
                // print lap counter value
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
                x_pos = X_5_6 + 8;
                y_pos = Y_1_6 + 6;
                GUI_DispDecAt(graph_info->lap_count, x_pos, y_pos,
                        graph_info->lap_count>9?2:1);
            }
            else // other dive modes
            {
                /* draw MaxDeep Arrow */
                x_pos = 6;
                y_pos = Y_1_6 + 7 - (bmcran_Serenity_13B_iconprofmax.YSize/2);
                GUI_DrawBitmap(&bmcran_Serenity_13B_iconprofmax, x_pos ,y_pos);

                snprintf(Tstr, sizeof(Tstr), "%.1f %s", graph_info->max_depth,
                         languages_get_string(TXT_M_UNIT));
                x_pos = ((LCD_GetXSize() - (6+bmcran_Serenity_13B_iconprofmax.XSize)) / 2) + (6+bmcran_Serenity_13B_iconprofmax.XSize);
                y_pos = Y_1_6 + 6;
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
                GUI_SetBkColor(GUI_MAKE_COLOR(0x0000B1E5));
                GUI_ClearRect(0,Y_2_3+ 1,LCD_GetXSize(),LCD_GetYSize());
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_DispStringAt(Tstr, x_pos ,y_pos);
            }

            //*****************************************************************
            //********************** MIDDLE SECTION ***************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652)); // dark blue
            GUI_ClearRect(0,Y_1_3 ,LCD_GetXSize(),Y_2_3);
            // draw vertical lines
            GUI_SetColor(GUI_BLACK);
            GUI_DrawVLine(X_1_4, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_2_4, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_3_4, Y_1_3, Y_2_3);
            // draw graph
            GUI_SetColor(GUI_WHITE);
            GUI_DrawGraph(graph_info->dive_profile_graph,
                    graph_info->dive_profile_graph_idx, 0, 42);

            //*****************************************************************
            //********************** LOWER SECTION ****************************
            //*****************************************************************
            // draw Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767)); // normal blue
            GUI_ClearRect(0,Y_2_3 ,LCD_GetXSize(),LCD_GetYSize());
            hr = graph_info->dive_duration / (60*60);
            min = (graph_info->dive_duration / 60 ) % 60;
            sec = graph_info->dive_duration % 60;
            if(graph_info->last_dive_mode == Mode_FreeDive)
            {
                //**************** LEFT SECTION *******************
                // print current lap time text
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetColor(GUI_WHITE);
                x_pos = X_1_4;
                y_pos = Y_2_3 + 10;
                GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_MAX_LAP), x_pos,y_pos);
                // print current lap time
                sprintf(Tstr, "%02d:%02d", graph_info->lap_max_duration/60,
                                           graph_info->lap_max_duration%60);
                y_pos = Y_5_6 + 10;
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
                GUI_SetColor(GUI_WHITE);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_DispStringAt(Tstr, x_pos ,y_pos);

                //**************** RIGHT SECTION *******************
                // print total time text
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetColor(GUI_WHITE);
                x_pos = X_3_4;
                y_pos = Y_2_3 + 10;
                GUI_DispStringAt(languages_get_string(TXT_FREE_DIVE_TOTAL_TIME), x_pos,y_pos);
                // print current lap time
                if(graph_info->dive_duration<3600)
                {
                    sprintf(Tstr, "%02d:%02d",min,sec);
                }
                else
                {
                    sprintf(Tstr, "%02d:%02d",hr ,min);
                }
                y_pos = Y_5_6 + 10;
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
                GUI_SetColor(GUI_WHITE);
                GUI_SetTextAlign(GUI_TA_VCENTER|GUI_TA_HCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_DispStringAt(Tstr, x_pos ,y_pos);
            }
            else
            {
                /* print DiveTime */
                sprintf(Tstr, "%02d:%02d",hr,min);
                x_pos = ((LCD_GetXSize() - (6+bmcran_Serenity_13B_icontempstotal.XSize)) / 2) + (6+bmcran_Serenity_13B_icontempstotal.XSize);
                y_pos = Y_5_6;
                GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced26pts);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_DispStringAt(Tstr, x_pos ,y_pos);

                /* draw total Time Icon */
                x_pos = 6;
                y_pos = Y_5_6 - (bmcran_Serenity_13B_icontempstotal.YSize/2);
                GUI_DrawBitmap(&bmcran_Serenity_13B_icontempstotal, x_pos, y_pos);
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateShowLastDive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
            _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}


