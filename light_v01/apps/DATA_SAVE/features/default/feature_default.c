//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_database.h>
#include <al_feature.h>
#include <GUI_Serenity.h>
#include <touchscreen.h>
#include <trace.h>
#include <LSM9DS1_Includes.h>
#include <ds2782_driver.h>

#include "feature_default.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define     FEATURE_DEFAULT_MAX_RECORD  64

//----------------------------------------------------------------------
// Local types
//----------------------------------------------------------------------
enum feature_default_state {
    default_state_lock,
    default_state_menu,
    default_state_hist,
    default_state_info,
};

typedef struct feat_default_priv_ctx {
    int hist_count;
    int current_hist;
    TimeType prev_time;
    WM_HWIN current_window;
    TimeType diverec_list[FEATURE_DEFAULT_MAX_RECORD];
    serenity_state_t parent_state;
    feature_default_graph_info_t graph_info;
    enum feature_default_state current_state;
}feat_default_priv_ctx_t;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
int _default_release_func(void * priv_data);
int _default_timer_func(void * priv_data, int time);
int _default_init_func(void ** priv_data, int parent);
int _default_button_func(void * priv_data, uint32_t button);
int _default_gest_func(void * priv_data, gestures_t gesture);
int _default_event_func(void * priv_data, al_feature_cmd_t cmd);

// helpers
int _get_last_dive_data(feature_default_graph_info_t * graph_info, TimeType dive_start);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_default_ctx = {
        .name       = "DEFAULT",
        .rls_func = _default_release_func,
        .tmr_func = _default_timer_func,
        .ini_func = _default_init_func,
        .gui_func = _default_button_func,
        .usr_func = _default_gest_func,
        .evt_func = _default_event_func,
};

static feat_default_priv_ctx_t _default_local_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _default_init_func(void ** priv_data, int parent)
{
    al_feature_msg_t feat_msg;

    memset(&_default_local_ctx, 0, sizeof(_default_local_ctx));

    // enable components
    LSM9DS1_PowerUp();
    HAL_NVIC_EnableIRQ(int1_3D_EXTI_IRQn);

    // background is parent window of all screen
    // delete previous screen if needed
    _default_local_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _default_local_ctx.current_window )
    {
        WM_DeleteWindow(_default_local_ctx.current_window);
    }

    // create new window and set initial state
    if ( (parent == serenity_state_configure)   ||
         (parent == serenity_state_compass_dry) ||
         (parent == serenity_state_ble)          )
    {
        _default_local_ctx.current_window = CreateMainMenu_1();
        _default_local_ctx.current_state = default_state_menu;
    }
    else
    {
        _default_local_ctx.current_window = Create_00_01_HourScreen();
        _default_local_ctx.current_state = default_state_lock;
    }

    // set feature timer/timeout to 1second
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 60 * 1000; // 60 sec
    al_feature_send_to_tsk(&feat_msg);

    // return context
    _default_local_ctx.parent_state = parent;
    *priv_data = &_default_local_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _default_timer_func(void * priv_data, int time)
{
    (void)time;
    TimeType now;
    feat_default_priv_ctx_t * ctx = (feat_default_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    now = RTC_GetTime();

    // compare if the new clock value changed
    if ( now.Minutes !=  ctx->prev_time.Minutes )
    {
        WM_Invalidate(ctx->current_window);
        ctx->prev_time = now;
    }

    return 0;
}

//----------------------------------------------------------------------
int _default_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t msg;
    feat_default_priv_ctx_t * ctx = (feat_default_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( cmd  )
    {
        case CMD_BAT_CHRGE:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_charging;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case CMD_DVE_START:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case CMD_RCV_FLICK:
        {
            if (  ctx->current_state != default_state_lock )
            {
                msg.cmd = CMD_END_STATE;
                msg.data.value = serenity_state_compass_dry;
                al_feature_send_to_tsk(&msg);
            }
            break;
        }
        default:
        {
            debug_printf("unknown command\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _default_release_func(void * priv_data)
{
    (void)priv_data;

    LSM9DS1_PowerDown();

    return 0;
}

//----------------------------------------------------------------------
int _default_gest_func(void * priv_data, gestures_t gesture)
{
    feat_default_priv_ctx_t * ctx = (feat_default_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( gesture )
    {
        case swipe_right:
        {
            if ( ctx->current_state == default_state_hist ||
                 ctx->current_state == default_state_info )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = CreateMainMenu_1();
                ctx->current_state = default_state_menu;
            }
            else if ( ctx->current_state == default_state_menu )
            {
                WM_DeleteWindow(ctx->current_window);
                ctx->current_window = Create_00_01_HourScreen();
                ctx->current_state = default_state_lock;
            }
            break;
        }
        case swipe_down:
        {
            if ( ctx->current_hist > 0 )
            {
                ctx->current_hist--;
                _get_last_dive_data(&(ctx->graph_info),
                                    ctx->diverec_list[ctx->current_hist]);
                WM_InvalidateWindow(ctx->current_window);
            }
            break;
        }
        case swipe_up:
        {
            if ( ctx->current_hist < ctx->hist_count )
            {
                ctx->current_hist++;
                _get_last_dive_data(&(ctx->graph_info),
                                    ctx->diverec_list[ctx->current_hist]);
                WM_InvalidateWindow(ctx->current_window);
            }
            break;
        }
        default:
        {
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _default_button_func(void * priv_data, uint32_t button)
{
    int rc;
    WM_MESSAGE pMsg;
    al_feature_msg_t msg;
    feat_default_priv_ctx_t * ctx = (feat_default_priv_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( button )
    {
        case BUTTON_ICE:
        {
            WM_DeleteWindow(ctx->current_window);
            ctx->current_window = CreateICE_WINDOW();
            ctx->current_state = default_state_info;
            break;
        }
        case BUTTON_MENU:
        {
            WM_DeleteWindow(ctx->current_window);
            ctx->current_window = CreateMainMenu_1();
            ctx->current_state = default_state_menu;
            break;
        }
        case BUTTON_MODE:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_configure;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case BUTTON_BLUE:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_ble;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case BUTTON_COMP:
        {
            msg.cmd = CMD_END_STATE;
            msg.data.value = serenity_state_compass_dry;
            al_feature_send_to_tsk(&msg);
            break;
        }
        case BUTTON_HIST:
        {
            WM_DeleteWindow(ctx->current_window);
            ctx->current_window = CreateShowLastDive_Window();
            ctx->current_state = default_state_hist;

            // notify GUI
            pMsg.MsgId = FEATURE_DEFAULT_SET_GRAPH;
            pMsg.Data.v = (int)&(ctx->graph_info);
            WM_SendMessage(ctx->current_window, &pMsg);

            rc = al_database_diverec_list(&ctx->diverec_list[0],
                                          FEATURE_DEFAULT_MAX_RECORD);
            if ( rc <= 0)
            {
                ctx->hist_count = 0;
                ctx->current_hist = 0;
                ctx->graph_info.dive_duration = 0;
                WM_InvalidateWindow(ctx->current_window);
                break;
            }

            // start with the latest dive
            ctx->hist_count = rc - 1;
            ctx->current_hist = ctx->hist_count;

            // convert entry to an array of point
            rc = _get_last_dive_data(&(ctx->graph_info),
                                     ctx->diverec_list[ctx->current_hist]);
            if ( rc )
            {
                ctx->hist_count = 0;
                ctx->current_hist = 0;
                ctx->graph_info.dive_duration = 0;
                WM_InvalidateWindow(ctx->current_window);
                break;
            }

            WM_InvalidateWindow(ctx->current_window);
            break;
        }
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}


//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
int _get_last_dive_data(feature_default_graph_info_t * graph_info, TimeType dive_start)
{
    float interval;
    size_t  length;
    int rc, count = 0, lap_duration = 0;
    al_database_diverec_config_t config;
    al_database_diverec_entry_t entry;
    float DiveData[GUI_GRAPHDATA_LEN];

    // sanity check
    if ( ! graph_info )
    {
        debug_printf("invalid param\n");
        return -1;
    }

    // initialise local variables
    graph_info->lap_count = 0;
    graph_info->max_depth = 0;
    graph_info->dive_duration = 0;
    graph_info->lap_max_duration = 0;

    rc = al_database_diverec_open(dive_start);
    if ( rc )
    {
        debug_printf("cannot open last dive\n");
        return -1;
    }

    // get last dive configuration
    config.type = AL_DATABASE_DIVEREC_TYPE_PARAMS;
    rc = al_database_diverec_get(&config);
    if ( rc ) {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    // save last dive type
    graph_info->last_dive_mode = config.data.params.dive_mode;

    config.type = AL_DATABASE_DIVEREC_TYPE_DCOUNT;
    rc = al_database_diverec_get(&config);
    if ( rc ) {
        debug_printf("unable to retrieve data count\n");
        al_database_diverec_close();
        return -1;
    }

    // we need to get depth data every 'interval'th points
    if ( config.data.entry_count > GUI_GRAPHDATA_LEN ) {
       interval = (float)((float)config.data.entry_count / (float)GUI_GRAPHDATA_LEN);
    } else {
        interval = 1.0;
    }

    // read the dive data and fill the data array
    for (int i = 0; i <  config.data.entry_count; ++i) {
        length =  sizeof(entry);
        al_database_diverec_read(&entry, &length);

        if ( i == (int)(count * interval) ) {
            if ( count >= GUI_GRAPHDATA_LEN ) break;
            DiveData[count] = entry.depth;
            ++count;
        }
    }

    // get Max Depth of the whole dive to scale Graph
    for (int i = 0 ; i < count; ++i )
    {
        if ( DiveData[i] > graph_info->max_depth )
        {
            graph_info->max_depth = DiveData[i];
        }

        if ( DiveData[i] > 0.8 )
        {
            lap_duration++;
        }
        else
        {
            if ( lap_duration )
            {
                if ( (lap_duration * 1000 / SAMPLE_PERIOD) > graph_info->lap_max_duration )
                {
                    graph_info->lap_max_duration = (lap_duration * 1000 / SAMPLE_PERIOD);
                }

                lap_duration = 0;
                graph_info->lap_count++;
            }
        }
    }

    // fill Graph array
    for (int i = 0 ; i < count ; ++i )
    {
        graph_info->dive_profile_graph[i] = (uint16_t)((DiveData[i]*40.0)/graph_info->max_depth);
    }



    config.type = AL_DATABASE_DIVEREC_TYPE_DIVEND;
    rc = al_database_diverec_get(&config);
    if ( rc ) {
        debug_printf("unable to retrieve dive end\n");
        al_database_diverec_close();
        return -1;
    }

    graph_info->dive_start = dive_start;
    graph_info->dive_duration =
            (uint16_t)get_TimeDiffInSeconds(dive_start, config.data.dive_end);
    graph_info->dive_profile_graph_idx = count;

    al_database_diverec_close();

    return 0;
}
