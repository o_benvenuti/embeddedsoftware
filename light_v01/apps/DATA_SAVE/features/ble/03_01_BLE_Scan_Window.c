//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <common.h>
#include <GUI_serenity.h>
#include <babel.h>

#include <al_feature.h>
#include "feature_ble.h"
#include <trace.h>

//----------------------------------------------------------------------
// defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_BUTTON_0    (GUI_ID_USER + 0x01)
#define ID_BUTTON_1    (GUI_ID_USER + 0x02)
#define ID_LISTBOX_0   (GUI_ID_USER + 0x03)

//----------------------------------------------------------------------
// External data
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_bluetoothpetit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

//----------------------------------------------------------------------
// Static data
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        {
                WINDOW_CreateIndirect, "ble_scan_window",
                ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0,
        },
        {
                BUTTON_CreateIndirect, "up",
                ID_BUTTON_0, 85, 40, 75, 40, 0, 0x0, 0
        },
        {
                BUTTON_CreateIndirect, "down",
                ID_BUTTON_1, 85, 88, 75, 40, 0, 0x0, 0,
        },
        {
                LISTBOX_CreateIndirect, "ble_listbox",
                ID_LISTBOX_0, 5, 40, 130, 83, WM_CF_SHOW, 0x0, 0,
        },
};

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};

static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cb_hide_border(WM_MESSAGE * pMsg);
static void _cb_ble_scan_window(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    int Id, NCode, index;
    WM_HWIN hItem, hItemLB;
    al_feature_msg_t feat_msg;
    static feat_ble_priv_ctx_t * ctx = NULL;

    hItem = pMsg->hWin;
    hItemLB = WM_GetDialogItem(pMsg->hWin, ID_LISTBOX_0);

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            ctx = NULL;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cb_ble_scan_window);
            WM_SetCallback(hItemLB, _cb_hide_border);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);

            LISTBOX_SetFont(hItemLB,
                            &GUI_FontSairaSemiCondensedSemiBoldReduced11pts);

            LISTBOX_SetBkColor(hItemLB, LISTBOX_CI_UNSEL,
                               GUI_MAKE_COLOR(0x00003652));
            LISTBOX_SetBkColor(hItemLB, LISTBOX_CI_SEL, GUI_WHITE);
            LISTBOX_SetBkColor(hItemLB, LISTBOX_CI_SELFOCUS, GUI_WHITE);

            LISTBOX_SetTextColor(hItemLB, LISTBOX_CI_SEL,
                                 GUI_MAKE_COLOR(0x00003652));
            LISTBOX_SetTextColor(hItemLB, LISTBOX_CI_SELFOCUS,
                                 GUI_MAKE_COLOR(0x00003652));
            LISTBOX_SetTextColor(hItemLB, LISTBOX_CI_UNSEL, GUI_WHITE);

            LISTBOX_SetAutoScrollV(hItemLB, 0);
            break;
        }
        case FEATURE_SHARE_SCANDEV:
        {
            ctx = (feat_ble_priv_ctx_t *)pMsg->Data.p;

            //delete all items
            while ( (index = LISTBOX_GetNumItems(hItemLB)) )
            {
                LISTBOX_DeleteString(hItemLB, (index-1));
            }

            // and refill...
            for (int i = 0; i < ctx->scan_dev_count; ++i)
            {
                LISTBOX_AddString(hItemLB, ctx->scan_data[i].name);
            }

            break;
        }
        case WM_NOTIFY_PARENT:
        {
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;

            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'Up'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        LISTBOX_DecSel(hItemLB);
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'down'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        LISTBOX_IncSel(hItemLB);
                    }
                    break;
                }
                case ID_LISTBOX_0:
                {
                    if ( NCode == WM_NOTIFICATION_CLICKED )
                    {
                        // sanity check
                        if ( ! ctx )
                        {
                            debug_printf("LISTBOX empty\n");
                            break;
                        }

                        // if already selected, notify feature with
                        // selected broadcast message
                        if ( ctx->selected_broadcast == LISTBOX_GetSel(hItemLB) )
                        {
                            feat_msg.cmd = CMD_GUI_INPUT;
                            feat_msg.data.value = BUTTON_SAVE_SCN;
                            al_feature_send_to_tsk(&feat_msg);
                        }
                        else
                        {
                            ctx->selected_broadcast = LISTBOX_GetSel(hItemLB);
                        }
                    }

                    break;
                }
            }
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
static void _cb_ble_scan_window(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            /* draw background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_Draw_MenuBar_Dots(2,2);

            /* draw bluetooth logo */
            GUI_DrawBitmap(&bmcran_Serenity_13a_bluetoothpetit, 6, 0);

            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            GUI_DispStringAt("Selected the configuration:", 5, 16);

            /* draw arrows */
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 45);
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 110);

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }

}

//----------------------------------------------------------------------
static void _cb_hide_border(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_POST_PAINT:
        {
            // draw rectangle to cover up the borders
            // we did not find how to wipe it off
            GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_DrawRect(0, 0, 129, 82);

            break;
        }
        default:
        {
            LISTBOX_Callback(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
// public API
//----------------------------------------------------------------------
WM_HWIN create_ble_scan_window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}


