#ifndef _FEATURE_BLE_PRIVATE_H_
#define _FEATURE_BLE_PRIVATE_H_
/**
 * @file        feature_ble_private.h
 *
 * @brief       Private implementation of BLE routines
 *
 * @date        24/04/2019
 *
 * @platform    STM32L486
 *
 */
//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef feature_ble_states_t state_func_t(feat_ble_priv_ctx_t * ctx);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern state_func_t* const _state_table[ ];

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
// states
feature_ble_states_t _do_state_listen(feat_ble_priv_ctx_t * ctx);
feature_ble_states_t _do_state_app(feat_ble_priv_ctx_t * ctx);
feature_ble_states_t _do_state_agm(feat_ble_priv_ctx_t * ctx);
feature_ble_states_t _do_state_upgrade(feat_ble_priv_ctx_t * ctx);
feature_ble_states_t _do_state_scanning(feat_ble_priv_ctx_t *ctx);
feature_ble_states_t _do_state_broadcast(feat_ble_priv_ctx_t * ctx);

int _prepare_next_state(feat_ble_priv_ctx_t * ctx);

#endif  //  _FEATURE_BLE_PRIVATE_H_

