#ifndef _FEATURE_BLE_H_
#define _FEATURE_BLE_H_
/**
 * @file:  feature_ble.h
 *
 * @brief
 *
 * @date   24/09/2018
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FEATURE_UPDATE_STATE        WM_USER + 0x07
#define FEATURE_SHARE_SCANDEV       WM_USER + 0x08
#define FEATURE_BLE_MAX_SCAN_DEV    4

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef enum _feature_ble_states_t {
    ble_state_idle,
    ble_state_listen,
    ble_state_app,
    ble_state_agm,
    ble_state_upgrade,
    ble_state_broadcast,
    ble_state_scanning,
    ble_state_end,
}feature_ble_states_t;

typedef struct feature_ble_scan_data {
    char name[16];
    uint8_t data[24];
}feature_ble_scan_data_t;

typedef struct feat_ble_priv_ctx {
    size_t agm_total_size;
    size_t agm_current_pos;
    int scan_dev_count;
    int selected_broadcast;
    size_t firmware_size;
    WM_HWIN current_window;
    uint32_t upgrade_flash_addr;
    serenity_state_t parent_state;
    feature_ble_states_t current_state;
    feature_ble_states_t next_state;
    feature_ble_scan_data_t scan_data[FEATURE_BLE_MAX_SCAN_DEV];
}feat_ble_priv_ctx_t;

enum _feature_ble_button_value {
    BUTTON_APP = 0,
    BUTTON_BRD,
    BUTTON_RCV,
    BUTTON_SAVE_SCN,
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateBLE_Window(void);
WM_HWIN create_ble_scan_window(void);

#endif  /* _FEATURE_BLE_H_ */
