//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <GUI_serenity.h>
#include <al_tools.h>
#include <accel_lib.h>
#include <babel.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)


//----------------------------------------------------------------------
// Static
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
  { WINDOW_CreateIndirect, "CompassCalibration_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

static void _cbRepaint(WM_MESSAGE * pMsg);
extern GUI_CONST_STORAGE GUI_BITMAP bmcompass_calibration_O;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x005B91AB));
            WM_SetCallback(hItem, _cbRepaint);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
WM_HWIN CreateCompass_calibration_O_Window(void)
{
  WM_HWIN hWin;

  hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
  return hWin;
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
	  switch (pMsg->MsgId) {
	  case WM_PAINT:
		  /* draw Background */
		  GUI_SetBkColor(GUI_MAKE_COLOR(0x005B91AB));
		  GUI_Clear();

		  /* display calibrO icon */
		  GUI_DrawBitmap(&bmcompass_calibration_O,
				  LCD_GetXSize() / 2 - bmcompass_calibration_O.XSize/2,
				  LCD_GetYSize() / 2 - bmcompass_calibration_O.YSize/2 - 5);

		  /* print texts */
		  GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
		  GUI_SetColor(GUI_WHITE);
		  GUI_SetTextMode(GUI_TM_TRANS);
		  GUI_SetTextAlign(GUI_TA_HCENTER);
		  GUI_DispStringAt(languages_get_string(TXT_MAGCAL_TITLE),80,0);
		  GUI_SetTextAlign(GUI_TA_HCENTER);
		  GUI_DispStringAt(languages_get_string(TXT_MAGCAL_O_L1),80,95);
		  GUI_SetTextAlign(GUI_TA_HCENTER);
		  GUI_DispStringAt(languages_get_string(TXT_MAGCAL_O_L2),80,108);
		  break;
	  default:
		  WINDOW_Callback(pMsg);
	  }
}
