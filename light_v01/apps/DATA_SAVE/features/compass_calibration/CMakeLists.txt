include_drivers(RTC TOUCHSCREEN ACC FUEL_GAUGE EFLASH)        # RTC for common.h
include_libraries(FEATURE GUI STemWin TRACE TOOLS DATABASE MAGCAL LANGUAGES ILLBEBACK)
include_freertos()


INCLUDE_DIRECTORIES (${CMAKE_CURRENT_SOURCE_DIR}/../../Inc
                     ${CMAKE_SOURCE_DIR}/libraries/STemWin/Config)
                     
ADD_LIBRARY (DATASAVE_FEATURE_COMPASS_CALIBRATION feature_compass_calibration.c
                                               01_23_Compass_calibration_8.c
                                               01_24_Compass_calibration_ready_O.c
                                               01_25_Compass_calibration_O.c)