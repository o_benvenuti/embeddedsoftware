/*
 * feature_dive.c
 *
 *  Created on: 28 sept. 2018
 *      Author: user
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdlib.h> // rand

#include <common.h>
#include <accel_lib.h>
#include <agm_data_save.h>
#include <algapi.h>
#include <al_database.h>
#include <al_feature.h>
#include <al_tools.h>
#include <LSM9DS1_Includes.h>
#include <ms5837_driver.h>
#include <touchscreen.h>
#include <trace.h>
#include <GUI_Serenity.h>
#include <ds2782_driver.h>
#include <illbeback.h>
#include <compass.h>
#include <magcal.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
// Public
int _dive_init_func(void ** priv_data, int parent);
int _dive_event_func(void * priv_data, al_feature_cmd_t cmd);
int _dive_timer_func(void * priv_data, int time);
int _dive_release_func(void * priv_data);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_dive_ctx = {
        .name       = "DIVE",
        .ini_func   = _dive_init_func,
        .rls_func   = _dive_release_func,
        .evt_func   = _dive_event_func,
        .tmr_func   = _dive_timer_func,
};


/**
 * NOTE static context allocation to be used only in init function,
 * then use pointer provided by al_feature library to callbacks
**/
static feat_dive_priv_ctx_t _dive_local_ctx;

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _dive_init_func(void ** priv_data, int parent)
{
    int rc;
    WM_MESSAGE window_msg;
    al_feature_msg_t feat_msg;
    serenity_context_t global_ctx;

    // get global configuration from flash
    rc = serenity_retrieve_status(&global_ctx);
    if ( rc )
    {
        debug_printf("unable to retrieve global context\n");
        return -1;
    }
    // turn devices on
    LSM9DS1_PowerUp();

    // power down Touchscreen Device
    touchscreen_PowerDown();

    // configure the dive with global configuration
    start_new_dive(&_dive_local_ctx, &global_ctx);

    // start API to save AGM data
    agm_data_save_start();

    // background is parent window of all screen
    // delete previous screen if needed
    _dive_local_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _dive_local_ctx.current_window )
    {
        WM_DeleteWindow(_dive_local_ctx.current_window);
    }

    _dive_local_ctx.compass_window = CreateCompass_dive_Window();
    WM_HideWindow(_dive_local_ctx.compass_window);

    _dive_local_ctx.dive_window = CreateDive_Window();

    // save info from parent
    _dive_local_ctx.parent = parent;

    _dive_local_ctx.base_window =  _dive_local_ctx.dive_window;
    _dive_local_ctx.current_window =  _dive_local_ctx.dive_window;

    // send local context to windows
    window_msg.MsgId = FEATURE_DIVE_SHARE_CTX;
    window_msg.Data.p = &_dive_local_ctx;
    WM_SendMessage(_dive_local_ctx.dive_window, &window_msg);
    WM_SendMessage(_dive_local_ctx.compass_window, &window_msg);

    //edit timer delay
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 50;
    al_feature_send_to_tsk(&feat_msg);

    _dive_local_ctx.last_tick = HAL_GetTick();

    // send private configuration to feature manager
    *priv_data = &_dive_local_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _dive_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    feat_dive_priv_ctx_t * ctx = (feat_dive_priv_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    switch ( cmd  )
    {
        case CMD_RCV_FLICK:
        {
            // switch between dive window and compass window
            ctx->base_window = (ctx->base_window == ctx->compass_window) ?
                        ctx->dive_window : ctx->compass_window;

            // refresh screen with previous values
            refresh_screen(ctx);

            break;
        }
        default:
        {
            debug_printf("unknown command\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _dive_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc = 0;
    AxesRaw_t axe_buff;
    al_feature_msg_t msg;
    bool dive_end = false;
    agm_data_save_entry_t entry;

    // NOTE those values are kept static in case
    // that the sensor request fails. Then we keep
    // the previous value as a reference
    static int32_t pressure = 0, temperature = 0;
    static float mag[3] = {0.0};

    feat_dive_priv_ctx_t * dive_ctx = (feat_dive_priv_ctx_t *)priv_data;
    if ( ! dive_ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    //-------------------------------------------
    // DATA SAVING PART
    //-------------------------------------------
    // MAGNETOMETER (min timing is every 50ms)
    if(LSM9DS1_GetMagnetometerValuesGs(&mag[0],&mag[1],&mag[2])<0)
    {
        debug_printf("error getting Magnetometer values\n");
        return 0;
    }

    // compensate magnetometer with calibration matrix
    magcal_compensate_mag(mag);

    entry.dTsec = (float)(DeltaT_Calculation(dive_ctx->last_tick) / 1000.0);
    dive_ctx->last_tick = HAL_GetTick();

    // read data from gyroscope
    rc = LSM9DS1_GetGyroAxesRaw(&axe_buff);
    if ( rc )
    {
        entry.gx = 0;
        entry.gy = 0;
        entry.gz = 0;

        debug_printf("unable to read gyro\n");
    }
    else
    {
        entry.gx = axe_buff.AXIS_X;
        entry.gy = axe_buff.AXIS_Y;
        entry.gz = axe_buff.AXIS_Z;
    }

    // retrieve data from accelerometer
    rc = LSM9DS1_GetAccAxesRaw(&axe_buff);
    if ( rc )
    {
        entry.ax = 0;
        entry.ay = 0;
        entry.az = 0;

        debug_printf("unable to read acc\n");
    }
    else
    {
        entry.ax = axe_buff.AXIS_X;
        entry.ay = axe_buff.AXIS_Y;
        entry.az = axe_buff.AXIS_Z;
    }

    entry.depth = dive_ctx->current_depth;
    entry.mx = (int16_t)LSM9DS1_Inv_calcMag(mag[0]);
    entry.my = (int16_t)LSM9DS1_Inv_calcMag(mag[1]);
    entry.mz = (int16_t)LSM9DS1_Inv_calcMag(mag[2]);

    // read temperature and discard possible error
    LSM9DS1_readTemp(&entry.temp);

    agm_data_save_add(&entry);

    //-------------------------------------------
    // DIVE INFO DISPLAY PART
    //-------------------------------------------
    // and do everything else every seconds
    if ( ++dive_ctx->time_keeper % 20 )
    {
        return 0;
    }

    /* Get depth meter information */
    rc = get_PressureTempDepth(&pressure, &temperature,
                               &dive_ctx->current_depth,
                               gStartDiveLevelPressure);
    if ( rc )
    {
        debug_printf("pressure, temperature and depth skipped\n");
    }

    dive_ctx->curr_dive_duration = get_TimeDiffInSeconds(dive_ctx->dive_start,
                                                RTC_GetTime());


    // update max depth value
    if( dive_ctx->current_depth > dive_ctx->current_max_depth )
    {
        dive_ctx->current_max_depth = dive_ctx->current_depth;
    }

    // update min temperature
    if ( temperature < dive_ctx->min_temperature )
    {
        dive_ctx->min_temperature = temperature;
    }

    /*******************************************/
    /*                 DISPLAY                 */
    /*******************************************/
    refresh_screen(dive_ctx);
    WM_Invalidate(dive_ctx->current_window);

    /*******************************************/
    /*               END OF DIVE               */
    /*******************************************/
    dive_end = end_of_dive(dive_ctx);
    if ( dive_end == true )
    {
        msg.cmd = CMD_END_STATE;
        msg.data.value = dive_ctx->parent;
        al_feature_send_to_tsk(&msg);
    }

    return 0;
}

//----------------------------------------------------------------------
int _dive_release_func(void * priv_data)
{
    feat_dive_priv_ctx_t * ctx = (feat_dive_priv_ctx_t *)priv_data;
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // release AGM data save API
    agm_data_save_stop();

    // turn devices off
    LSM9DS1_PowerDown();

    // start touchscreen
	touchscreen_Init();

    WM_DeleteWindow(ctx->dive_window);
    WM_DeleteWindow(ctx->compass_window);

    // enable touchscreen interrupt, disable pocpoc
    HAL_NVIC_EnableIRQ (EXTI9_5_IRQn); /* enable touchscreen */
    HAL_NVIC_DisableIRQ(int1_3D_EXTI_IRQn);   /* disable flick */

    return 0;
}

