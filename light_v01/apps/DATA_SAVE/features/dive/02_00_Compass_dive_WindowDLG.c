//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <algapi.h>
#include <DIALOG.h>
#include <common.h>
#include <al_tools.h>
#include <GUI_serenity.h>
#include <spi.h>
#include <LSM9DS1.h>
#include <compass.h>
#include <illbeback.h>
#include <magcal.h>
#include <trace.h>
#include <ds2782_driver.h>
#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_PROGBAR_0             (GUI_ID_USER + 0x01)
#define ID_PROGBAR_1             (GUI_ID_USER + 0x02)

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battrouge;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battorange;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_battverte;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_topbasse;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_tophaute;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_flchepalier_topOK;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced20pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Compass_S1_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
WM_HWIN CreateCompass_dive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    static int16_t cHeading = 0;
    static int16_t bHeading = 0;
    static int16_t lHeading = 0; /* local heading value for comparison */
    static int16_t lbHeading = 0; /* local heading value for comparison */
    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case WM_PRE_PAINT:
        {
            cHeading = COMPASS_get_heading();
            bHeading = ibb_get_origin_heading();
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* High resolution settings for Anti-aliasing */
            GUI_AA_SetFactor(HIGHRES_FACTOR);
#if HIGHRES_FACTOR > 1
            GUI_AA_EnableHiRes();
#endif
            /* draw battery icon */
            if ( dive_ctx->battery_level >= 30 )
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battverte, 75, 3);
            }
            else if ( ISBETWEEN(dive_ctx->battery_level, 10, 29) )
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battorange, 75, 3);
            }
            else
            {
                GUI_DrawBitmap(&bmcran_Serenity_13a_battrouge, 75, 3);
            }

            /* get smallest difference between the two angles actual heading & compass reading */
            int diff = cHeading - lHeading;
            diff += (diff>180) ? -360 : (diff<-180) ? 360 : 0;
            /* calculate the amount of degrees to move : faster if far away, slower if closer */
            if	   ((diff>-20)&&(diff<-6 )) lHeading -= 2; /* -20 to -6  */
            else if((diff>=-6)&&(diff<-1 )) lHeading -= 1; /* -6  to -1  */
            else if((diff>=-1)&&(diff<=1 ))	lHeading += 0; /* -1  to  1  */
            else if((diff> 1 )&&(diff<=6 )) lHeading += 1; /*  1  to  6  */
            else if((diff> 6 )&&(diff<20 )) lHeading += 2; /*  6  to  20 */
            else  							lHeading += diff /10;
            /* modify range between 0 & 360 */
            while (lHeading>360)  {lHeading -= 360;}
            while (lHeading<0)    {lHeading += 360;}

            /* get smallest difference between the two angles actual heading & compass reading */
            int diff2 = bHeading - lbHeading;
            diff2 += (diff2>180) ? -360 : (diff2<-180) ? 360 : 0;
            /* calculate the amount of degrees to move */
            if	   ((diff2>-20)&&(diff2<-6 )) lbHeading -= 2; /* -20 to -6  */
            else if((diff2>=-6)&&(diff2<-1 )) lbHeading -= 1; /* -6  to -1  */
            else if((diff2>=-1)&&(diff2<=1 ))	lbHeading += 0; /* -1  to  1  */
            else if((diff2> 1 )&&(diff2<=6 )) lbHeading += 1; /*  1  to  6  */
            else if((diff2> 6 )&&(diff2<20 )) lbHeading += 2; /*  6  to  20 */
            else  							lbHeading += diff2 /10;
            /* modify range between 0 & 360 */
            while (lbHeading>360)  lbHeading -= 360;
            while (lbHeading<0)    lbHeading += 360;

            /* print compass value */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_SetColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_DispDecAt(lHeading,11,-9,lHeading>99?3:lHeading>9?2:1);
            GUI_DispChar('d');

            /* print compass text */
            char * DirectionsTab[] = {"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW","N"};
            char * CompassDir = DirectionsTab[(int)round(((lHeading*10)+112 % 3600) / 225)];
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(CompassDir,11,17);

            GUI_SetColor(GUI_WHITE);

            GUI_ClearRect(90,0,160,25);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_GotoXY((dive_ctx->current_depth>=10?100:dive_ctx->current_depth<0?132:114),-9);
            GUI_DispFloat(dive_ctx->current_depth,dive_ctx->current_depth>9.99?4:dive_ctx->current_depth<0?1:3);

            GUI_DrawCompass(lHeading, lbHeading, ibb_are_we_arrived(),
                            ibb_get_status(), 1);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}
