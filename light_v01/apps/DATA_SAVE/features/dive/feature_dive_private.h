#ifndef _FEATURE_DIVE_PRIVATE_H_
#define _FEATURE_DIVE_PRIVATE_H_
/**
 * @file:  feature_dive_private.h
 *
 * @brief
 *
 * @date   13/02/2013
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define FEATURE_DIVE_GRAPH_SIZE 144

#define FREEDIVE_ALLOW_FLICK_END        (5 * 60)   // in second
#define DECO_BOTTOM_END_TIMEOUT         (1 * 60)    // in second
#ifdef DEBUG_DIVE
extern float DEBUG_DIVE_MANUAL_SPEED;
#endif

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct feat_dive_priv_ctx {
    int parent;
    uint32_t last_tick;             /**< to compute delta time between acquisition    */
    int time_keeper;                /**< used to split actions when timer is raised   */
    uint8_t battery_level;          /**< current battery status for compass window    */
    uint8_t dive_mode;              /**< local value of configured dive mode          */
    uint16_t curr_dive_duration;    /**< total dive duration in seconds               */
    TimeType dive_start;            /**< dive start time used to compute duration     */
    float current_depth;            /**< current depth retrieved every sample period  */
    float current_max_depth;        /**< planned max depth and if occurs current max  */
    int32_t min_temperature;        /**< to be saved at the end of dive               */
    TimerHandle_t compass_tmr;      /**< handle for compass screen refresh            */
    WM_HWIN base_window, current_window;        /**< windows handles for every screen */
    WM_HWIN dive_window, compass_window;        /**< TODO use states machine to have  */
    serenity_depth_param_t  depth_params;   /**< Bottom timer rule by itself      */

}feat_dive_priv_ctx_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
// Helpers
#ifdef DEBUG_DIVE
float _fake_depth(void);
float _fake_depth2(feat_dive_priv_ctx_t * dive_ctx);
float _fake_depth3(feat_dive_priv_ctx_t * dive_ctx);
#endif  // DEBUG_DIVE

bool manage_speed(float depth, feat_dive_priv_ctx_t * ctx);
void graph_data_management(feat_dive_priv_ctx_t * ctx);
int start_new_dive(feat_dive_priv_ctx_t * priv, serenity_context_t * glob);
int manage_stops(feat_dive_priv_ctx_t * ctx);
bool end_of_dive(feat_dive_priv_ctx_t * ctx);
void refresh_screen(feat_dive_priv_ctx_t * ctx);
void compass_timer(TimerHandle_t xTimer);

#endif  // _FEATURE_DIVE_PRIVATE_H_


