//----------------------------------------------------------------------
// Include
//----------------------------------------------------------------------
#include <stdint.h>
#include <DIALOG.h>
#include <algapi.h>
#include <common.h>
#include <GUI_serenity.h>
#include <ms5837_driver.h>
#include <babel.h>
#include <trace.h>

#include "feature_dive.h"
#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)

///----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Dive_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
};

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13B_iconprofmax;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced26pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced24pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced18pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_BITMAP bmTimeLeftDive;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{

   switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            /* Initialization of 'Dive_Window' */
            WM_SetCallback(pMsg->hWin, _cbRepaint);
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateDive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Tstr[20] = {0};
    int sec, hr, min;
    int xPos = 0;
    int yPos = 0;

    static feat_dive_priv_ctx_t * dive_ctx = NULL;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            dive_ctx = NULL;
            break;
        }
        case FEATURE_DIVE_SHARE_CTX:
        {
            dive_ctx = (feat_dive_priv_ctx_t *)pMsg->Data.p;
            break;
        }
        case WM_PAINT:
        {
            // check that we received context
            if ( ! dive_ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            /* draw Background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            //*****************************************************************
            //********************** UPPER SECTION ****************************
            //*****************************************************************
            /* draw MaxDeep Arrow */
            if ( dive_ctx->current_depth >= dive_ctx->current_max_depth )
            {
                xPos = 6+7;
                yPos = (LCD_GetYSize()/6) - (bmcran_Serenity_13B_iconprofmax.YSize/2);
                GUI_DrawBitmap(&bmcran_Serenity_13B_iconprofmax, xPos ,yPos);
                xPos = -3+LCD_GetXSize() / 2;
                yPos = LCD_GetYSize()/6;
            }
            else
            {
                xPos = -7+LCD_GetXSize() / 2;
                yPos = LCD_GetYSize() / 6;
            }

            /* print depth */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced26pts);
            GUI_SetBkColor(GUI_BLACK);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_GotoXY(xPos ,yPos);
            GUI_DispFloatFix(dive_ctx->current_depth, dive_ctx->current_depth<100?4:5, 1);
            GUI_DispString(" ");
            GUI_DispString(languages_get_string(TXT_M_UNIT));


            //-----------------------------------
            // MIDDLE SECTION
            //-----------------------------------
            /* draw graph background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_ClearRect(8, 41, LCD_GetXSize()-9, LCD_GetYSize() - LCD_GetYSize()/3);
            GUI_SetColor(GUI_BLACK);
            GUI_DrawVLine(X_1_3, Y_1_3, Y_2_3);
            GUI_DrawVLine(X_2_3, Y_1_3, Y_2_3);

            //-----------------------------------
            // LOWER SECTION
            //-----------------------------------
            // display low Section Background
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00004767));
            GUI_ClearRect(0,LCD_GetYSize()-LCD_GetYSize()/3 ,LCD_GetXSize(),LCD_GetYSize());
            /* print DiveTime */
            GUI_SetColor(GUI_WHITE);
            min = dive_ctx->curr_dive_duration/60;
            sec = dive_ctx->curr_dive_duration%60;
            hr = min/60;
            min = min%60;

            xPos = 7 + 6 + bmTimeLeftDive.XSize + 8;
            yPos = LCD_GetYSize() - (LCD_GetYSize()/6);
            /* if more than 1h -> format H:MM:SS */
            if(dive_ctx->curr_dive_duration>3600)
            {
                sprintf(Tstr, "%1d:%02d:%02d",hr,min,sec);
            }
            /* if less than 1h -> format MM:SS */
            else
            {
                sprintf(Tstr, "%02d:%02d",min,sec);
                xPos+=10;
            }

            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced24pts);
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_VCENTER);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(Tstr, xPos ,yPos);

            /* draw 'total Time' Icon */
            xPos = 7+6;
            yPos = (LCD_GetYSize() - (LCD_GetYSize()/6)) - (bmTimeLeftDive.YSize/2);
            GUI_DrawBitmap(&bmTimeLeftDive, xPos, yPos);

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
        }
    }
}

