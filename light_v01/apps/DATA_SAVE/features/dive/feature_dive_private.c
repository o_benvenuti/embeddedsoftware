//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
// standard
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

// private
#include <algapi.h>
#include <al_database.h>
#include <common.h>
#include <GUI_Serenity.h>
#include <serenityConfig.h>
#include <stm32l4xx_hal.h>
#include <trace.h>

#include "feature_dive_private.h"

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int start_new_dive(feat_dive_priv_ctx_t * priv, serenity_context_t * glob)
{
    // sanity check
    if ( ! priv || ! glob )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // initialise local context
    priv->time_keeper = 0;
    priv->current_depth = 0;
    priv->curr_dive_duration = 0;
    priv->min_temperature = 259;
    priv->dive_start = RTC_GetTime();
    priv->dive_mode = glob->dive_mode;

    // get global dive configuration
    memcpy(&priv->depth_params,
           &glob->depth_params,
           sizeof(serenity_depth_param_t));

    priv->current_max_depth = priv->depth_params.depth_max;


    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn); /* disable touchscreen */
    HAL_NVIC_EnableIRQ (int1_3D_EXTI_IRQn);   /* enable flick */

    return 0;
}

//----------------------------------------------------------------------
bool end_of_dive(feat_dive_priv_ctx_t * ctx)
{
    static int surfaceTimer = 0;

    // paranoia
    if ( ! ctx )
    {
        debug_printf("invalid argument\n");
        return true;
    }

    /* stop current dive if depth inferior than 80 cm for more than 1 minutes */
    if( (ctx->current_depth < 0.80) && (ctx->curr_dive_duration > 60) )
    {
        surfaceTimer++;
    } else {
        surfaceTimer = 0;
    }

    // is dive ended
    if( (surfaceTimer > DECO_BOTTOM_END_TIMEOUT) || (ctx->curr_dive_duration > 18000) )
    {
        surfaceTimer = 0;
        gStartDiveLevelPressure = SEALEVELPRESSURE;

        return true;
    }

    return false;
}

//----------------------------------------------------------------------
void refresh_screen(feat_dive_priv_ctx_t * ctx)
{
    // for all mode: return from special case window to dive window if needed
    if ( ctx->current_window != ctx->base_window )
    {
        GUI_Switch_Screens(ctx->current_window, ctx->base_window);
        ctx->current_window = ctx->base_window;
    }
}
