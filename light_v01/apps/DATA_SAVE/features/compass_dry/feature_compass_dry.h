#ifndef _FEATURE_COMPASS_DRY_H_
#define _FEATURE_COMPASS_DRY_H_
/**
 * @file:  feature_compass_dry.h
 *
 * @brief
 *
 * @date   18/10/2018
 *
 * <b>Description:</b>\n

 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
enum _feature_compass_dry_button_value {
    BUTTON_START = 0,
    BUTTON_STOP,
    BUTTON_PRINT,
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
WM_HWIN CreateCompass_dry_Window(void);

#endif  // _FEATURE_CHARGING_H_
