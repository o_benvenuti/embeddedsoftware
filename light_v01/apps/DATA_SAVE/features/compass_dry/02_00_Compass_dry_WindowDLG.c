//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include "DIALOG.h"
#include <al_feature.h>
#include <compass.h>
#include "GUI_serenity.h"
#include "feature_compass_dry.h"
#include "al_tools.h"
#include <User_RTC.h>
#include <math.h>
#include "serenityConfig.h"
#include <magcal.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_BUTTON_0              (GUI_ID_USER + 0x03)
#define ID_BUTTON_1              (GUI_ID_USER + 0x04)
#define ID_BUTTON_2              (GUI_ID_USER + 0x05)

//----------------------------------------------------------------------
// Local types
//----------------------------------------------------------------------
enum _compassdry_window_state
{
    IDLE,
    SAVING,
    PRINTING,
};

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static int _local_state;

static void _cbCompassBackground(WM_MESSAGE * pMsg);
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced20pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced18pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedRegularReduced18pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
  { WINDOW_CreateIndirect, "Compass_S1_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
  { BUTTON_CreateIndirect, "START", ID_BUTTON_0, 0, 88, 50, 40, 0, 0x0, 0 },
  { BUTTON_CreateIndirect, "STOP", ID_BUTTON_1, 55, 88, 50, 40, 0, 0x0, 0 },
  { BUTTON_CreateIndirect, "PRINT", ID_BUTTON_2, 110, 88, 50, 40, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
  WM_HWIN hItem;
  int     NCode;
  int     Id;
  al_feature_msg_t feat_msg;

  switch (pMsg->MsgId)
  {
      case WM_INIT_DIALOG:
      {
          // Initialisation of 'Compass_S1_Window'
          hItem = pMsg->hWin;
          _local_state = IDLE;
          WINDOW_SetBkColor(hItem,GUI_BLACK);
          WM_SetCallback(hItem, _cbCompassBackground);
          break;
      }
      case WM_NOTIFY_PARENT:
      {
          Id    = WM_GetId(pMsg->hWinSrc);
          NCode = pMsg->Data.v;
          switch ( Id )
          {
              case  ID_BUTTON_0:
              {
                  if ( NCode == WM_NOTIFICATION_RELEASED )
                  {
                      _local_state = SAVING;
                      feat_msg.cmd = CMD_GUI_INPUT;
                      feat_msg.data.value = BUTTON_START;
                      al_feature_send_to_tsk(&feat_msg);
                      WM_InvalidateWindow(pMsg->hWin);
                  }
                  break;
              }
              case ID_BUTTON_1:
              {
                  if ( NCode == WM_NOTIFICATION_RELEASED )
                  {
                      _local_state = IDLE;
                      feat_msg.cmd = CMD_GUI_INPUT;
                      feat_msg.data.value = BUTTON_STOP;
                      al_feature_send_to_tsk(&feat_msg);
                      WM_InvalidateWindow(pMsg->hWin);
                  }
                  break;
              }
              case ID_BUTTON_2:
              {
                  if ( NCode == WM_NOTIFICATION_RELEASED )
                  {
                      _local_state = PRINTING;
                      feat_msg.cmd = CMD_GUI_INPUT;
                      feat_msg.data.value = BUTTON_PRINT;
                      al_feature_send_to_tsk(&feat_msg);
                      WM_InvalidateWindow(pMsg->hWin);
                  }
                  break;
              }
          }
          break;
      }
      default:
      {
          WM_DefaultProc(pMsg);
          break;
      }
  }
}

//----------------------------------------------------------------------
WM_HWIN CreateCompass_dry_Window(void)
{
  WM_HWIN hWin;

  hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                             _cbDialog, WM_HBKWIN, 0, 0);

  return hWin;
}

//----------------------------------------------------------------------
static void _cbCompassBackground(WM_MESSAGE * pMsg)
{
    static int16_t cHeading = 0;
    static int16_t lHeading = 0; /* local heading value for comparaison */

    switch (pMsg->MsgId)
    {
        case WM_PRE_PAINT:
        {
            cHeading = COMPASS_get_heading();
            break;
        }
        case WM_PAINT:
        {
            /* draw background */
            GUI_SetBkColor(GUI_BLACK);
            GUI_Clear();

            /* High resolution settings for Anti-aliasing */
            GUI_AA_SetFactor(HIGHRES_FACTOR);
#if HIGHRES_FACTOR > 1
            GUI_AA_EnableHiRes();
#endif
            // draw 'toward' arrow
            GUI_AA_FillPolygon(GUI_toward_arrow, countof(GUI_toward_arrow),
                               80 * HIGHRES_FACTOR , 5 * HIGHRES_FACTOR);

            /* get smallest difference between the two angles actual heading & compass reading */
            int diff = cHeading - lHeading;
            diff += (diff>180) ? -360 : (diff<-180) ? 360 : 0;
            /* calculate the amount of degrees to move */
            if	   ((diff>-20)&&(diff<-6 )) lHeading -= 2; /* -20 to -6  */
            else if((diff>=-6)&&(diff<-1 )) lHeading -= 1; /* -6  to -1  */
            else if((diff>=-1)&&(diff<=1 ))	lHeading += 0; /* -1  to  1  */
            else if((diff> 1 )&&(diff<=6 )) lHeading += 1; /*  1  to  6  */
            else if((diff> 6 )&&(diff<20 )) lHeading += 2; /*  6  to  20 */
            else  							lHeading += diff /10;
            /* modify range between 0 & 360 */
            while (lHeading>360)  lHeading -= 360;
            while (lHeading<0)    lHeading += 360;

            /* print compass value */
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced20pts);
            GUI_SetColor(GUI_MAKE_COLOR(0x0000B1E7));
            GUI_DispDecAt(lHeading,11,-9,lHeading>99?3:lHeading>9?2:1);
            GUI_DispChar('�');

            /* print compass text */
            char * DirectionsTab[] = {"N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW","N"};
            char * CompassDir = DirectionsTab[(int)round((((lHeading*10)+112) % 3600) / 225)];
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(CompassDir,11,17);

            GUI_SetColor(GUI_WHITE);
            /* print Hour */
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced18pts);
            TimeType Time = RTC_GetTime();
            GUI_DispHexAt(Time.Hours,96,-7,(Time.Hours>9?2:1));
            GUI_DispChar(':');
            GUI_SetFont(&GUI_FontSairaSemiCondensedRegularReduced18pts);
            GUI_DispHex(Time.Minutes,2);

            // calibration quality
            float quality = magcal_get_quality();

            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_SetColor(GUI_WHITE);
            GUI_DispDecAt(quality,139,36,2);

            int bars = 0;
            if      (quality>99) {quality = 99;}
            if      (quality<75) {bars = 0;}
            else if (quality<85) {bars = 1;}
            else if (quality<90) {bars = 2;}
            else   /*quality>90*/{bars = 3;}
            GUI_draw_quality_bars(bars,140,27);
            GUI_DrawCompass(lHeading, 0, false, 2, 0);

            switch ( _local_state )
            {
                case SAVING:
                {
                    GUI_SetColor(GUI_GREEN);GUI_FillCircle(25,80,8);
                    break;
                }
                case PRINTING:
                {
                    GUI_SetColor(GUI_RED);GUI_FillCircle(135,80,8);
                    break;
                }
                case IDLE:
                default:
                {
                    GUI_SetColor(GUI_BLUE);GUI_FillCircle(80,80,8);
                    break;
                }

            }

            break;
        }
        default:
        {
            WINDOW_Callback(pMsg);
            break;
        }
    }
}

