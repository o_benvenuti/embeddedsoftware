/*
 * feature_compass_dry.C
 *
 *  Created on: 12 feb. 2019
 *      Author: Aix Sonic
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <common.h>
#include <accel_lib.h>
#include <agm_data_save.h>
#include <al_feature.h>
#include <al_tools.h>
#include <GUI_Serenity.h>
#include <stm32l4xx_hal.h>
#include <touchscreen.h>
#include <trace.h>
#include <LSM9DS1.h>
#include <GUI_serenity.h>
#include <compass.h>
#include <illbeback.h>
#include <magcal.h>

#include "feature_compass_dry.h"

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct compass_dry_private_ctx {
    int time_keeper;
    uint16_t prev_heading;
    uint16_t prev_start_heading;
    WM_HWIN current_window;
    serenity_state_t parent_state;
    int last_tick;
    bool save_agm;
}compass_dry_private_ctx_t;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static int _compass_dry_init_func(void ** priv_data, int parent);
static int _compass_dry_gest_func(void * priv_data, gestures_t gesture);
static int _compass_dry_event_func(void * priv_data, al_feature_cmd_t cmd);
static int _compass_dry_timer_func(void * priv_data, int time);
static int _compass_dry_button_func(void * priv_data, uint32_t button);
static int _compass_dry_release_func(void * priv_data);

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
al_feature_ctx_t feature_compass_dry_ctx = {
        .name       = "COMPASS DRY",
        .ini_func   = _compass_dry_init_func,
        .usr_func   = _compass_dry_gest_func,
        .evt_func   = _compass_dry_event_func,
        .tmr_func   = _compass_dry_timer_func,
        .gui_func   = _compass_dry_button_func,
        .rls_func   = _compass_dry_release_func,
};

static compass_dry_private_ctx_t _private_ctx = { 0 };

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int _compass_dry_init_func(void ** priv_data, int parent)
{
    al_feature_msg_t feat_msg;

    _private_ctx.time_keeper = 0;
    _private_ctx.save_agm = false;
    _private_ctx.last_tick = HAL_GetTick();

    // save parent feature
    _private_ctx.prev_heading = 0;
    _private_ctx.parent_state = parent;

    // background is parent window of all screen
    // delete previous screen if needed
    _private_ctx.current_window = WM_GetFirstChild(WM_HBKWIN);
    if ( _private_ctx.current_window )
    {
        WM_DeleteWindow(_private_ctx.current_window);
    }

    // create screen
    _private_ctx.current_window = CreateCompass_dry_Window();

    /* activate the component */
    if ( LSM9DS1_PowerUp() < 0 )
    {
        debug_printf("Compass dry feature - impossibility to start component\n");
    }

    // set feature timer/timeout to 50ms
    feat_msg.cmd = CMD_CHG_TIMER;
    feat_msg.data.value = 50;
    al_feature_send_to_tsk(&feat_msg);

    // return private context
    *priv_data = &_private_ctx;

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_timer_func(void * priv_data, int time)
{
    (void)time;
    int rc = 0;
    float acc[3] = {0};
    agm_data_save_entry_t entry;
    AxesRaw_t axe_buff = {0};
    static float mag[3] = {0}; // keep static as we don't update it every loop 
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    // MAGNETOMETER
    if(LSM9DS1_GetMagnetometerValuesGs(&mag[0],&mag[1],&mag[2])<0)
    {
        debug_printf("error getting Magnetometer values\n");
        return 0;
    }

    // compensate magnetometer with calibration matrix
    magcal_compensate_mag(mag);

    // retrieve data from accelerometer
    rc = LSM9DS1_GetAccAxesRaw(&axe_buff);
    if ( rc )
    {
        debug_printf("unable to read acc\n");
    }

    // save those raw data if needed
    if ( ctx->save_agm )
    {
        entry.ax = axe_buff.AXIS_X;
        entry.ay = axe_buff.AXIS_Y;
        entry.az = axe_buff.AXIS_Z;
    }

    // convert raw acc to G for compass heading processing
    acc[0] = LSM9DS1_calcAccel(axe_buff.AXIS_X);
    acc[1] = LSM9DS1_calcAccel(axe_buff.AXIS_Y);
    acc[2] = LSM9DS1_calcAccel(axe_buff.AXIS_Z);

    // compute new compass heading
    COMPASS_update_heading(&mag[0],&acc[0]);

    if ( ctx->save_agm )
    {
        entry.dTsec = (float)(DeltaT_Calculation(ctx->last_tick) / 1000.0);
        ctx->last_tick = HAL_GetTick();

        // read data from gyroscope
        rc = LSM9DS1_GetGyroAxesRaw(&axe_buff);
        if ( rc )
        {
            debug_printf("unable to read gyro\n");
        }

        entry.gx = axe_buff.AXIS_X;
        entry.gy = axe_buff.AXIS_Y;
        entry.gz = axe_buff.AXIS_Z;

        entry.depth = 0.0;
        entry.mx = (int16_t)LSM9DS1_Inv_calcMag(mag[0]);
        entry.my = (int16_t)LSM9DS1_Inv_calcMag(mag[1]);
        entry.mz = (int16_t)LSM9DS1_Inv_calcMag(mag[2]);

        // read temperature and discard possible error
        LSM9DS1_readTemp(&entry.temp);

        agm_data_save_add(&entry);
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_event_func(void * priv_data, al_feature_cmd_t cmd)
{
    al_feature_msg_t feat_msg;
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( cmd )
    {
        case CMD_BAT_CHRGE:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = serenity_state_charging;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        case CMD_DVE_START:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = serenity_state_dive;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        case CMD_RCV_FLICK:
        {
            // return to previous state
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        default:
        {
            debug_printf("Unknown button\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_release_func(void * priv_data)
{
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    /* turn off the component */
    LSM9DS1_PowerDown();
    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_gest_func(void * priv_data, gestures_t gesture)
{
    al_feature_msg_t feat_msg;
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    switch ( gesture )
    {
        case swipe_right:
        {
            feat_msg.cmd = CMD_END_STATE;
            feat_msg.data.value = ctx->parent_state;
            al_feature_send_to_tsk(&feat_msg);
            break;
        }
        default:
        {
            debug_printf("unknown gesture\n");
            break;
        }
    }

    return 0;
}

//----------------------------------------------------------------------
int _compass_dry_button_func(void * priv_data, uint32_t button)
{
    compass_dry_private_ctx_t * ctx = (compass_dry_private_ctx_t *)priv_data;

    // sanity check
    if ( ! ctx )
    {
        debug_printf("invalid context\n");
        return -1;
    }

    switch ( button )
    {
        case BUTTON_START:
        {
            // init library when starting
            if ( ! ctx->save_agm )
            {
                agm_data_save_start();
            }
            ctx->save_agm = true;
            break;
        }
        case BUTTON_STOP:
        {
            // stop only if previously started
            if ( ctx->save_agm )
            {
                agm_data_save_stop();
            }

            ctx->save_agm = false;
            break;
        }
        case BUTTON_PRINT:
        {
            if ( ! ctx->save_agm )
            {
                agm_data_save_print();
            }
            break;
        }
        default:
        {
            debug_printf("unknown button\n");
            break;
        }
    }

    return 0;
}


