//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>

#include <stdio.h>
#include <common.h>
#include <GUI_serenity.h>
#include <User_RTC.h>
#include <ILI9163_driver.h>
#include <LSM9DS1_Includes.h>
#include <trace.h>
#include <babel.h>
#include <al_feature.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0    (GUI_ID_USER + 0x00)
#define ID_SLIDER_0    (GUI_ID_USER + 0x01)
#define ID_BUTTON_0    (GUI_ID_USER + 0x02)
#define ID_BUTTON_1    (GUI_ID_USER + 0x03)
#define ID_BUTTON_2    (GUI_ID_USER + 0x04)
#define ID_BUTTON_3    (GUI_ID_USER + 0x05)
#define ID_BUTTON_4    (GUI_ID_USER + 0x06)
#define ID_BUTTON_5    (GUI_ID_USER + 0x07)
#define ID_BUTTON_6    (GUI_ID_USER + 0x08)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
static bool pocpoc_Clicked = true;
static bool brightness_Clicked = false;
static bool compass_calibration_clicked = false;
static serenity_context_t * ctx = NULL;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedSemiBoldReduced11pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaSemiCondensedMediumReduced9pts;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "WatchSettings_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Flick_calib"         , ID_BUTTON_3, 0, 15, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "brightness_calib"    , ID_BUTTON_4, 0, 52, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus_Button"         , ID_BUTTON_1, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button"        , ID_BUTTON_2, 85, 88, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Compass_calib"       , ID_BUTTON_0, 0, 90, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "no"                  , ID_BUTTON_5, 13, 84, 62, 21, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "yes"                 , ID_BUTTON_6, 85, 84, 62, 21, 0, 0x0, 0 },
};

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;
    al_feature_msg_t feat_msg;

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
        {
            //
            // Initialisation of 'WatchSettings_Window'
            //
            ctx = NULL;
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(hItem, _cbRepaint);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_2),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_4),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_5),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_6),
                           GUI_transparent_button_cb);
            break;
        }
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            ctx = &(((feat_conf_priv_ctx_t *)pMsg->Data.p)->global_ctx);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:
        {
            if ( ! ctx )
            {
                debug_printf("context not received\n");
                break;
            }

            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_BUTTON_0: // Notifications sent by 'compass calib button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(!compass_calibration_clicked)
                        {
                            compass_calibration_clicked = true;
                            pocpoc_Clicked = false;
                            brightness_Clicked = false;
                        }
                    }
                    break;
                }
                case ID_BUTTON_1: // Notifications sent by 'Plus_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(!compass_calibration_clicked)
                        {
                            if(brightness_Clicked)
                            {
                                ctx->Brightness = (ctx->Brightness == 100 ? 100 :
                                        ctx->Brightness + 10);
                                change_BaklightIntensity(ctx->Brightness);
                                WM_Invalidate(pMsg->hWin);
                            }
                            else if (pocpoc_Clicked)
                            {
                                if(ctx->flick_sensitivity < 5)
                                {
                                    ctx->flick_sensitivity++;
                                    agm_set_flick_sensitivity(ctx->flick_sensitivity);
                                }
                            }
                        }
                    }
                    break;
                }
                case ID_BUTTON_2: // Notifications sent by 'Moins_Button'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(!compass_calibration_clicked)
                        {
                            if(brightness_Clicked)
                            {
                                ctx->Brightness = (ctx->Brightness == 10 ? 10 :
                                        ctx->Brightness - 10);
                                change_BaklightIntensity(ctx->Brightness);
                                WM_Invalidate(pMsg->hWin);
                            }
                            else if (pocpoc_Clicked)
                            {
                                if  ( ctx->flick_sensitivity > 1 )
                                {
                                    ctx->flick_sensitivity--;
                                    agm_set_flick_sensitivity(ctx->flick_sensitivity);
                                }
                            }
                        }
                    }
                    break;
                }
                case ID_BUTTON_3: // Notifications sent by 'Flick_calib'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(!compass_calibration_clicked)
                        {
                            pocpoc_Clicked = true;
                            brightness_Clicked = false;
                        }
                    }
                    break;
                }
                case ID_BUTTON_4: // Notifications sent by 'brightness_calib'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(!compass_calibration_clicked)
                        {
                            pocpoc_Clicked = false;
                            brightness_Clicked = true;
                        }
                    }
                    break;
                }
                case ID_BUTTON_5: // Notifications sent by 'No'
                {
                    if ( NCode == WM_NOTIFICATION_RELEASED )
                    {
                        if(compass_calibration_clicked)
                        {
                            pocpoc_Clicked = true;
                            brightness_Clicked = false;
                            compass_calibration_clicked = false;
                        }
                    }
                    break;
                }
                case ID_BUTTON_6: // Notifications sent by 'Yes'
                {
                    if ( NCode ==  WM_NOTIFICATION_RELEASED )
                    {
                        if(compass_calibration_clicked)
                        {
                            pocpoc_Clicked = true;
                            brightness_Clicked = false;
                            compass_calibration_clicked = false;
                            feat_msg.cmd = CMD_GUI_INPUT;
                            feat_msg.data.value = BUTTON_MAGCAL;
                            al_feature_send_to_tsk(&feat_msg);
                        }
                    }
                    break;
                }
            }
            WM_Invalidate(pMsg->hWin);
            break; // WM_NOTIFY_PARENT
        }
        default:
            WM_DefaultProc(pMsg);
            break;
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateWatchSettings_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate),
                               _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Bstr[8];
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
            /* draw Background */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            GUI_Draw_MenuBar_Dots(4,2);

            /* draw arrows */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 20);
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);

            GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
            if(pocpoc_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 15, 80, 52);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_FLICK_SENSITIVITY_L1),4,14);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_FLICK_SENSITIVITY_L2),4,28);

            if(brightness_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 53, 80, 90);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_BRIGHTNESS),4,59);

            if(compass_calibration_clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 91, 80, 128);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_COMPASS_CALIBRATION_L1),4,90);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_COMPASS_CALIBRATION_L2),4,104);

            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
            if(pocpoc_Clicked)
            {
                GUI_DispDecAt(ctx->flick_sensitivity,152,71,1);
            }
            else if(brightness_Clicked)
            {
                sprintf(Bstr, "%d%%",ctx->Brightness);
                GUI_DispStringAt(Bstr,152,71);
            }

            if(compass_calibration_clicked)
            {
                /* fill background */
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_AA_FillRoundedRect(8, 42, 152, 101,5);

                /* text */
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetColor(GUI_WHITE);
                GUI_SetFont(&GUI_FontSairaSemiCondensedMediumReduced9pts);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_DispStringAt(languages_get_string(TXT_START_COMPASS_CALIBRATION),80,30+(2*14));

                /* 'no' button */
                GUI_SetFont(&GUI_FontSairaSemiCondensedSemiBoldReduced11pts);
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect(13, 75, 75, 96,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_DispStringAt(languages_get_string(TXT_NO),44,86);

                /* 'yes' button */
                GUI_SetColor(GUI_WHITE);
                GUI_AA_FillRoundedRect(85, 75, 146, 96,5);
                GUI_SetTextMode(GUI_TM_TRANS);
                GUI_SetTextAlign(GUI_TA_HCENTER | GUI_TA_VCENTER);
                GUI_SetColor(GUI_MAKE_COLOR(0x00012136)); /* dark blue */
                GUI_DispStringAt(languages_get_string(TXT_YES),116,86);
            }
            break;
        default:
            WINDOW_Callback(pMsg);
    }
}

