//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <DIALOG.h>
#include <common.h>
#include <stdio.h>
#include <GUI_serenity.h>
#include <languages.h>
#include <babel.h>
#include <trace.h>

#include "feature_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define ID_WINDOW_0              (GUI_ID_USER + 0x00)
#define ID_BUTTON_0              (GUI_ID_USER + 0x01)
#define ID_BUTTON_1              (GUI_ID_USER + 0x02)
#define ID_BUTTON_3              (GUI_ID_USER + 0x04)
#define ID_BUTTON_4              (GUI_ID_USER + 0x05)
#define ID_BUTTON_5              (GUI_ID_USER + 0x06)


//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconparamtres_tres_petit;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedSemiBoldReduced22pts;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontSairaCondensedMediumReduced13pts;
static bool Idle_Time_Button_Clicked = false;
static bool Max_Depth_Button_Clicked = false;

static const GUI_WIDGET_CREATE_INFO _aDialogCreate[] = {
        { WINDOW_CreateIndirect, "Param_Plongee_01_Window", ID_WINDOW_0, 0, 0, 160, 128, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Rest_Time_Button", ID_BUTTON_0, 0, 14, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Max_Depth_Button", ID_BUTTON_1, 0, 52, 80, 38, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Plus_Button", ID_BUTTON_3, 85, 15, 75, 40, 0, 0x0, 0 },
        { BUTTON_CreateIndirect, "Moins_Button", ID_BUTTON_4, 85, 88, 75, 40, 0, 0x0, 0 },
};

static serenity_free_param_t * free_params = NULL;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
static void _cbDialog(WM_MESSAGE * pMsg)
{
    WM_HWIN hItem;
    int     NCode;
    int     Id;

    switch (pMsg->MsgId) {
        case WM_INIT_DIALOG:
            hItem = pMsg->hWin;
            WINDOW_SetBkColor(hItem, GUI_MAKE_COLOR(0x00003652));
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_0),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_1),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_3),
                           GUI_transparent_button_cb);
            WM_SetCallback(WM_GetDialogItem(pMsg->hWin, ID_BUTTON_4),
                           GUI_transparent_button_cb);
            WM_SetCallback(pMsg->hWin,_cbRepaint);
            Idle_Time_Button_Clicked = true;
            Max_Depth_Button_Clicked = false;
            break;
        case FEATURE_CONFIGURE_SHARE_CTX:
        {
            free_params = &(((feat_conf_priv_ctx_t *)pMsg->Data.p)->global_ctx.free_params);

            // set new callback once we've got context handle
            WM_SetCallback(pMsg->hWin,_cbRepaint);
            break;
        }
        case WM_NOTIFY_PARENT:

            if ( ! free_params )
            {
                debug_printf("context not received\n");
                break;
            }

            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id) {
                case ID_BUTTON_0: // Rest_time
                    if(NCode ==  WM_NOTIFICATION_RELEASED)
                    {
                        Idle_Time_Button_Clicked = !Idle_Time_Button_Clicked;
                        Max_Depth_Button_Clicked = false;
                    }
                    break;
                case ID_BUTTON_1: // Max Depth
                    if(NCode ==  WM_NOTIFICATION_RELEASED)
                    {
                        Idle_Time_Button_Clicked = false;
                        Max_Depth_Button_Clicked = !Max_Depth_Button_Clicked;
                    }
                    break;
                case ID_BUTTON_3: // Notifications sent by 'Plus_Button'
                    if(NCode ==  WM_NOTIFICATION_RELEASED)
                    {
                        if(Idle_Time_Button_Clicked)
                        {
                            if(free_params->rest_time_to_end < IDLE_TIME_MAX-59)
                            {
                                free_params->rest_time_to_end+=60;
                            }
                        }
                        if(Max_Depth_Button_Clicked)
                        {
                            if(free_params->depth_max < MAX_DEPTH_MAX)
                            {
                                free_params->depth_max++;
                            }
                        }
                    }
                    break;
                case ID_BUTTON_4: // Notifications sent by 'Moins_Button'
                    if(NCode ==  WM_NOTIFICATION_RELEASED)
                    {
                        if(Idle_Time_Button_Clicked)
                        {
                            if(free_params->rest_time_to_end > IDLE_TIME_MIN+59)
                            {
                                free_params->rest_time_to_end-=60;
                            }
                        }
                        if(Max_Depth_Button_Clicked)
                        {
                            if(free_params->depth_max > MAX_DEPTH_MIN)
                            {
                                free_params->depth_max--;
                            }
                        }
                    }
                    break;
            }
            WM_Invalidate(pMsg->hWin); /* invalidate whole window to force redraw */
            break;
                default:
                    WM_DefaultProc(pMsg);
                    break;
    }
}

//----------------------------------------------------------------------
WM_HWIN CreateParam_Free_Dive_Window(void)
{
    WM_HWIN hWin;

    hWin = GUI_CreateDialogBox(_aDialogCreate, GUI_COUNTOF(_aDialogCreate), _cbDialog, WM_HBKWIN, 0, 0);
    return hWin;
}

//----------------------------------------------------------------------
static GUI_POINT triangle_up[] = {
        { 0, 0 },
        { 5, 10 },
        {-5, 10 }
};
static GUI_POINT triangle_down[] = {
        { 0, 0 },
        { 10, 0 },
        { 5, 10 }
};

//----------------------------------------------------------------------
static void _cbRepaint(WM_MESSAGE * pMsg)
{
    char Tstr[20] = {0};
    int min, sec;
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();

            /* draw arrows */
            GUI_AA_DrawPolyOutline(triangle_down, countof(triangle_down), 1, 143, 113);
            GUI_AA_DrawPolyOutline(triangle_up, countof(triangle_up), 1, 148, 19);

            /* draw menu bar */
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00012136));
            GUI_ClearRect(0, 0, 160, 14);
            GUI_DrawBitmap(&bmcran_Serenity_13a_iconparamtres_tres_petit, 4, 1);

            /* draw dots */
            GUI_Draw_MenuBar_Dots(2,2);

            /* draw buttons */
            GUI_SetFont(&GUI_FontSairaCondensedMediumReduced13pts);
            if(Idle_Time_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 15, 80, 52);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_FREE_DIVE_IDLE_TIME_L1),4,14);
            GUI_SetTextMode(GUI_TM_TRANS);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_FREE_DIVE_IDLE_TIME_L2),4,28);

            if(Max_Depth_Button_Clicked)
            {
                GUI_SetBkColor(GUI_WHITE);
                GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            }
            else
            {
                GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
                GUI_SetColor(GUI_WHITE);
            }
            GUI_ClearRect(0, 53, 80, 90);
            GUI_DispStringAt(languages_get_string(TXT_PARAM_MAX_DEPTH),4,59);

            /* display numeric value */
            GUI_SetFont(&GUI_FontSairaCondensedSemiBoldReduced22pts);
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_SetColor(GUI_WHITE);
            GUI_SetTextAlign(GUI_TA_RIGHT | GUI_TA_VCENTER);
            if(Idle_Time_Button_Clicked)
            {
                min = free_params->rest_time_to_end / 60;
                sec = free_params->rest_time_to_end % 60;
                sprintf(Tstr, "%1d:%02d",min,sec);
                GUI_DispStringAt(Tstr,152,71);
            }
            else if(Max_Depth_Button_Clicked)
            {
                sprintf(Tstr, "%d%s",(int)free_params->depth_max,languages_get_string(TXT_M_UNIT));
                GUI_DispStringAt(Tstr,152,71);
            }
            break;
        default:
            WINDOW_Callback(pMsg);
    }
}

