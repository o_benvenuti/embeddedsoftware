#ifndef _COMPASS_H_
#define _COMPASS_H_
/**
 * @file:  magcal.h
 *
 * @brief
 *
 * @date   23/01/2019
 *
 *
 *
 */

#include <stdint.h>

int8_t COMPASS_heading_tilt_compensated(uint16_t * heading, float comp_mag[3], float raw_acc[3]);
uint16_t COMPASS_get_heading(void);
int8_t   COMPASS_acq_and_update_heading(void);
int8_t   COMPASS_update_heading(float mag[3], float acc[3]);
void     COMPASS_force_heading(uint16_t heading);


#endif  /* _COMPASS_H_ */









