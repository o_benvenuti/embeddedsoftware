/*
 * magcal.c
 *
 *  Created on: 23 janv. 2019
 *      Author: BGE
 */
#include "Inc/compass.h"

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "LSM9DS1.h"
#include <al_tools.h>
#include <magcal.h>
#include <trace.h>

static uint16_t COMPASS_lib_heading = 0;

int8_t COMPASS_heading_tilt_compensated(uint16_t * heading, float comp_mag[3], float raw_acc[3])
{
	float aRoll = 0.0;
	float aPitch = 0.0;
	float angle = 0.0;
	float acc[3] = {0};
	float mag[3] = {0};
	float x = 0.0;
	float y = 0.0;

	// check input parameters validity
	if ( ! comp_mag[0] && ! comp_mag[1] && ! comp_mag[2])
	{
	    return -1;
	}

	// Change Accelerometer X & Y sign
	acc[0] = -raw_acc[0];
	acc[1] = -raw_acc[1];
	acc[2] =  raw_acc[2];
	// change magnetometer X & Z sign
	mag[0] = -comp_mag[0];
	mag[1] =  comp_mag[1];
	mag[2] = -comp_mag[2];

	aRoll  = atan2( acc[1],sqrt(acc[0]*acc[0]+acc[2]*acc[2]))*signf(acc[2]) ;
	aPitch = atan2(-acc[0],sqrt(acc[1]*acc[1]+acc[2]*acc[2]))*signf(acc[2]) ;

	x = -(mag[2]*sin(aRoll)-mag[1]*cos(aRoll))*signf(acc[2]);
	y = mag[0]*cos(aPitch)+mag[1]*sin(aPitch)*sin(aRoll)+mag[2]*sin(aPitch)*cos(aRoll);

	if(y==0 && x<0)
	{
		angle = M_PI;
	}
	else if(y==0 && x>0)
	{
		angle = M_PI + (M_PI/2.0);
	}
	else
	{
		if(y>0)
		{
			if(x<0)
			{
				angle = -atan(x/y);
			}
			else
			{
				angle = 2*M_PI - atan(x/y);
			}
		}
		else
		{
			angle = M_PI - atan(x/y);
		}
	}
	* heading = rad2deg(angle);

	return 0;
}

uint16_t COMPASS_get_heading(void)
{
	return COMPASS_lib_heading;
}

int8_t COMPASS_acq_and_update_heading(void)
{
	int8_t ret;
	float mag[3] = {0};
	float acc[3] = {0};

	ret = LSM9DS1_GetMagnetometerValuesGs(&mag[0], &mag[1], &mag[2]);
	if(ret < 0){return ret;}

	// ACCELEROMETER
	if(LSM9DS1_GetAccelerometerValuesG(&acc[0],&acc[1],&acc[2])<0)
	{
		debug_printf("error getting Accelerometer values\n");
		return -1;
	}

	magcal_compensate_mag(mag);

	COMPASS_heading_tilt_compensated(&COMPASS_lib_heading,mag,acc);

	return ret;
}

int8_t COMPASS_update_heading(float mag[3], float acc[3])
{
	return COMPASS_heading_tilt_compensated(&COMPASS_lib_heading,mag,acc);
}

void COMPASS_force_heading(uint16_t heading)
{
	COMPASS_lib_heading = heading;
}


