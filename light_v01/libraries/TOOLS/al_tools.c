//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <math.h>
#include <cmsis_os.h>

// hal
#include <stm32l4xx_hal.h>

// local
#include <al_tools.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define AL_TOOLS_METER_IN_FEET  3.28084

//----------------------------------------------------------------------
// Private variable
//----------------------------------------------------------------------
__IO uint32_t uwTick;
static uint32_t ChronoTick;

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
/*
 *  SYSTEM TICK functions re-writed to match the freertos environnement
 */
void HAL_IncTick(void)
{
  uwTick++;
}

//----------------------------------------------------------------------
uint32_t HAL_GetTick(void)
{
    if ( ! osKernelRunning() )
    {
        return uwTick;
    }
    else
    {
        return osKernelSysTick();
    }
}

//----------------------------------------------------------------------
void HAL_Delay(uint32_t Delay)
{
    /* if the kernel is running, use the freeRTOS way to get tick */
    if ( osKernelRunning() )
    {
        osDelay(Delay);
    }
    else
    {
        /* else, use the HAL way to get tick
           this is just a copy of the original HAL_Delay function */
        uint32_t tickstart = HAL_GetTick();
        uint32_t wait = Delay;

        /* Add a period to guaranty minimum wait */
        if (wait < HAL_MAX_DELAY)
        {
            wait++;
        }

        while((HAL_GetTick() - tickstart) < wait)
        {
        }
    }
}

//----------------------------------------------------------------------
uint8_t bcd2int(uint8_t bcd)
{
    return 10 * (bcd >> 4) + (bcd & 0xF);
}

//----------------------------------------------------------------------
uint8_t int2bcd(uint8_t integer)
{
    return ((integer%100)/10)<<4 | integer%10;
}

//----------------------------------------------------------------------
uint32_t DeltaT_Calculation(uint32_t TickStart)
{
  return (HAL_GetTick() - TickStart);
}

//----------------------------------------------------------------------
void StartChrono(void)
{
    ChronoTick = HAL_GetTick();
}

//----------------------------------------------------------------------
uint32_t StopChrono(void)
{
    return DeltaT_Calculation(ChronoTick);
}

//----------------------------------------------------------------------
int al_tools_write_uid(uint64_t uid_for_otp[2])
{
    int rc = 0;

    // uid will be written only if it as been configured
    if ( uid_for_otp[0] || uid_for_otp[1] )
    {
        rc = HAL_FLASH_Unlock();
        if ( rc )
        {
            return -1;
        }

        rc = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,
                               AL_TOOLS_UID_FLASH_ADDR_1,
                               uid_for_otp[0]);
        if ( rc )
        {
            HAL_FLASH_Lock();
            return -1;
        }


        rc = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,
                               AL_TOOLS_UID_FLASH_ADDR_2,
                               uid_for_otp[1]);
        if ( rc )
        {
            HAL_FLASH_Lock();
            return -1;
        }

        rc = HAL_FLASH_Lock();
        return rc;
    }

    return -1;
}

//----------------------------------------------------------------------
int al_tools_read_uid(uint64_t uid_from_otp[2])
{
    // sanity check
    if ( ! uid_from_otp )
    {
        return -1;
    }

    uid_from_otp[0] = *((uint64_t *)AL_TOOLS_UID_FLASH_ADDR_1);
    uid_from_otp[1] = *((uint64_t *)AL_TOOLS_UID_FLASH_ADDR_2);

    return 0;
}

//----------------------------------------------------------------------
float euclidian_norm(int size, float a[])
{
    float sum_square = 0;

    // sanity check
    if ( ! size || ! a )
    {
        return -1;
    }

    for(int i = 0; i < size; ++i)
    {
        sum_square += square(a[i]);
    }

    return sqrt(sum_square);
}

//----------------------------------------------------------------------
float al_tools_to_feet(float value)
{
    return (value * AL_TOOLS_METER_IN_FEET);
}

//-------------------------------------------------------------------
int al_tools_variance_update(al_tools_variance_t * existing_aggregate, float new_value)
{
    // sanity check
    if ( ! existing_aggregate )
    {
        return -1;
    }

    existing_aggregate->count += 1;
    float delta = new_value - existing_aggregate->mean;
    existing_aggregate->mean += delta / (float)existing_aggregate->count;
    float delta2 = new_value - existing_aggregate->mean;
    existing_aggregate->M2 += delta * delta2;

    return 0;
}

// returns sample variance from an aggregate
//-------------------------------------------------------------------
float al_tools_variance_finalize(al_tools_variance_t * existing_aggregate)
{
    // sanity check
    if ( ! existing_aggregate )
    {
        return -1;
    }

    return existing_aggregate->M2/(float)(existing_aggregate->count-1);
}

//-------------------------------------------------------------------
float al_tools_variance_get_mean(al_tools_variance_t * existing_aggregate)
{
    // sanity check
    if ( ! existing_aggregate )
    {
        return -1;
    }

    return existing_aggregate->mean;
}

