//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
// standard
#include <string.h>

// private
#include <trace.h>
#include "Inc/al_circbuf.h"

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------

/*
 * al_circbuf_push: This function write a new value into the circular
 * buffer
 */
int al_circbuf_push(al_circbuf_t * p , void * data)
{
    int next;
    uint8_t * data_addr = p->buffer;

    // sanity check
    if ( ! data || ! p )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // save next head position
    next = p->head + 1;

    // end of buffer, return to its start
    if ( next >= p->maxlen )
    {
        next = 0;
    }

    // is the circular buffer is full ?
    if ( next == p->tail )
    {
        // overwrite of buffer oldest entry
        p->tail = ((p->tail + 1) >= p->maxlen) ? 0 : p->tail + 1;
    }

    // copy data
    data_addr += (p->head * p->esize);
    memcpy(data_addr, (uint8_t *)data, p->esize);

    // head to next element
    p->head = next;

    return 0;
}

//----------------------------------------------------------------------
/*
 * al_circbuf_pop: This function allows to read and erase a value into the circular
 * buffer
 */
int al_circbuf_pop(al_circbuf_t * p, void * data)
{
    int next;
    uint8_t * data_addr = p->buffer;

    // sanity check
    if ( ! data || ! p )
    {
        return -1;
    }

    // is buffer empty ?
    if ( p->head == p->tail )
    {
        return -1;
    }

    // save last element position
    next = p->tail + 1;

    if(next >= p->maxlen)
    {
        next = 0;
    }

    // copy data from buffer
    data_addr += (p->tail * p->esize);
    memcpy((uint8_t *)data, data_addr, p->esize);

    // update last element postion
    p->tail = next;

    return 0;
}

//----------------------------------------------------------------------
int al_circbuf_available(al_circbuf_t *c)
{
    int available;

    available = c->tail - c->head;

    if ( available <= 0 )
    {
        available += c->maxlen;
    }

    // available space less extra empty element
    return available - 1;
}

