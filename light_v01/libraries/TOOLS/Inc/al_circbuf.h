#ifndef _AL_CIRCBUF_H
#define _AL_CIRCBUF_H

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdint.h>
#include <stddef.h>

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef struct _al_circbuf {
    int head;                   /**< first element of buffer    */
    int tail;                   /**< last element of buffer     */
    size_t esize;               /**< size of one element        */
    void * const buffer;        /**< actual buffer of element   */
    const int maxlen;           /**< maximum number of elements */
}al_circbuf_t;

//----------------------------------------------------------------------
// Macro
//----------------------------------------------------------------------
// one extra slot to check if the buffer is full or empty
#define AL_CIRCBUF_DEF(type, name, size)                \
    type _##name##_priv_data[size+1];                   \
    al_circbuf_t name = {                               \
            .head = 0,                                  \
            .tail = 0,                                  \
            .esize = sizeof(type),                      \
            .buffer = _##name##_priv_data,              \
            .maxlen = size+1                            \
    }

#define AL_CIRCBUF_FOREACH(item, cb)                                                \
    item = *((typeof(item) *)((uint8_t *)cb.buffer + (cb.esize * ((cb.tail) % (cb.maxlen - 1)))));\
    for ( int cursor = cb.tail;                                                    \
          cursor != cb.head;                                                       \
          item = *((typeof(item) *)((uint8_t *)cb.buffer + (cb.esize * ((++cursor) % (cb.maxlen - 1))))) \
        )

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
/**
  * @brief  Pop an element from the circular buffer
  *
  * @return 0 on success else error code
  **/
int al_circbuf_pop(al_circbuf_t * p, void * data);

/**
  * @brief  Push an element into the circular buffer
  *
  * @return 0 on success else error code
  **/
int al_circbuf_push(al_circbuf_t * p, void * data);

/**
  * @brief  return number of available slot remaining
  *
  * @return available space
  **/
int al_circbuf_available(al_circbuf_t *c);

#endif //   _AL_CIRCBUF_H
