#ifndef _AL_TOOLS_H
#define _AL_TOOLS_H

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <math.h>
#include <stdint.h>

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define AL_TOOLS_UID_FLASH_ADDR_1     0x1FFF7000
#define AL_TOOLS_UID_FLASH_ADDR_2     0x1FFF7008

//----------------------------------------------------------------------
// Macro
//----------------------------------------------------------------------
/*+-----------------------------------------------------------------------------------+
  |                                        SIZE = 6                                   |
  |                                   <----------------->                             |
  |TARGET                             +-----------------+                             |
  |31 30 29 28 27 26 25 24 23 22 21 20|19 18 17 16 15 14|13 12 11 9 8 7 6 5 4 3 2 1 0 |
  |                                   +-----------------+                             |
  |                                                    ^                              |
  |                                                    +---------+ OFFSET = 14        |
  +-----------------------------------------------------------------------------------+
*/
#define SETBIT(TARGET,OFFSET)                   (TARGET |=  (1<<OFFSET))
#define CLEARBIT(TARGET,OFFSET)                 (TARGET &= ~(1<<OFFSET))
#define FLIPBIT(TARGET,OFFSET)                  (TARGET ^=  (1<<OFFSET))
#define READBIT(TARGET,OFFSET)                  (((TARGET)>>(OFFSET)) & 1)
#define WRITEBIT(TARGET,OFFSET,VALUE)           (VALUE) ? SETBIT((TARGET),(OFFSET)) : CLEARBIT((TARGET),(OFFSET))

#define SETBITS32(TARGET,OFFSET,SIZE)           WRITEBITS32(TARGET,OFFSET,SIZE,(0xFFFFFFFF>>(32-(SIZE))))
#define CLEARBITS32(TARGET,OFFSET,SIZE)             ((TARGET)&=~( (0xFFFFFFFF>>(32-(SIZE)))<<(OFFSET)))
#define WRITEBITS32(TARGET,OFFSET,SIZE,VALUE)     CLEARBITS32(TARGET,OFFSET,SIZE);(TARGET)|=((VALUE)<<(OFFSET))
#define READBITS32(TARGET,OFFSET,SIZE)          (((TARGET)>>(OFFSET)) & (0xFFFFFFFF>>(32-(SIZE))))

#define SETBITS8(TARGET,OFFSET,SIZE)            WRITEBITS8(TARGET,OFFSET,SIZE,(0xFF>>(8-(SIZE))))
#define CLEARBITS8(TARGET,OFFSET,SIZE)          ((TARGET)&=~( (0xFF>>(8-(SIZE)))<<(OFFSET)))
#define WRITEBITS8(TARGET,OFFSET,SIZE,VALUE)     CLEARBITS8(TARGET,OFFSET,SIZE);(TARGET)|=((VALUE)<<(OFFSET))
#define READBITS8(TARGET,OFFSET,SIZE)           (((TARGET)>>(OFFSET)) & (0xFF>>(8-(SIZE))))

#define ISBETWEEN(x, a, b)  (((a) <= (x)) && ((x) <= (b)))
#define ISOUTSIDE(x, a, b)  (((x) < (a)) || ((b) < (x)))

#define COUNTOF(a)          (sizeof(a) / sizeof(a[0]))

//----------------------------------------------------------------------
// Inline functions
//----------------------------------------------------------------------
static inline float deg2rad(float angleDegrees)
{
    return ((angleDegrees) * 0.01745329251); // pi/180
}

//----------------------------------------------------------------------
static inline float rad2deg(float angleRadians)
{
    return ((angleRadians) * 57.2957795131); // 180/pi
}

//----------------------------------------------------------------------
static inline float signf(float x)
{
	return (x>=0?(1.0):(-1.0));
}

//----------------------------------------------------------------------
static inline float square(float x)
{
	return (x*x);
}

//----------------------------------------------------------------------
static inline float mbar2altitude(float pressure)
{
	return (((1-pow((pressure/1013.25),0.190284))*145366.45)*0.3048);
}

//-------------------------------------------------------------------
// Local types
//-------------------------------------------------------------------
typedef struct al_tools_variance_t
{
    int count;
    float mean;
    float M2;

} al_tools_variance_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
/**
 *  @brief  interrupt routine used by abstraction
 *          layer to increment tick
**/
void HAL_IncTick(void);

/**
 *  @brief  helper that return current tick
 *
 *  @return  tick value on uint32_t
**/
uint32_t HAL_GetTick(void);


/**
 * @brief This function provides minimum delay (in milliseconds) based
 *        on variable incremented.
 * @note In the default implementation , SysTick timer is the source of time base.
 *       It is used to generate interrupts at regular time intervals where uwTick
 *       is incremented.
 * @note This function is declared as __weak to be overwritten in case of other
 *       implementations in user file.
 * @param Delay  specifies the delay time length, in milliseconds.
 * @retval None
**/
void HAL_Delay(uint32_t Delay);

/**
 *  @brief      convert BCD format to integer value
 *
 *  @param[in]  BCD value
 *
 *  @return     converted value on unsigned 8bit
**/
uint8_t bcd2int(uint8_t bcd);

/**
 *  @brief      convert integer format to BCD value
 *
 *  @param[in]  integer value
 *
 *  @return     converted value on unsigned 8bit
**/
uint8_t int2bcd(uint8_t integer);

/**
 *  @brief      Calculate time elapsed since provided tick value
 *
 *  @param[in]  tick value to calculate delta with
 *
 *  @return     calculated elapsed time
**/
uint32_t DeltaT_Calculation(uint32_t TickStart);

/**
 *  @brief  Start a millisecond chronometer
**/
void StartChrono(void);

/**
 *  @brief  Stop the previously started chronometer
 *
 *  @return elapsed time in millisecond
**/
uint32_t StopChrono(void);

/**
 *  @brief  Write UID in OTP flash
 *
 *  @return 0 or system error code
**/
int al_tools_write_uid(uint64_t uid_for_otp[2]);

/**
 *  @brief  Read UID stored in OTP flash
 *
 *  @return 0 or system error code
**/
int al_tools_read_uid(uint64_t uid_from_otp[2]);

/**
 *  @brief  Compute euclidian norm of variable
 *          sized array
 *
 *  @return 0 or system error code
**/
float euclidian_norm(int size, float a[]);

/**
 *  @brief  Convert meters to feet
 *
 *  @param[in]  Distance in meters
 *
 *  @return Distance in feet
**/
float al_tools_to_feet(float value);


int al_tools_variance_update(al_tools_variance_t * existing_aggregate, float new_value);


float al_tools_variance_finalize(al_tools_variance_t * existing_aggregate);


float al_tools_variance_get_mean(al_tools_variance_t * existing_aggregate);


#endif // _AL_TOOLS_H

