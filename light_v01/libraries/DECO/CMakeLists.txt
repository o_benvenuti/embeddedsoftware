include_drivers(RTC)
include_libraries(TOOLS DATABASE TRACE)
include_freertos()

INCLUDE_DIRECTORIES (${CMAKE_CURRENT_SOURCE_DIR}/Inc)

ADD_LIBRARY (DECO algapi.c)