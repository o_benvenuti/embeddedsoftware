#ifndef ALGAPI_H
#define ALGAPI_H
#include <stdint.h>

/* parameters */
#define DECO_GRADIENT_FACTOR_HIGH 0.6
#define DECO_GRADIENT_FACTOR_LOW  0.4

#define Normal 0x0
#define NormalStop 0x1
#define Buff 0x2
#define BuffStop 0x3
#define DecoCal 0x4
#define DecoMgr 0x5
#define DecoStop 0x6
#define NormalExit 0x7
#define BuffExit 0x8
#define DecoExit 0x9

typedef struct DiveTime {
   unsigned int year;
   unsigned int month;
   unsigned int day;
   unsigned int hour;
   unsigned int minute;
   unsigned int second;
   int hour_offset;
   int minute_offset;
} DiveTime;

typedef struct DiveInfo {
   DiveTime current_time;						/* 1 - input  */
   DiveTime last_dive_start;                    /* 1 - input  */
   DiveTime last_dive_end;                      /* 1 - input  */
   unsigned int is_high_altitude;               /* 1 - input  */
   unsigned int is_trimix;                      /* 1 - input  */
   unsigned int * deco_status;					/* 0 - output */
   float atm_pressure;							/* 1 - input  */
   float oxygen_ratio;                          /* 1 - input  */
   float helium_ratio;                          /* 1 - input  */
   float safety_factor;                         /* 1 - input  */
   float gradient_factor_high;                  /* 1 - input  */
   float gradient_factor_low;                   /* 1 - input  */
   float * nitro_pressure;						/* 0 - output */
   float * nitro_pressure_limit_ratio;			/* 0 - output */
   float * helium_pressure;						/* 0 - output */
   float * helium_pressure_limit_ratio;			/* 0 - output */
   float * ndl;									/* 0 - output */
   float * cns_ratio;							/* 0 - output */
   float * ceiling_depth;						/* 0 - output */
   float * ceiling_stop;						/* 0 - output */
} DiveInfo;

extern void init_info_api(DiveInfo dive_info);

extern void update_info_api(DiveInfo dive_info, float amb_pressure, float time_interval);

char DECO_saveTissuePressureInFlash(void);
char DECO_getTissuePressureFromFlash(void);
int  deco_main(void);
void DECO_init_algo(DiveInfo * ptr, int32_t initial_pressure);
void DECO_debug(void);

#endif
