///*
// * algapi.c
// *
// *  Created on: 31 mai 2018
// *      Author: AixSonic
// */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <al_tools.h>
#include <al_database.h>
#include <trace.h>

#include <algapi.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define PRESSURE_ARRAY_SIZE 16

//----------------------------------------------------------------------
// Private variables
//----------------------------------------------------------------------
static float _algapi_nitro_pressure[PRESSURE_ARRAY_SIZE];
static float _algapi_nitro_pressure_limit[PRESSURE_ARRAY_SIZE];
static float _algapi_helium_pressure[PRESSURE_ARRAY_SIZE];
static float _algapi_helium_pressure_limit[PRESSURE_ARRAY_SIZE];

static float _algapi_ndl = 0;
static float _algapi_cns_ratio = 0;
static uint _algapi_deco_status = 0;
static float _algapi_ceiling_stop = 0;
static float _algapi_ceiling_depth = 0;

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
DiveTime TimeType2DiveTime(TimeType i)
{
	DiveTime o = {0};

	o.hour 	 =      bcd2int(i.Hours  );
	o.minute =      bcd2int(i.Minutes);
	o.second =      bcd2int(i.Seconds);
	o.day    =      bcd2int(i.Date   );
	o.month  =      bcd2int(i.Month  );
	o.year   = 2000+bcd2int(i.Year   );

	return o;
}
TimeType DiveTime2TimeType(DiveTime i)
{
	TimeType o = {0};

	o.Hours   = int2bcd(i.hour  );
	o.Minutes = int2bcd(i.minute);
	o.Seconds = int2bcd(i.second);
	o.Date    = int2bcd(i.day   );
	o.Month   = int2bcd(i.month );
	o.Year    = int2bcd(i.year - 2000);

	return o;
}
//----------------------------------------------------------------------
char DECO_saveTissuePressureInFlash(void)
{
	int rc = 0;

    rc = al_database_set_decoconf(_algapi_nitro_pressure,
                                  sizeof(_algapi_nitro_pressure),
                                  _algapi_nitro_pressure_limit,
                                  sizeof(_algapi_nitro_pressure_limit));
    if ( rc )
    {
        debug_printf("unable to retrieve algapi data\n");
    }

	return rc;
}

//----------------------------------------------------------------------
char DECO_getTissuePressureFromFlash(void)
{
    int rc = 0;
    size_t pressure_len = sizeof(_algapi_nitro_pressure);
    size_t limit_len = sizeof(_algapi_nitro_pressure_limit);

    rc = al_database_get_decoconf(_algapi_nitro_pressure, &pressure_len,
                                  _algapi_nitro_pressure_limit, &limit_len);
    if ( rc )
    {
        // this is not actually an error as it might only be the first dive
        memset(&_algapi_nitro_pressure[0], 0,
               sizeof(_algapi_nitro_pressure));
        memset(&_algapi_nitro_pressure_limit[0], 0,
               sizeof(_algapi_nitro_pressure_limit));
    }
	return 0;
}

//----------------------------------------------------------------------
void DECO_init_algo(DiveInfo * ptr, int32_t initial_pressure)
{
    int rc = 0;
    TimeType tmpStartTime     = {0};
    TimeType tmpStopTime      = {0};
    DiveTime current_time     = {2017,12,31,12,0,0,0,0};
    struct tm last_dive_end   = {0};
    struct tm last_dive_start = {0};
    int dive_time_sec         =  0;
    int inter_dive_time_sec   =  0;
    al_database_diverec_config_t config;

    // retrieve algo's previous data
    DECO_getTissuePressureFromFlash();

    // try to get last dive info
    do
    {
        // retrieve last dive time
        rc = al_database_diverec_latest(&tmpStartTime);
        if ( rc ) { break; }

        // open the dive in order to get end time
        rc = al_database_diverec_open(tmpStartTime);
        if ( rc ) { break; }

        // request for last dive end time
        config.type = AL_DATABASE_DIVEREC_TYPE_DIVEND;
        rc = al_database_diverec_get(&config);
        if ( rc ) { break; }

        // assign dive end date, close and continue
        tmpStopTime = config.data.dive_end;
        al_database_diverec_close();
    } while ( 0 );

    // set fake old data if there is any error
    if ( rc )
    {
        // set default value
        tmpStartTime.Date  = 0x01;
        tmpStartTime.Month = 0x01;
        tmpStartTime.Year  = 0x10;
        tmpStopTime = tmpStartTime;
    }

    // important
    // the api does not work after 2018 - we need to trick it to make it work
    // current time is always set to 2017-12-31-00-00

    //get diff time between start & stop to get dive duration
    dive_time_sec = get_TimeDiffInSeconds(tmpStartTime,tmpStopTime);
    //get interval in sec between today and last dive
    inter_dive_time_sec = get_TimeDiffInSeconds(tmpStopTime,RTC_GetTime());

    //substract time interval to get last dive end relative to the fake current time
    last_dive_end = TimeType2tm(DiveTime2TimeType(current_time));
    last_dive_end.tm_sec -= inter_dive_time_sec;
    //normalize substracted time
    mktime(&last_dive_end);

    //substract time interval to get last dive start relative to last dive end
    last_dive_start = TimeType2tm(DiveTime2TimeType(current_time));
    last_dive_start.tm_sec -= (inter_dive_time_sec + dive_time_sec);
    //normalize substracted time
    mktime(&last_dive_start);

    ptr->current_time                = current_time;
    ptr->last_dive_start             = TimeType2DiveTime(tm2TimeType(last_dive_start));
    ptr->last_dive_end               = TimeType2DiveTime(tm2TimeType(last_dive_end));

    if(mbar2altitude(initial_pressure/100.0) > 300)
    {
        ptr->is_high_altitude        = 1; /* altitude > 300m */
    }
    else
    {
        ptr->is_high_altitude        = 0; /* altitude < 300m */
    }

    ptr->is_trimix                   = 0;
    ptr->atm_pressure                = (initial_pressure / 100.0);// + 100; // +100 mbar to trick algo
    ptr->gradient_factor_high        = DECO_GRADIENT_FACTOR_HIGH;
    ptr->gradient_factor_low         = DECO_GRADIENT_FACTOR_LOW;

    ptr->nitro_pressure              = _algapi_nitro_pressure;
    ptr->helium_pressure             = _algapi_helium_pressure;
    ptr->nitro_pressure_limit_ratio  = _algapi_nitro_pressure_limit;
    ptr->helium_pressure_limit_ratio = _algapi_helium_pressure_limit;

    ptr->deco_status                 = &_algapi_deco_status;
    ptr->ndl                         = &_algapi_ndl             ;
    ptr->cns_ratio                   = &_algapi_cns_ratio           ;
    ptr->ceiling_depth               = &_algapi_ceiling_depth       ;
    ptr->ceiling_stop                = &_algapi_ceiling_stop        ;

    init_info_api(*ptr);
}
