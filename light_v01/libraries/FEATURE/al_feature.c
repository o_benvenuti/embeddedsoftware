/*
 * al_feature.c
 *
 *  Created on: 25 sept. 2018
 *      Author: user
 */

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <al_feature.h>
#include <al_tools.h>
#include <stdbool.h>
#include <trace.h>

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define TASK_QUEUE_SIZE         4
#define FEATURE_TASK_STACK      1536
#define FEATURE_DEFAULT_TIMEOUT (1000 * 1800)  // 1000ms x 1800 = 30min
#define FEATURE_WATCHDOG_PERIOD (2 * FEATURE_DEFAULT_TIMEOUT)

//----------------------------------------------------------------------
// Variables
//----------------------------------------------------------------------
extern al_feature_ctx_t * serenity_features[];

static osThreadId _current_task;

static QueueHandle_t feat_msg_queue;
static QueueHandle_t return_msg_queue;

static osStaticThreadDef_t __attribute__((__section__("_edata"))) _feature_task_control_block;
static StackType_t __attribute__((__section__("_edata"))) _feature_task_stack[FEATURE_TASK_STACK];

static osStaticMessageQDef_t _feature_msg_queue_hdl;
static osStaticMessageQDef_t _feature_return_queue_hdl;
static osStaticMessageQDef_t _feature_queue_set_hdl;

static void * _feature_queue_set_buffer[8];
static uint32_t _feature_return_queue_buf[TASK_QUEUE_SIZE];
static al_feature_msg_t _feature_msg_queue_buf[TASK_QUEUE_SIZE];

static TimerHandle_t _watchdog_hdl;
static StaticTimer_t _feature_watchdog_timer;

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
void _feature_thread_func(void const * argument);
static void _watchdog_kicker(TimerHandle_t xTimer);

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int al_feature_init(void)
{
    int rc;
    _current_task = NULL;

    // receive message queue
    return_msg_queue = xQueueCreateStatic(TASK_QUEUE_SIZE, sizeof(uint32_t),
                                          (uint8_t*)&_feature_return_queue_buf[0],
                                          &_feature_return_queue_hdl);
    if ( return_msg_queue == NULL )
    {
        debug_printf("unable to create message queue\n");
        return -1;
    }

    // send message queue
    feat_msg_queue = xQueueCreateStatic(TASK_QUEUE_SIZE,
                                        sizeof(al_feature_msg_t),
                                        (uint8_t*)&_feature_msg_queue_buf[0],
                                        &_feature_msg_queue_hdl);
    if ( feat_msg_queue == NULL )
    {
        debug_printf("unable to create message queue\n");
        return -1;
    }

    xQueueReset(return_msg_queue);
    xQueueReset(feat_msg_queue);

    // create timer that check for standby
    _watchdog_hdl = xTimerCreateStatic("Feature watchdog",
                                       FEATURE_WATCHDOG_PERIOD,
                                       pdTRUE, 0,
                                       _watchdog_kicker,
                                       &_feature_watchdog_timer);
    if ( ! _watchdog_hdl )
    {
        debug_printf("error while creating feature watchdog\n");
        return -1;
    }

    rc = xTimerStart(_watchdog_hdl, 0);
    if ( pdPASS != rc )
    {
        debug_printf("error while starting watchdog timer\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_feature_send_to_tsk(al_feature_msg_t * msg)
{
    int rc;

    // TODO number of attempt and restart the task
    rc = uxQueueSpacesAvailable(feat_msg_queue);
    if ( rc == 0 )
    {
        debug_printf("feature queue full\n");
        return -1;
    }

    // send message to task
    rc = xQueueSend(feat_msg_queue, msg, 0);
    if ( rc != pdPASS )
    {
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_feature_get_return_state(void)
{
    int rc, state;

    // read message queue
    rc = xQueueReceive(return_msg_queue, &state, 0);
    if ( rc != pdPASS ) {
        return -1;
    }

    return state;
}

//----------------------------------------------------------------------
int al_feature_pause_watchdog(void)
{
    int rc;

    // stop the timer
    rc = xTimerStop(_watchdog_hdl, 0);
    if ( rc != pdPASS )
    {
        debug_printf("unable to pause watchdog");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_feature_restart_watchdog(void)
{
    int rc;

    // kick the timer prior to restart it
    xTimerReset(_watchdog_hdl, 0);

    // start the timer
    rc = xTimerStart(_watchdog_hdl, 0);
    if ( rc != pdPASS )
    {
        debug_printf("unable to restart watchdog");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_feature_start(int prev, int next)
{
    if ( ! _current_task )
    {
        serenity_features[next]->parent = prev;

        // start the new task
        xQueueReset(return_msg_queue);
        xQueueReset(feat_msg_queue);

        osThreadStaticDef(feature_task,
                          _feature_thread_func,
                          osPriorityNormal,
                          0,
                          FEATURE_TASK_STACK,
                          _feature_task_stack,
                          &_feature_task_control_block);
        _current_task = osThreadCreate(osThread(feature_task),
                                       serenity_features[next]);
        if ( ! _current_task ) {
            debug_printf("OOM condition\n");

            // unexpected error: restart
            NVIC_SystemReset();
        }
    }
    else
    {
        debug_printf("Next state does not provide a valid task\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_feature_stop(void)
{
    // end current task
    osThreadTerminate(_current_task);

    _current_task = NULL;

    return 0;

}

//----------------------------------------------------------------------
int al_feature_get_queue(QueueHandle_t * queue)
{
    // sanity check
    if ( ! queue )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    *queue = return_msg_queue;
    return 0;
}

//----------------------------------------------------------------------
void _feature_thread_func(void const * argument)
{
    int return_state = 0;
	int rc = 0;
    unsigned int timeout = FEATURE_DEFAULT_TIMEOUT;
    bool exit = false;
    QueueSetHandle_t queue_set;
    al_feature_msg_t rcv_message;
    QueueHandle_t available_queue;
    uint32_t tick_last = 0, time_to_wait = 0;

    // sanity check
    al_feature_ctx_t * ctx = (al_feature_ctx_t*)argument;
    if ( ! ctx )
    {
        debug_printf("Invalid argument\n");
    }
    return_state = ctx->parent;

    // create the communication object
    queue_set = xQueueCreateSetStatic(8, (uint8_t*)&_feature_queue_set_buffer[0],
                                         &_feature_queue_set_hdl);

    xQueueReset(feat_msg_queue);
    rc = xQueueAddToSet(feat_msg_queue, queue_set );
    if ( pdPASS != rc )
    {
        debug_printf("cannot add task_queue to set\n");

        // here we could restart the system properly
        // but till we manage that: reset
        NVIC_SystemReset();
    }

    debug_printf(">>> FEATURE %s STARTED <<<\n", ctx->name);

    // feature thread process initialisation
    if ( ctx->ini_func )
    {
        rc = ctx->ini_func(&(ctx->priv_data), ctx->parent);
        if ( rc )
        {
            debug_printf("thread init callback error %d\n", rc);
            exit = true;
        }
    }

    while( exit != true )
    {
        // check how much time we have to wait (still)
        if ( DeltaT_Calculation(tick_last) > timeout )
        {
            time_to_wait = 0;
        }
        else
        {
            time_to_wait = timeout - DeltaT_Calculation(tick_last);
        }

        // wait on any possible event on queues
        available_queue = (QueueHandle_t) xQueueSelectFromSet(queue_set, time_to_wait);

        // update last loop timestamp
        tick_last = HAL_GetTick();

        // Very important to kick the timer here
        xTimerReset(_watchdog_hdl, 0);

        // timeout event
        if ( ! available_queue )
        {
            // feature thread process timer
            if ( ctx->tmr_func )
            {
                rc = ctx->tmr_func(ctx->priv_data, timeout);
                if ( rc )
                {
                    // on error return to previous state
                    debug_printf("thread timer callback error %d\n", rc);
                    return_state = ctx->parent;
                    exit = true;
                }
            }
        }

        // received message from manager task
        if ( available_queue == feat_msg_queue )
        {
            rc = xQueueReceive(available_queue, &rcv_message, 0);
            if ( rc != pdPASS )
            {
                debug_printf("error while getting message\n");
            }

            // process request locally or send to features
            switch ( rcv_message.cmd )
            {
                // if request is to change the timeout
                case  CMD_CHG_TIMER:
                {
                    // check cast from unsigned to signed
                    if ( (int)rcv_message.data.value < 0 )
                    {
                        debug_printf("timeout value too big\n");
                        break;
                    }

                    // assign new timeout
                    timeout = (int)rcv_message.data.value;
                    break;
                }
                // if request is end of task
                case CMD_END_STATE:
                {
                    // get next feature to execute
                    return_state = rcv_message.data.value;
                    exit = true;
                    break;
                }
                // if request is user input
                case  CMD_USR_INPUT:
                {
                    // does feature provide a callback ?
                    if ( ! ctx->usr_func )
                    {
                        break;
                    }

                    // feature thread process user input
                    rc = ctx->usr_func(ctx->priv_data, rcv_message.data.gesture);
                    if ( rc )
                    {
                        debug_printf("thread user callback error %d\n", rc);
                        return_state = ctx->parent;
                        exit = true;
                    }
                    break;
                }
                // if request is input from GUI
                case  CMD_GUI_INPUT:
                {
                    // check if there is a callback
                    if ( ! ctx->gui_func )
                    {
                        break;
                    }

                    // feature thread process gui input
                    rc = ctx->gui_func(ctx->priv_data, rcv_message.data.value);
                    if ( rc )
                    {
                        debug_printf("thread user callback error %d\n", rc);
                        return_state = ctx->parent;
                        exit = true;
                    }
                    break;
                }
                default:
                {
                    // feature thread process event callback
                    if ( ctx->evt_func )
                    {
                        rc = ctx->evt_func(ctx->priv_data, rcv_message.cmd);
                        if ( rc )
                        {
                            debug_printf("thread event callback error %d\n", rc);
                            return_state = ctx->parent;
                            exit = true;
                        }
                        break;
                    }
                }
            }
        }
    }

    // feature private release function
    if ( ctx->rls_func )
    {
        rc = ctx->rls_func(ctx->priv_data);
        if ( rc )
        {
            debug_printf("thread release callback error %d\n", rc);
        }
    }

    debug_printf(">>> FEATURE %s END <<<\n", ctx->name);

    // delete task's objects
    xQueueReset(feat_msg_queue);
    xQueueRemoveFromSet(feat_msg_queue, queue_set);
    vQueueDelete(queue_set);

    // notify exit with next status
    rc = xQueueSend(return_msg_queue, &return_state, 0);
    if ( rc != pdPASS )
    {
        debug_printf("cannot send next state\n");
    }

    // wait forever to be deleted
    vTaskSuspend(NULL);
}

//----------------------------------------------------------------------
static void _watchdog_kicker(TimerHandle_t xTimer)
{
    (void)xTimer;

    debug_printf("FEATURE MUST BE RESTARTED\n");
    NVIC_SystemReset();
}

