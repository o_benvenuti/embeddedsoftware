/*
 * serenity_features.h
 *
 *  Created on: 24 sept. 2018
 *      Author: user
 */

#ifndef _AL_FEATURES_H_
#define _AL_FEATURES_H_
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <cmsis_os.h>
#include <touchscreen.h>

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define FEATURE_NAME_LEN    32

//----------------------------------------------------------------------
// Types
//----------------------------------------------------------------------
typedef enum _al_feature_cmd_t {
    CMD_BAT_CHRGE,
    CMD_DVE_START,
    CMD_RCV_FLICK,
    CMD_CHG_TIMER,
    CMD_USR_INPUT,
    CMD_GUI_INPUT,
    CMD_END_STATE,
}al_feature_cmd_t;

typedef struct _al_feature_msg_t {
    al_feature_cmd_t cmd;
    union {
        uint32_t            value;
        gestures_t          gesture;
    }data;
}al_feature_msg_t;

typedef struct _al_feature_ctx_t {
    int parent;
    void * priv_data;
    char name[FEATURE_NAME_LEN];
    int (* ini_func)(void ** priv_data, int parent);
    int (* rls_func)(void * priv_data);
    int (* tmr_func)(void * priv_data, int time);
    int (* gui_func)(void * priv_data, uint32_t button);
    int (* usr_func)(void * priv_data, gestures_t gesture);
    int (* evt_func)(void * priv_data, al_feature_cmd_t cmd);
}al_feature_ctx_t;

//----------------------------------------------------------------------
// API
//----------------------------------------------------------------------
int al_feature_init(void);
int al_feature_exit(int state);
int al_feature_send_to_tsk(al_feature_msg_t * msg);
int al_feature_get_return_state(void);
int al_feature_pause_watchdog(void);
int al_feature_restart_watchdog(void);
int al_feature_start(int prev, int next);
int al_feature_stop(void);
int al_feature_get_queue(QueueHandle_t * queue);

#endif /* _AL_FEATURES_H_ */
