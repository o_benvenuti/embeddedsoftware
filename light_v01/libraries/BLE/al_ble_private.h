#ifndef _AL_BLE_PRIVATE_H_
#define _AL_BLE_PRIVATE_H_
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include "bm71/bm71.h"

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define MAX_STR_LEN	128

//----------------------------------------------------------------------
// Typedef
//----------------------------------------------------------------------
typedef struct _al_ble_ctx_t
{
    char name[MAX_STR_LEN];
    struct {
        int (*init)();
        int (*deinit)();
        int (*listen)();
        int (*get_status)();
        int (*set_scanning)();
        int (*set_broadcast)(uint8_t *, uint16_t);
        int (*get_notif)(uint8_t *, uint8_t *, size_t *);
        int (*set_notif)(uint16_t, uint8_t *, uint16_t);
    }func;
}al_ble_ctx_t;

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
al_ble_ctx_t ble_available_ctx[] =
{
        {
                .name = "bm71",
                .func = {
                        .init = bm71_init,
                        .deinit = bm71_deinit,
                        .listen = bm71_listen,
                        .get_status = bm71_get_status,
                        .get_notif = bm71_get_notification,
                        .set_notif = bm71_set_notification,
                        .set_scanning = bm71_set_scanning,
                        .set_broadcast = bm71_set_broadcast,
                },
        }
};
#endif /* _AL_BLE_PRIVATE_H_ */
