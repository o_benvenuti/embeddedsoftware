/**
 * @file:  al_ble.h
 *
 * @brief
 *
 * @date    14/05/2019
 *
 * @description Library providing functions communicate through
 *              Bluetooth Low Energy medium
 *
 * @platform    platform agnostic
 *
 */
#ifndef _AL_BLE_H_
#define _AL_BLE_H_

//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define AL_BLE_NOTIF_TYPE_REQUEST_CHAR          0x98
#define AL_BLE_NOTIF_TYPE_STATUS_REPORT         0x81

#define AL_BLE_CONNECTION_HANDLE                0x80
#define AL_BLE_CHAR_HANDLE_TRANSP_UART          0x0053
#define AL_BLE_CHAR_HANDLE_APP_UUID             0xB3C3
#define AL_BLE_CHAR_HANDLE_APP_RECORD           0xB4C4
#define AL_BLE_CHAR_HANDLE_APP_CONFIG           0xB6C6
#define AL_BLE_CHAR_HANDLE_FWOTA_TYPE           0x8002
#define AL_BLE_CHAR_HANDLE_FWOTA_SIZE           0x8007
#define AL_BLE_CHAR_HANDLE_FWOTA_FW_VERSION     0x8004
#define AL_BLE_CHAR_HANDLE_FWOTA_NOTIFY_1       0x8005
#define AL_BLE_CHAR_HANDLE_FWOTA_NOTIFY_2       0x800A
#define AL_BLE_CHAR_HANDLE_FWOTA_RX             0x8007
#define AL_BLE_CHAR_HANDLE_FWOTA_TX             0x8009

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
/**
  * @brief  Initialise device and abstraction layer
  *
  * @retval 0 or system error code
  **/
int al_ble_init(void);

/**
  * @brief  Enable advertisement and listen
  *         for subscription and connection
  *
  * @retval 0 or system error code
  **/
int al_ble_listen(void);

/**
  * @brief  Enable scanning mode and listen
  *         for neighbourhood advertisement
  *
  * @retval 0 or system error code
  **/
int al_ble_set_scanning(void);

/**
  * @brief  Broadcast provided buffer in
  *         non connectable mode
  *
  * @param[in]  buffer  the data to broadcast
  * @param[in]  length  length of data to broadcast
  *
  * @retval 0 or system error code
  **/
int al_ble_set_broadcast(uint8_t * buffer, uint16_t length);

/**
  * @brief  To be implemented
  *
  * @retval 0 or system error code
  **/
int al_ble_get_status(uint8_t * status);

/**
  * @brief  Abstraction useful only for current usage
  *         of BLE devices. Return message received
  *         from ble device.
  *
  * @param[out] command type of notification received
  * @param[out] value   buffer received with the command
  * @param[out] len     size of the received buffer
  *
  * @retval 0 or system error code
  **/
int al_ble_get_notification(uint8_t * command, uint8_t * value, size_t * len);

/**
  * @brief  Abstraction useful only for current usage
  *         of BLE devices. Send message through ble device.
  *
  * @param[in] command type of notification to be sent
  * @param[in] value   buffer to send with the command
  * @param[in] len     size of the buffer to send
  *
  * @retval 0 or system error code
  **/
int al_ble_set_notification(uint16_t command, uint8_t * value, uint16_t len);

/**
  * @brief  Release device and make library available
  *
  * @retval 0 or system error code
  **/
int al_ble_deinit(void);

#endif /* _AL_BLE_H_ */
