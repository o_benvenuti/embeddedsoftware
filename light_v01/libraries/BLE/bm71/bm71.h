#ifndef _BM71_BM71_TMP_H_
#define _BM71_BM71_TMP_H_

//----------------------------------------------------------------------
// PUBLIC API
//----------------------------------------------------------------------
#include <stdint.h>
#include <stddef.h>

//----------------------------------------------------------------------
// PUBLIC API
//----------------------------------------------------------------------
int bm71_init(void);
int bm71_deinit(void);
int bm71_listen(void);
int bm71_set_broadcast(uint8_t * broadbuffer, uint16_t length);
int bm71_set_scanning(void);
int bm71_get_status(uint8_t * status);
int bm71_get_notification(uint8_t * char_hdl, uint8_t * value, size_t * len);
int bm71_set_notification(uint16_t char_hdl, uint8_t * value, uint16_t len);


#endif /* _BM71_BM71_TMP_H_ */
