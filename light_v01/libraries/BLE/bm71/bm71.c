//----------------------------------------------------------------------
// INCLUDES
//----------------------------------------------------------------------
#include <string.h>
#include <stdbool.h>
#include <stm32l4xx_hal.h>
#include <usart.h>
#include <main.h>

// internals
#include <al_circbuf.h>
#include <trace.h>

#include "definitions.h"
#include "bm71.h"
#include "bm71_configure.h"

//----------------------------------------------------------------------
// Defines
//----------------------------------------------------------------------
#define	BM71_UART_SYNC_HDR      0xAA
#define	BM71_UART_SYNC_LEN      1
#define	BM71_UART_LENGTH_LEN    2
#define	BM71_UART_BUF_SIZE      1024
#define BM71_UART_MAX_TRY       4
#define	BM71_UART_CRC_LEN       1
#define BM71_UART_PAYLOAD_LEN   42

//----------------------------------------------------------------------
// Typedef
//----------------------------------------------------------------------
typedef struct _bm71_msg {
    uint8_t type;
    uint8_t data[BM71_UART_PAYLOAD_LEN];       // max packet size
    uint16_t length;
}bm71_msg_t;

//----------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------
static uint8_t _bm71_dma_buffer[BM71_UART_BUF_SIZE] = { 0 };

// message FIFO
AL_CIRCBUF_DEF(bm71_msg_t, _bm71_msg_fifo, 32);

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
void _bm71_process_data(uint8_t * const buf, size_t len);

//----------------------------------------------------------------------
// PRIVATE API
//----------------------------------------------------------------------
int8_t bmResetSoft(void)
{
    int rc = 0;
    uint8_t ack[6] = { 0xaa, 0x00, 0x02, 0x81, 0x09, 0x74 };
    uint8_t buffer[6];
    uint8_t checksum = 0;       //Initial value of checksum

    buffer[0] = 0xaa;         //Load start byte into the transmit buffer
    buffer[1] = 0x00;         //Load high byte of length into the transmit buffer
    checksum -= buffer[1];    //Do running checksum calculation
    buffer[2] = 0x01;         //Load low byte of length into the transmit buffer
    checksum -= buffer[2];    //Do running checksum calculation
    buffer[3] = 0x02;         //Load opcode into the transmit buffer
    checksum -= buffer[3];    //Do running checksum calculation
    buffer[4] = checksum;     //Load the checksum

    __HAL_UART_FLUSH_DRREGISTER(&huart1);

    //Start transmitting the bytes
    rc = HAL_UART_Transmit(&huart1, &buffer[0], 5, 100);
    if ( rc )
    {
        debug_printf("transmit reset command failed\n");
        return -1;
    }

    rc = HAL_UART_Receive(&huart1, &buffer[0], 6, 100);
    if ( rc )
    {
        debug_printf("do not receive acknowledge\n");
        return -1;
    }

    if ( memcmp(&ack[0], &buffer[0], 6) )
    {
        debug_printf("bm71 acknowledged error\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int bmSetAdvEnable(void)
{
    int rc = 0;
    uint8_t checksum = 0;
    uint8_t buffer[8];
    uint8_t ack_1[7] = { 0xaa, 0x00, 0x03, 0x80, 0x1C, 0x00, 0x61 };
    uint8_t ack_2[6] = { 0xaa, 0x00, 0x02, 0x81, 0x03, 0x7A };
    uint16_t length = 0x0002;

    buffer[0] = 0xaa;                                  //Load start byte into the transmit buffer
    buffer[1] = (uint8_t)(length >> 8);                //Calculate high byte of length
    checksum -= buffer[1];                             //Do running checksum calculation
    buffer[2] = (uint8_t)(length & 0xff);              //Calculate low byte of length
    checksum -= buffer[2];                             //Do running checksum calculation
    buffer[3] = 0x1C;                                  //Load opcode into the transmit buffer
    checksum -= buffer[3];                             //Do running checksum calculation
    buffer[4] = 0x01;                                  //Load opcode into the transmit buffer
    checksum -= buffer[4];                             //Do running checksum calculation
    buffer[5] = checksum;                              //Load the checksum

    __HAL_UART_FLUSH_DRREGISTER(&huart1);

    //Start transmitting the bytes
    rc = HAL_UART_Transmit(&huart1, &buffer[0], 6, 100);
    if ( rc )
    {
        debug_printf("transmit advertisement enable command failed\n");
        return -1;
    }

    rc = HAL_UART_Receive(&huart1, &buffer[0], 7, 100);
    if ( rc )
    {
        debug_printf("do not receive acknowledge\n");
        return -1;
    }

    if ( memcmp(&ack_1[0], &buffer[0], 7) )
    {
        debug_printf("bm71 acknowledged error\n");
        return -1;
    }

    rc = HAL_UART_Receive(&huart1, &buffer[0], 6, 100);
    if ( rc )
    {
        debug_printf("do not receive acknowledge\n");
        return -1;
    }

    if ( memcmp(&ack_2[0], &buffer[0], 6) )
    {
        debug_printf("bm71 acknowledged error\n");
        return -1;
    }

    return rc;
}

//----------------------------------------------------------------------
int bm71_set_broadcast(uint8_t * broadbuffer, uint16_t length)
{
    int ret = 0, idx = 0;
    size_t lengthtot;
    uint8_t checksum = 0, buffer[32];

    //Check validity of pointer
    if ( ! broadbuffer )
    {
        debug_printf("Null pointer broadbuffer\n");
        return -1;
    }

    //buffer length check
    if( length > 33 )
    {
        debug_printf("Broadcast buffer too long\n");
        return -1;
    }

    lengthtot = length + 2;                     // 2 bytes of control
    buffer[idx++] = 0xaa;
    buffer[idx] = (uint8_t)(lengthtot >> 8);    // size msb
    checksum -= buffer[idx++];
    buffer[idx] = (uint8_t)(lengthtot & 0xff);  // size lsb
    checksum -= buffer[idx++];
    buffer[idx] = 0x11;                         // command
    checksum -= buffer[idx++];
    buffer[idx] = 0x0;                          // no flags
    checksum -= buffer[idx++];

    for ( int i = 0; i < length; i++ )
    {
        buffer[idx] = broadbuffer[i];
        checksum -= buffer[idx];
        idx++;
    }

    // add checksum to buffer
    buffer[idx++] = checksum;

    //Start transmitting the bytes
    ret = HAL_UART_Transmit(&huart1, &buffer[0], idx, 100);
    if ( ret )
    {
        debug_printf("unable to send data\n");
        return -1;
    }

    // receive acknowledge
    ret  = HAL_UART_Receive(&huart1, &buffer[0], 7, 100);
    if ( ret )
    {
        debug_printf("unable to read characteristic: no ack\n");
        return -1;
    }

    // verify acknowledge
    if ( buffer[3] != 0x80 || buffer[4] != 0x11 || buffer[5] != 0x00 )
    {
        debug_printf("advertisement data error 0x%02X\n", buffer[5]);
        return -1;
    }

    // set advert param
    checksum = idx = 0;
    buffer[idx++] = 0xaa;             // sync byte
    buffer[idx] = 0x00;             // size msb
    checksum -= buffer[idx++];
    buffer[idx] = 0x0B;             // size lsb
    checksum -= buffer[idx++];
    buffer[idx] = 0x13;             // command
    checksum -= buffer[idx++];
    buffer[idx] = 0x00;             // Advertising interval msb (100ms)
    checksum -= buffer[idx++];
    buffer[idx] = 0xA0;             // Advertising interval lsb (100ms)
    checksum -= buffer[idx++];
    buffer[idx] = 0x03;             // not connectable (broadcast mode)
    checksum -= buffer[idx++];
    buffer[idx] = 0x00;             // public device address
    checksum -= buffer[idx++];
    buffer[idx] = 0x00;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx] = 0x00;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx] = 0x50;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx] = 0x67;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx] = 0x11;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx] = 0x00;             // for direct advert (unused)
    checksum -= buffer[idx++];
    buffer[idx++] = checksum;

    //Start transmitting the bytes
    ret = HAL_UART_Transmit(&huart1, &buffer[0], idx, 100);
    if ( ret )
    {
        debug_printf("unable to send data\n");
    }

    // receive acknowledge
    ret  = HAL_UART_Receive(&huart1, &buffer[0], 7, 100);
    if ( ret )
    {
        debug_printf("unable to write characteristic: no ack\n");
        return ret;
    }

    // verify acknowledge
    if ( buffer[3] != 0x80 || buffer[4] != 0x13 || buffer[5] != 0x00 )
    {
        debug_printf("params advertisement error 0x%02X\n", buffer[5]);
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int bm71_set_scanning(void)
{
    int ret, idx =0;
    uint8_t checksum = 0, buffer[32];
    uint8_t ack[6] = { 0xAA, 0x00, 0x03, 0x80, 0x16, 0x00 };

    // 13 data
    buffer[idx++] = 0xaa;
    buffer[idx] = 0x00;
    checksum -= buffer[idx++];
    buffer[idx] = 0x03;
    checksum -= buffer[idx++];
    buffer[idx] = 0x16;             // scan enable command
    checksum -= buffer[idx++];
    buffer[idx] = 0x01;             // enable scan
    checksum -= buffer[idx++];
    buffer[idx] = 0x01;             // enable duplicate filtering
    checksum -= buffer[idx++];
    buffer[idx++] = checksum;

    ret = HAL_UART_Transmit(&huart1, buffer, idx, 100); //Start transmitting the bytes
    if ( ret )
    {
        debug_printf("unable to send data\n");
    }

    // receive status report
    ret  = HAL_UART_Receive(&huart1, &buffer[0], 6, 100);
    if ( ret )
    {
        debug_printf("unable to write characteristic: no ack\n");
        return ret;
    }

    // receive acknowledge
    ret  = HAL_UART_Receive(&huart1, &buffer[0], 7, 100);
    if ( ret )
    {
        debug_printf("unable to write characteristic: no ack\n");
        return ret;
    }

    // verify acknowledge
    if ( memcmp(&buffer[0], &ack[0], 6) )
    {
        debug_printf("data transmit acknowledged error 0x%02X\n", buffer[6]);
        return -1;
    }

    // disable unused interupt
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_WUF | UART_IT_PE);
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE | UART_IT_TC | UART_IT_RXNE);
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_LBD | UART_IT_CTS | UART_IT_CM);

    // clear those interrupts flags
    __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_PEF | UART_CLEAR_FEF);
    __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_NEF | UART_CLEAR_OREF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_TCCF | USART_ICR_LBDCF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_CTSCF | USART_ICR_CMCF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_WUCF);

    // enable idle line interrupt
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

    ret = HAL_UART_Receive_DMA(&huart1, &_bm71_dma_buffer[0], BM71_UART_BUF_SIZE);
    if ( ret )
    {
        debug_printf("unable to start DMA reception\n");
    }

    return 0;
}

//----------------------------------------------------------------------
static void _bm71_parse_uart(void)
{
    size_t pos;
    static size_t old_pos = 0;

    // calculate current position in buffer
    pos = sizeof(_bm71_dma_buffer) - __HAL_DMA_GET_COUNTER(huart1.hdmarx);

    // check change in received data
    if ( pos != old_pos )
    {
        // current position is over previous one
        if ( pos > old_pos )
        {
            // we are in "linear" mode then process
            // data directly by subtracting pointers
            _bm71_process_data(&_bm71_dma_buffer[old_pos], pos - old_pos);
        }
        else
        {
            // we are in "overflow" mode then first
            // process data to the end of buffer
            _bm71_process_data(&_bm71_dma_buffer[old_pos],
                               sizeof(_bm71_dma_buffer) - old_pos);

            // check and continue with beginning of buffer
            if ( pos )
            {
                _bm71_process_data(&_bm71_dma_buffer[0], pos);
            }
        }
    }

    // save current position as old
    old_pos = pos;

    // check and manually update if we reached end of buffer
    if ( old_pos == sizeof(_bm71_dma_buffer) )
    {
        old_pos = 0;
    }
}

//----------------------------------------------------------------------
int _bm71_validate_checksum(bm71_msg_t * p, uint8_t checksum)
{
    uint8_t calculated = 0;

    // sanity check
    if ( ! p )
    {
        return -1;
    }

    // compute header part checksum (add 1 to
    // length as type was a part of payload)
    calculated -= (uint8_t)((p->length+1) >> 8);
    calculated -= (uint8_t)((p->length+1) & 0xff);
    calculated -= p->type;

    for ( int i = 0; i < p->length; ++i )
    {
        calculated -= p->data[i];
    }

    if ( calculated != checksum )
    {
        debug_printf("message invalid checksum: discard\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
void _bm71_process_data(uint8_t * const buf, size_t len)
{
    int rc = 0;
    uint8_t * p = buf;
    uint8_t * const end = buf + len;

    // using this variable to notify that we are constructing a message
    // it is also used to save the current position in message construction
    static int msg_idx = 0;

    // the current message being constructed. it must be
    // reset every time we have got all data expected
    static bm71_msg_t current_msg = { 0 };

    // Make sure cache is sync
    __DSB();

    while ( p < end )
    {
        switch ( msg_idx )
        {
            case 0:
            {
                // reset possible previous message data
                memset(&current_msg, 0, sizeof(current_msg));

                // discard uart content while looking for sync byte
                if ( *p == BM71_UART_SYNC_HDR )
                {
                    // sync byte found
                    msg_idx++;
                }

                p++;
                break;
            }
            case 1:
            {
                // get first part of packet size
                current_msg.length = (uint16_t)(*p << 8);
                p++;
                msg_idx++;
                break;
            }
            case 2:
            {
                // second part of payload length
                current_msg.length |= (uint16_t)(*p & 0xff);
                p++;
                msg_idx++;
                break;
            }
            case 3:
            {
                // get message type (first byte of payload)
                current_msg.type = *p;

                // remove 1 from length as we take the first
                // byte of the payload which is the message type
                current_msg.length -= 1;

                // update cursors
                p++;
                msg_idx++;
                break;
            }
            case 4:
            {
                // paranoia: check if received message have
                // too big payload if so discard the full buffer
                if ( current_msg.length > BM71_UART_PAYLOAD_LEN )
                {
                    debug_printf("payload too big\n");
                    msg_idx = 0;
                    break;
                }

                // Remaining buffer is bigger than data.
                // Add 1 for the checksum
                if ( (end - p) >= (current_msg.length + 1) )
                {
                    // copy from buffer to message data
                    memcpy(&(current_msg.data[0]), p, current_msg.length);

                    // update current buffer position
                    p += current_msg.length;

                    // validate the checksum (increment index on the way)
                    rc = _bm71_validate_checksum(&current_msg, *p++);
                    if ( rc )
                    {
                        debug_printf("invalid checksum\n");
                        msg_idx = 0;
                        break;
                    }

                    // add this complete message to message fifo
                    rc = al_circbuf_push(&_bm71_msg_fifo, &current_msg);
                    if ( rc )
                    {
                        debug_printf("unable to add new message to fifo\n");
                    }

                    // tell the process we are done with this message
                    msg_idx = 0;
                    break;
                }
                // partial payload size received or missing checksum
                else
                {
                    // copy available data and wait for remaining
                    memcpy(&(current_msg.data[0]), p, (end - p));

                    // update cursors and let the next
                    // loop processing remaining data
                    msg_idx += (end - p);
                    p = end;
                    break;
                }
            }
            default:
            {
                // here we have done first part of the message
                // (i.e: type, length and partial payload) so either
                // it misses part of the payload or only the checksum
                size_t missing_size = current_msg.length;

                // we already have sync byte, length and type
                missing_size += BM71_UART_SYNC_LEN + BM71_UART_LENGTH_LEN + 1;

                // how much does it miss to fill the payload and checksum
                missing_size = missing_size - msg_idx + 1;

                // we have enough to finish this message
                if ( missing_size <= (end - p) )
                {
                    // compute position in payload buffer
                    size_t tmp = msg_idx;
                    tmp -= BM71_UART_SYNC_LEN + BM71_UART_LENGTH_LEN + 1;

                    // copy remaining data without checksum
                    memcpy(&(current_msg.data[tmp]), p, (missing_size - 1));
                    p += (missing_size - 1);

                    // validate the checksum (increment index on the way)
                    rc = _bm71_validate_checksum(&current_msg, *p++);
                    if ( rc )
                    {
                        debug_printf("invalid checksum\n");
                        msg_idx = 0;
                        break;
                    }

                    // push message in fifo
                    rc = al_circbuf_push(&_bm71_msg_fifo, &current_msg);
                    if ( rc )
                    {
                        debug_printf("unable to add new message to fifo\n");
                    }

                    // update cursors and let the next
                    // loop processing remaining data
                    msg_idx = 0;
                    break;
                }
                else
                {
                    // compute position in payload buffer
                    size_t tmp = msg_idx;
                    tmp -= BM71_UART_SYNC_LEN + BM71_UART_LENGTH_LEN + 1;

                    // copy remaining data without checksum
                    memcpy(&(current_msg.data[tmp]), p, (end - p));
                    msg_idx += (end - p);
                    p = end;
                }

            }
        }
    }
}

//----------------------------------------------------------------------
// Interrupt handlers
//----------------------------------------------------------------------
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
    // Prevent unused argument
    // compilation warning
    (void)(huart);

        __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_IDLEF);
        _bm71_parse_uart();
}

//----------------------------------------------------------------------
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    // Prevent unused argument
    // compilation warning
    (void)(huart);

        __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_IDLEF);
        _bm71_parse_uart();
}

//----------------------------------------------------------------------
void USART1_IRQHandler(void)
{

    if((READ_REG(huart1.Instance->ISR) & USART_ISR_IDLE) != RESET)
    {
        __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_IDLEF);
        _bm71_parse_uart();
    }
}

//----------------------------------------------------------------------
// PUBLIC API
//----------------------------------------------------------------------
int bm71_init(void)
{
    int rc=-1;
    // power the device on
    HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_SET);

    // configure and reset
    cfLocalEnterAppMode();

    rc = bmResetSoft();
    if( rc != 0 )
    {
        return rc;
    }

    // flush communication registers
    __HAL_UART_FLUSH_DRREGISTER(&huart1);
    HAL_Delay(100);

    // change device name
    rc = bmSetDeviceID();
    if( rc )
    {
        return rc;
    }

    return rc;
}

//----------------------------------------------------------------------
int bm71_deinit(void)
{
    HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_RESET); // Alim 3V is disabled

    HAL_UART_DMAStop(&huart1);

    __HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);

    __HAL_UART_FLUSH_DRREGISTER(&huart1);

    memset(_bm71_dma_buffer, 0, sizeof(_bm71_dma_buffer));

    return 0;
}

//----------------------------------------------------------------------
int bm71_listen(void)
{
    int rc = 0;
    // call straight this API in case
    // of package update
    rc = bmSetAdvEnable();
    if ( rc )
    {
        return -1;
    }

    // disable unused interupt
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_WUF | UART_IT_PE);
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE | UART_IT_TC | UART_IT_RXNE);
    __HAL_UART_DISABLE_IT(&huart1, UART_IT_LBD | UART_IT_CTS | UART_IT_CM);

    // clear those interrupts flags
    __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_PEF | UART_CLEAR_FEF);
    __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_NEF | UART_CLEAR_OREF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_TCCF | USART_ICR_LBDCF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_CTSCF | USART_ICR_CMCF);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_WUCF);

    // enable idle line interrupt
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

    rc = HAL_UART_Receive_DMA(&huart1, &_bm71_dma_buffer[0], BM71_UART_BUF_SIZE);
    if ( rc )
    {
        debug_printf("unable to start DMA reception\n");
    }

    return rc;

}

//----------------------------------------------------------------------
int bm71_get_status(uint8_t * status)
{
    (void)status;

    return -1;
}

//----------------------------------------------------------------------
int bm71_get_notification(uint8_t * type, uint8_t * value, size_t * len)
{
    int rc = 0;
    bm71_msg_t last_msg;

    // sanity check
    if ( ! type || ! value || ! len )
    {
        debug_printf("invalid arguments\n");
        return -1;
    }

    rc = al_circbuf_pop(&_bm71_msg_fifo, &last_msg);
    if ( rc )
    {
        return -1;
    }

    *type = last_msg.type;
    *len = (size_t)(last_msg.length & 0xffff);
    memcpy(value, &(last_msg.data[0]), last_msg.length);

    return HAL_OK;
}

//----------------------------------------------------------------------
int bm71_set_notification(uint16_t type, uint8_t * value, uint16_t len)
{
    int rc;
    bm71_msg_t last_msg;
    uint8_t buffer[32];
    uint8_t idx = 0, checksum = 0;

    // sanity check
    if ( ! value || ! len )
    {
        return -1;
    }

    // add command, connection handle and characteristic handle
    len += 4;

    // check that length is within bounds
    if ( len > SIZE_OUTGOING_DATA)
    {
        return -1;
    }

    buffer[idx++] = 0xaa;
    buffer[idx] = (uint8_t)(len >> 8);
    checksum -= buffer[idx++];
    buffer[idx] = (uint8_t)(len & 0x00ff);
    checksum -= buffer[idx++];
    buffer[idx] = 0x38;
    checksum -= buffer[idx++];
    buffer[idx] = CONNECTION_HANDLE;
    checksum -= buffer[idx++];
    buffer[idx] = (uint8_t)(type >> 8);
    checksum -= buffer[idx++];
    buffer[idx] = (uint8_t)(type & 0x00ff);
    checksum -= buffer[idx++];

    // do running checksum calculation
    for (int j = 0; j < (len - 4); ++j)
    {
        buffer[idx++] = value[j];
        checksum -= value[j];
    }

    // load the checksum
    buffer[idx++] = checksum;

    // start transmitting the bytes
    rc = HAL_UART_Transmit(&huart1, &buffer[0], idx, 100);
    if ( rc )
    {
        debug_printf("unable to write characteristic: write error\n");
        return rc;
    }

    // maximum timeout is 2s for commands that don't concern RF comm. However,
    // for robustness purpose, we'll set a timeout for every type of message
    for ( int i = 0; i < 25; ++i )
    {
        if ( i == 24 )
        {
            debug_printf("unable to write characteristic: no ack\n");
            return -1;
        }

        // give the device some time
        HAL_Delay(5);

        // receive acknowledge
        rc  = al_circbuf_pop(&_bm71_msg_fifo, &last_msg);
        if ( rc == 0 )
        {
            break;
        }
    }

    // verify acknowledge
    if ( (last_msg.type != 0x80)    ||
         (last_msg.data[0] != 0x38) ||
         (last_msg.data[1] != 0x00)  )
    {
        debug_printf("transmit ack. error type %d cmd %d status %d\n",
                     last_msg.type, last_msg.data[0], last_msg.data[1] != 0x00);
        return -1;
    }

    return 0;
}
