//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <main.h>
#include <string.h>
#include <stm32l4xx_hal.h>
#include <trace.h>
#include <usart.h>


//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
void cfLocalEnterAppMode(void)
{
    // Reset module and put into application mode
	HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_RESET);
	HAL_Delay(1);                                                           //Hold BM70 reset pin low for at least 1ms
	HAL_GPIO_WritePin(GPIOA, Blue_MODE_Pin, GPIO_PIN_SET);                  //Go into application mode
	HAL_Delay(1);                                                           //Hold BM70 reset pin low for at least 1ms
	HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_SET);
	HAL_Delay(500);                                                         //Wait at least 68ms for module to enter application mode
}

// Reset module and put into test mode
//----------------------------------------------------------------------
void cfLocalEnterTestMode(void)
{
    HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_RESET);
    HAL_Delay(1);                                                           //Hold BM70 reset pin low for at least 1ms
    HAL_GPIO_WritePin(GPIOA, Blue_MODE_Pin, GPIO_PIN_RESET);  //Go into test mode to read or write configuration flash
    HAL_Delay(1);                                                           //Hold BM70 reset pin low for at least 1ms
    HAL_GPIO_WritePin(EN_3V_GPIO_Port, EN_3V_Pin, GPIO_PIN_SET);

    HAL_Delay(100);                                                         //Wait at least 46ms for module to enter test mode
}

//----------------------------------------------------------------------
int8_t bmSetDeviceID(void) // Set an unique device ID
{
    int rc, i = 0;
    uint8_t checksum = 0;
    uint8_t buffer[32];
    uint16_t length = 0x0012;
    uint8_t ack[6] = { 0xAA, 0x00, 0x03, 0x80, 0x11, 0x00 };

    // Capture device UID
     uint32_t UID = HAL_GetUIDw0();

    // set opcode and conf
    buffer[i++] = 0xaa;
    buffer[i] = (uint8_t)(length >> 8);
    checksum -= buffer[i++];
    buffer[i] = (uint8_t)(length & 0x00ff);
    checksum -= buffer[i++];
    buffer[i] = 0x11;
    checksum -= buffer[i++];
    buffer[i] = 0x00;       // no flags
    checksum -= buffer[i++];
    buffer[i] = 0x02;       // no flags
    checksum -= buffer[i++];
    buffer[i] = 0x01;       // no flags
    checksum -= buffer[i++];
    buffer[i] = 0x06;       // no flags
    checksum -= buffer[i++];
    buffer[i] = 0x0C;       // adv param size
    checksum -= buffer[i++];
    buffer[i] = 0x09;       // param type: complete name
    checksum -= buffer[i++];

    // set actual name
    sprintf((char *)&buffer[i], "S1_%08lX", UID);
    for (int j = 0; j < 11; ++j)
    {
        checksum -= buffer[i++];
    }

    // load the checksum
    buffer[i++] = checksum;

    __HAL_UART_FLUSH_DRREGISTER(&huart1);

    rc = HAL_UART_Transmit(&huart1, &buffer[0], i, 100); //Start transmitting the bytes
    if ( rc )
    {
        debug_printf("unable to write new device ID\n");
        return -1;
    }

    rc = HAL_UART_Receive(&huart1, &buffer[0], 7, 1000);
    if ( rc )
    {
        debug_printf("don't get ack form previous command\n");
        return -1;
    }

    // verify acknowledge
    if ( memcmp(&buffer[0], &ack[0], 6) )   {
        debug_printf("data transmit get no ack 0x%02X\n", buffer[4]);
        return -1;
    }

    return 0;
}
