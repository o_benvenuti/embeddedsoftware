#ifndef _BM71_BM71_CONFIGURE_H_
#define _BM71_BM71_CONFIGURE_H_

void cfLocalEnterAppMode(void);
void cfLocalEnterTestMode(void);
int8_t bmSetDeviceID(void);


#endif /* _BM71_BM71_CONFIGURE_H_ */
