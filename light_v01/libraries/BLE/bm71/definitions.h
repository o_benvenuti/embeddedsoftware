 /*
 * File:   definitions.h
 *
 * General definitions for the project
 *
 */

#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

#include <stdint.h>

//Buffer sizes
#define SIZE_UART_RX_BUFFER 1024
#define SIZE_UART_TX_BUFFER 256
#define SIZE_MSG_IN_BUFFER  5                                                   //Buffer up to five messages from module
#define SIZE_MSG_OUT_BUFFER 3                                                   //Buffer up to three messages to module
#define SIZE_INCOMING_DATA  50                                                  //Characteristic read/write can return 23 bytes, read local specific service returns 45 bytes
#define SIZE_OUTGOING_DATA  24                                                  //Characteristic write can use 23 bytes

//Bluetooth service & characteristic handles - These handles are obtained from the BM70 UI Tool

#define CONNECTION_HANDLE           	(uint8_t) 0x80

#define PRIVATE_DATA_RX_TX_DESCRIPTOR_UUID 				(uint16_t) 0x0053
#define PRIVATE_FWOTA_FW_VERSION_DESCRIPTOR_UUID      	(uint16_t) 0x8005
#define PRIVATE_FWOTA_TX_DESCRIPTOR_UUID      			(uint16_t) 0x800A



#define PACKET_HEADER_BYTE_SIZE (uint8_t) 5

enum EventOpcode { NoEventOpcode,
                   PasskeyEntryRequest = 0x60,
                   PairingComplete,
                   PasskeyConfirmRequest,
                   AdvertisingReport = 0x70,
                   LeConnectionComplete,
                   DisconnectionComplete,
                   ConnectionParameterUpdateNotify,
                   CommandComplete = 0x80,
                   StatusReport,
                   ConfigureModeStatus = 0x8f,
                   DiscoverAllPrimaryServiceResponse,
                   DiscoverSpecificPrimaryServiceCharacteristicResponse,
                   DiscoverAllCharacteristicDescriptorResponse,
                   CharacteristicValueReceived,
                   ClientWriteCharacteristicValue = 0x98,
                   ReceivedTransparentData = 0x9a
                 };

enum CommandOpcode { NoCommandOpcode,
                     ReadLocalInformation = 0x01,
                     Reset =0x02,
                     ReadStatus =0x03,
                     ReadADCValue,
                     IntoShutdownMode,
                     DebugCommand,
                     ReadDeviceName =0x07,
                     WriteDeviceName =0x08,
                     EraseAllPairedDeviceInformation,
                     ReadPairingModeSetting,
                     WritePairingModeSetting,
                     ReadAllPairedDeviceInformation,
                     DeletePairedDevice,
                     DitalIOControl,
                     PWMControl,
                     ReadRSSIValue,
                     WriteAdvertisingData,
                     WriteScanResultData,
                     SetAdvertisingParameter,
                     SetScanParameter = 0x15,
                     SetScanEnable,
                     LeCreateConnection,
                     LeCreateConnectionCancel,
                     ConnectionParameterUpdateRequest,
                     Disconnect = 0x1b,
                     SetAdvertisingEnable = 0x1c,                                          //Previously called invisible setting
                     ReadRemoteDeviceName = 0x1f,
                     DiscoverAllPrimaryServices = 0x30,
                     DiscoverSpecificPrimaryServiceCharacteristics,
                     ReadCharacteristicValue,
                     ReadUsingCharacteristicUUID,
                     WriteCharacteristicValue,
                     EnableTransparent,
                     SendCharacteristicValue = 0x38,
                     UpdateCharacteristicValue,
                     ReadLocalCharacteristicValue,
                     ReadLocalAllPrimaryService,
                     ReadLocalSpecificPrimaryService,
                     SendTransparentData = 0x3f,
                     PasskeyEntryResponse,
                     UserConfirmResponse,
                     PairingRequest,
                     LeaveConfigureMode = 0x52,
                   };
                  
enum StatusResponse { NoStatusResponse,
                      ScanningMode,
                      ConnectingMode,
                      StandbyMode,
                      BroadcastMode = 0x05,
                      TransparentServiceEnabledMode = 0x08,
                      IdleMode,
                      ShutdownMode,
                      ConfigureMode,
                      BleConnectedMode
                    };

enum ErrorCode { CommandSucceeded,
                 UnknownCommand,
                 UnknownConnectionIdentifier,
                 HardwareFailure,
                 AuthenticationFailure = 0x05,
                 PinOrKeyMissing,
                 MemoryCapacityExceeded,
                 ConnectionTimeout,
                 ConnectionLimitExceeded,
                 ACLConnectionAlreadyExists = 0x0b,
                 CommandDisallowed,
                 ConnectionRejectedDueToLimitedResources,
                 ConnectionRejectedDueToSecurityReasons,
                 ConnectionRejectedDueToUnacceptableBD_ADDR,
                 ConnectionAcceptTimeoutExceeded,
                 UnsupportedFeatureOrParameterValue,
                 InvalidCommandParameters,
                 RemoteUserTerminatedConnection,
                 RemoteDeviceTerminatedConnectionDueToLowResources,
                 RemoteDeviceTerminatedConnectionDueToPowerOff,
                 ConnectionTerminatedByLocalHost,
                 PairingNotAllowed = 0x18,
                 UnspecifiedError = 0x1f,
                 InstantPassed = 0x28,
                 PairingWithUnitKeyNotSupported,
                 InsufficientSecurity = 0x2f,
                 ConnectionRejectedDueToNoSuitableChannelFound = 0x39,
                 ControllerBusy,
                 UnacceptableConnectionInterval,
                 DirectedAdvertisingTimeout,
                 ConnectionTerminatedDueToMICFailure,
                 ConnectionFailedToBeEstablished,
                 InvalidHandle = 0x81,
                 ReadNotPermitted,
                 WriteNotPermitted,
                 InvalidPDU,
                 InsufficientAuthentication,
                 RequestNotSupported,
                 InvalidOffset, //Docs say 0x77, but assume typo, actually 0x87
                 InsufficientAuthorization,
                 PrepareQueueFull,
                 AttributeNotFound,
                 AttributeNotLong,
                 InsufficientEncryptionKeySize,
                 InvalidAttributeValueLength,
                 UnlikelyError,
                 InsufficientEncryption,
                 UnsupportedGroutType,
                 InsufficientResources,
                 ApplicationDefinedError = 0xf0,
                 UARTCheckSumError =0xff
               };                    

enum SetAdvertisingEnable { LeaveStandbyMode,
                            EnterStandbyMode,
                            EnterStandbyModeForTrusted,
                            EnterStandbyModeWithBeacon = 0x81,
                            EnterStandbyModeWithBeaconForTrusted
                          };
               
struct MessageIn {
    enum EventOpcode event;                                                     //Event that caused the message
    enum CommandOpcode command;                                                 //Only applicable if the event is 0x80 CommandComplete
    uint16_t length;                                                            //Length of the data packet, not including opcodes and checksum
    uint8_t data[SIZE_INCOMING_DATA];                                           //Data
};

struct MessageOut {
    enum CommandOpcode command;                                                 //Only applicable if the event is 0x80 CommandComplete
    uint16_t length;                                                            //Length of the data packet, not including opcodes and checksum
    uint8_t data[SIZE_OUTGOING_DATA];                                           //Data
};

struct __attribute__ ((packed)) FlashConfigurationLine {                        //Represents line of text file with flash configuration data
   uint16_t address;                                                            //Address is LSB first
   uint8_t length;                                                              //Length value is always 16
   uint8_t data[16];
};
                    
static const struct FlashConfigurationLine configFlashTable[] = {

//#include "bm71_config_lab4p_solution.txt"                                       //Use BM7x EEPROM Table Utility to create this file
//#include "bm71_flash_config.txt"                                       //Use BM7x EEPROM Table Utility to create this file

};

#endif
