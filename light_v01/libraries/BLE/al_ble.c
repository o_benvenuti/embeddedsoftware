/**
 * @file:  al_ble.c
 *
 * @brief
 *
 * @date   19/07/2018
 *
 * <b>Description:</b>\n
 *    BLE abstraction library
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>

#include "al_ble_private.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// File scope variables
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int al_ble_init(void)
{
	if ( ! ble_available_ctx[0].func.init ) {
		return -1;
	}

	return ble_available_ctx[0].func.init();
}

//----------------------------------------------------------------------
int al_ble_deinit(void)
{
	if ( ! ble_available_ctx[0].func.deinit ) {
		return -1;
	}

	return ble_available_ctx[0].func.deinit();

}

//----------------------------------------------------------------------
int al_ble_set_scanning(void)
{
    if ( ! ble_available_ctx[0].func.set_scanning ) {
        return -1;
    }

    return ble_available_ctx[0].func.set_scanning();
}

//----------------------------------------------------------------------
int al_ble_set_broadcast(uint8_t * broadbuf, uint16_t length)
{
    if ( ! ble_available_ctx[0].func.set_broadcast ) {
        return -1;
    }

    return ble_available_ctx[0].func.set_broadcast(broadbuf, length);
}

//----------------------------------------------------------------------
int al_ble_listen(void)
{
	if ( ! ble_available_ctx[0].func.listen ) {
		return -1;
	}

	// TODO must rework this to return no error
	return ble_available_ctx[0].func.listen();
}

//----------------------------------------------------------------------
int al_ble_get_status(uint8_t * status)
{
	//sanity check
	if ( ! status ) {
		return -1;
	}

	ble_available_ctx[0].func.get_status(status);

	return 0;
}

//----------------------------------------------------------------------
int al_ble_get_notification(uint8_t * command, uint8_t * value, size_t * len)
{
	//sanity check
	if ( ! command || ! value || ! len ) {
		return -1;
	}

	return ble_available_ctx[0].func.get_notif(command, value, len);
}

//----------------------------------------------------------------------
int al_ble_set_notification(uint16_t command, uint8_t * value, uint16_t len)
{
	//sanity check
	if ( ! value || ! len ) {
		return -1;
	}

	return ble_available_ctx[0].func.set_notif(command, value, len);
}



