/**
 * @file:  al_database.h
 *
 * @brief
 *
 * @date   05/11/2018
 *
 * <b>Description:</b>\n
 *    Library providing functions to save data on flash
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
#ifndef _AL_DATABASE_H_
#define _AL_DATABASE_H_
//--------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------
#include <User_RTC.h>

//--------------------------------------------------------------------
// Constant
//--------------------------------------------------------------------
#define AL_DATABASE_MAX_STOP    3

//--------------------------------------------------------------------
// Types
//--------------------------------------------------------------------
typedef enum al_database_diverec_type {
    AL_DATABASE_DIVEREC_TYPE_SAMPLE,
    AL_DATABASE_DIVEREC_TYPE_PARAMS,
    AL_DATABASE_DIVEREC_TYPE_DCOUNT,
    AL_DATABASE_DIVEREC_TYPE_MINTMP,
    AL_DATABASE_DIVEREC_TYPE_DIVEND,
}al_database_diverec_type_t;

typedef struct al_database_diverec_params {
    uint8_t dive_mode;
    uint8_t stop_number;
    struct {
        uint8_t stop_depth;
        uint8_t stop_duration;
        uint8_t ascent_speed;
    }dive_stops[AL_DATABASE_MAX_STOP];
}al_database_diverec_params_t;

typedef struct al_database_diverec_entry {
	float depth;
	uint16_t direction;
}al_database_diverec_entry_t;

typedef struct al_database_diverec_config {
    al_database_diverec_type_t type;
    union {
        uint8_t sample;
        TimeType dive_end;
        uint8_t temperature;
        uint16_t entry_count;
        al_database_diverec_params_t params;
    }data;
}al_database_diverec_config_t;

//--------------------------------------------------------------------
// Public API
//--------------------------------------------------------------------
/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_set_context(uint32_t magic, void * buffer, size_t length);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_get_context(uint32_t * magic, void * buffer, size_t length);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_set_decoconf(float * pressure, size_t plen,
                             float * limit, size_t llen);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_get_decoconf(float * pressure, size_t * plen,
                             float * limit, size_t * llen);


int al_database_set_agm_cal(uint8_t * buf, size_t len, int mem_start);
int al_database_set_mag_cal(uint8_t * buf, size_t len);
int al_database_set_acc_cal(uint8_t * buf, size_t len);
int al_database_set_gyr_cal(uint8_t * buf, size_t len);
int al_database_get_mag_cal(uint8_t * buf, size_t len);
int al_database_get_acc_cal(uint8_t * buf, size_t len);
int al_database_get_gyr_cal(uint8_t * buf, size_t len);

//------------------------------------------------
// DIVE RECORD SPECIFIC
//------------------------------------------------
/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_init(void);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_start(void);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_set(al_database_diverec_config_t * config);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_get(al_database_diverec_config_t * config);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_entry(float depth, uint16_t direction);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_end(uint8_t water_min);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_get_count(void);

/**
  * @brief      Return date of the oldest dive in memory
  *
  * @param[out] Pointer to date struct to fill
  *
  * @retval     0 or system error code
  **/
int al_database_diverec_oldest(TimeType * date);

/**
  * @brief      Return date of the latest dive in memory
  *
  * @param[out] Pointer to date struct to fill
  *
  * @retval     0 or system error code
  **/
int al_database_diverec_latest(TimeType * date);

/**
  * @brief  Fill a list of recorded dive date
  *         from oldest to newest
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_list(TimeType start[], size_t size);

/**
  * @brief  Open a dive record regarding its start date
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_open(TimeType start);


/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_read(al_database_diverec_entry_t * e, size_t * len);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_close(void);

/**
  * @brief
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_delete_oldest(void);

/**
  * @brief Delete header and then all dive record
  *
  * @retval 0 or system error code
  **/
int al_database_diverec_reset(void);


#endif  // _AL_DATABASE_H_
