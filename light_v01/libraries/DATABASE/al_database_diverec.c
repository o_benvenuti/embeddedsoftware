/**
 * @file:  al_database_diverec.c
 *
 * @brief
 *
 * @date   05/11/2018
 *
 * <b>Description:</b>\n
 *
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <string.h>

#include <trace.h>
#include <User_RTC.h>
#include "al_database_private.h"
#include "Inc/al_database.h"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define DEBUG_VERBOSE   0

//----------------------------------------------------------------------
// Local types
//----------------------------------------------------------------------
enum diverec_state { unknown, ready, started, opened };

/** Diverec global context describing in-flash structure **/
typedef struct diverec_ctx
{
    uint32_t header_count;      /**< number of dive recorded */
    struct
    {
        uint32_t head;          /**< current start of headers buffer    **/
        uint32_t tail;          /**< current end of headers buffer      **/
        uint32_t buffer;        /**< actual start of buffer in memory   **/
        uint32_t bufend;        /**< actual end of buffer in-memory     **/
    }header_circ;               /**< header's circular buffer descriptor**/
    struct
    {
        uint32_t head;          /**< current start of headers buffer    **/
        uint32_t tail;          /**< current end of context buffer      **/
        uint32_t buffer;        /**< actual start of buffer in memory   **/
        uint32_t bufend;        /**< actual end of buffer in-memory     **/
    }entry_circ;                /**< context's circular buffer desc.    **/
}diverec_ctx_t;

typedef struct diverec_hdr
{
    TimeType start_time;
    TimeType stop_time;
    uint8_t sample_period; /* in multiples of 500ms */
    uint8_t dive_mode;
    uint8_t water_temp_min; /* to be written at the end of the dive */
    uint16_t entry_count;
    al_database_diverec_params_t params;
    uint32_t entries_addr;
    uint32_t entries_end;
}diverec_hdr_t;

struct diverec_instance
{
    uint32_t offset;
    enum diverec_state state;
    diverec_ctx_t context;
    diverec_hdr_t header;
};

//----------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------
static uint8_t _sector_mirror[DATABASE_SECTOR_SIZE];
static struct diverec_instance _current_instance = { .state = unknown };

//----------------------------------------------------------------------
// Prototypes
//----------------------------------------------------------------------
static int _init_diverec_hdr(void);
static int _add_header_to_flash(void);
static int _delete_oldest_diverec_safe(void);
static void _print_dive(diverec_hdr_t * hdr);
static int _write_entry(al_database_diverec_entry_t * entry);

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int al_database_diverec_init(void)
{
    int rc = 0;
    diverec_ctx_t * ctx = &(_current_instance.context);

    // sanity check
    if ( _current_instance.state != unknown )
    {
        debug_printf("already initialized\n");
        return 0;
    }

    // paranoia check
    if ( sizeof(diverec_ctx_t) > DIVEREC_CTX_SIZE )
    {
        debug_printf("not enough space in flash for context\n");
        return -1;
    }

    // get context number
    rc = eflash_read(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            (uint8_t*)ctx, sizeof(diverec_ctx_t));
    if ( rc )
    {
        debug_printf("no diverec context\n");
        ctx->header_count = 0;
    }

    // verify possible new configuration
    if ( ctx->header_circ.buffer != DIVEREC_HDR_START ||
         ctx->header_circ.bufend != DIVEREC_HDR_END   ||
         ctx->entry_circ.buffer != DIVEREC_DATA_START ||
         ctx->entry_circ.bufend != DIVEREC_DATA_END    )

    {
        debug_printf("reset buffer configuration\n");
        // discard all data (will be updated on flash later on closing)
        ctx->header_count = 0;
        ctx->header_circ.head = DIVEREC_HDR_START;
        ctx->header_circ.tail = DIVEREC_HDR_START;
        ctx->header_circ.buffer = DIVEREC_HDR_START;
        ctx->header_circ.bufend = DIVEREC_HDR_END;

        ctx->entry_circ.head = DIVEREC_DATA_START;
        ctx->entry_circ.tail = DIVEREC_DATA_START;
        ctx->entry_circ.buffer = DIVEREC_DATA_START;
        ctx->entry_circ.bufend = DIVEREC_DATA_END;
    }

    _current_instance.state = ready;

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_start(void)
{
    int rc = 0;

    // sanity check
    if ( _current_instance.state != ready  ) {
        debug_printf("dive record busy\n");
        return -1;
    }

    // make sure header is clear
    memset(&(_current_instance.header), 0, sizeof(diverec_hdr_t));

    // retrieve start time to 'compute' filename
    _current_instance.header.start_time = RTC_GetTime();
    _current_instance.state = started;

    rc = _init_diverec_hdr();
    return rc;
}

//----------------------------------------------------------------------
int al_database_diverec_set(al_database_diverec_config_t * config)
{
    // sanity check
    if ( ! config )
    {
        debug_printf("invalid parameter\n");
        return -1;
    }

    // verify currently running dive
    if ( _current_instance.state != started ) {
        debug_printf("No dive on going\n");
        return -1;
    }

    // only update in ram as header will
    // be written at the end of the dive
    switch ( config->type ) {
    case AL_DATABASE_DIVEREC_TYPE_PARAMS:
        // avoid array overflow
        if ( config->data.params.stop_number > AL_DATABASE_MAX_STOP )
        {
            debug_printf("too many stops %d\n", config->data.params.stop_number);
            return -1;
        }

        // update parameters
        _current_instance.header.params = config->data.params;
        break;
    case AL_DATABASE_DIVEREC_TYPE_MINTMP:
        // update parameters
        _current_instance.header.water_temp_min = config->data.temperature;
        break;
    case AL_DATABASE_DIVEREC_TYPE_SAMPLE:
        // update parameters
        _current_instance.header.sample_period = config->data.sample;
        break;
    default:
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_get(al_database_diverec_config_t * config)
{
    // sanity check
    if ( ! config ) {
        debug_printf("invalid parameter\n");
        return -1;
    }

    // verify currently running dive
    if ( (_current_instance.state != started) &&
         (_current_instance.state != opened ) )
    {
        debug_printf("No dive running\n");
        return -1;
    }

    // copy value out
    switch ( config->type )
    {
    case AL_DATABASE_DIVEREC_TYPE_PARAMS:
        // hope that caller provided a buffer big enough
        config->data.params = _current_instance.header.params;
        break;
    case AL_DATABASE_DIVEREC_TYPE_SAMPLE:
        config->data.sample = _current_instance.header.sample_period;
        break;
    case AL_DATABASE_DIVEREC_TYPE_DCOUNT:
        config->data.entry_count = _current_instance.header.entry_count;
        break;
    case AL_DATABASE_DIVEREC_TYPE_MINTMP:
        config->data.temperature = _current_instance.header.water_temp_min;
        break;
    case AL_DATABASE_DIVEREC_TYPE_DIVEND:
        config->data.dive_end = _current_instance.header.stop_time;
        break;
    default:
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_entry(float depth, uint16_t direction)
{
    int rc;
    al_database_diverec_entry_t entry;

    // verify currently running dive
    if ( _current_instance.state != started )
    {
        debug_printf("No dive on going\n");
        return -1;
    }

    // set values
    entry.depth = depth;
    entry.direction = direction;

    // write last entry
    rc = _write_entry(&entry);
    if ( rc )
    {
        debug_printf("unable to write entry\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_end(uint8_t water_min)
{
    int rc = 0;

    // verify currently running dive
    if ( _current_instance.state != started )
    {
        debug_printf("No dive on going\n");
        return -1;
    }

    // update parameters
    _current_instance.header.water_temp_min = water_min;
    _current_instance.header.stop_time = RTC_GetTime();

    // write current headers
    rc = _add_header_to_flash();
    if ( rc )
    {
        debug_printf("unable to write database_diverec_header\n");
    }

    // we are done with this dive
    _current_instance.state = ready;

    return rc;
}

//----------------------------------------------------------------------
int al_database_diverec_get_count(void)
{
    int rc = 0;
    diverec_ctx_t ctx;

    // verify currently running dive
    if ( _current_instance.state != ready ) {
        debug_printf("Dive on going\n");
        return -1;
    }

    // get context number
    rc = eflash_read(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            (uint8_t*)&ctx, sizeof(diverec_ctx_t));
    if ( rc )
    {
        debug_printf("unable to read diverec context\n");
        return -1;
    }

    return ctx.header_count;
}

//----------------------------------------------------------------------
int al_database_diverec_oldest(TimeType * date)
{
    int rc;
    diverec_hdr_t hdr;
    diverec_ctx_t * ctx = &(_current_instance.context);

    // check availability
    if ( _current_instance.state != ready )
    {
        debug_printf("dive running\n");
        return -1;
    }

    // no dive recorded
    if ( ctx->header_count == 0 )
    {
        return -1;
    }

    // sanity check
    if ( ! date )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // get oldest header
    rc = eflash_read(FLASH_DEVICE_IDX, ctx->header_circ.tail,
                     (uint8_t*)&hdr, sizeof(diverec_hdr_t));
    if ( rc )
    {
        debug_printf("unable to read last header\n");
        return -1;
    }

    // copy start time out
    *date = hdr.start_time;

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_latest(TimeType * date)
{
    int rc;
    uint32_t hdr_addr;
    diverec_hdr_t hdr;
    diverec_ctx_t * ctx = &(_current_instance.context);

    // check availability
    if ( _current_instance.state != ready )
    {
        debug_printf("dive running\n");
        return -1;
    }

    // no dive recorded
    if ( ctx->header_count == 0 )
    {
        return -1;
    }

    // sanity check
    if ( ! date )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    hdr_addr = ctx->header_circ.head - sizeof(diverec_hdr_t);
    if ( hdr_addr < ctx->header_circ.buffer )
    {
        hdr_addr = ctx->header_circ.bufend - sizeof(diverec_hdr_t);
        if ( hdr_addr <= ctx->header_circ.head )
        {
            return -1;
        }
    }


    // get oldest header
    rc = eflash_read(FLASH_DEVICE_IDX, hdr_addr,
                     (uint8_t*)&hdr, sizeof(diverec_hdr_t));
    if ( rc )
    {
        debug_printf("unable to read last header\n");
        return -1;
    }

    // copy start time out
    *date = hdr.start_time;

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_list(TimeType list[], size_t size)
{
    int rc;
    uint32_t count = 0;
    uint32_t hdr_addr;
    diverec_hdr_t hdr;
    diverec_ctx_t * ctx = &(_current_instance.context);

    // check availability
    if ( _current_instance.state != ready )
    {
        debug_printf("dive running\n");
        return -1;
    }

    // no dive recorded
    if ( ctx->header_count == 0 )
    {
        return -1;
    }

    // sanity check
    if ( ! list  || ctx->header_count > size )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // start from oldest
    hdr_addr = ctx->header_circ.tail;

    do
    {
        // get context number
        rc = eflash_read(FLASH_DEVICE_IDX, hdr_addr, (uint8_t*)&hdr,
                               sizeof(diverec_hdr_t));
        if ( rc )
        {
            debug_printf("unable to read last header\n");
            return -1;
        }

        // copy start time out
        list[count] = hdr.start_time;

        // update counters
        count += 1;
        hdr_addr += sizeof(diverec_hdr_t);
        if ( hdr_addr >= ctx->header_circ.bufend )
        {
            hdr_addr = ctx->header_circ.buffer;
        }
    } while ( count  < ctx->header_count );

    return count;
}

//----------------------------------------------------------------------
int al_database_diverec_open(TimeType start)
{
    int rc;
    uint32_t hdr_addr = 0;
    diverec_hdr_t * hdr = &(_current_instance.header);
    diverec_ctx_t * ctx = &(_current_instance.context);

    // check availability
    if ( _current_instance.state != ready )
    {
        debug_printf("dive running\n");
        return -1;
    }

    // no dive recorded
    if ( ctx->header_count == 0 )
    {
        return -1;
    }

    // start from oldest
    hdr_addr = ctx->header_circ.tail;

    do
    {
        // get context number
        rc = eflash_read(FLASH_DEVICE_IDX, hdr_addr, (uint8_t*)hdr,
                               sizeof(diverec_hdr_t));
        if ( rc )
        {
            debug_printf("unable to read last header\n");
            hdr = NULL;
            return -1;
        }

        // is it the one we are looking for
        if ( ! memcmp(&(hdr->start_time), &start, sizeof(TimeType)) )
        {
            break;
        }

        // update address
        hdr_addr += sizeof(diverec_hdr_t);
        if ( hdr_addr >= ctx->header_circ.bufend )
        {
            hdr_addr = ctx->header_circ.buffer;
        }

    } while ( hdr_addr != ctx->header_circ.head );

    // set initial values
    _current_instance.offset = 0;
    _current_instance.state = opened;

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_read(al_database_diverec_entry_t * e, size_t * len)
{
    int rc = 0;
    uint32_t address;
    diverec_hdr_t * hdr = &(_current_instance.header);
    diverec_ctx_t * ctx = &(_current_instance.context);

    // check availability
    if ( _current_instance.state != opened )
    {
        debug_printf("no dive record opened\n");
        return -1;
    }

    // sanity check
    if ( ! e || ! len )
    {
        debug_printf("invalid parameters\n");
        return -1;
    }

    // check length consistency
    if ( *len % sizeof(al_database_diverec_entry_t) )
    {
        debug_printf("length must be multiple of entry type\n");
        return -1;
    }

    address = hdr->entries_addr + _current_instance.offset;

    // length bigger than size
    if ( (hdr->entries_addr + *len) > hdr->entries_end )
    {
    	*len = hdr->entries_end - hdr->entries_addr;
    	*len = *len - _current_instance.offset;
    }

    if ( address > ctx->entry_circ.bufend )
    {
    	// try at the beginning of the buffer
    	debug_printf("exception: invalid offset\n");
    	return -1;
    }
    else if ( (address + *len) > ctx->entry_circ.bufend )
    {
    	// must read too parts
    	debug_printf("need to read two parts\n");
    }

    // get context number
    rc = eflash_read(FLASH_DEVICE_IDX, address, (uint8_t*)e, *len);
    if ( rc )
    {
        debug_printf("unable to read data @0x%lX\n", address);
        return -1;
    }

    _current_instance.offset += *len;

   return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_close(void)
{
    // verify currently running dive
    if ( _current_instance.state != opened ) {
        debug_printf("No dive opened\n");
        return -1;
    }

    _current_instance.state = ready;

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_delete_oldest(void)
{
    int rc = 0;
    uint32_t address;
    diverec_hdr_t * hdr = &(_current_instance.header);
    diverec_ctx_t * ctx = &(_current_instance.context);

    // sanity check
    if ( _current_instance.state != ready )
    {
        debug_printf("library not initialized\n");
        return -1;
    }

    address = ctx->header_circ.tail;

    // get context number
    rc = eflash_read(FLASH_DEVICE_IDX, address,
            (uint8_t*)hdr, sizeof(diverec_hdr_t));
    if ( rc )
    {
        debug_printf("unable to read latest header\n");
        return -1;
    }

    debug_printf("=== Delete record===\n");
    _print_dive(hdr);

    // check if tail entries address is consistent and
    // let calling the process handling the situation
    if ( hdr->entries_addr != ctx->entry_circ.tail )
    {
        debug_printf("inconsistent circular buffer configuration\n");
        return -1;
    }

    // update entry tail according to deleted header info
    ctx->header_count--;
    ctx->header_circ.tail += sizeof(diverec_hdr_t);
    if ( ctx->header_circ.tail >= ctx->header_circ.bufend )
    {
        ctx->header_circ.tail = ctx->header_circ.buffer;
    }
    ctx->entry_circ.tail = hdr->entries_end;

    // update status in flash
    rc = eflash_read(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            _sector_mirror, DATABASE_SECTOR_SIZE);
    if ( rc )
    {
        debug_printf("unable to read mirror buffer\n");
        return -1;
    }

    // copy context at the beginning
    memcpy(&_sector_mirror[0], ctx, sizeof(diverec_ctx_t));
    eflash_erase_sector(FLASH_DEVICE_IDX, DIVEREC_CTX_START);
    eflash_prog(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            _sector_mirror, DATABASE_SECTOR_SIZE);

    return 0;
}

//----------------------------------------------------------------------
int al_database_diverec_reset(void)
{
    int rc = 0;
    uint32_t sect_addr = DIVEREC_CTX_START;

    // verify currently running dive
    if ( _current_instance.state != ready )
    {
        debug_printf("dive record on going\n");
        return -1;
    }

    // reset by erasing global context (and will be updated on reboot)
    while ( sect_addr < DIVEREC_HDR_END )
    {
        rc = eflash_erase_sector(FLASH_DEVICE_IDX, sect_addr);
        if ( rc )
        {
            debug_printf("cannot erase diverec context\n");
            return -1;
        }

        sect_addr = sect_addr + DATABASE_SECTOR_SIZE;
    }

    // erase first data sector. Next ones we'll be erased on the way
    rc = eflash_erase_sector(FLASH_DEVICE_IDX, DIVEREC_DATA_START);
    if ( rc )
    {
        debug_printf("cannot erase first diverec data sector\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
// Helpers
//----------------------------------------------------------------------
static int _init_diverec_hdr(void)
{
    int rc = 0;
    uint32_t next;
    diverec_hdr_t * hdr = &(_current_instance.header);
    diverec_ctx_t * ctx = &(_current_instance.context);

    // find free slot
    next = ctx->header_circ.head + sizeof(diverec_hdr_t);
    if ( next >= ctx->header_circ.bufend )
    {
        next = ctx->header_circ.buffer;
    }

    // is the buffer full ?
    if ( next == ctx->header_circ.tail )
    {
        debug_printf("delete a dive record\n");
        rc = _delete_oldest_diverec_safe();
        if ( rc )
        {
            return -1;
        }
    }

    // get the next sector
    hdr->entries_addr = ctx->entry_circ.head;
    hdr->entries_end = ctx->entry_circ.head;

    return 0;
}

//----------------------------------------------------------------------
static int _delete_oldest_diverec_safe(void)
{
    int rc = 0;
    diverec_hdr_t hdr;
    diverec_ctx_t * ctx = &(_current_instance.context);

    // sanity check
    if ( _current_instance.state == unknown )
    {
        debug_printf("library not initialized\n");
        return -1;
    }

    // get context number
    rc = eflash_read(FLASH_DEVICE_IDX, ctx->header_circ.tail, (uint8_t*)&hdr,
                           sizeof(diverec_hdr_t));
    if ( rc )
    {
        debug_printf("unable to read oldest header\n");
        return -1;
    }

    // check if tail entries address is consistent and
    // let calling the process handling the situation
    if ( hdr.entries_addr != ctx->entry_circ.tail )
    {
        debug_printf("inconsistent circular buffer configuration\n");
        return -1;
    }

    // update entry tail according to deleted header info
    ctx->entry_circ.tail = hdr.entries_end;

    ctx->header_count--;
    ctx->header_circ.tail += sizeof(diverec_hdr_t);
    if ( ctx->header_circ.tail >= ctx->header_circ.bufend )
    {
        ctx->header_circ.tail = ctx->header_circ.buffer;
    }

    debug_printf("=== Delete record===\n");
    _print_dive(&hdr);

    return 0;
}

//----------------------------------------------------------------------
static int _write_entry(al_database_diverec_entry_t * entry)
{
    uint32_t next;
    uint32_t * head, * tail;
	diverec_ctx_t * ctx = &(_current_instance.context);

    // sanity check
    if ( ! entry )
    {
        return -1;
    }

    if ( _current_instance.state != started )
    {
    	debug_printf("dive not started\n");
    	return -1;
    }

    // set positions (use variable to improve readability)
    head = &(ctx->entry_circ.head);
    tail = &(ctx->entry_circ.tail);
    next = *head + sizeof(al_database_diverec_entry_t);

    // check that we do not overlap end of buffer
    if ( next >= ctx->entry_circ.bufend )
    {
        next = ctx->entry_circ.buffer;
    }

    // is there enough space
    if ( next == *tail )
    {
        _delete_oldest_diverec_safe();
    }

    // if this is a new sector we need to erase it prior to write
    // WARNING works only if data area is bigger than sector size
    if ( (next % DATABASE_SECTOR_SIZE) == 0 )
    {
        // if it contains delete also the next record
        while ( ((next + DATABASE_SECTOR_SIZE) > *tail) && (*tail > next) )
        {
            _delete_oldest_diverec_safe();
        }

        // erase the current sector
        eflash_erase_sector(FLASH_DEVICE_IDX, next);
    }

    eflash_prog(FLASH_DEVICE_IDX, *head, entry,
            sizeof(al_database_diverec_entry_t));

    // if this is exactly end of buffer
    // it will be handled at the next write
    *head = next;

    // update entry counter
    _current_instance.header.entry_count++;
    _current_instance.header.entries_end = *head;

    return 0;
}

//----------------------------------------------------------------------
static int _add_header_to_flash(void)
{
	int rc = 0;
    uint32_t next, head;
    diverec_hdr_t * hdr = &(_current_instance.header);
    diverec_ctx_t * ctx = &(_current_instance.context);

    // next is where head will point to after this write.
    next = ctx->header_circ.head + sizeof(diverec_hdr_t);
    if ( next >= ctx->header_circ.bufend )
    {
        next = ctx->header_circ.buffer;
    }

    // if the head + 1 == tail, circular buffer is full
    if ( next == ctx->header_circ.tail )
    {
        debug_printf("header circular buffer full\n");
        return -1;
    }

    ctx->header_count++;
    head = ctx->header_circ.head;
	ctx->header_circ.head = next;

    rc = eflash_read(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            _sector_mirror, DATABASE_SECTOR_SIZE);
    if ( rc )
    {
        debug_printf("unable to read mirror buffer\n");
        return -1;
    }

    // if the next header is in the first sector
    if ( head < (DIVEREC_CTX_START + DATABASE_SECTOR_SIZE) )
    {
        head = head % DATABASE_SECTOR_SIZE;
        memcpy(&_sector_mirror[head], hdr, sizeof(diverec_hdr_t));
    }

    debug_printf("=== Saved record ===\n");
    _print_dive(hdr);

    // copy context at the beginning
    memcpy(&_sector_mirror[0], ctx, sizeof(diverec_ctx_t));
    eflash_erase_sector(FLASH_DEVICE_IDX, DIVEREC_CTX_START);
    eflash_prog(FLASH_DEVICE_IDX, DIVEREC_CTX_START,
            _sector_mirror, DATABASE_SECTOR_SIZE);

    // get out if the header as been written
    if ( head < (DIVEREC_CTX_START + DATABASE_SECTOR_SIZE) )
    {
        return 0;
    }

    // read the second sector
    rc = eflash_read(FLASH_DEVICE_IDX, DIVEREC_CTX_START + DATABASE_SECTOR_SIZE,
                           _sector_mirror, DATABASE_SECTOR_SIZE);
    if ( rc )
    {
        debug_printf("unable to read mirror buffer\n");
        return -1;
    }

    head = head % DATABASE_SECTOR_SIZE;
    memcpy(&_sector_mirror[head], hdr, sizeof(diverec_hdr_t));
    eflash_erase_sector(FLASH_DEVICE_IDX,
            (DIVEREC_CTX_START + DATABASE_SECTOR_SIZE));
    eflash_prog(FLASH_DEVICE_IDX, DIVEREC_CTX_START + DATABASE_SECTOR_SIZE,
                       _sector_mirror, DATABASE_SECTOR_SIZE);

    return 0;
}

//----------------------------------------------------------------------
static void _print_dive(diverec_hdr_t * hdr)
{
#if ! DEBUG_VERBOSE
    (void)hdr;
#else
    diverec_ctx_t * ctx = &(_current_instance.context);

    debug_printf("Dive of %X/%X/20%X:\n",
            hdr->start_time.Date, hdr->start_time.Month, hdr->start_time.Year);
    debug_printf("\tFrom %X:%X:%X to %X:%X:%X\n",
            hdr->start_time.Hours, hdr->start_time.Minutes,
            hdr->start_time.Seconds, hdr->stop_time.Hours,
            hdr->stop_time.Minutes, hdr->stop_time.Seconds);
    debug_printf("\tEntry count: %d (from 0x%lX to 0x%lX)\n",
            hdr->entry_count, hdr->entries_addr, hdr->entries_end);
    debug_printf("\n");

    debug_printf("Circular buffers:\n");
    debug_printf("\tHeader count: %ld\n", ctx->header_count);
    debug_printf("\tHeaders: tail 0x%lX head 0x%lX\n",
            ctx->header_circ.tail, ctx->header_circ.head);
    debug_printf("\tEntries: tail 0x%lX head 0x%lX\n",
            ctx->entry_circ.tail, ctx->entry_circ.head);
#endif // DEBUG_VERBOSE
}

