/**
 * @file:  al_database_private.h
 *
 * @brief
 *
 * @date   15/11/2018
 *
 * <b>Description:</b>\n
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
#ifndef _AL_DATABASE_PRIVATE_H_
#define _AL_DATABASE_PRIVATE_H_
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <eflash.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FLASH_DEVICE_IDX        0
#define DATABASE_SECTOR_SIZE    0x1000     // 4kB

#if PLATFORMID == 112
    #define FLASH_TOTAL_SIZE    0x800000     // 8MB
#elif PLATFORMID == 113
    #define FLASH_TOTAL_SIZE    0x1000000     // 16MB
#else
    #error "UNSUPPORTED PLATFORM"
#endif

#define CONTEXT_START       0x0
#define CONTEXT_SIZE        DATABASE_SECTOR_SIZE

#define DECOCONF_START      (CONTEXT_START + CONTEXT_SIZE)
#define DECOCONF_SIZE       DATABASE_SECTOR_SIZE

#define AGMCAL_START        (DECOCONF_START + DECOCONF_SIZE)
#define AGMCAL_SIZE         DATABASE_SECTOR_SIZE
#define AGMCAL_MAG_START	AGMCAL_START
#define AGMCAL_MAG_SIZE     0x100 //real size in magcal.h (108 bytes)
#define AGMCAL_ACC_START	(AGMCAL_MAG_START + AGMCAL_MAG_SIZE)
#define AGMCAL_ACC_SIZE		0xC // real size in LSM9DS1.h (12 bytes)
#define AGMCAL_GYR_START    (AGMCAL_ACC_START + AGMCAL_ACC_SIZE)
#define AGMCAL_GYR_SIZE		0xC // real size in LSM9DS1.h (12 bytes)
#define AGM_CAL_BUFFER_SIZE (AGMCAL_MAG_SIZE + AGMCAL_ACC_SIZE + AGMCAL_GYR_SIZE)

#define MAGCAL_MATRIX_START (AGMCAL_START + AGMCAL_SIZE)
#define MAGCAL_MATRIX_SIZE  (10*DATABASE_SECTOR_SIZE)

#define DIVEREC_CTX_START   (MAGCAL_MATRIX_START + MAGCAL_MATRIX_SIZE)
#define DIVEREC_CTX_SIZE    0x100
#define DIVEREC_CTX_END     (DIVEREC_CTX_START + DIVEREC_CTX_SIZE)

#define DIVEREC_HDR_START   DIVEREC_CTX_END
#define DIVEREC_HDR_SIZE    0x1F00
#define DIVEREC_HDR_END     (DIVEREC_HDR_START + DIVEREC_HDR_SIZE)

#define DIVEREC_DATA_START  DIVEREC_HDR_END
#define DIVEREC_DATA_SIZE   (FLASH_TOTAL_SIZE - DIVEREC_DATA_START)
#define DIVEREC_DATA_END    (DIVEREC_DATA_START + DIVEREC_DATA_SIZE)



#endif // _AL_DATABASE_PRIVATE_H_
