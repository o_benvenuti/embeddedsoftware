/**
 * @file:  al_database.c
 *
 * @brief
 *
 * @date   05/11/2018
 *
 * <b>Description:</b>\n
 *    Library providing functions to save data on flash
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */
//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include "al_database_private.h"
#include "Inc/al_database.h"
#include <eflash.h>

#include <trace.h>      // debug_printf
#include <string.h>     // memcpy
#include <al_tools.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define CONTEXT_MAGIC   0xDEADBEEF
#define MAGNCAL_MAGIC   0xDEADD00D

//----------------------------------------------------------------------
// Local types
//----------------------------------------------------------------------
enum deco_tag
{
    DECO_TAG_PRESSURE = 0x5A,
    DECO_TAG_LIMIT    = 0xA5,
};
//----------------------------------------------------------------------
// Local variables
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int al_database_set_context(uint32_t magic, void * buffer, size_t length)
{
    int rc = 0;

    // sanity check
    if ( ! buffer || ! length )
    {
        debug_printf("invalid param\n");
        return  -1;
    }

    // flash functions don't return status
    eflash_erase_sector(FLASH_DEVICE_IDX, CONTEXT_START);

    // first write magic to make sure we are reading right data after
    rc = eflash_prog(FLASH_DEVICE_IDX, CONTEXT_START, &magic, sizeof(magic));
    if ( rc )
    {
        debug_printf("unable to write context\n");
        return -1;
    }

    // and then write context data
    rc = eflash_prog(FLASH_DEVICE_IDX,
                     CONTEXT_START + sizeof(magic),
                     buffer, length);
    if ( rc )
    {
        debug_printf("unable to write context\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_get_context(uint32_t * magic, void * buffer, size_t length)
{
    int rc = 0;

    // sanity check
    if ( ! magic || ! buffer || ! length )
    {
        debug_printf("invalid param\n");
        return  -1;
    }

    // get context magic to validate data after
    rc = eflash_read(FLASH_DEVICE_IDX, CONTEXT_START,
                     magic, sizeof(*magic));
    if ( rc )
    {
        debug_printf("unable to read context magic \n");
        return -1;
    }

    // get context data
    rc = eflash_read(FLASH_DEVICE_IDX,
                     CONTEXT_START + sizeof(*magic),
                     buffer, length);
    if ( rc )
    {
        debug_printf("unable to read context data\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_set_decoconf(float * pressure, size_t plen,
                             float * limit, size_t llen)
{
    size_t idx, total;
    enum deco_tag tag;
    uint8_t buffer[256];

    // sanity check
    if ( ! pressure || ! plen || ! limit || ! llen )
    {
        debug_printf("invalid params\n");
        return -1;
    }

    // compute total size
    total = 2 * sizeof(enum deco_tag);
    total += 2 * sizeof(size_t);
    total += plen + llen;

    // check if it fit in buffer
    if ( total > sizeof(buffer) )
    {
        debug_printf("too much data\n");
        return -1;
    }

    // prepare buffer with data as TLVs
    tag = DECO_TAG_PRESSURE;
    memcpy(&buffer[0], (uint8_t*)&tag, sizeof(enum deco_tag));
    idx = sizeof(enum deco_tag);
    memcpy(&buffer[idx], (uint8_t*)&plen, sizeof(size_t));
    idx += sizeof(size_t);
    memcpy(&buffer[idx], (uint8_t*)pressure, plen);
    idx += plen;

    tag = DECO_TAG_LIMIT;
    memcpy(&buffer[idx], (uint8_t*)&tag, sizeof(enum deco_tag));
    idx += sizeof(enum deco_tag);
    memcpy(&buffer[idx], (uint8_t*)&llen, sizeof(size_t));
    idx += sizeof(size_t);
    memcpy(&buffer[idx], (uint8_t*)limit, llen);
    idx += llen;

    // flash functions don't return status
    eflash_erase_sector(FLASH_DEVICE_IDX, DECOCONF_START);
    eflash_prog(FLASH_DEVICE_IDX, DECOCONF_START, &buffer[0], idx);

    return 0;
}

//----------------------------------------------------------------------
int al_database_get_decoconf(float * pressure, size_t * plen,
                             float * limit, size_t * llen)
{
    int rc = 0;
    size_t idx, tlen;
    enum deco_tag tag;

    // sanity check
    if ( ! pressure || ! plen || ! limit || ! llen )
    {
        debug_printf("invalid params\n");
        return -1;
    }

    // read first tag
    rc = eflash_read(FLASH_DEVICE_IDX, DECOCONF_START,
            (uint8_t*)&tag,sizeof(enum deco_tag));
    if ( rc )
    {
        debug_printf("unable to read deco first tag\n");
        return -1;
    }

    // check consistency
    if ( tag != DECO_TAG_PRESSURE )
    {
        debug_printf("unexpected deco configuration\n");
        return -1;
    }

    // read pressure array size
    idx = sizeof(enum deco_tag);
    rc = eflash_read(FLASH_DEVICE_IDX, (DECOCONF_START + idx),
            (uint8_t*)&tlen, sizeof(size_t));
    if ( rc )
    {
        debug_printf("unable to read deco pressure len\n");
        return -1;
    }

    // check overflow
    if ( tlen > *plen )
    {
        debug_printf("output pressure buffer too small\n");
        return -1;
    }

    *plen = tlen;
    idx += sizeof(size_t);

    // read actual pressure array
    rc = eflash_read(FLASH_DEVICE_IDX, (DECOCONF_START + idx),
            (uint8_t*)pressure, tlen);
    if ( rc )
    {
        debug_printf("unable to read deco pressure tab\n");
        return -1;
    }

    idx += tlen;

    // read second tag
    rc = eflash_read(FLASH_DEVICE_IDX, (DECOCONF_START + idx),
            (uint8_t*)&tag, sizeof(enum deco_tag));
    if ( rc )
    {
        debug_printf("unable to read deco first tag\n");
        return -1;
    }

    // check consistency
    if ( tag != DECO_TAG_LIMIT )
    {
        debug_printf("unexpected flash configuration\n");
        return -1;
    }

    // read pressure array size
    idx += sizeof(enum deco_tag);
    rc = eflash_read(FLASH_DEVICE_IDX, (DECOCONF_START + idx),
            (uint8_t*)tlen, sizeof(size_t));
    if ( rc )
    {
        debug_printf("unable to read deco limit len\n");
        return -1;
    }

    // check overflow
    if ( tlen > *llen )
    {
        debug_printf("output limit buffer too small\n");
        return -1;
    }

    *llen = tlen;
    idx += sizeof(size_t);

    // read actual pressure array
    rc = eflash_read(FLASH_DEVICE_IDX, (DECOCONF_START + idx),
            (uint8_t*)limit, tlen);
    if ( rc )
    {
        debug_printf("unable to read deco limit tab\n");
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------------
int al_database_set_agm_cal(uint8_t * buf, size_t len, int mem_start)
{
    int rc = 0;
    int buffer_start;
	uint8_t tmp[AGM_CAL_BUFFER_SIZE] = {0};

	// sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid params\n");
        return  -1;
    }

    // read temporary buffer to avoid losing the other calibration values
    rc = eflash_read(FLASH_DEVICE_IDX, AGMCAL_START, tmp, sizeof(tmp));
    if ( rc != 0 )
    {
        debug_printf("unable to read agm calibration\n");
        return -1;
    }

    //edit temporary buffer with new data
    buffer_start = mem_start - AGMCAL_START;
    memcpy(&tmp[buffer_start],buf,len);

    rc = eflash_erase_sector(FLASH_DEVICE_IDX, AGMCAL_START);
    if ( rc != 0 )
    {
        debug_printf("unable to erase agm calibration sector\n");
        return -1;
    }
    rc = eflash_prog(FLASH_DEVICE_IDX,AGMCAL_START, tmp, sizeof(tmp));
    if ( rc != 0 )
    {
        debug_printf("unable to write agm calibration\n");
        return -1;
    }
    return 0;
}

//----------------------------------------------------------------------
int al_database_set_mag_cal(uint8_t * buf, size_t len)
{
    // check we've got enough room
    if ( len > AGMCAL_MAG_SIZE )
    {
        debug_printf("Magnenometer calibration bigger than allocated size\n");
        return -1;
    }

    return al_database_set_agm_cal(buf, len, AGMCAL_MAG_START);
}

//----------------------------------------------------------------------
int al_database_set_acc_cal(uint8_t * buf, size_t len)
{
    // check we've got enough room
    if ( len > AGMCAL_ACC_SIZE )
    {
        debug_printf("Accelerometer calibration bigger than allocated size\n");
        return -1;
    }

    return al_database_set_agm_cal(buf, len, AGMCAL_ACC_START);
}

//----------------------------------------------------------------------
int al_database_set_gyr_cal(uint8_t * buf, size_t len)
{
    // check we've got enough room
    if ( len > AGMCAL_GYR_SIZE )
    {
        debug_printf("Gyroscope calibration bigger than allocated size\n");
        return -1;
    }

    return al_database_set_agm_cal(buf, len, AGMCAL_GYR_START);
}

//----------------------------------------------------------------------
int al_database_get_mag_cal(uint8_t * buf, size_t len)
{
    int rc = 0;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid params\n");
        return  -1;
    }

    rc = eflash_read(FLASH_DEVICE_IDX, AGMCAL_MAG_START, (uint8_t*)buf, len);
    if ( rc )
    {
        debug_printf("unable to read magnetometer calibration\n");
        return -1;
    }

    return 0;
}

int al_database_get_acc_cal(uint8_t * buf, size_t len)
{
    int rc = 0;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid params\n");
        return  -1;
    }

    rc = eflash_read(FLASH_DEVICE_IDX, AGMCAL_ACC_START, (uint8_t*)buf, len);
    if ( rc )
    {
        debug_printf("unable to read accelerometer calibration\n");
        return -1;
    }

    return 0;
}

int al_database_get_gyr_cal(uint8_t * buf, size_t len)
{
    int rc = 0;

    // sanity check
    if ( ! buf || ! len )
    {
        debug_printf("invalid params\n");
        return  -1;
    }

    rc = eflash_read(FLASH_DEVICE_IDX, AGMCAL_GYR_START, (uint8_t*)buf, len);
    if ( rc )
    {
        debug_printf("unable to read gyroscope calibration\n");
        return -1;
    }

    return 0;
}

