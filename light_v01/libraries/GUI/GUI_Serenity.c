/**
  * @file:  GUI_Serenity.c
  *
  * @date   15/02/2012
  *
  * @description Library providing helpful GUI functions
  *
  * @platform    platform agnostic
  **/
//-------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------
#include <BUTTON.h>
#include <al_tools.h>

#include "Inc/GUI_Serenity.h"

//-------------------------------------------------------------------
// Variables
//-------------------------------------------------------------------
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_maison;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconN;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconS;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconE;
extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_13a_iconW;

const GUI_POINT GUI_toward_arrow[6] = {
        {  0  * HIGHRES_FACTOR, 0  * HIGHRES_FACTOR},
        { -10 * HIGHRES_FACTOR, 20 * HIGHRES_FACTOR},
        { -5  * HIGHRES_FACTOR, 20 * HIGHRES_FACTOR},
        {  0  * HIGHRES_FACTOR, 10 * HIGHRES_FACTOR},
        {  5  * HIGHRES_FACTOR, 20 * HIGHRES_FACTOR},
        {  10 * HIGHRES_FACTOR, 20 * HIGHRES_FACTOR},
};

static const GUI_POINT Serenity_arrow[] = {
        { 0  * HIGHRES_FACTOR,-45  * HIGHRES_FACTOR},
        { 16  * HIGHRES_FACTOR, 0  * HIGHRES_FACTOR},
        { 14  * HIGHRES_FACTOR, 5  * HIGHRES_FACTOR},
        {  7  * HIGHRES_FACTOR, 5  * HIGHRES_FACTOR},
        {  9  * HIGHRES_FACTOR, 0  * HIGHRES_FACTOR},
        {  0  * HIGHRES_FACTOR,-23 * HIGHRES_FACTOR},
        { -10 * HIGHRES_FACTOR, 0  * HIGHRES_FACTOR},
        { -8  * HIGHRES_FACTOR, 5  * HIGHRES_FACTOR},
        { -15 * HIGHRES_FACTOR, 5  * HIGHRES_FACTOR},
        { -17 * HIGHRES_FACTOR, 0  * HIGHRES_FACTOR},
};
static GUI_POINT Serenity_arrow_Rotated[GUI_COUNTOF(Serenity_arrow)];

static const GUI_POINT Masking_Poly_1[] = {
        { -7 * HIGHRES_FACTOR ,-48 * HIGHRES_FACTOR },
        {  7 * HIGHRES_FACTOR ,-48 * HIGHRES_FACTOR },
        {  9 * HIGHRES_FACTOR ,-40 * HIGHRES_FACTOR },
        { -9 * HIGHRES_FACTOR ,-40 * HIGHRES_FACTOR },
};
static GUI_POINT Masking_Poly_1_Rotated[GUI_COUNTOF(Masking_Poly_1)];

static const GUI_POINT Masking_Poly_2[] = {
        { -3  * HIGHRES_FACTOR, 41  * HIGHRES_FACTOR},
        {  3  * HIGHRES_FACTOR, 41  * HIGHRES_FACTOR},
        {  3  * HIGHRES_FACTOR, 47  * HIGHRES_FACTOR},
        { -3  * HIGHRES_FACTOR, 47  * HIGHRES_FACTOR},
};
static GUI_POINT Masking_Poly_2_Rotated[GUI_COUNTOF(Masking_Poly_2)];

static const GUI_POINT Small_Dash[] = {
        { -1  * HIGHRES_FACTOR, 63  * HIGHRES_FACTOR},
        {  2  * HIGHRES_FACTOR, 63  * HIGHRES_FACTOR},
        {  2  * HIGHRES_FACTOR, 55  * HIGHRES_FACTOR},
        { -1  * HIGHRES_FACTOR, 55  * HIGHRES_FACTOR},
};
static GUI_POINT Small_Dash_Rotated[GUI_COUNTOF(Small_Dash)];

static const GUI_POINT Big_Dash[] = {
        { -3  * HIGHRES_FACTOR, 63  * HIGHRES_FACTOR},
        {  4  * HIGHRES_FACTOR, 63  * HIGHRES_FACTOR},
        {  4  * HIGHRES_FACTOR, 55  * HIGHRES_FACTOR},
        { -3  * HIGHRES_FACTOR, 55  * HIGHRES_FACTOR},
};
static GUI_POINT Big_Dash_Rotated[GUI_COUNTOF(Big_Dash)];

//-------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------
void _cbBk(WM_MESSAGE * pMsg);
void _draw_ibb_arrived_moving_circle(void);

//-------------------------------------------------------------------
// Public API
//-------------------------------------------------------------------
bool GUI_init(void)
{
    BUTTON_SetReactOnLevel();

    /* set background callback */
    WM_SetCallback(WM_HBKWIN, _cbBk);

    return true;
}

//-------------------------------------------------------------------
void GUI_transparent_button_cb(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
            break;
        default:
            BUTTON_Callback(pMsg);
    }
}

//-------------------------------------------------------------------
void GUI_Switch_Screens(WM_HWIN hide, WM_HWIN show)
{
    WM_HideWindow(hide);
    WM_ShowWindow(show);
    WM_Invalidate(hide);
    WM_Invalidate(show);
}

//-------------------------------------------------------------------
void GUI_Draw_MenuBar_Dots(uint8_t numberOfDots, uint8_t PositionOfCurrentDot)
{
    const uint8_t dotRadius = 4;
    const uint8_t dotYPos    = 7;
    const uint8_t houseYPos  = 2;

    // Could be optimised
    switch(numberOfDots)
    {
        case 1:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2 - 14, houseYPos);
            GUI_SetColor(GUI_WHITE);
            GUI_AA_FillCircle(LCD_GetXSize()/2+7,dotYPos,dotRadius);
            break;
        case 2:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2-20, houseYPos);
            if(PositionOfCurrentDot == 1) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 2) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14,dotYPos,dotRadius);
            break;
        case 3:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2-27, houseYPos);
            if(PositionOfCurrentDot == 1) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-7,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 2) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+7,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 3) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+7+14,dotYPos,dotRadius);
            break;
        case 4:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2-14-20, houseYPos);
            if(PositionOfCurrentDot == 1) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 2) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 3) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 4) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14+14,dotYPos,dotRadius);
            break;
        case 5:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2-27-14, houseYPos);
            if(PositionOfCurrentDot == 1) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-7-14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 2) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-7,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 3) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+7,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 4) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+7+14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 5) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+7+14+14,dotYPos,dotRadius);
            break;
        case 6:
            GUI_DrawBitmap(&bmcran_Serenity_13a_maison, LCD_GetXSize()/2-14-14-20, houseYPos);
            if(PositionOfCurrentDot == 1) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-14-14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 2) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2-14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 3) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 4) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 5) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14+14,dotYPos,dotRadius);
            if(PositionOfCurrentDot == 6) GUI_SetColor(GUI_WHITE);
            else                          GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
            GUI_AA_FillCircle(LCD_GetXSize()/2+14+14+14,dotYPos,dotRadius);
            break;
        default :
            break;
    }

}

//-------------------------------------------------------------------
void GUI_draw_quality_bars(int quality,int x0, int y0)
{
    GUI_SetColor(GUI_WHITE);
    switch(quality)
    {
        case 1:
            GUI_FillRect((x0   ),(y0+6 ),(x0+2 ),(y0+12));
            GUI_FillRect((x0+5 ),(y0+11),(x0+7 ),(y0+12));
            GUI_FillRect((x0+10),(y0+11),(x0+12),(y0+12));
            break;
        case 2:
            GUI_FillRect((x0   ),(y0+6 ),(x0+2 ),(y0+12));
            GUI_FillRect((x0+5 ),(y0+3 ),(x0+7 ),(y0+12));
            GUI_FillRect((x0+10),(y0+11),(x0+12),(y0+12));
            break;
        case 3:
            GUI_FillRect((x0   ),(y0+6),(x0+2 ),(y0+12 ));
            GUI_FillRect((x0+5 ),(y0+3),(x0+7 ),(y0+12 ));
            GUI_FillRect((x0+10),(y0  ),(x0+12),(y0+12 ));
            break;
        default :
            GUI_FillRect((x0   ),(y0+11),(x0+2 ),(y0+12));
            GUI_FillRect((x0+5 ),(y0+11),(x0+7 ),(y0+12));
            GUI_FillRect((x0+10),(y0+11),(x0+12),(y0+12));
    }
}

//-------------------------------------------------------------------
void GUI_DrawCompass(uint16_t WatchHeading, uint16_t BoatHeading,
                     bool arrived, int ibb_status, bool blinker)
{
    float radWatchHeading = deg2rad((float)WatchHeading);
    float radBoatHeading  = deg2rad((float)BoatHeading);

    /* draw compass background */
    GUI_SetColor(GUI_MAKE_COLOR(0x00003652)); // blue
    GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,70 * HIGHRES_FACTOR);
    GUI_SetColor(GUI_WHITE);
    GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,46 * HIGHRES_FACTOR);
    GUI_SetColor(GUI_MAKE_COLOR(0x00003652)); // blue
    GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,43 * HIGHRES_FACTOR);

    // IllBeBack algorithm is lost
    if (ibb_status == 2)
    {
        GUI_SetColor(GUI_BLACK);
        GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,37 * HIGHRES_FACTOR);
    }
    else if (arrived)
    {
        GUI_SetColor(GUI_BLACK);
        GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,37 * HIGHRES_FACTOR);
        // if we are at destination, draw moving ring
        _draw_ibb_arrived_moving_circle();
    }
    else if((ibb_status == 1) && blinker)
    {
        GUI_SetColor(GUI_BLACK);
        GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,37 * HIGHRES_FACTOR);
    }
    else
    {
        GUI_SetColor(GUI_BLACK);
        GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,37 * HIGHRES_FACTOR);

        /* cut white circle */
        GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
        GUI_RotatePolygon(Masking_Poly_1_Rotated, Masking_Poly_1, countof(Masking_Poly_1_Rotated),radBoatHeading);
        GUI_AA_FillPolygon(Masking_Poly_1_Rotated, countof(Masking_Poly_1_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
        GUI_SetColor(GUI_MAKE_COLOR(0x00003652));
        GUI_RotatePolygon(Masking_Poly_2_Rotated, Masking_Poly_2, countof(Masking_Poly_2_Rotated),radBoatHeading);
        GUI_AA_FillPolygon(Masking_Poly_2_Rotated, countof(Masking_Poly_2_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);

        /* draw Compass arrow */
        GUI_SetColor(GUI_WHITE);
        GUI_RotatePolygon(Serenity_arrow_Rotated, Serenity_arrow, countof(Serenity_arrow_Rotated),radBoatHeading);
        GUI_AA_FillPolygon(Serenity_arrow_Rotated, countof(Serenity_arrow_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
        GUI_AA_FillCircle(80 * HIGHRES_FACTOR,100 * HIGHRES_FACTOR,4 * HIGHRES_FACTOR);
    }

    /* Draw Dashes */
    GUI_SetColor(GUI_WHITE);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 0.261799388     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 0.523598776     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Big_Dash_Rotated, Big_Dash, countof(Big_Dash_Rotated),      radWatchHeading + 0.785398163     );
    GUI_AA_FillPolygon(Big_Dash_Rotated, countof(Big_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR    );
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 1.047197551     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 1.308996939     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);

    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 1.832595715     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 2.094395102     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Big_Dash_Rotated, Big_Dash, countof(Big_Dash_Rotated),      radWatchHeading + 2.35619449      );
    GUI_AA_FillPolygon(Big_Dash_Rotated, countof(Big_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR    );
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 2.617993878     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 2.879793266     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);

    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 3.403392041     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 3.665191429     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Big_Dash_Rotated, Big_Dash, countof(Big_Dash_Rotated),      radWatchHeading + 3.926990817     );
    GUI_AA_FillPolygon(Big_Dash_Rotated, countof(Big_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR    );
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 4.188790205     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 4.450589593     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);

    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 4.974188368     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 5.235987756     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Big_Dash_Rotated, Big_Dash, countof(Big_Dash_Rotated),      radWatchHeading + 5.497787144     );
    GUI_AA_FillPolygon(Big_Dash_Rotated, countof(Big_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR    );
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 5.759586532     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);
    GUI_RotatePolygon(Small_Dash_Rotated, Small_Dash, countof(Small_Dash_Rotated),radWatchHeading + 6.021385919     );
    GUI_AA_FillPolygon(Small_Dash_Rotated, countof(Small_Dash_Rotated),80 * HIGHRES_FACTOR ,100 * HIGHRES_FACTOR);

    /* draw images N S E W */
    GUI_DrawBitmap(&bmcran_Serenity_13a_iconE, 58*cos(-radWatchHeading              ) + 73, 58*sin(-radWatchHeading              ) + 93);
    GUI_DrawBitmap(&bmcran_Serenity_13a_iconS, 58*cos(-radWatchHeading + 1.570796327) + 73, 58*sin(-radWatchHeading + 1.570796327) + 93);
    GUI_DrawBitmap(&bmcran_Serenity_13a_iconW, 58*cos(-radWatchHeading + 3.141592654) + 73, 58*sin(-radWatchHeading + 3.141592654) + 93);
    GUI_DrawBitmap(&bmcran_Serenity_13a_iconN, 58*cos(-radWatchHeading + 4.71238898 ) + 73, 58*sin(-radWatchHeading + 4.71238898 ) + 93);

}

//-------------------------------------------------------------------
// Private API
//-------------------------------------------------------------------
void _cbBk(WM_MESSAGE * pMsg)
{
    switch (pMsg->MsgId)
    {
        case WM_PAINT:
        {
            GUI_SetBkColor(GUI_MAKE_COLOR(0x00003652));
            GUI_Clear();
            break;
        }
        default:
        {
            WM_DefaultProc(pMsg);
            break;
        }
    }
}

//-------------------------------------------------------------------
void _draw_ibb_arrived_moving_circle(void)
{
    static uint8_t animation_counter = 0;
    // draw background white circle (external diameter) (max 37)
    GUI_SetColor(GUI_WHITE);
    GUI_AA_FillCircle(80  * HIGHRES_FACTOR,
            100 * HIGHRES_FACTOR,
            (4+(animation_counter/2)) * HIGHRES_FACTOR);
    // draw inner black circle (internal diameter) (max 27)
    GUI_SetColor(GUI_BLACK);
    GUI_AA_FillCircle(80  * HIGHRES_FACTOR,
            100 * HIGHRES_FACTOR,
            (animation_counter/2) * HIGHRES_FACTOR);
    if(animation_counter < 60){animation_counter++;}
    else                      {animation_counter=0;}
}

