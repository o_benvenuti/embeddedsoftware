/*
 * GUI_Serenity.h
 *
 *  Created on: 15 fevr. 2018
 *      Author: AixSonic
 */

#ifndef GUI_SERENITY_H_
#define GUI_SERENITY_H_

//-------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------
#include <stdio.h>
#include <stdbool.h>
#include <WM.h>

//-------------------------------------------------------------------
// Defines
//-------------------------------------------------------------------
// X helper macros
// full X size
#define X_SIZE   (LCD_GetXSize())
// 1/2
#define X_1_2 (LCD_GetXSize()/2)
// 1/3
#define X_1_3 (LCD_GetXSize()/3)
#define X_2_3 (2*X_1_3)
// 1/4
#define X_1_4 (LCD_GetXSize()/4)
#define X_2_4 (X_1_2)
#define X_3_4 (3*X_1_4)
// 1/6
#define X_1_6 (LCD_GetXSize()/6)
#define X_2_6 (X_1_3)
#define X_3_6 (X_1_2)
#define X_4_6 (X_2_3)
#define X_5_6 (5*X_1_6)

// Y helper macros
// full Y size
#define Y_SIZE   (LCD_GetYSize())
// 1/2
#define Y_1_2 (LCD_GetYSize()/2)
// 1/3
#define Y_1_3 (LCD_GetYSize()/3)
#define Y_2_3 (2*Y_1_3)
// 1/4
#define Y_1_4 (LCD_GetYSize()/4)
#define Y_2_4 (Y_1_2)
#define Y_3_4 (3*Y_1_4)
// 1/6
#define Y_1_6 (LCD_GetYSize()/6)
#define Y_2_6 (Y_1_3)
#define Y_3_6 (Y_1_2)
#define Y_4_6 (Y_2_3)
#define Y_5_6 (5*Y_1_6)

#define WM_HWIN GUI_HWIN

/* High resolution factor */
#define HIGHRES_FACTOR  2

#define GUI_GRAPHDATA_LEN       144

#define countof(Array) (sizeof(Array) / sizeof(Array[0]))

//-------------------------------------------------------------------
// Public constant definitions
//-------------------------------------------------------------------
extern const GUI_POINT GUI_toward_arrow[6];
extern GUI_CONST_STORAGE GUI_BITMAP bmicon_nofly;

//-------------------------------------------------------------------
// API
//-------------------------------------------------------------------
void GUI_transparent_button_cb(WM_MESSAGE * pMsg);
void GUI_Switch_Screens(WM_HWIN hide, WM_HWIN show);
bool GUI_init(void);
void GUI_Draw_MenuBar_Dots(uint8_t numberOfDots, uint8_t PositionOfCurrentDot);
void GUI_draw_quality_bars(int quality,int x0, int y0);
void GUI_DrawCompass(uint16_t WatchHeading, uint16_t BoatHeading,
                     bool arrived, int ibb_status, bool blinker);

#endif /* GUI_SERENITY_H_ */
