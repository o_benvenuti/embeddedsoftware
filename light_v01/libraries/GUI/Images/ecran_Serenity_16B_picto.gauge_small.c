/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
*                           www.segger.com                           *
**********************************************************************
*                                                                    *
* C-file generated by                                                *
*                                                                    *
*        Bitmap Converter (ST) for emWin V5.40.                      *
*        Compiled Mar 17 2017, 15:33:27                              *
*                                                                    *
*        (c) 1998 - 2017 Segger Microcontroller GmbH & Co. KG        *
*                                                                    *
**********************************************************************
*                                                                    *
* Source file: cran_Serenity_16B_pictogauge_small                    *
* Dimensions:  40 * 36                                               *
* NumColors:   32bpp: 16777216 + 256                                 *
*                                                                    *
**********************************************************************
*/

#include <GUI.h>
#include <stdlib.h>


#ifndef GUI_CONST_STORAGE
  #define GUI_CONST_STORAGE const
#endif

extern GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictogauge_small;

static GUI_CONST_STORAGE unsigned long _accran_Serenity_16B_pictogauge_small[] = {
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x9BFEFEFE, 0xE7FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFCFFFFFF, 0xE3FEFEFE, 0xCEFEFEFE, 0xCBFEFEFE, 0xCBFEFEFE, 
        0xCCFFFFFF, 0xE1FFFFFF, 0xFBFFFFFF, 0xFF000000, 0xFF000000, 0xC6FEFEFE, 0x15FEFEFE, 0xF1FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFDFFFFFF, 0xD1FEFEFE, 0x8DFEFEFE, 0x44FEFEFE, 0x0AFEFEFE, 0x03FEFEFE, 0x01FEFEFE, 0x01FEFEFE, 0x01FEFEFE, 
        0x01FEFEFE, 0x02FEFEFE, 0x09FEFEFE, 0x3FFFFFFF, 0x80FEFEFE, 0x1BFEFEFE, 0x10FEFEFE, 0xFAFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF6FEFEFE, 0xA8FEFEFE, 0x2FFEFEFE, 0x03FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x02FEFEFE, 0x16FEFEFE, 0x27FEFEFE, 0x31FEFEFE, 0x31FEFEFE, 
        0x28FEFEFE, 0x18FEFEFE, 0x03FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x07FEFEFE, 0x9EFEFEFE, 0xF0FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFDFFFFFF, 0xBBFFFFFF, 0x28FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x11FEFEFE, 0x51FEFEFE, 0xA2FEFEFE, 0xE4FEFEFE, 0xF5FFFFFF, 0xFCFFFFFF, 0xFF000000, 0xFF000000, 
        0xFCFFFFFF, 0xF6FEFEFE, 0xE6FEFEFE, 0x4BFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x1AFEFEFE, 0xA5FEFEFE, 0xFBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF0FFFFFF, 0x5EFEFEFE, 0x03FEFEFE, 0x00FFFFFF, 0x1CFEFEFE, 0x8CFEFEFE, 0xEEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xBDFEFEFE, 0x05FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x17FEFEFE, 0x22FEFEFE, 0x00FFFFFF, 0x01FEFEFE, 0x49FEFEFE, 0xE7FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xDCFEFEFE, 0x29FEFEFE, 0x00FFFFFF, 0x06FEFEFE, 0x74FEFEFE, 0xF0FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xE5FEFEFE, 0x18FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x3DFEFEFE, 0xF5FFFFFF, 0x8CFEFEFE, 0x0DFEFEFE, 0x00FFFFFF, 0x1CFEFEFE, 0xCCFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xD0FEFEFE, 0x1AFEFEFE, 0x00FFFFFF, 0x1CFEFEFE, 0xC3FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFBFFFFFF, 0x43FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x4FFEFEFE, 0xFF000000, 0xFF000000, 0xD9FEFEFE, 0x2DFEFEFE, 0x00FFFFFF, 0x10FEFEFE, 0xBCFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xDEFEFEFE, 0x1AFEFEFE, 0x00FFFFFF, 0x31FEFEFE, 0xE5FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0x82FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x53FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xF1FEFEFE, 0x46FEFEFE, 0x00FFFFFF, 0x11FEFEFE, 0xCFFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xF3FFFFFF, 0x30FEFEFE, 0x00FFFFFF, 0x30FEFEFE, 0xF0FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xBEFEFEFE, 
        0x02FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x6AFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF7FFFFFF, 0x46FEFEFE, 0x00FFFFFF, 0x1FFEFEFE, 0xEBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0x67FEFEFE, 0x00FFFFFF, 0x1CFEFEFE, 0xE7FFFFFF, 0xEFFFFFFF, 0x69FEFEFE, 0xECFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xE4FEFEFE, 0x17FEFEFE, 
        0x00FFFFFF, 0x00FFFFFF, 0x01FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x73FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF0FFFFFF, 0x2AFEFEFE, 0x00FFFFFF, 0x4BFEFEFE, 0xFCFFFFFF, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xC0FEFEFE, 0x02FEFEFE, 0x07FEFEFE, 0xC0FEFEFE, 0xF7FFFFFF, 0x43FEFEFE, 0x00FFFFFF, 0x32FEFEFE, 0xEBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF8FEFEFE, 0x3FFFFFFF, 0x00FFFFFF, 
        0x00FFFFFF, 0x00FFFFFF, 0x57FEFEFE, 0x09FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x84FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xD5FEFEFE, 0x0BFEFEFE, 0x02FEFEFE, 0xAAFFFFFF, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFCFFFFFF, 0x32FEFEFE, 0x00FFFFFF, 0x6AFEFEFE, 0xFEFFFFFF, 0x7EFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x32FEFEFE, 0xF3FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFEFFFFFF, 0x74FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 
        0x00FFFFFF, 0x31FEFEFE, 0xDFFFFFFF, 0x07FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x98FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x8AFEFEFE, 0x00FFFFFF, 0x20FEFEFE, 0xF6FEFEFE, 0xFF000000,
  0xFF000000, 0xB6FEFEFE, 0x01FEFEFE, 0x13FEFEFE, 0xEFFFFFFF, 0xD0FEFEFE, 0x07FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x47FEFEFE, 0xFBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xB1FEFEFE, 0x02FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 
        0x11FEFEFE, 0xDDFFFFFF, 0xDEFEFEFE, 0x05FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0xA2FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF3FFFFFF, 0x20FEFEFE, 0x00FFFFFF, 0x9CFEFEFE, 0xFF000000,
  0xFF000000, 0x4AFEFEFE, 0x00FFFFFF, 0x75FEFEFE, 0xFDFFFFFF, 0x45FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x20FEFEFE, 0xEFFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xDBFEFEFE, 0x11FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x02FEFEFE, 
        0xA7FEFEFE, 0xFF000000, 0xD7FFFFFF, 0x03FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0xB8FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x95FEFEFE, 0x00FFFFFF, 0x2AFEFEFE, 0xFDFFFFFF,
  0xEAFEFEFE, 0x03FEFEFE, 0x05FEFEFE, 0xE5FEFEFE, 0xD2FEFEFE, 0x04FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x03FEFEFE, 0xBBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF4FEFEFE, 0x34FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x6CFEFEFE, 
        0xFF000000, 0xFF000000, 0xCCFFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0xC5FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xEBFFFFFF, 0x0FFFFFFF, 0x03FEFEFE, 0xCFFFFFFF,
  0xA5FEFEFE, 0x00FFFFFF, 0x38FEFEFE, 0xFEFFFFFF, 0x77FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x3FFFFFFF, 0xFEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFEFFFFFF, 0x69FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x32FEFEFE, 0xF7FFFFFF, 
        0xFF000000, 0xFF000000, 0xC6FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0xD4FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x50FEFEFE, 0x00FFFFFF, 0x84FEFEFE,
  0x69FEFEFE, 0x00FFFFFF, 0x7BFEFEFE, 0xFDFFFFFF, 0x1FFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x01FEFEFE, 0xB6FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xAAFFFFFF, 0x03FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x11FEFEFE, 0xDEFEFEFE, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xB4FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x05FEFEFE, 0xDCFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xA1FEFEFE, 0x00FFFFFF, 0x40FEFEFE,
  0x37FEFEFE, 0x00FFFFFF, 0xB4FEFEFE, 0xE2FEFEFE, 0x06FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x0FFFFFFF, 0xF3FFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xD8FEFEFE, 0x0DFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x04FEFEFE, 0xADFEFEFE, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xA8FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x07FEFEFE, 0xDFFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xD8FEFEFE, 0x00FFFFFF, 0x0FFFFFFF,
  0x11FEFEFE, 0x00FFFFFF, 0xE6FEFEFE, 0xC6FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x3BFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x4FFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x77FFFFFF, 0xFF000000, 0xFCFFFFFF, 0xF9FFFFFF, 
        0xFF000000, 0xFF000000, 0x9EFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x0DFEFEFE, 0xE9FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF1FEFEFE, 0x0DFEFEFE, 0x00FFFFFF,
  0x00FFFFFF, 0x04FEFEFE, 0xFCFFFFFF, 0xABFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x60FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x3BFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x2CFEFEFE, 0xF9FFFFFF, 0xCAFEFEFE, 0x19FEFEFE, 0x10FEFEFE, 
        0xAFFFFFFF, 0xFF000000, 0x8BFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x10FEFEFE, 0xEEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF6FEFEFE, 0x1CFEFEFE, 0x00FFFFFF,
  0x00FFFFFF, 0x11FEFEFE, 0xFCFFFFFF, 0x9EFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x6AFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x23FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x52FEFEFE, 0xFF000000, 0x4CFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 
        0x34FEFEFE, 0xFEFFFFFF, 0x84FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x14FEFEFE, 0xF4FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFAFFFFFF, 0x2AFEFEFE, 0x00FFFFFF,
  0x00FFFFFF, 0x0CFEFEFE, 0xFCFFFFFF, 0xA1FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x69FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x1AFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x5CFEFEFE, 0xFF000000, 0x76FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 
        0x56FEFEFE, 0xFF000000, 0x6FFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x19FEFEFE, 0xFDFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF8FEFEFE, 0x22FEFEFE, 0x00FFFFFF,
  0x08FEFEFE, 0x00FFFFFF, 0xF4FEFEFE, 0xB9FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x4EFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x65FEFEFE, 0x16FEFEFE, 0x00FFFFFF, 0x70FEFEFE, 0xFF000000, 0xF8FEFEFE, 0x92FEFEFE, 0x85FEFEFE, 
        0xF3FFFFFF, 0xE3FEFEFE, 0x17FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x1DFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF5FFFFFF, 0x17FEFEFE, 0x00FFFFFF,
  0x25FEFEFE, 0x00FFFFFF, 0xCBFEFEFE, 0xD5FEFEFE, 0x03FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x20FEFEFE, 0xFEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF3FFFFFF, 0xB3FEFEFE, 0xA8FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFAFFFFFF, 0x42FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x67FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xEAFEFEFE, 0x02FEFEFE, 0x02FEFEFE,
  0x52FEFEFE, 0x00FFFFFF, 0x94FEFEFE, 0xF4FEFEFE, 0x0CFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x06FEFEFE, 0xDAFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0x7EFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x2DFEFEFE, 0xF4FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xBDFEFEFE, 0x00FFFFFF, 0x26FEFEFE,
  0x89FEFEFE, 0x00FFFFFF, 0x55FFFFFF, 0xFF000000, 0x4EFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x7FFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFEFFFFFF, 
        0x80FEFEFE, 0x21FEFEFE, 0x00FFFFFF, 0x0EFEFEFE, 0xD9FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x78FEFEFE, 0x00FFFFFF, 0x60FEFEFE,
  0xCCFFFFFF, 0x00FFFFFF, 0x18FEFEFE, 0xF8FEFEFE, 0xABFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x12FEFEFE, 0xEEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFEFFFFFF, 0xC1FEFEFE, 0xB6FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF9FFFFFF, 0x25FEFEFE, 0x00FFFFFF, 0xA9FEFEFE,
  0xFDFFFFFF, 0x23FEFEFE, 0x00FFFFFF, 0xABFEFEFE, 0xF2FEFEFE, 0x17FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x6DFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF6FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xCBFEFEFE, 0x02FEFEFE, 0x0DFEFEFE, 0xF0FFFFFF,
  0xFF000000, 0x8AFEFEFE, 0x00FFFFFF, 0x39FEFEFE, 0xFCFFFFFF, 0x94FEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x01FEFEFE, 0xB0FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xCCFFFFFF, 0x1AFEFEFE, 0xB2FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFEFFFFFF, 0x56FEFEFE, 0x00FFFFFF, 0x60FEFEFE, 0xFF000000,
  0xFF000000, 0xEAFEFEFE, 0x0EFEFEFE, 0x01FEFEFE, 0xB5FEFEFE, 0xF6FEFEFE, 0x2CFEFEFE, 0x00FFFFFF, 0x00FFFFFF, 0x00FFFFFF, 0x0FFFFFFF, 0xEDFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xF9FFFFFF, 0x25FEFEFE, 0x00FFFFFF, 0x02FEFEFE, 0x66FEFEFE, 0xF0FFFFFF, 0xFF000000, 0xCAFEFEFE, 0x04FEFEFE, 0x08FEFEFE, 0xD7FFFFFF, 0xFF000000,
  0xFF000000, 0xFF000000, 0x83FEFEFE, 0x00FFFFFF, 0x24FEFEFE, 0xF2FEFEFE, 0xD0FEFEFE, 0x0BFEFEFE, 0x00FFFFFF, 0x05FEFEFE, 0x9FFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xC1FEFEFE, 0x0EFEFEFE, 0x00FFFFFF, 0x07FEFEFE, 0xC5FEFEFE, 0xF7FFFFFF, 0x32FEFEFE, 0x00FFFFFF, 0x5FFFFFFF, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xF3FFFFFF, 0x23FEFEFE, 0x00FFFFFF, 0x61FEFEFE, 0xFDFFFFFF, 0xA4FEFEFE, 0x0AFEFEFE, 0x9FFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xBFFFFFFF, 0x0DFEFEFE, 0x8AFEFEFE, 0xFEFFFFFF, 0x7EFEFEFE, 0x00FFFFFF, 0x15FEFEFE, 0xE7FFFFFF, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xC1FEFEFE, 0x06FEFEFE, 0x02FEFEFE, 0x94FEFEFE, 0xFDFFFFFF, 0xD9FEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xDEFEFEFE, 0xFDFFFFFF, 0xA9FEFEFE, 0x03FEFEFE, 0x04FEFEFE, 0xAAFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0x8AFEFEFE, 0x01FEFEFE, 0x0CFEFEFE, 0xDEFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xE9FEFEFE, 0x0FFFFFFF, 0x01FEFEFE, 0x72FEFEFE, 0xFEFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000,
  0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFDFFFFFF, 0x6DFEFEFE, 0x9DFEFEFE, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 
        0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000, 0xB8FEFEFE, 0x64FEFEFE, 0xFBFFFFFF, 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000
};

GUI_CONST_STORAGE GUI_BITMAP bmcran_Serenity_16B_pictogauge_small = {
  40, // xSize
  36, // ySize
  160, // BytesPerLine
  32, // BitsPerPixel
  (unsigned char *)_accran_Serenity_16B_pictogauge_small,  // Pointer to picture data
  NULL,  // Pointer to palette
  GUI_DRAW_BMP8888
};

/*************************** End of file ****************************/
