/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
*                           www.segger.com                           *
**********************************************************************
*                                                                    *
* C-file generated by                                                *
*                                                                    *
*        Bitmap Converter (ST) for emWin V5.40.                      *
*        Compiled Mar 17 2017, 15:33:27                              *
*                                                                    *
*        (c) 1998 - 2017 Segger Microcontroller GmbH & Co. KG        *
*                                                                    *
**********************************************************************
*                                                                    *
* Source file: ButtonCompassCalibration                              *
* Dimensions:  30 * 36                                               *
* NumColors:   65536 colors + 8 bit alpha channel                    *
*                                                                    *
**********************************************************************
*/

#include <GUI.h>
#include <stdlib.h>


#ifndef GUI_CONST_STORAGE
  #define GUI_CONST_STORAGE const
#endif

extern GUI_CONST_STORAGE GUI_BITMAP bmButtonCompassCalibration;

static GUI_CONST_STORAGE unsigned char _acButtonCompassCalibration[] = {
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xC5,0xFF,0xFF, 0xF1,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xC3,0xFF,0xFF, 0x25,0xFF,0xFF, 0x3C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xD9,0xFF,0xFF, 0x24,0xFF,0xFF, 0x06,0xFF,0xFF, 0x5C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xC3,0xFF,0xFF, 0x7C,0xFF,0xFF, 0x40,0xFF,0xFF, 0x28,0xFF,0xFF, 0x0C,0xFF,0xFF, 
        0x0C,0xFF,0xFF, 0x28,0xFF,0xFF, 0x3B,0xFF,0xFF, 0x10,0xFF,0xFF, 0x03,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF7,0xFF,0xFF, 0x8F,0xFF,0xFF, 0x18,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xF7,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xAF,0xFF,0xFF, 0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x2C,0xFF,0xFF, 0x7C,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xDF,0xFF,0xFF, 0xF3,0xFF,0xFF, 
        0xF3,0xFF,0xFF, 0x81,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x1C,0xFF,0xFF, 0xAF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x83,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x30,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xD3,0xFF,0xFF, 0x08,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x06,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7F,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x6C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x04,0xFF,0xFF, 0x8B,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF7,0xFF,0xFF, 
        0x2C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x25,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x33,0xFF,0xFF, 0x8B,0xFF,0xFF, 0x04,0xFF,0xFF, 0x00,0xFF,0xFF, 0x5C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x83,0xFF,0xFF, 0x00,0xFF,0xFF, 0x10,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x6C,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF, 0xA1,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x40,0xFF,0xFF, 0xFF,0x00,0x00, 0xBB,0xFF,0xFF, 0x0C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7F,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xAF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x04,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xB3,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x97,0xFF,0xFF, 0xA7,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x50,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x04,0xFF,0xFF, 0x00,0xFF,0xFF, 0xAF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xF7,0xFF,0xFF, 0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 0x8B,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xE7,0xFF,0xFF, 0x14,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x50,0xFF,0xFF, 0xFF,0x00,0x00, 0x9F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x60,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x97,0xFF,0xFF, 0x00,0xFF,0xFF, 0x20,0xFF,0xFF, 0xF7,0xFF,0xFF, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0x8F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x30,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x48,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x1C,0xFF,0xFF, 0xEB,0xFF,0xFF, 0xFF,0x00,0x00, 0x8B,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x6C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x38,0xFF,0xFF, 0x00,0xFF,0xFF, 0x8F,0xFF,0xFF, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0x18,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x8F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x04,0xFF,0xFF, 
        0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7F,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x1C,0xFF,0xFF, 0xFF,0x00,0x00,
  0xC3,0xFF,0xFF, 0x00,0xFF,0xFF, 0x2C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xD0,0xFF,0xFF, 0x04,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x78,0xFF,0xFF, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x70,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x87,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x2C,0xFF,0xFF, 0x00,0xFF,0xFF, 0xC3,0xFF,0xFF,
  0x7C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF5,0xFF,0xFF, 0x2B,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x34,0xFF,0xFF, 0xFB,0xFF,0xFF, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x60,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x9F,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7C,0xFF,0xFF,
  0x40,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x29,0xFF,0xFF, 0x02,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF, 0xDB,0xFF,0xFF, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x54,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xA3,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBB,0xFF,0xFF, 0x00,0xFF,0xFF, 0x40,0xFF,0xFF,
  0x28,0xFF,0xFF, 0x00,0xFF,0xFF, 0xDF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x13,0xFF,0xFF, 0x01,0xFF,0xFF, 0x00,0xFF,0xFF, 0x9B,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x40,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xDF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x28,0xFF,0xFF,
  0x0C,0xFF,0xFF, 0x00,0xFF,0xFF, 0xF3,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xEA,0xFF,0xFF, 0x05,0xFF,0xFF, 0x00,0xFF,0xFF, 0x08,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x60,0xFF,0xFF, 
        0x20,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x38,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF3,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF,
  0x0C,0xFF,0xFF, 0x00,0xFF,0xFF, 0xF3,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xE0,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x20,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x20,0xFF,0xFF, 
        0x60,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x13,0xFF,0xFF, 0x00,0xFF,0xFF, 0x02,0xFF,0xFF, 0xE0,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF3,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF,
  0x28,0xFF,0xFF, 0x00,0xFF,0xFF, 0xDF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xD3,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x24,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x01,0xFF,0xFF, 0x0D,0xFF,0xFF, 0xE5,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xDF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x28,0xFF,0xFF,
  0x40,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x40,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xF3,0xFF,0xFF, 0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 0x06,0xFF,0xFF, 0x24,0xFF,0xFF, 0xFD,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBB,0xFF,0xFF, 0x00,0xFF,0xFF, 0x40,0xFF,0xFF,
  0x78,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x40,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0x6C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x1F,0xFF,0xFF, 0xDA,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x78,0xFF,0xFF,
  0xC3,0xFF,0xFF, 0x00,0xFF,0xFF, 0x2C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x9F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x5C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xB0,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x2C,0xFF,0xFF, 0x00,0xFF,0xFF, 0xC3,0xFF,0xFF,
  0xFF,0x00,0x00, 0x14,0xFF,0xFF, 0x00,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x9F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x60,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF3,0xFF,0xFF, 
        0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x54,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x14,0xFF,0xFF, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0x87,0xFF,0xFF, 0x00,0xFF,0xFF, 0x30,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x8B,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x70,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x6C,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x14,0xFF,0xFF, 0xEB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x38,0xFF,0xFF, 0x00,0xFF,0xFF, 0x87,0xFF,0xFF, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xF7,0xFF,0xFF, 0x1C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x8B,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7F,0xFF,0xFF, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0xAB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x97,0xFF,0xFF, 0x00,0xFF,0xFF, 0x1C,0xFF,0xFF, 0xF7,0xFF,0xFF, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xAF,0xFF,0xFF, 0x00,0xFF,0xFF, 0x04,0xFF,0xFF, 0xBB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x78,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x83,0xFF,0xFF, 0xF3,0xFF,0xFF, 0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x55,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x04,0xFF,0xFF, 0x00,0xFF,0xFF, 0xAF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x10,0xFF,0xFF, 0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0x60,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x9F,0xFF,0xFF, 0x6C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x1A,0xFF,0xFF, 0xEC,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x0C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7C,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x5C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x04,0xFF,0xFF, 0x97,0xFF,0xFF, 0x60,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x77,0xFF,0xFF, 0x00,0xFF,0xFF, 0x01,0xFF,0xFF, 0x0C,0xFF,0xFF, 
        0xBF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x97,0xFF,0xFF, 0x04,0xFF,0xFF, 0x00,0xFF,0xFF, 0x54,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x7F,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0E,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x17,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0D,0xFF,0xFF, 0x94,0xFF,0xFF, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xBF,0xFF,0xFF, 0x38,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x7C,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xAF,0xFF,0xFF, 0x20,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x06,0xFF,0xFF, 0x5C,0xFF,0xFF, 0xF1,0xFF,0xFF, 
        0xF3,0xFF,0xFF, 0xDF,0xFF,0xFF, 0xBB,0xFF,0xFF, 0x7C,0xFF,0xFF, 0x2C,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x1C,0xFF,0xFF, 0xAF,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xF7,0xFF,0xFF, 0x19,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 
        0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x14,0xFF,0xFF, 0x87,0xFF,0xFF, 0xF7,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x1E,0xFF,0xFF, 0x00,0xFF,0xFF, 0x00,0xFF,0xFF, 0x0C,0xFF,0xFF, 0x31,0xFF,0xFF, 0x28,0xFF,0xFF, 0x0C,0xFF,0xFF, 
        0x0C,0xFF,0xFF, 0x28,0xFF,0xFF, 0x40,0xFF,0xFF, 0x78,0xFF,0xFF, 0xC3,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x0A,0xFF,0xFF, 0x01,0xFF,0xFF, 0x0F,0xFF,0xFF, 0x81,0xFF,0xFF, 0xFE,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x00,0xFF,0xFF, 0x13,0xFF,0xFF, 0x76,0xFF,0xFF, 0xFB,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00,
  0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0x81,0xFF,0xFF, 0x5B,0xFF,0xFF, 0xF1,0xFF,0xFF, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 
        0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00, 0xFF,0x00,0x00
};

GUI_CONST_STORAGE GUI_BITMAP bmButtonCompassCalibration = {
  30, // xSize
  36, // ySize
  90, // BytesPerLine
  24, // BitsPerPixel
  (unsigned char *)_acButtonCompassCalibration,  // Pointer to picture data
  NULL,  // Pointer to palette
  GUI_DRAW_BMPA565
};

/*************************** End of file ****************************/
