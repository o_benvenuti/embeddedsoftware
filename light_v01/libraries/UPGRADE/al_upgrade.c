/**
 * @file:  al_upgrade.c
 *
 * @brief
 *
 * @date   18/07/2018
 *
 * <b>Description:</b>\n
 *    Upgrade flash using dualbank flash
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <al_ble.h>
#include <al_upgrade.h>
#include <ds2782_driver.h>
#include <trace.h>

#include <stm32l4xx_hal.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define FLASH_START_BANK1             	((uint32_t)0x08000000)
#define FLASH_START_BANK2             	((uint32_t)0x08080000)
#define USER_FLASH_END_ADDRESS        	((uint32_t)0x08100000)
#define STM32L486_NVIC_CH_NUMBER		82

//----------------------------------------------------------------------
// File scope variables
//----------------------------------------------------------------------
// GCC compliant aligned vector table
__attribute__ ((aligned (512))) uint32_t vector_t[STM32L486_NVIC_CH_NUMBER];

//----------------------------------------------------------------------
// Private API
//----------------------------------------------------------------------
static inline void _relocate_nvic(uint32_t address)
{
    uint32_t i;

    // It is important to make sure the vector
    // table stays put when playing with memory mapping
    for (i = 0; i < STM32L486_NVIC_CH_NUMBER; ++i)
    {
        vector_t[i] = *((uint32_t*)(address + (i << 2)));
    }

    SCB->VTOR = (uint32_t)vector_t;
}

//----------------------------------------------------------------------
int al_upgrade_flash_init(uint32_t * dest_address)
{
    uint32_t error;
    int bank_active;
    FLASH_EraseInitTypeDef pEraseInit;
    FLASH_OBProgramInitTypeDef OBConfig;

    // sanity check
    if ( ! dest_address ) {
        return -1;
    }

    // check currently executed bank
    bank_active = READ_BIT(SYSCFG->MEMRMP, SYSCFG_MEMRMP_FB_MODE);
    debug_printf("bank %d currently executed\n", (bank_active ? 2 : 1));

    // get the current configuration
    HAL_FLASHEx_OBGetConfig( &OBConfig );

    // check configuration consistency
    if ( OBConfig.USERConfig & OB_BFB2_ENABLE ) {
        debug_printf("inconsistent used bank regarding boot bank\n");
    }

    // set address to write
    *dest_address = FLASH_START_BANK2;
    debug_printf("write new firmware to 0x%lX\n", *dest_address);

    // unlock in order to erase
    HAL_FLASH_Unlock();

    // select bank to erase
    pEraseInit.TypeErase = FLASH_TYPEERASE_MASSERASE;
    pEraseInit.Banks = bank_active ? FLASH_BANK_1 : FLASH_BANK_2;
    HAL_FLASHEx_Erase(&pEraseInit, &error);

    // unlock flash
    HAL_FLASH_Lock();

    return 0;
}

//----------------------------------------------------------------------
int al_upgrade_flash_write(uint32_t * dst, uint32_t * src, uint32_t size)
{
    int rc = 0;

    // sanity check
    if ( ! dst || ! src ) {
        return -1;
    }

    // unlock the Flash
    HAL_FLASH_Unlock();

    // size must be a multiple of 64 bit
    for ( unsigned int i = 0;
          (i < size / 2) && (*dst <= (USER_FLASH_END_ADDRESS - 8));
          ++i )
    {
        //operation will be done by word
        rc = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, *dst, *((uint64_t *)(src + 2*i)));
        if ( rc != HAL_OK) {
            // debug message
            debug_printf("test console");
            break;
        }

        // check the written value
        // just to be sure, could be removed if never happens
        if (*(uint64_t*)*dst != *(uint64_t *)(src + 2*i)) {
            // flash content doesn't match SRAM content
            rc = -1;
            break;
        }

        // increment FLASH destination address
        *dst += 8;
    }


    // lock the Flash to protect against possible unwanted operation
    HAL_FLASH_Lock();

    return rc;
}

//----------------------------------------------------------------------
void al_upgrade_bank_switch(void)
{
    HAL_StatusTypeDef result;
    FLASH_OBProgramInitTypeDef ob_config;

    HAL_FLASH_Lock();

    // Clear OPTVERR bit set on virgin samples
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR);

    // Get the current configuration
    HAL_FLASHEx_OBGetConfig( &ob_config );

    ob_config.OptionType = OPTIONBYTE_USER;
    ob_config.USERType = OB_USER_BFB2;

    // BANK1 active for boot
    if ((ob_config.USERConfig) & (OB_BFB2_ENABLE)) {
        ob_config.USERConfig = OB_BFB2_DISABLE;
    }
    else {
        ob_config.USERConfig = OB_BFB2_ENABLE;
    }

    // Initiating the modifications
    result = HAL_FLASH_Unlock();

    // program if unlock is successful
    if ( result == HAL_OK ) {
        result = HAL_FLASH_OB_Unlock();

        // program if unlock is successful
        if ((READ_BIT(FLASH->CR, FLASH_CR_OPTLOCK) == RESET)) {
            result = HAL_FLASHEx_OBProgram(&ob_config);
        }

        if (result == HAL_OK) {
            HAL_FLASH_OB_Launch();
        }
    }
}

//----------------------------------------------------------------------
static inline int _wait_characteristic(uint16_t value)
{
    int rc;
    size_t len;
    uint8_t type, tmp[128];

    // wait for expected subscriptions
    for ( int attempt = 0; attempt < 128; ++attempt ) {
        len = sizeof(tmp);
        rc = al_ble_get_notification(&type, &tmp[0], &len);
        if ( rc ) {
            continue;
        }

        // check for client subscribing
        if ( type != AL_BLE_NOTIF_TYPE_REQUEST_CHAR ) continue;

        // TODO can be a way optimized
        if ( tmp[0] == AL_BLE_CONNECTION_HANDLE ) {
            // could be ntohs(*(uint16_t *)&tmp[1])
            if ( (tmp[1] == (value >> 8)) && (tmp[2] == (value & 0xff)) ) {
                return 0;
            }
        }
    }

    return -1;
}

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
void al_upgrade_init(void)
{
    // move vector table in ram to keep interrupt capability
    _relocate_nvic(FLASH_START_BANK1);
}

//----------------------------------------------------------------------
int al_upgrade_process(uint16_t version)
{
    uint8_t BatteryLoadValue = 0;
    uint8_t type, tmp[32];
    uint8_t UART_TxBuffer[32];
    uint16_t packet_received = 0;
    uint32_t dest_addr;
    size_t size;
    int rc, firmware_size = 0;

    // send firmware version
    rc = BAT_LoadValue(&BatteryLoadValue);

    UART_TxBuffer[0] = 0x00;
    UART_TxBuffer[1] = BatteryLoadValue;
    UART_TxBuffer[2] = version >> 8;
    UART_TxBuffer[3] = version;

    // Make sure the phone will be ready to get the message
    HAL_Delay(1000);

    // TODO see why there is no acknowledge here
    rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_FW_VERSION, &UART_TxBuffer[0], 4);

    rc = _wait_characteristic(AL_BLE_CHAR_HANDLE_FWOTA_TYPE);
    if ( rc )
    {
        return -1;
    }

    // init the flash
    rc = al_upgrade_flash_init(&dest_addr);
    if ( rc )
    {
        return -1;
    }

    // send ready message
    memset(&UART_TxBuffer[0], 0x00, 15);
    UART_TxBuffer[15] = 0x52;
    UART_TxBuffer[16] = 0x45;
    UART_TxBuffer[17] = 0x41;
    UART_TxBuffer[18] = 0x44;
    UART_TxBuffer[19] = 0x59;

    // TODO see why there is no acknowledge here
    rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &UART_TxBuffer[0], 20);

    // wait for firmware size
    for ( int attempt = 0; attempt < 256; ++attempt ) {
        size = sizeof(tmp);

        // expect total number of packet
        rc = al_ble_get_notification(&type, &tmp[0], &size);
        if ( rc ) continue;

        // characteristic update
        if ( type != AL_BLE_NOTIF_TYPE_REQUEST_CHAR ) continue;

        if ( tmp[0] == AL_BLE_CONNECTION_HANDLE ) {
            if ( (tmp[1] == 0x80) && (tmp[2] == 0x07) ) {
                firmware_size = (tmp[3] << 8) | tmp[4];
                break;
            }
        }
    }

    // TODO verify that firmware size does not exceed flash size
    // check firmware size
    if ( ! firmware_size )
    {
        return -1;
    }

    int buffer_pos = 0;
    int attempt = 0;
    uint16_t packet_received_lsb, packet_received_msb;
    uint8_t buffer_to_write[16];

    do {
        if ( attempt > 256 )
        {
            // _send_NOK()
            UART_TxBuffer[0] = 0x4B;
            UART_TxBuffer[1] = 0x4F;
            rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &UART_TxBuffer[0], 2);

            return -1;
        }

        // read from source (BLE/UART) and write to other bank
        size = sizeof(tmp);
        rc = al_ble_get_notification(&type, &tmp[0], &size);
        if ( rc )
        {
            ++attempt;
            continue;
        }

        // characteristic update
        if ( type != AL_BLE_NOTIF_TYPE_REQUEST_CHAR )
        {
            ++attempt;
            continue;
        }

        if ( (tmp[0] != AL_BLE_CONNECTION_HANDLE) ||
                (tmp[1] != 0x80) || (tmp[2] != 0x07) )
        {
            ++attempt;
            continue;
        }

        packet_received++;
        packet_received_msb = packet_received >> 8;
        packet_received_lsb = packet_received & 0x00FF;

        // validate packet number
        if ((tmp[size-2] != packet_received_msb) ||
                (tmp[size-1] != packet_received_lsb)  )
        {
            // _send_NOK()
            UART_TxBuffer[0] = 0x4B;
            UART_TxBuffer[1] = 0x4F;
            rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &UART_TxBuffer[0], 2);

            return -1;
        }

        // remove packet number
        size -= 2;

        // write every 20 bytes (protocol packet size)
        for ( unsigned int i = 3; (i < size) ; ++i)
        {
            buffer_to_write[buffer_pos] = tmp[i];
            buffer_pos++;

            if ( buffer_pos == 8 ) {
                al_upgrade_flash_write(&dest_addr, (uint32_t*)&buffer_to_write[0], 2);
                buffer_pos = 0;
            }
        }

        // for the last data check if full buffer can be written
        if ( buffer_pos && (packet_received == firmware_size) )
        {
            // fill with zero to write doubleword
            memset(&buffer_to_write[buffer_pos], 0, (8-buffer_pos));
            al_upgrade_flash_write(&dest_addr, (uint32_t*)&buffer_to_write[0], 2);
        }

        // last packet is valid
        attempt = 0;
    }while( packet_received < firmware_size );

    // send number of received packet
    UART_TxBuffer[0] = packet_received >> 8;
    UART_TxBuffer[1] = packet_received;

    // send received size
    rc = al_ble_set_notification(AL_BLE_CHAR_HANDLE_FWOTA_TX, &UART_TxBuffer[0], 2);

    if ( packet_received == firmware_size ) {
        // switch bank to boot on
        al_upgrade_bank_switch();
        NVIC_SystemReset();
    }

    return 0;
}
