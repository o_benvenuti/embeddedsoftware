#ifndef _AL_UPGRADE_H_
#define _AL_UPGRADE_H_
/**
 * @file:  al_upgrade.h
 *
 * @brief
 *
 * @date   18/07/2018
 *
 * <b>Description:</b>\n
 *    Upgrade program binary using dualbank flash
 *
 * <b>Platform:</b>\n
 *    STM32L486
 *
 */


//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
/**
  * @brief  Init platform regarding upgrade type (i.e dual bank)
  * @param  None
  * @retval None
  **/
void al_upgrade_init(void);

/**
  * @brief  Processing the whole upgrade (BLE, UI, ...)
  * @param  None
  * @retval None
  **/
int al_upgrade_process(uint16_t version);

void al_upgrade_bank_switch(void);
int al_upgrade_flash_write(uint32_t * dst, uint32_t * src, uint32_t size);
int al_upgrade_flash_init(uint32_t * dest_address);

#endif  /* _AL_UPGRADE_H_ */

