#ifndef _LANGUAGES_H_
#define _LANGUAGES_H_
//----------------------------------------------------------------------
// Constant
//----------------------------------------------------------------------
#define LANGUAGES_MAX_LANGUAGES   5
#define LANGUAGES_MAX_STRINGS     128

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
int languages_load_package(char * package[LANGUAGES_MAX_STRINGS][LANGUAGES_MAX_LANGUAGES]);
int languages_set_language(int lang);
char * languages_get_string(int string_number);

#endif // _LANGUAGES_H_
