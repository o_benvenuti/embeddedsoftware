#include <languages.h>
#include <stddef.h>
#include <trace.h>

static int _current_language = 0;

static char error_str[] = "error";

char * (*_language_pack)[LANGUAGES_MAX_STRINGS][LANGUAGES_MAX_LANGUAGES] = { NULL };

//----------------------------------------------------------------------
// Tasks definitions
//----------------------------------------------------------------------
int languages_load_package(char * package[LANGUAGES_MAX_STRINGS][LANGUAGES_MAX_LANGUAGES])
{
    if ( ! package )
    {
        debug_printf("invalid arguments\n");
        return -1;
    }

    // assign language pack
    _language_pack = package;

    return 0;
}

//----------------------------------------------------------------------
int languages_set_language(int lang)
{
    if ( lang > LANGUAGES_MAX_LANGUAGES )
    {
        debug_printf("invalid language selected\n");
        return -1;
    }
    _current_language = lang;
    return 0;
}

//----------------------------------------------------------------------
char * languages_get_string(int string_number)
{
    if ( ! _language_pack )
    {
        debug_printf("no language pack loaded\n");
        return error_str;
    }

    if ( string_number > LANGUAGES_MAX_STRINGS )
    {
        return error_str;
    }

    return (*_language_pack)[string_number][_current_language];
}
