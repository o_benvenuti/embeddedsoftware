#include <stdio.h>

#ifdef DEBUG
    #define debug_printf( format, ... )                             \
        printf("[l.%d]%s(): " format, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
    #define debug_printf( ... )
#endif
