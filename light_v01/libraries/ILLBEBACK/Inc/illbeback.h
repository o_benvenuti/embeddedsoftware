#ifndef _ILLBEBACK_H_
#define _ILLBEBACK_H_
/**
  * @file:  illbeback.h
  *
  * @date   09/07/2019
  *
  * @description Library providing functions to save relative
  *              position and displacement
  *
  * @platform    platform agnostic
  **/

//-------------------------------------------------------------------
// Required includes
//-------------------------------------------------------------------
#include <stdint.h>
#include <stdbool.h>

//-------------------------------------------------------------------
// Typedef
//-------------------------------------------------------------------
typedef enum _ibb_status
{
    ibb_alright     = 0,
    ibb_almost_lost = 1,
    ibb_incoherent  = 2,
}ibb_status_t;

//-------------------------------------------------------------------
// API
//-------------------------------------------------------------------
int ibb_init(bool watch_stance_left);
int ibb_feed(float * acc_in, float * gyr_in, float * mag_in, float dt_sec, uint16_t heading);
int ibb_get_origin_heading(void);
bool ibb_are_we_arrived(void);
bool ibb_moving_status(void);
ibb_status_t ibb_get_status(void);

#endif  //  _ILLBEBACK_H_

