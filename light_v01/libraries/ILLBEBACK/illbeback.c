//-------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------
// local
#include "Inc/illbeback.h"

// sdk
#include <al_circbuf.h>
#include <al_tools.h>
#include <trace.h>

// standard
#include <math.h>
#include <string.h>     // memcpy
#include <stdlib.h>     // qsort

//-------------------------------------------------------------------
// Defines
//-------------------------------------------------------------------
// Globals
#define IBB_INPUT_WINDOW_LEN    15
#define IBB_PAUSE_WINDOW_LEN    64
#define IBB_THRES_WINDOW_LEN    IBB_PAUSE_WINDOW_LEN * 2 / 16
#define IBB_THRESHOLD_MIN_VAL   0.0001

// Pause detection (IBB_PAUSE)
#define IBB_PAUSE_CRITERIA_CALC_WINDOW_LEN  128  // TF = 6.4 sec = 128*50ms
#define IBB_PAUSE_STARTWINDOW_CALC_THRSHOLD 800  // I0 = 40 sec = 800*50ms
#define IBB_PAUSE_ENDWINDOW_CALC_THRSHOLD   3200 // I1 = 160 sec = 3200*50ms
#define IBB_PAUSE_COEF_THRESHOLD            3    // COEF_SEUIL_PAUSE
#define IBB_PAUSE_NMAX_BINS                 12   // NMAX_BINS
#define IBB_PAUSE_NBINS                     61   // nbins
#define IBB_PAUSE_BUFFERSIZE                60   // IBB_PAUSE_NBINS - 1
#define IBB_PAUSE_HMAX                      2000 // hmax
#define IBB_PAUSE_HMIN                      0    // hmin

// Quaternion
#define IBB_ALPHA_QJ                0.9
#define IBB_PRATIO                  0.01
#define IBB_GRATIO                  100
#define IBB_ALPHA_TREFFER           0.3
#define IBB_GOAL_REACHED_TRESHOLD   0.9
#define IBB_MIN_ACQ_NB              6000    // 5 min
#define IBB_VMAX_THRESHOLD          0.2
#define IBB_VMAX_QUANTUM            2       // must be ALWAYS more than 1
#define IBB_WATCH_POS_ANGLE         (float)(15.0 * M_PI / 180.0)

// Filters constants
#define IBB_BUTTERWORTH_LP_NZEROS  5
#define IBB_BUTTERWORTH_LP_NPOLES  5
#define IBB_BUTTERWORTH_LP_GAIN    5.205422613e+02
#define IBB_BUTTERWORTH_HP_NZEROS  5
#define IBB_BUTTERWORTH_HP_NPOLES  5
#define IBB_BUTTERWORTH_HP_GAIN    1.250922666e+00



//-------------------------------------------------------------------
// Static variables
//-------------------------------------------------------------------
// Global
static bool _ibb_stance_left = true;
static bool _ibb_arrived = false;
static bool _ibb_is_moving = false;
static float _ibb_csxplus  = 0.0;
static float _ibb_csxminus = 0.0;
static float _ibb_csyplus  = 0.0;
static float _ibb_csyminus = 0.0;
static float _ibb_position[3] = {0.0};
static float _ibb_origin_heading = 0;
static int _ibb_input_data_count = 0;
static int _ibb_pause_data_count = 0;
static int _ibb_invalid_acc_count = 0;
static float _ibb_thres_variance = 0.0;
static ibb_status_t _ibb_current_status;
static int _ibb_invalid_neg_az_count = 0;
static int _ibb_threshold_data_count = 0;
static float _ibb_pause_data_window[IBB_PAUSE_WINDOW_LEN];
static float _ibb_pause_time_window[IBB_PAUSE_WINDOW_LEN];
static float _ibb_hamming_pause_window[IBB_PAUSE_WINDOW_LEN];
static float _ibb_regression_out_window[IBB_PAUSE_WINDOW_LEN];
static float _ibb_partialreg_out_window[IBB_PAUSE_WINDOW_LEN];
static float _ibb_threshold_window[IBB_THRES_WINDOW_LEN];
static float _ibb_acc_window[3][IBB_INPUT_WINDOW_LEN];
static float _ibb_mag_window[3][IBB_INPUT_WINDOW_LEN];
static float _ibb_prev_acc_norm;

// Pause detection (_ibb_pause)
AL_CIRCBUF_DEF(float, _ibb_pause_acc_cirbug, IBB_PAUSE_CRITERIA_CALC_WINDOW_LEN);
AL_CIRCBUF_DEF(float, _ibb_pause_gyr_cirbug, IBB_PAUSE_CRITERIA_CALC_WINDOW_LEN);
static float _ibb_pause_histogram[IBB_PAUSE_BUFFERSIZE] = {0}; // histogramme
static float _ibb_pause_sumperbins[IBB_PAUSE_BUFFERSIZE] = {0}; // sommeparbins
static float _ibb_pause_slot_mean = 0.0; // MR
static float _ibb_pause_ntotal = 0.0; // ntotal
static float _ibb_pause_threshold = 0.0; // seuil

// quaternion
static float _ibb_qJ[4] = {1,0,0,0};
static float _ibb_prev_roll =  0.0 ;
static float _ibb_prev_pitch =  0.0 ;

// filters
static float _ibb_butterworth_lp_in[IBB_BUTTERWORTH_LP_NZEROS+1];
static float _ibb_butterworth_lp_out[IBB_BUTTERWORTH_LP_NPOLES+1];
static float _ibb_butterworth_hp_in[IBB_BUTTERWORTH_HP_NZEROS+1];
static float _ibb_butterworth_hp_out[IBB_BUTTERWORTH_HP_NPOLES+1];

//-------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------
static int _ibb_pause_max_value(void);

static float _ibb_median(float * array);

/**
  * @brief  fill a an array of 3 value per column.
  *         that's a 2 dimensional array: axis / column
  *         if the buffer is full, first value will be
  *         discarded and the buffer will be left-shifted.
  *
  * @param[in]  input_window    array to fill
  * @param[in]  data            value to insert
  * @param[in]  len             number of columns
  *
  **/
int _ibb_fill_buffer(float * input_window, float * data, int len);

/**
  * @brief      get yaw/pitch/roll from raw AGM data
  * @param[in]  acc accelerometer values [G] (x,y,z)
  * @param[in]  gyr gyroscope values [dps] (x,y,z)
  * @param[in]  mag magnetometer calibrated values [Gauss](x,y,z)
  * @param[in]  dT difference in sec from last data acquisition
  * @param[out] yaw
  * @param[out] pitch
  * @param[out] roll
  **/
static void _ibb_treffers(float * acc, float * gyr, float * mag, int dt_sec,
                          float * prev_roll, float * prev_pitch,
                          float * roll, float * pitch, float * yaw);

/**
  * @brief  Compute quaternion using two vectors
  *
  * @param[in]  A[4]   first input vector of floats
  * @param[in]  B[4]   second input vector of floats
  * @param[out] C[4]   resulting quaternion
  *
  **/
static void _ibb_quaternion_product(float A[4],float B[4],float C[4]);

/**
  * @brief  Apply fifth order low-pass filter on signal
  *         acquired at 20 sample per second.
  *         /!\ Cutoff frequency is 2.2Hz.
  *
  * @WARNING MUST BE USED ONLY FOR ONE SIGNAL
  *
  * @param[in]  value   pointer on current value
  *
  * @return filtered signal value
  **/
static float _ibb_butterworth_lp(float * value);

/**
  * @brief  Apply fifth order high-pass filter on signal
  *         acquired at 20 sample per second.
  *         /!\ Cutoff frequency is 0.44Hz.
  *
  * @WARNING MUST BE USED ONLY FOR ONE SIGNAL
  *
  * @param[in]  value   pointer on current value
  *
  * @return filtered signal value
  **/
static float _ibb_butterworth_hp(float * value);

static bool _ibb_moving_detection(float * threshold, int len);
static bool _ibb_pause_detection(float acc, float gyr);
static int _ibb_hamming_window(float * in, float * out, int len);
static float _ibb_matrix_inverse(int size, float Min[size][size], float Mout[size][size]);
static int _ibb_regression(float * signal, float * output, float * dt_sec, int len);

// helpers
int _ibb_compare_crescent(const void *a, const void *b);

//-------------------------------------------------------------------
// Public API
//-------------------------------------------------------------------
int ibb_init(bool watch_stance_left)
{
    _ibb_csxplus  = 0.0;
    _ibb_csxminus = 0.0;
    _ibb_csyplus  = 0.0;
    _ibb_csyminus = 0.0;
    _ibb_thres_variance = IBB_THRESHOLD_MIN_VAL;
    _ibb_stance_left = watch_stance_left;

    // counters
    _ibb_input_data_count = 0;
    _ibb_pause_data_count = 0;
    _ibb_threshold_data_count = 0;

    // quaternion
    _ibb_qJ[0] = 1.0; _ibb_qJ[1] = 0.0; _ibb_qJ[2] = 0.0; _ibb_qJ[3] = 0.0;

    memset(&_ibb_position[0], 0 , sizeof(float) * 3);
    memset(&_ibb_pause_data_window[0], 0, sizeof(float) * IBB_PAUSE_WINDOW_LEN);
    memset(&_ibb_acc_window[0][0], 0, sizeof(float) * IBB_INPUT_WINDOW_LEN * 3);
    memset(&_ibb_mag_window[0][0], 0, sizeof(float) * IBB_INPUT_WINDOW_LEN * 3);
    memset(&_ibb_pause_histogram[0], 0, sizeof(float) * IBB_PAUSE_BUFFERSIZE); // histogramme
    memset(&_ibb_pause_sumperbins[0], 0, sizeof(float) * IBB_PAUSE_BUFFERSIZE); // histogramme

    // previously computed pitch and roll
    _ibb_arrived = false;
    _ibb_prev_roll = 0.0;
    _ibb_prev_pitch = 0.0;
    _ibb_prev_acc_norm = 1.0;
    _ibb_current_status = ibb_alright;
    _ibb_origin_heading = 0.0;
    _ibb_invalid_acc_count = 0;
    _ibb_invalid_neg_az_count = 0;

    return 0;
}

//-------------------------------------------------------------------
/**
  * @brief  Implementation of an "going back to starting point"
  *         algorithm
  *
  * @WARNING This algorithm version works according to followings
  *          conditions :
  *          - The diver shall keep his arms:
  *            -> static
  *            -> in front of him
  *            -> with screen surface water facing
  *
  * @param 0  value   accelerometer float input data from LSM9DS1
  *                   without any processing
  * @param 1  value   gyroscope meter float input data after
  *                   calibration step
  * @param 2  value   magnetometer float input data after
  *                   calibration step
  * @param 3  value   delta time in millisecond
  * @param 4  value   compass heading computes in another function
  *
  * @return filtered signal value
  **/
int ibb_feed(float * acc_in, float * gyr_in, float * mag_in,
             float dt_sec, uint16_t heading)
{
    int rc = 0;
    float yaw;
    float coef_static;
    float temp, omega;
    float acc_rot_j_tmp;
    float acc_rot_j[3];
    float ri[3] = {0.0};
    float qK[4], SwI4qJ[4];
    float Rz[3][3] = {{0.0}};
    float goal_criteria = 0.0;
    float acc_rot_yaw_j[3] = {0.0};
    float Sw[4][4], Swgx, Swgy, Swgz;
    float norm_acc[3], med_acc[3], med_mag[3];
    float conv_acc[3], conv_gyr[3], conv_mag[3];
    float roll_comp, pitch_comp;
    float acc_euclidian_norm, gyr_euclidian_norm = 0.0;


    // sanity check
    if ( ! acc_in || ! gyr_in || ! mag_in )
    {
        debug_printf("invalid argument\n");
        return -1;
    }

    // if the algorithm is lost no need to go further
    if ( _ibb_current_status == ibb_incoherent )
    {
        return 0;
    }

    //-------------------------------------------
    // Move to orthonormal space
    //-------------------------------------------
    // accelerometer
    conv_acc[0] = -acc_in[0];
    conv_acc[1] =  acc_in[1];
    conv_acc[2] =  acc_in[2];

    // magnetometer
    conv_mag[0] = -mag_in[0];
    conv_mag[1] = -mag_in[1];
    conv_mag[2] = -mag_in[2];

    // local gyoscope
    conv_gyr[0] = -gyr_in[0];
    conv_gyr[1] =  gyr_in[1];
    conv_gyr[2] =  gyr_in[2];

    //-------------------------------------------
    // Fill window buffers
    //-------------------------------------------
    // fill accelerometer buffer
    rc = _ibb_fill_buffer((float *)_ibb_acc_window,
                          &conv_acc[0], IBB_INPUT_WINDOW_LEN);
    if ( rc )
    {
        debug_printf("failed to fill IBB with acc\n");
        return -1;
    }

    // fill magnetometer buffer
    rc = _ibb_fill_buffer((float *)_ibb_mag_window,
                          &conv_mag[0], IBB_INPUT_WINDOW_LEN);
    if ( rc )
    {
        debug_printf("failed to fill IBB with mag\n");
        return -1;
    }

    // update counter
    _ibb_input_data_count = _ibb_input_data_count + 1;

    // wait for new data till buffer is full
    if ( _ibb_input_data_count < IBB_INPUT_WINDOW_LEN )
    {
        return 0;
    }


    //-------------------------------------------
    // Compute medians out of each buffer
    //-------------------------------------------
    med_acc[0] = _ibb_median(&_ibb_acc_window[0][0]);
    med_acc[1] = _ibb_median(&_ibb_acc_window[1][0]);
    med_acc[2] = _ibb_median(&_ibb_acc_window[2][0]);

    med_mag[0] = _ibb_median(&_ibb_mag_window[0][0]);
    med_mag[1] = _ibb_median(&_ibb_mag_window[1][0]);
    med_mag[2] = _ibb_median(&_ibb_mag_window[2][0]);

    //-------------------------------------------
    // Compute Sabatelli matrix
    //-------------------------------------------
    // normalise acc
    temp = euclidian_norm(COUNTOF(med_acc),med_acc);
    for(int i = 0; i < 3; ++i)
    {
        norm_acc[i] = med_acc[i] / (temp ? temp : 1);
    }

    // very few chance to happen but paranoia check
    if ( norm_acc[2] == 1.0 )
    {
         norm_acc[2] = 0.999999;
    }
    else if ( norm_acc[2] == -1.0 )
    {
        norm_acc[2] = -0.999999;
    }

    // get the angle between current and flat position
    omega = acos(norm_acc[2]);

    // create a quaternion with this angle
    qK[0] = cos(omega/2.0);
    qK[1] = ( norm_acc[1]/sin(omega)) * sin(omega/2.0);
    qK[2] = (-norm_acc[0]/sin(omega)) * sin(omega/2.0);
    qK[3] = 0.0;

    Swgx = (deg2rad(conv_gyr[0]) * dt_sec) / 2.0;
    Swgy = (deg2rad(conv_gyr[1]) * dt_sec) / 2.0;
    Swgz = (deg2rad(conv_gyr[2]) * dt_sec) / 2.0;

    // fill Sw 4x4 matrix
    Sw[0][0] =   1.0; Sw[0][1] = -Swgx; Sw[0][2] = -Swgy; Sw[0][3] = -Swgz;
    Sw[1][0] =  Swgx; Sw[1][1] =   1.0; Sw[1][2] =  Swgz; Sw[1][3] = -Swgy;
    Sw[2][0] =  Swgy; Sw[2][1] = -Swgz; Sw[2][2] =   1.0; Sw[2][3] =  Swgx;
    Sw[3][0] =  Swgz; Sw[3][1] =  Swgy; Sw[3][2] = -Swgx; Sw[3][3] =   1.0;

    //-------------------------------------------
    // Compute quaternion out of Sabatelli matrix
    //-------------------------------------------
    // alpha *(Sw)
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0 ; j < 4 ; ++j)
        {
            Sw[i][j] = Sw[i][j] * IBB_ALPHA_QJ;
        }
    }

    // alpha*(Sw)*qJ[i-1]
    for (int i = 0; i < 4; ++i)
    {
        SwI4qJ[i] = 0.0;
        for (int j = 0; j < 4; ++j)
        {
            SwI4qJ[i] += Sw[i][j] * _ibb_qJ[j];
        }
    }

    // qJ = (1-alpha)*qK + alpha*(Sw)*qJ[i-1]
    for (int i = 0; i < 4; ++i)
    {
        _ibb_qJ[i] = (1-IBB_ALPHA_QJ) * qK[i] + SwI4qJ[i];
    }

    _ibb_qJ[3] = 0;

    // normalise qJ
    temp = euclidian_norm(COUNTOF(_ibb_qJ),_ibb_qJ);
    if ( temp > 0.01 )
    {
        for (int i = 0; i < 4; ++i)
        {
            _ibb_qJ[i] = _ibb_qJ[i] / temp;
        }
    }

    //-------------------------------------------
    // Translate accelerometer with quaternion
    //-------------------------------------------
    float qiJ[4], qACC[4], qTMP[4], qTMP2[4];

    // fill temporary quaternion with accelerometer data
    qACC[0] = 0.0;
    qACC[1] = med_acc[0];
    qACC[2] = med_acc[1];
    qACC[3] = med_acc[2];

    // qiJ = conjug qJ
    qiJ[0] =  _ibb_qJ[0];
    qiJ[1] = -_ibb_qJ[1];
    qiJ[2] = -_ibb_qJ[2];
    qiJ[3] = -_ibb_qJ[3];

    // qJ* qACC * qiJ
    _ibb_quaternion_product(_ibb_qJ,qACC,qTMP2);
    _ibb_quaternion_product(qTMP2,qiJ,qTMP);

    // fill acc_rot_j with parts of the result of quaternion product
    acc_rot_j[0] = qTMP[1];
    acc_rot_j[1] = qTMP[2];
    acc_rot_j[2] = qTMP[3];

    //-------------------------------------------
    // limit speed value
    //-------------------------------------------
    for (int i = 0; i < 3; ++i )
    {
        // calculate absolute value and remove gravity on z-axis
        float abs_acc_rot_j = ((i == 2) ? acc_rot_j[i] - 1 : acc_rot_j[i]);
        abs_acc_rot_j = fabs(abs_acc_rot_j);

        while ( IBB_VMAX_THRESHOLD < abs_acc_rot_j )
        {
            abs_acc_rot_j = abs_acc_rot_j / IBB_VMAX_QUANTUM;
        }

        // save new value and treat specific z-axis z situation
        if ( i == 2 )
        {
            acc_rot_j[i] = ((acc_rot_j[i]-1 > 0) ? abs_acc_rot_j : -abs_acc_rot_j);
            acc_rot_j[i] = acc_rot_j[i] + 1;
        }
        else
        {
            acc_rot_j[i] = ((acc_rot_j[i] > 0) ? abs_acc_rot_j : -abs_acc_rot_j);
        }
    }

    // ------------------------------------------
    // Avoid an acceleration null for x, y and z
    // axis
    // ------------------------------------------
    acc_rot_j[0] = acc_rot_j[0] + ((IBB_VMAX_THRESHOLD/4)*signf(acc_rot_j[0]));
    acc_rot_j[1] = acc_rot_j[1] + ((IBB_VMAX_THRESHOLD/4)*signf(acc_rot_j[1]));
    acc_rot_j[2] = acc_rot_j[2] + ((IBB_VMAX_THRESHOLD/4)*signf(acc_rot_j[2]-1));

    //-------------------------------------------
    // Force a certain position as won't get
    // his arm hundred percent straight, apply
    // proportional acceleration to y-axis
    //-------------------------------------------
    acc_rot_j_tmp = euclidian_norm(2, &acc_rot_j[0]);

    // apply normalised acceleration to x and y axis proportionally
    acc_rot_j[0] = cos((_ibb_stance_left ? IBB_WATCH_POS_ANGLE
                                         : -IBB_WATCH_POS_ANGLE));
    acc_rot_j[0] = acc_rot_j[0] * acc_rot_j_tmp;
    acc_rot_j[1] = sin((_ibb_stance_left ? IBB_WATCH_POS_ANGLE
                                         : -IBB_WATCH_POS_ANGLE));
    acc_rot_j[1] = acc_rot_j[1] * acc_rot_j_tmp;

    //-------------------------------------------
    // Obtain static coefficient
    //-------------------------------------------
    coef_static = IBB_PRATIO / (IBB_PRATIO + square(conv_gyr[0] / IBB_GRATIO)
                                           + square(conv_gyr[1] / IBB_GRATIO)
                                           + square(conv_gyr[2] / IBB_GRATIO));

    //-------------------------------------------
    // get yaw, pitch and roll
    //-------------------------------------------
    // currently unused we only apply heading has a yaw
    yaw = deg2rad((float)heading);

    //-------------------------------------------
    // Verify if diver is having a nap
    //-------------------------------------------
    acc_euclidian_norm = euclidian_norm(COUNTOF(conv_acc),&conv_acc[0])-1;
    gyr_euclidian_norm = euclidian_norm(COUNTOF(conv_gyr),&conv_gyr[0]);

    _ibb_is_moving = _ibb_pause_detection(acc_euclidian_norm, gyr_euclidian_norm);

    //-------------------------------------------
    // Verify that we're not moving too much
    //-------------------------------------------
    bool valid_acc = (bool)(fabs(temp - _ibb_prev_acc_norm) < 0.1);
    bool valid_gyr = (bool)(euclidian_norm(COUNTOF(conv_gyr), conv_gyr) < 40);

    // if data is valid
    bool valid_data_input = valid_acc && valid_gyr && _ibb_is_moving;

    // save previous acc norm
    _ibb_prev_acc_norm = temp;

    //-------------------------------------------
    // Check if we are lost
    //-------------------------------------------
    // wait for two minutes before checking
    if ( _ibb_input_data_count > 2400 )
    {
        // add number of fails on acc norm
        if ( ! valid_acc )
        {
            _ibb_invalid_acc_count = _ibb_invalid_acc_count + 1;

            // if too many invalid values on acc norm: WE ARE LOST
            if ( _ibb_invalid_acc_count > 80 )
            {
                _ibb_current_status = ibb_incoherent;
                return 0;
            }
            else if ( _ibb_invalid_acc_count > 40 )
            {
                _ibb_current_status = ibb_almost_lost;
            }
        }

        // add number of negative value on acc's z-axis
        if ( med_acc[2] < 0 )
        {
            _ibb_invalid_neg_az_count = _ibb_invalid_neg_az_count + 1;

            // if too many negative values on z: WE ARE LOST
            if ( _ibb_invalid_neg_az_count > 2049 )
            {
                _ibb_current_status = ibb_incoherent;
                return 0;
            }
        }
    }

    //-------------------------------------------
    // Speed compensation regarding pitch & roll
    //-------------------------------------------
    pitch_comp = atan2(-med_acc[0] , sqrtf(pow(med_acc[1], 2) + pow(med_acc[2], 2)));
    pitch_comp = 2 * fabs(pitch_comp) / M_PI;
    pitch_comp = 1 - (0.9 * pitch_comp);

    roll_comp = atan2(med_acc[1], sqrtf(pow(med_acc[0], 2) + pow(med_acc[2], 2)));
    roll_comp = 2 * fabs(roll_comp) / M_PI;
    roll_comp = 1 - (0.9 * roll_comp);

    //-------------------------------------------
    // Apply yaw and calculate position
    //-------------------------------------------
    Rz[0][0] = cos(yaw);
    Rz[0][1] = -sin(yaw);
    Rz[1][0] = sin(yaw);
    Rz[1][1] = cos(yaw);
    Rz[2][2] = 1.0;

    for (int i=0; i<3; i++)
    {
        acc_rot_yaw_j[i] = 0.0;
        for (int j=0; j<3; j++)
        {
            acc_rot_yaw_j[i] += Rz[i][j]*acc_rot_j[j];
        }
    }

    for (int i=0; i<3; i++)
    {
        // calculate distance in time
        ri[i] = valid_data_input ? (acc_rot_yaw_j[i] * dt_sec * coef_static) : 0 ;

        // compensate with relative watch attitude
        ri[i] = ri[i] * (pitch_comp + roll_comp);

        // final crazy check to avoid Nan in position
        if ( isnanf(ri[i]) )
        {
            ri[i] = 0;
        }

        // add to absolute position
        _ibb_position[i] += ri[i];
    }

    //-------------------------------------------
    // Goal criteria
    //-------------------------------------------
    _ibb_csxplus  += ri[0] * (ri[0] > 0);
    _ibb_csxminus += ri[0] * (ri[0] < 0);
    _ibb_csyplus  += ri[1] * (ri[1] > 0);
    _ibb_csyminus += ri[1] * (ri[1] < 0);

    goal_criteria = 1/(1+sqrt(square(1+((_ibb_csxplus+1)/(_ibb_csxminus-1)))+square(1+((_ibb_csyplus+1)/(_ibb_csyminus-1)))));

    if( (goal_criteria > IBB_GOAL_REACHED_TRESHOLD)     &&
        (_ibb_input_data_count > IBB_MIN_ACQ_NB)        )
    {
        _ibb_arrived = true;
    }
    else
    {
        _ibb_arrived = false;
    }

    //******************* Origin Heading ************************
    _ibb_origin_heading = (float)atan2(_ibb_position[1], _ibb_position[0]) + M_PI - yaw;
    _ibb_origin_heading = -rad2deg(_ibb_origin_heading);

    return 0;
}

//-------------------------------------------------------------------
ibb_status_t ibb_get_status(void)
{
    return _ibb_current_status;
}

//-------------------------------------------------------------------
int ibb_get_origin_heading(void)
{
    return (int)round(_ibb_origin_heading);
}

//-------------------------------------------------------------------
bool ibb_are_we_arrived(void)
{
    return _ibb_arrived;
}


//-------------------------------------------------------------------
bool ibb_moving_status(void)
{
    return _ibb_is_moving;
}

//-------------------------------------------------------------------
// Private API
//-------------------------------------------------------------------
int _ibb_fill_buffer(float * input_window, float * data, int len)
{
    //sanity check
    if ( ! input_window || ! data )
    {
        return -1;
    }

    // shift all values one step to the left
    for(int axis = 0; axis < 3; ++axis)
    {
        for(int i = 0; i < (len - 1); ++i)
        {
            *((input_window + axis*len) + i) = *((input_window + axis*len) + i+1);
        }
    }

    // fill last window element
    for(int axis = 0; axis < 3; ++axis)
    {
        *((input_window + axis*len) + (len-1)) = data[axis];
    }

    return 0;
}

//-------------------------------------------------------------------
static int _ibb_pause_max_value(void)
{

	float max_value = _ibb_pause_histogram[0];
	int index = 0;

	for (int i=1; i<IBB_PAUSE_BUFFERSIZE; i++)
	{
		if (_ibb_pause_histogram[i] > max_value)
		{
			max_value = _ibb_pause_histogram[i];
			index = i;
		}
	}

	return index;
}

//-------------------------------------------------------------------
static float _ibb_median(float * array)
{
    float sorted_array[IBB_INPUT_WINDOW_LEN];

    // copy input buffer to temporary sorted buffer
    memcpy(&sorted_array[0], &array[0], sizeof(float) * IBB_INPUT_WINDOW_LEN);

    qsort(&sorted_array[0], IBB_INPUT_WINDOW_LEN,
          sizeof(float), _ibb_compare_crescent);

    // odd buffer size
    if ( IBB_INPUT_WINDOW_LEN % 2 )
    {
        return sorted_array[(IBB_INPUT_WINDOW_LEN-1)/2];
    }
    else        // even
    {
        // mean of the two middle values
        float tmp = sorted_array[IBB_INPUT_WINDOW_LEN/2];
        tmp = tmp + sorted_array[(IBB_INPUT_WINDOW_LEN/2) - 1];

        return tmp / 2.0;
    }
}

//-------------------------------------------------------------------
static void _ibb_quaternion_product(float A[4],float B[4],float C[4])
{
    C[0] = A[0]*B[0] - A[1]*B[1] - A[2]*B[2] - A[3]*B[3];
    C[1] = A[0]*B[1] + A[1]*B[0] + A[2]*B[3] - A[3]*B[2];
    C[2] = A[0]*B[2] + A[2]*B[0] - A[1]*B[3] + A[3]*B[1];
    C[3] = A[3]*B[0] + A[0]*B[3] + A[1]*B[2] - A[2]*B[1];
}

//-------------------------------------------------------------------
static void _ibb_treffers(float * acc, float * gyr, float * mag, int dt_sec,
                          float * prev_roll, float * prev_pitch,
                          float * roll, float * pitch, float * yaw)
{
    float magx   = 0.0;
    float magy   = 0.0;
    float aRoll, aPitch;

    // first step, compute roll and pitch using accelerometer
    aRoll  = atan2( acc[1],sqrt( square(acc[0])+square(acc[2]) ) );
    aPitch = atan2(-acc[0],sqrt( square(acc[1])+square(acc[2]) ) );

    // complementary filter using gyro to obtain roll and pitch
    *roll = (1-IBB_ALPHA_TREFFER) * aRoll ;
    *roll = *roll + (IBB_ALPHA_TREFFER * (gyr[0] * dt_sec * M_PI/180 + *prev_roll));

    *pitch =  (1-IBB_ALPHA_TREFFER) * aPitch;
    *pitch = *pitch + (IBB_ALPHA_TREFFER * (gyr[1] * dt_sec * M_PI/180 + *prev_pitch));

    // compute yaw with magnetometer
    magy = mag[2] * sin(*roll ) - mag[1] * cos(*roll );
    magx = mag[0] * cos(*pitch) + mag[1] * sin(*pitch) * sin(*roll) + mag[2] * sin(*pitch) * cos(*roll) ;

    // unit is radian
    *yaw   = atan2(-magy, -magx) ;

    // return previous values
    *prev_roll = *roll;
    *prev_pitch = *pitch;
}

//-------------------------------------------------------------------
static bool _ibb_pause_detection(float acc_euclidian_norm, float gyr_euclidian_norm)
{
	int rc;
    static bool ret = true;
    float c1, c2, criteria = 0.0;
    al_tools_variance_t acc_aggr, gyr_aggr = {0, 0.0, 0.0};
    float tmp = 0.0;
    int slot = 0;
    float histogram_max_value = 0.0;
    int histogram_index = 0;


    // add new samples into the circular buffer. Data can be overwritten
    rc = al_circbuf_push(&_ibb_pause_acc_cirbug, &acc_euclidian_norm);
    // TODO: check error code
    rc = al_circbuf_push(&_ibb_pause_gyr_cirbug, &gyr_euclidian_norm);
    // TODO: check error code

    if (al_circbuf_available(&_ibb_pause_acc_cirbug) || al_circbuf_available(&_ibb_pause_gyr_cirbug))
    {
    	return true;
    }

    // Compute C1 and C2 criteria

    memset(&acc_aggr, 0, sizeof(acc_aggr));
    memset(&gyr_aggr, 0, sizeof(gyr_aggr));

    // Compute variance for accelerometer buffer
    AL_CIRCBUF_FOREACH(tmp ,_ibb_pause_acc_cirbug)
    {
    	al_tools_variance_update(&acc_aggr, tmp);
    }

    // Compute variance for gyrometer buffer
    AL_CIRCBUF_FOREACH(tmp ,_ibb_pause_gyr_cirbug)
    {
    	al_tools_variance_update(&gyr_aggr, (1/(0.1 + tmp)));
    }

    // retrieve calculated values (al_tools_variance_update)
    c2 = 1 / al_tools_variance_finalize(&acc_aggr);

    // retrieve calculated values (al_tools_variance_update) and get standard deviation
    c1 = sqrtf(al_tools_variance_finalize(&gyr_aggr));    // retrieve calculated values
    criteria = c1 * c2;


    if (_ibb_input_data_count < IBB_PAUSE_STARTWINDOW_CALC_THRSHOLD)
    {
    	return true;
    }
    else if(_ibb_input_data_count < IBB_PAUSE_ENDWINDOW_CALC_THRSHOLD)
    {
    	// Compute histogram
        if (criteria < IBB_PAUSE_HMAX)
        {
        	slot = (int)(floor(((criteria-IBB_PAUSE_HMIN)*(IBB_PAUSE_NBINS-1))/(IBB_PAUSE_HMAX-IBB_PAUSE_HMIN)));
            _ibb_pause_histogram[slot] += 1;
            _ibb_pause_sumperbins[slot] += criteria;
        }
        return true;
    }
    else if(_ibb_input_data_count == IBB_PAUSE_ENDWINDOW_CALC_THRSHOLD)
    {
    	histogram_index = _ibb_pause_max_value();
    	_ibb_pause_slot_mean = _ibb_pause_sumperbins[histogram_index] / _ibb_pause_histogram[histogram_index];
    	_ibb_pause_ntotal = _ibb_pause_histogram[histogram_index];
    	_ibb_pause_histogram[histogram_index] = -1;

    	for(uint8_t i = 0; i < (IBB_PAUSE_NMAX_BINS-1); i++)
    	{
    		histogram_index = _ibb_pause_max_value();
        	_ibb_pause_slot_mean = ((_ibb_pause_slot_mean*_ibb_pause_ntotal) + _ibb_pause_sumperbins[histogram_index])/(_ibb_pause_ntotal+_ibb_pause_histogram[histogram_index]);
        	_ibb_pause_ntotal += _ibb_pause_histogram[histogram_index];
        	_ibb_pause_histogram[histogram_index] = -1;

    	}
    	_ibb_pause_threshold = _ibb_pause_slot_mean * IBB_PAUSE_COEF_THRESHOLD;

    	if (criteria < _ibb_pause_threshold)
    	{
    		// The diver is moving
    		return true;
    	}
		// The diver isn't moving
   		return false;

    }
    else
    {
    	if (criteria < _ibb_pause_threshold)
    	{
    		// The diver is moving
    		return true;
    	}
		// The diver isn't moving
   		return false;
    }

}

//-------------------------------------------------------------------
static bool _ibb_moving_detection(float * threshold, int len)
{
    float tmp;

    // does not work with empty array
    if ( 0 == len )
    {
        // return that we are moving by default
        return true;
    }

    // set variance threshold value once we've got 2 values
    if ( (len % IBB_THRES_WINDOW_LEN) == 2 )
    {
        // reduce variance threshold by 2
        tmp = pow((threshold[0] - threshold[1]), 2) / (2 * 2);
        _ibb_thres_variance = (_ibb_thres_variance > tmp) ?
                               _ibb_thres_variance : tmp;
        _ibb_thres_variance = (_ibb_thres_variance > IBB_THRESHOLD_MIN_VAL) ?
                               _ibb_thres_variance : IBB_THRESHOLD_MIN_VAL;
    }

    return (threshold[len-1] > _ibb_thres_variance);
}

//-------------------------------------------------------------------
#define IBB_REGRESSION_MATRIX_MAX_LEN   64
static int _ibb_regression(float * signal, float * output, float * dt_sec, int len)
{
    float temp;
    float regression[4];
    float time_cumul = 0.0;
    float tmp_matrix[4][4] = {{0.0}};
    float inv_tmp_matrix[4][4] = {{0.0}};
    float array[IBB_REGRESSION_MATRIX_MAX_LEN][4];

    // construct matrix
    for ( int i = 0; i < len; ++i)
    {
        time_cumul = time_cumul + dt_sec[i];

        array[i][0] = 1.0;
        array[i][1] = time_cumul;
        array[i][2] = pow(time_cumul, 2);
        array[i][3] = pow(time_cumul, 3);
    }

    // transpose and multiply matrix
    for (int i=0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            for (int idx = 0; idx < len; ++idx )
            {
                tmp_matrix[i][j] += array[idx][i] * array[idx][j];
            }
        }
    }

    // TODO verify that returned determinant is valid
    _ibb_matrix_inverse(4, tmp_matrix, inv_tmp_matrix);

    for ( int i = 0; i < 4; ++i )
    {
        regression[i] = 0.0;

        for (int idx = 0; idx < len; ++idx )
        {
            temp = 0.0;
            for ( int j = 0; j < 4; ++j )
            {
                temp += inv_tmp_matrix[i][j] * array[idx][j];
            }

            regression[i] += temp * signal[idx];
        }
    }

    for (int i = 0; i < len; ++i )
    {
        output[i] = regression[0];
        output[i] = output[i] + regression[1] * array[i][1];
        output[i] = output[i] + regression[2] * array[i][2];
        output[i] = output[i] + regression[3] * array[i][3];
    }

    return 0;
}

//-------------------------------------------------------------------
static int _ibb_hamming_window(float * in, float * out, int len)
{
    float coefficient = 0.0;

    // sanity check
    if ( ! in || ! out || ! len )
    {
        debug_printf("invalid arguments\n");
        return -1;
    }

    // Loop over the N entries to compute hamming
    // window on it and save the last half raw input
    // at the beginning of raw buffer
    for (int i = 0; i < len; ++i)
    {
        // calculate coefficient for each loop
        coefficient = 0.54;
        coefficient -= (float) 0.46 * cos(2 * M_PI * i /  IBB_INPUT_WINDOW_LEN);

        // apply coefficient to raw data
        out[i] = in[i] * coefficient;
    }

    return 0;
}

//-------------------------------------------------------------------
static float _ibb_butterworth_lp(float * value)
{
    // shift input and output values
    for ( int i = 0; i < 5; ++i )
    {
        _ibb_butterworth_lp_in[i] = _ibb_butterworth_lp_in[i+1];
        _ibb_butterworth_lp_out[i] = _ibb_butterworth_lp_out[i+1];
    }

    // insert input value
    _ibb_butterworth_lp_in[5] = *value / IBB_BUTTERWORTH_LP_GAIN;

    // calculate new value
    _ibb_butterworth_lp_out[5] =
            (_ibb_butterworth_lp_in[0] + _ibb_butterworth_lp_in[5])
            + 5 * (_ibb_butterworth_lp_in[1] + _ibb_butterworth_lp_in[4])
            + 10 * (_ibb_butterworth_lp_in[2] + _ibb_butterworth_lp_in[3])
            + (  0.1008741467 * _ibb_butterworth_lp_out[0])
            + ( -0.7289180829 * _ibb_butterworth_lp_out[1])
            + (  2.1754752773 * _ibb_butterworth_lp_out[2])
            + ( -3.3836225479 * _ibb_butterworth_lp_out[3])
            + (  2.7747168513 * _ibb_butterworth_lp_out[4]);

    return _ibb_butterworth_lp_out[5];
}

//-------------------------------------------------------------------
static float _ibb_butterworth_hp(float * value)
{
    // shift input and output values
    for ( int i = 0; i < 5; ++i )
    {
        _ibb_butterworth_hp_in[i] = _ibb_butterworth_hp_in[i+1];
        _ibb_butterworth_hp_out[i] = _ibb_butterworth_hp_out[i+1];
    }

    // insert input value
    _ibb_butterworth_hp_in[5] = *value / IBB_BUTTERWORTH_HP_GAIN;

    // calculate new value
    _ibb_butterworth_hp_out[5] =
            (_ibb_butterworth_hp_in[5] - _ibb_butterworth_hp_in[0])
            + 5 * (_ibb_butterworth_hp_in[1] - _ibb_butterworth_hp_in[4])
            + 10 * (_ibb_butterworth_hp_in[3] - _ibb_butterworth_hp_in[2])
            + (  0.6390562348 * _ibb_butterworth_hp_out[0])
            + ( -3.4810790205 * _ibb_butterworth_hp_out[1])
            + (  7.5987009164 * _ibb_butterworth_hp_out[2])
            + ( -8.3095001796 * _ibb_butterworth_hp_out[3])
            + (  4.5527813818 * _ibb_butterworth_hp_out[4]);

    return _ibb_butterworth_hp_out[5];
}

//-------------------------------------------------------------------
// Helpers
//-------------------------------------------------------------------
int _ibb_compare_crescent(const void *a, const void *b)
{
  const float * da = a;
  const float * db = b;
  return (*da < *db) - (*da > *db);
}

//-------------------------------------------------------------------
#define IBB_MATRIX_MAX_SIZE 5
static float _ibb_matrix_inverse(int size, float Min[size][size], float Mout[size][size])
{
    int i,j,k;
    float L[IBB_MATRIX_MAX_SIZE][IBB_MATRIX_MAX_SIZE] = {{0.0}};
    float U[IBB_MATRIX_MAX_SIZE][IBB_MATRIX_MAX_SIZE] = {{0.0}};
    float b[IBB_MATRIX_MAX_SIZE][IBB_MATRIX_MAX_SIZE] = {{0.0}};
    float d[IBB_MATRIX_MAX_SIZE] = {0.0};
    float x[IBB_MATRIX_MAX_SIZE] = {0.0};
    float T;
    float determinant = 1.0;

    if(Min[0][0] == 0.0) // will cause a div by zero
    {
        return 0;
    }
    // Creation of Identity matrix
    for(i=0 ; i<size ; i++)
    {
        U[i][i] = 1.0;
        b[i][i] = 1.0;
    }

    // LU Crout decomposition
    L[0][0] = Min[0][0];
    for(i=1 ; i<size ; i++)
    {
        L[i][0] = Min[i][0];
        U[0][i] = Min[0][i] / L[0][0];
    }
    for(j=1 ; j<=size-2 ; j++)
    {
        for(i=j ; i<=size-1 ; i++)
        {
            L[i][j] = Min[i][j];
            for(k=0 ; k<=j-1 ; k++)
            {
                L[i][j] -= L[i][k] * U[k][j];
            }
        }
        for(k=j+1 ; k<=size-1 ; k++)
        {
            U[j][k] = Min[j][k];
            for(i=0 ; i<=j-1 ; i++)
            {
                U[j][k] -= L[j][i] * U[i][k];
            }
            if(L[j][j] != 0.0)
            {
                U[j][k] = U[j][k] / L[j][j];
            }
            else // will cause a div by zero
            {
                return 0;
            }
        }
    }
    L[size-1][size-1] = Min[size-1][size-1];
    for(i=0 ; i<=size-2 ; i++)
    {
        L[size-1][size-1] -= L[size-1][i] * U[i][size-1];
    }

    // determinant
    for(i = 0 ; i<=size-1 ; i++)
    {
        determinant *= L[i][i] * U[i][i];
    }
    if(determinant == 0)
    {
        return 0;
    }

    // d can be solved by forward substitution of L
    for(k=0 ; k<=size-1 ; k++)
    {
        d[0] = b[0][k]/L[0][0];
        for(i=1 ; i<=size-1 ; i++)
        {
            T = 0.0;
            for(j=0 ; j<=i-1 ; j++)
            {
                T += L[i][j] * d[j];
            }
            if(L[i][i] != 0.0)
            {
                d[i] = (b[i][k] - T ) / L[i][i];
            }
            else // will cause a div by zero
            {
                return 0;
            }
        }

        // then x can be solved by backward substitution of U
        x[size-1] = d[size-1];
        for(i=size-2 ; i>=0 ; i--)
        {
            T = 0.0;
            for(j=i+1 ; j<=size-1 ; j++)
            {
                T += U[i][j] * x[j];
            }
            x[i] = d[i] - T;
        }

        // x is the k-th column of the inversed matrix
        for(i=0 ; i<size ; i++)
        {
            Mout[i][k] = x[i];
        }
    }

    return determinant;
}

