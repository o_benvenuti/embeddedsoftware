include_libraries(TRACE)
include_drivers(LCD)
include_freertos()

INCLUDE_DIRECTORIES (${CMAKE_CURRENT_SOURCE_DIR}/Inc ${CMAKE_CURRENT_SOURCE_DIR}/Config)

ADD_LIBRARY (STemWin_HAL OS/GUI_X.c Config/GUIConf.c Config/LCDConf.c Config/GUIDRV.c)