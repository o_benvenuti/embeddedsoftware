#ifndef _MAGCAL_H_
#define _MAGCAL_H_
/**
 * @file:  magcal.h
 *
 * @brief
 *
 * @date   23/01/2019
 *
 *
 *
 */

#define MATRIX_MULT_GET_C_COLUMN_SIZE(LA,CA,LB,CB) (CB)
#define MATRIX_MULT_GET_C_LINES_SIZE(LA,CA,LB,CB)  (LA)

#define PI M_PI
#define MATRIX_MAX_SIZE 9

#define QUALITY_KNOB 30

// current sectors available = 10 -> max size = 1137
// 113 sample / reserved sector
#define SPHERE_ACQ_NB_POINTS 1024
#define TILT_ACQ_NB_POINTS 768


typedef struct magcal_t
{
	float hard_iron     [3]   ;
	float soft_iron     [3][3];
	float mag_tilt      [3][3];
	float post_hard_iron[3]   ;
	float regression_error    ;
    int   aberration_count    ;
	float quality             ;
} magcal_t;

//----------------------------------------------------------------------
// Public API
//----------------------------------------------------------------------
float magcal_get_quality(void);

/**
  * @brief          compensate magnetometer raw data
  * @param[in/out]  raw_mag in raw magnetometer value in Gauss / compensated out
  **/
void magcal_compensate_mag(float mag[3]);

/**
  * @brief  get calibration matrix from flash
  * @retval 0 if ok, -1 if error
  **/
int8_t magcal_get_calibration_from_flash(void);

/**
  * @brief Reset magnetometer calibration to neutral value
  *        and save it to flash
  **/
int magcal_reset(void);


/**
  * @brief runs data acquisition & hard/soft compensation matrix calculation
  * @param[out] hard_iron_matrix
  * @param[out] soft_iron_matrix
  * @retval 0 if ok, -1 if error
  **/
int8_t magcal_hard_soft_iron(void);

/**
  * @brief runs data acquisition & tilt/post_hard compensation matrix calculation
  * @param[out] mag_tilt
  * @param[out] post_hard_iron
  * @retval 0 if ok, -1 if error
  **/
int8_t magcal_tilt_post_hard_iron(void);

/**
  * @brief 		calculate the inverse of a square matrix (MATRIX_MAX_SIZE size maximum)
  * 	   		uses Crout LU decomposition
  * @param[in]  size defines the size of input & output matrix
  * @param[in]  Min  input matrix to be inverted
  * @param[out] Mout output inverted matrix
  * @retval     determinant, will be zero if invertion is not possible or div/zero
  **/
float matrix_inverse(int size, float Min[size][size], float Mout[size][size]);

/**
  * @brief 		calculate the determinant of a square matrix (MATRIX_MAX_SIZE size maximum)
  * 	   		uses Crout LU decomposition
  * @param[in]  size defines the size of input matrix
  * @param[in]  Min  matrix to be inverted
  * @param[out] Mout inverted matrix
  * @retval determinant of Min, -1 if not possible
  **/
float matrix_determinant(int size, float Min[size][size]);

/**
  * @brief 		calculate the product of two square matrix (MATRIX_MAX_SIZE size maximum)
  * @param[in] 	size defines the size of inputs & output matrix
  * @param[in]  A first matrix
  * @param[in]  B second matrix
  * @param[out] C output matrix
  **/
void matrix_product_square(int size, float A[size][size], float B[size][size], float C[size][size]);

/**
  * @brief 		calculate the product of two matrix (MATRIX_MAX_SIZE size maximum)
  * @param[in]  lA defines the number of lines of A matrix
  * @param[in]  cA defines the number of columns of A matrix
  * @param[in]  lB defines the number of lines of B matrix
  * @param[in]  cB defines the number of columns of B matrix
  * @param[in]  A first matrix
  * @param[in]  B second matrix
  * @param[out] C output matrix
  * @note C must be A number of lines & B number of columns
  * 	  this can be computed using
  * 	  MATRIX_MULT_GET_C_COLUMN_SIZE(LA,CA,LB,CB);
  * 	  &
  * 	  MATRIX_MULT_GET_C_LINES_SIZE(LA,CA,LB,CB);
  * 	  macros while creating C array
  **/
void matrix_product(int lA, int cA, int lB, int cB, float A[lA][cA], float B[lB][cB], float C[lA][cB]);

/**
  * @brief 		calculate the square root of a diagonal matrix (MATRIX_MAX_SIZE size maximum)
  * @param[in]  x defines the number of lines of the two matrix
  * @param[in]  y defines the number of columns of the two matrix
  * @param[in]  A input diagonal matrix
  * @param[out] B  square root of diagonal A matrix
  **/
void matrix_diag_sqrt(int x, int y, float A[x][y], float B[x][y]);

/**
  * @brief 		calculate the cross_product for the eigen value function
  * @param[in]  n    indice of the Eigenspace E[n][][] to use
  * @param[in]  col1 first  column number of the Eigenspace E[][][col]
  * @param[in]  col2 second column number of the Eigenspace E[][][col]
  * @param[in]  E    Eigenspace E[3][3][3]
  * @param[out] x    result of the cross product
  **/
void matrix_eigen_cross_product(int n, int col1, int col2, float E[3][3][3], float x[3]);

/**
  * @brief 		get the first combinaison of non-collinear of column in eigenspace
  * 	   		for the eigen value function
  * @param[out] c1 first  non-collinear column number, -1 if collinear
  * @param[out] c2 second non-collinear column number, -1 if collinear
  * @param[in]  n  indice of the Eigenspace E[n][][] to use
  * @param[in]  E  Eigenspace E[3][3][3]
  **/
void matrix_eigen_non_collinearity(int *c1, int *c2, int n, float E[3][3][3]);

/**
  * @brief 		get eigen_vector & value for a 3x3 input matrix
  * @param[in]  A            input 3x3 matrix
  * @param[out] eigen_vector output 3x3 matrix
  * @param[out] eigen_value  output 3x3 matrix
  **/
int8_t matrix_eigen_vector_value_3x3(float A[3][3], float eigen_vector[3][3],float eigen_value[3][3]);


void matrix_print(int l, int c, float Mat[l][c], char * name);
void magcal_abort(char * s);

#endif  /* _MAGCAL_H_ */

