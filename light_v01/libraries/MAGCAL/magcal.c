/*
 * magcal.c
 *
 *  Created on: 23 janv. 2019
 *      Author: BGE
 */
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "Inc/magcal.h"
#include "LSM9DS1.h"
#include "trace.h"
#include "al_database.h"
#include <../al_database_private.h>
#include <al_tools.h>

//-------------------------------------------------------------------
// Defines
//-------------------------------------------------------------------
#define MAGCAL_FLASH_IDX    0
#define MAGCAL_SECT_SIZE    0x1000     // 4kB



//-------------------------------------------------------------------
// Private variables
//-------------------------------------------------------------------
magcal_t magcal = {0};

//-------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------
void magcal_init_compensation_matrix(void);

//-------------------------------------------------------------------
// Public API
//-------------------------------------------------------------------
float magcal_get_quality(void)
{
    return magcal.quality;
}

//-------------------------------------------------------------------
void magcal_compensate_mag(float mag[3])
{
    float hard_soft[3] = {0.0};
    uint8_t i = 0;
    uint8_t j = 0;

    // Hard iron & soft iron compensation
    for (i = 0; i < 3; i++)
    {
        hard_soft[i] = 0.0;
        for (j = 0; j < 3; j++)
        {
            hard_soft[i] += (mag[j]-magcal.hard_iron[j])*magcal.soft_iron[i][j];
        }
    }

    // mag_tilt & post hard iron compensation
    for (i = 0; i < 3; i++)
    {
        mag[i] = 0.0;
        for (j = 0; j < 3; j++)
        {
            mag[i] += hard_soft[j]*magcal.mag_tilt[i][j]; /* Gauss */
        }
        mag[i] -= magcal.post_hard_iron[i];
    }
}

//-------------------------------------------------------------------
int8_t magcal_get_calibration_from_flash(void)
{
    int ret;
    int i;
    int j;

    ret = al_database_get_mag_cal((uint8_t *)&magcal, sizeof(magcal_t));
    if(ret<0)
    {
        magcal_init_compensation_matrix();
        return ret;
    }
    // if data from flash is empty
    if(magcal.soft_iron[0][0] == 0.0)
    {
        magcal_init_compensation_matrix();
        return -1;
    }

    // integrity check
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            if(isnan(magcal.soft_iron[i][j]))
            {
                debug_printf("flash read error, soft iron is NaN\n");
                magcal_init_compensation_matrix();
                return -1;
            }
            if(isnan(magcal.mag_tilt[i][j]))
            {
                debug_printf("flash read error, mag tilt is NaN\n");
                magcal_init_compensation_matrix();
                return -1;
            }
        }
        if(isnan(magcal.hard_iron[i]))
        {
            debug_printf("flash read error, hard_iron is NaN\n");
            magcal_init_compensation_matrix();
            return -1;
        }
        if(isnan(magcal.post_hard_iron[i]))
        {
            debug_printf("flash read error, post_hard_iron is NaN\n");
            magcal_init_compensation_matrix();
            return -1;
        }
    }


    return 0;
}



//-------------------------------------------------------------------
float sigmoid(float x)
{
    return ( 1 / (1 + exp((double) -x)) );
}

//-------------------------------------------------------------------
float magcal_quality_E(float y)
{
    return ( 1 - fabs(y - 0.5) * 2 );
}

//-------------------------------------------------------------------
void magcal_init_compensation_matrix(void)
{
    // Neutralisation of compensation matrix
    // hard_iron      = 0 0 0
    //
    // post_hard_iron = 0 0 0
    //
    // soft_iron      = 1 0 0
    //                  0 1 0
    //                  0 0 1
    //
    // mag_tilt       = 1 0 0
    //                  0 1 0
    //                  0 0 1

    uint8_t i = 0;
    uint8_t j = 0;
    for (i = 0; i < 3; i++)
    {
        magcal.hard_iron     [i] = 0.0;
        magcal.post_hard_iron[i] = 0.0;
        for (j = 0; j < 3; j++)
        {
            if(i==j)
            {
                magcal.soft_iron [i][j] = 1.0;
                magcal.mag_tilt  [i][j] = 1.0;
            }
            else
            {
                magcal.soft_iron [i][j] = 0.0;
                magcal.mag_tilt  [i][j] = 0.0;
            }
        }
    }

    magcal.quality = 0.0;
    magcal.regression_error = 0.0;
    magcal.aberration_count = 0;
}

//-------------------------------------------------------------------
int8_t magcal_hard_soft_iron(void)
{
    uint16_t index_acq    =   0   ;
    uint16_t skipped_data =   0   ;
    uint8_t  i            =   0   ;
    uint8_t  j            =   0   ;
    float    magX         =   0.0 ;
    float    magY         =   0.0 ;
    float    magZ         =   0.0 ;
    float    old_magX     =   0.0 ;
    float    old_magY     =   0.0 ;
    float    old_magZ     =   0.0 ;
    float    A  [9]       =  {0.0} ;
    float    B  [9][9]    = {{0.0}};
    float    iB [9][9]    = {{0.0}};
    float    r  [9]       =  {0.0} ;
    float    M  [3][3]    = {{0.0}};
    float    iM [3][3]    = {{0.0}};
    float    n  [3]       =  {0.0} ;
    float    eigen_vector    [3][3] = {{0.0}};
    float    i_eigen_vector  [3][3] = {{0.0}};
    float    eigen_value     [3][3] = {{0.0}};
    float    sqrt_eigen_value[3][3] = {{0.0}};
    float    W[3][3] = {0};
    float    T[3][3] = {0};
    float    tnh = 0;
    float    Yi = 0;
    float    D = -1;
    float    C = 0;

    //********************** MAGNETOMETER *************************
    if(LSM9DS1_PowerUp()<0)
    {
        magcal_abort("unable to start magnetometer");
        return -1;
    }
    // reset built-in Hard Iron offset calibration
    LSM9DS1_magOffset(0, 0); // X axis
    LSM9DS1_magOffset(1, 0); // Y axis
    LSM9DS1_magOffset(2, 0); // Z axis
    //*************************************************************


    //************************** FLASH ****************************
    // erase flash sector to allow data storage for calibration
    for(i=0; i<(MAGCAL_MATRIX_SIZE / MAGCAL_SECT_SIZE); i++)
    {
        if(eflash_erase_sector(MAGCAL_FLASH_IDX,
                (MAGCAL_MATRIX_START+(i*MAGCAL_SECT_SIZE))) !=0 )
        {
            magcal_abort("Unable erase flash sector");
            return -1;
        }
    }
    //*************************************************************

    magcal_init_compensation_matrix();

    //******************** DATA ACQUISITION ***********************
    for(index_acq=0 ; index_acq<SPHERE_ACQ_NB_POINTS ; index_acq++)
    {

        if(LSM9DS1_GetMagnetometerValuesGs(&magX, &magY, &magZ)<0)
        {
            debug_printf("unable to read magnetometer value\n");
            skipped_data++;
            continue;
        }
        if((magX==old_magX)&&(magY==old_magY)&&(magZ==old_magZ))
        {
            skipped_data++;
            debug_printf("MAG data was not updated with fresh data | %d\n",skipped_data);
            continue;
        }
        old_magX = magX;
        old_magY = magY;
        old_magZ = magZ;

        A[0]=square(magX);
        A[1]=square(magY);
        A[2]=square(magZ);
        A[3]=2.0 * magX * magY;
        A[4]=2.0 * magX * magZ;
        A[5]=2.0 * magY * magZ;
        A[6]=2.0 * magX;
        A[7]=2.0 * magY;
        A[8]=2.0 * magZ;

        // save A in flash for each index_acq to be used later
        if(eflash_prog(MAGCAL_FLASH_IDX,
                MAGCAL_MATRIX_START+((index_acq-skipped_data)*sizeof(A)),
                A, sizeof(A)) !=0)
        {
            magcal_abort("Unable to write magnetometer data to flash");
            return -1;
        }

        /*   A'*A   somme[index_acq] += a[index_acq]i*a[index_acq]j*/
        for(i=0; i<9; i++)
        {
            for (j=0; j<9; j++)
            {
                B[i][j]+=A[i] * A[j];
            }
        }

        // Magetometer speed is 80Hz -> 1 sample every 12.5 ms
        HAL_Delay(13);
    }
    //*************************************************************


    //************************ HARD IRON **************************
    // iB = inv(B);
    // can't be inverted if determinant = 0
    if(matrix_inverse(9, B, iB)==0)
    {
        magcal_abort("input matrix cannot be inverted");
        return -1;
    }

    // r = iB*A'*b
    for(index_acq=0 ; index_acq<(SPHERE_ACQ_NB_POINTS-skipped_data) ; index_acq++)
    {
        // read A in flash for each index_acq
        eflash_read(MAGCAL_FLASH_IDX,
                    MAGCAL_MATRIX_START+(index_acq*sizeof(A)),
                    (uint8_t*)A, sizeof(A));

        for(i=0; i<9; i++)
        {
            for (j=0; j<9; j++)
            {
                r[j] += iB[j][i] * A[i]	;
            }
        }
    }

    //************************ REGRESSION **************************
    // compute regression error
    C = 0.0;
    for (index_acq=0 ; index_acq<(SPHERE_ACQ_NB_POINTS-skipped_data); index_acq++)
    {
        // read A in flash for each index_acq
        eflash_read(MAGCAL_FLASH_IDX,
                    MAGCAL_MATRIX_START+(index_acq*sizeof(A)),
                    (uint8_t*)A, sizeof(A));

        Yi = 0.0;
        for( i = 0; i < 9; ++i )
        {
            Yi += A[i] * r[i];
        }

        C += square(Yi - 1);
    }

    // divide by number of acquisition on 9 axis minus 1 as unbiased estimator
    magcal.regression_error = sqrtf(C) / (SPHERE_ACQ_NB_POINTS-skipped_data-10);

    // convert the regression to a square matrix
    M[0][0] = r[0];
    M[0][1] = r[3];
    M[0][2] = r[4];
    M[1][0] = r[3];
    M[1][1] = r[1];
    M[1][2] = r[5];
    M[2][0] = r[4];
    M[2][1] = r[5];
    M[2][2] = r[2];
    n[0]    = r[6];
    n[1]    = r[7];
    n[2]    = r[8];

    if(matrix_inverse(3, M, iM)==0)
    {
        magcal_abort("input matrix cannot be inverted");
        return -1;
    }

    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            magcal.hard_iron[i] += (-iM[i][j])*n[j]; // GAUSS
        }
        if(isnan(magcal.hard_iron[i]))
        {
            magcal_abort("error during computation, hard iron is NaN");
            return -1;
        }
    }
    //*************************************************************

    debug_printf("\nhard_iron_matrix\n");
    debug_printf("%f\t%f\t%f\n", magcal.hard_iron[0],
                 magcal.hard_iron[1], magcal.hard_iron[2]);


    //************************ SOFT IRON **************************
    // get eigen values from M
    matrix_eigen_vector_value_3x3(M,eigen_vector, eigen_value);
    // W = eigen_factor * sqrt(eigen_value) * inv(eigen_vector)
    // inversion of eigen_vector
    matrix_inverse(3, eigen_vector, i_eigen_vector);
    // square root of diag eigen_value
    matrix_diag_sqrt(3,3,eigen_value,sqrt_eigen_value);
    // T = eigen_vector * sqrt(eigen_value)
    matrix_product_square(3, eigen_vector, sqrt_eigen_value,T);
    // W = T * inv(eigen_vector)
    matrix_product_square(3, T, i_eigen_vector,W);

    // soft iron = 1/(-(n transpose) * hard - D) * W
    // tnh = -(n transpose) * hard
    for (i=0; i<3; i++)
    {
        tnh += magcal.hard_iron[i]*(-n[i]);
    }
    //soft iron= 1/(tnh-D) * W
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            magcal.soft_iron[i][j] = W[i][j] * (1/sqrt(fabs(tnh - D)));

            // sanity check
            if(isnan(magcal.soft_iron[i][j]))
            {
                magcal_abort("error during computation, soft iron is NaN");
                return -1;
            }
        }
    }

    //*************************************************************


    //************************** FLASH ****************************
    if(al_database_set_mag_cal((uint8_t *)&magcal, sizeof(magcal_t))<0)
    {
        magcal_abort("error during flash save");
        return -1;
    }
    //*************************************************************

#ifdef DEBUG
    matrix_print(3,3,magcal.soft_iron,"soft_iron_matrix");
#endif
    return 0;
}

//-------------------------------------------------------------------
int8_t magcal_tilt_post_hard_iron(void)
{
    int rc = 0;
    float C1, C2, C3, C4, C5       ;   // variables for quality calculation
    int32_t data_addr = 0          ;
    uint16_t index_acq    =   0    ;
    uint16_t skipped_data =   0    ;
    uint8_t  i            =   0    ;
    uint8_t  j            =   0    ;
    float A [3]           =  {0.0} ;
    float B [3][3]        = {{0.0}};
    float iB[3][3]        = {{0.0}};
    float r[3]            =  {0.0} ;
    float z00             =   0.0  ;
    float z10             =   0.0  ;
    float z01             =   0.0  ;
    float ty              =   0.0  ;
    float tx              =   0.0  ;
    float R1[3][3]        = {{0.0}};
    float R2[3][3]        = {{0.0}};
    float data_plan       =   0.0  ;
    float min[3] = { 20.0, 20.0, 20.0};
    float max[3] = {-20.0,-20.0,-20.0};
    float mag_stdev       =   0.0  ;
    float mag_mean        =   0.0  ;
    al_tools_variance_t magz_aggr = { 0, 0.0, 0.0 };
    al_tools_variance_t norm_aggr = { 0, 0.0, 0.0 };
    al_tools_variance_t aber_aggr = { 0, 0.0, 0.0 };
    float calibrated_mag[3] = {0};
    float norm_mag_array[16];
    float comp_mag_matrix[8][3];


    //********************** MAGNETOMETER *************************
    if(LSM9DS1_PowerUp()<0)
    {
        magcal_abort("unable to start magnetometer");
        return -1;
    }
    //*************************************************************


    //************************** FLASH ****************************
    // erase flash sector to allow data storage for calibration
    for(i=0; i<(MAGCAL_MATRIX_SIZE / MAGCAL_SECT_SIZE); i++)
    {
        eflash_erase_sector(MAGCAL_FLASH_IDX,
                (MAGCAL_MATRIX_START+(i*MAGCAL_SECT_SIZE)));
    }
    //*************************************************************


    //******************** DATA ACQUISITION ***********************
    for(index_acq = 0 ; index_acq < TILT_ACQ_NB_POINTS ; index_acq++)
    {
        if(LSM9DS1_GetMagnetometerValuesGs(&A[0],&A[1],&A[2])<0)
        {
            debug_printf("unable to read magnetometer value\n");
            skipped_data++;
            continue;
        }
        // compensate only in hard & soft iron
        magcal_compensate_mag(A);

        // save A in flash for each index_acq to be used later
        eflash_prog(MAGCAL_FLASH_IDX,
                MAGCAL_MATRIX_START+((index_acq-skipped_data)*sizeof(A)),
                (uint8_t*)A, sizeof(A));

        for(i=0; i<3; i++)
        {
            for (j=0; j<3; j++)
            {
                B[i][j]+=A[i] * A[j];
            }
        }
        // Magnetometer speed is 80Hz -> 1 sample every 12.5 ms
        HAL_Delay(13);

    }
    //********************* ABERRATIVE DATA COUNT ************************
    i = 0;
    data_addr = MAGCAL_MATRIX_START;
    for ( index_acq = 0 ;
          index_acq < (TILT_ACQ_NB_POINTS - skipped_data - (sizeof(comp_mag_matrix) / 3));
          index_acq = index_acq + (sizeof(comp_mag_matrix) / 3) )
    {
        // read A in flash for each index_acq
        rc = eflash_read(MAGCAL_FLASH_IDX, data_addr,
                         &comp_mag_matrix[0][0],
                         sizeof(comp_mag_matrix));
        if ( rc )
        {
            debug_printf("unable to retrieve data from flash\n");
            return -1;
        }

        // update flash data address
        data_addr = data_addr + sizeof(comp_mag_matrix);

        // fill as a circular buffer
        for ( j = 0; j < 8; ++j )
        {
            norm_mag_array[i % 16] = square(comp_mag_matrix[j][0]);
            norm_mag_array[i % 16] += square(comp_mag_matrix[j][1]);
            norm_mag_array[i % 16] = sqrtf(norm_mag_array[i % 16]);

            // increment global counter
            i = i + 1;
        }

        // wait till buffer is fulfilled
        if ( i < 16 )
        {
            continue;
        }

        // compute mean value and variance of normalised magnetometer
        memset(&aber_aggr, 0, sizeof(aber_aggr));
        for ( j = 0; j < 16; ++j )
        {
        	al_tools_variance_update(&aber_aggr, norm_mag_array[j]);
        }

        // retrieve calculated values
        mag_stdev = al_tools_variance_finalize(&aber_aggr);
        mag_mean = al_tools_variance_get_mean(&aber_aggr);

        // get standard deviation from variance
        mag_stdev = sqrtf(mag_stdev);

        // count of aberrant values
        for ( j = 0; j < 16; ++j )
        {
            // aberrant superior upper threshold
            if ( norm_mag_array[j] > (mag_mean + (3 * mag_stdev)) )
            {
                magcal.aberration_count += 2;
            }

            // aberrant inferior lower threshold
            if ( norm_mag_array[j] < (mag_mean - (3 * mag_stdev)) )
            {
                magcal.aberration_count += 1;
            }
        }
    }
    //*************************************************************

    //************************** TILT *****************************
    // iB = inv(B);
    // can't be inverted if determinant = 0
    if(matrix_inverse(3, B, iB)==0)
    {
        magcal_abort("input matrix cannot be inverted");
        return -1;
    }
    // r = iB*A'*b
    for(index_acq=0 ; index_acq<(TILT_ACQ_NB_POINTS-skipped_data) ; index_acq++)
    {
        // read A in flash for each index_acq
        eflash_read(MAGCAL_FLASH_IDX,
                    MAGCAL_MATRIX_START+(index_acq*sizeof(A)),
                    (uint8_t*)A, sizeof(A));
        for(i=0; i<3; i++)
        {
            for (j=0; j<3; j++)
            {

                r[j] += iB[j][i] * A[i];
            }
        }
    }

    z00      =  1.0/r[2]       ;
    z10      =  (1.0-r[0])/r[2];
    z01      =  (1.0-r[1])/r[2];
    ty       =  atan(z10-z00)  ;
    tx       = -atan(z01-z00)  ;
    R1[0][0] =  1.0            ;
    R1[1][1] =  cos(tx)        ;
    R1[1][2] = -sin(tx)        ;
    R1[2][1] =  sin(tx)        ;
    R1[2][2] =  cos(tx)        ;

    R2[0][0] =  cos(ty)        ;
    R2[0][2] =  sin(ty)        ;
    R2[1][1] =  1.0            ;
    R2[2][0] = -sin(ty)        ;
    R2[2][2] =  cos(ty)        ;

    matrix_product_square(3, R1, R2, magcal.mag_tilt);

    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            if(isnan(magcal.mag_tilt[i][j]))
            {
                magcal_abort("error during computation, tilt is NaN");
                return -1;
            }
        }
    }

#ifdef DEBUG
    matrix_print(3,3,magcal.mag_tilt,"mag_tilt");
#endif
    //*************************************************************


    //********************* POST HARD ***************************
    // read hard&soft compensated data from flash & apply mag_tilt compensation
    for(index_acq=0 ; index_acq<(TILT_ACQ_NB_POINTS-skipped_data) ; index_acq++)
    {
        // read A in flash for each index_acq
        eflash_read(MAGCAL_FLASH_IDX, MAGCAL_MATRIX_START+(index_acq*sizeof(A)),
                (uint8_t*)A, sizeof(A));
        for (i = 0; i < 3; i++)
        {
            data_plan = 0.0;
            for (j = 0; j < 3; j++)
            {
                data_plan += A[j]*magcal.mag_tilt[i][j]; // Gauss
            }
            if(data_plan < min[i]){ min[i] = data_plan;}
            if(data_plan > max[i]){ max[i] = data_plan;}
        }
    }
    for (i = 0; i < 2; i++)
    {
        magcal.post_hard_iron[i] = (min[i] + max[i])/2.0;
        if(isnan(magcal.post_hard_iron[i]))
        {
            magcal_abort("error during computation, post hard is NaN");
            return -1;
        }
    }
    //*************************************************************

    //************************* QUALITY ***************************
    // init min & max values
    for(i=0;i<3;i++)
    {
        min[i] =  20.0;
        max[i] = -20.0;
    }

    // read hard&soft compensated data from flash & apply mag_tilt compensation
    for(index_acq=0 ; index_acq<(TILT_ACQ_NB_POINTS-skipped_data) ; index_acq++)
    {
        // read A in flash for each index_acq
        eflash_read(MAGCAL_FLASH_IDX, MAGCAL_MATRIX_START+(index_acq*sizeof(A)),
                (uint8_t*)A, sizeof(A));
        // apply tilt/post hard compensation
        for (i = 0; i < 3; i++)
        {
            calibrated_mag[i] = 0.0;
            for (j = 0; j < 3; j++)
            {
                calibrated_mag[i] += A[j]*magcal.mag_tilt[i][j]; // Gauss
            }
            calibrated_mag[i] -= magcal.post_hard_iron[i];
        }

        if(min[0] > calibrated_mag[0]){min[0] = calibrated_mag[0];}
        if(max[0] < calibrated_mag[0]){max[0] = calibrated_mag[0];}
        if(min[1] > calibrated_mag[1]){min[1] = calibrated_mag[1];}
        if(max[1] < calibrated_mag[1]){max[1] = calibrated_mag[1];}

        al_tools_variance_update(&norm_aggr,
                  sqrtf(square(calibrated_mag[0]) + square(calibrated_mag[1])));
        al_tools_variance_update(&magz_aggr, calibrated_mag[2]);
    }

    C1 = sqrt(al_tools_variance_finalize(&magz_aggr)) * QUALITY_KNOB;
    C1 = sigmoid(C1);
    C1 = fabs(magcal_quality_E(C1));

    C2 = (min[0]-min[1]) * QUALITY_KNOB;
    C2 = sigmoid(C2);
    C2 = fabs(magcal_quality_E(C2));

    C3 = sqrt(al_tools_variance_finalize(&norm_aggr)) * (QUALITY_KNOB + 5);
    C3 = sigmoid(C3);
    C3 = fabs(magcal_quality_E(C3));

    // scale this value after empirical testing
    C4 = magcal.regression_error * 150;
    C4 = sigmoid(C4);
    C4 = fabs(magcal_quality_E(C4));

    C5 = 1 / powf(2, (magcal.aberration_count));

    magcal.quality = (5*C1) + (5*C2) + (50*C3) + (35*C4) + (5*C5);
    //*************************************************************

    //************************** FLASH ****************************
    if(al_database_set_mag_cal((uint8_t *)&magcal, sizeof(magcal_t))<0)
    {
        magcal_abort("error during flash save");
        return -1;
    }
    //*************************************************************

    //********************** MAGNETOMETER *************************
    // de-activate the magnetometer
    if(LSM9DS1_PowerDown()<0){return -1;}
    //*************************************************************
#ifdef DEBUG
    printf("\npost_hard_iron_matrix\n");
    for(i=0;i<3;i++)
    {
        printf("%f\t",magcal.post_hard_iron[i]);
    }
    printf("\n");
#endif
    return 0;
}

//@todo Extract LU decomposition from both matrix inversion & matrix determinant functions
//-------------------------------------------------------------------
float matrix_inverse(int size, float Min[size][size], float Mout[size][size])
{
    int i,j,k;
    float L[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE] = {{0.0}};
    float U[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE] = {{0.0}};
    float b[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE] = {{0.0}};
    float d[MATRIX_MAX_SIZE] = {0.0};
    float x[MATRIX_MAX_SIZE] = {0.0};
    float T;
    float determinant = 1.0;

    if(Min[0][0] == 0.0) // will cause a div by zero
    {
        return 0;
    }
    // Creation of Identity matrix
    for(i=0 ; i<size ; i++)
    {
        U[i][i] = 1.0;
        b[i][i] = 1.0;
    }

    // LU Crout decomposition
    L[0][0] = Min[0][0];
    for(i=1 ; i<size ; i++)
    {
        L[i][0] = Min[i][0];
        U[0][i] = Min[0][i] / L[0][0];
    }
    for(j=1 ; j<=size-2 ; j++)
    {
        for(i=j ; i<=size-1 ; i++)
        {
            L[i][j] = Min[i][j];
            for(k=0 ; k<=j-1 ; k++)
            {
                L[i][j] -= L[i][k] * U[k][j];
            }
        }
        for(k=j+1 ; k<=size-1 ; k++)
        {
            U[j][k] = Min[j][k];
            for(i=0 ; i<=j-1 ; i++)
            {
                U[j][k] -= L[j][i] * U[i][k];
            }
            if(L[j][j] != 0.0)
            {
                U[j][k] = U[j][k] / L[j][j];
            }
            else // will cause a div by zero
            {
                return 0;
            }
        }
    }
    L[size-1][size-1] = Min[size-1][size-1];
    for(i=0 ; i<=size-2 ; i++)
    {
        L[size-1][size-1] -= L[size-1][i] * U[i][size-1];
    }

    // determinant
    for(i = 0 ; i<=size-1 ; i++)
    {
        determinant *= L[i][i] * U[i][i];
    }
    if(determinant == 0)
    {
        return 0;
    }

    // d can be solved by forward substitution of L
    for(k=0 ; k<=size-1 ; k++)
    {
        d[0] = b[0][k]/L[0][0];
        for(i=1 ; i<=size-1 ; i++)
        {
            T = 0.0;
            for(j=0 ; j<=i-1 ; j++)
            {
                T += L[i][j] * d[j];
            }
            if(L[i][i] != 0.0)
            {
                d[i] = (b[i][k] - T ) / L[i][i];
            }
            else // will cause a div by zero
            {
                return 0;
            }
        }

        // then x can be solved by backward substitution of U
        x[size-1] = d[size-1];
        for(i=size-2 ; i>=0 ; i--)
        {
            T = 0.0;
            for(j=i+1 ; j<=size-1 ; j++)
            {
                T += U[i][j] * x[j];
            }
            x[i] = d[i] - T;
        }

        // x is the k-th column of the inversed matrix
        for(i=0 ; i<size ; i++)
        {
            Mout[i][k] = x[i];
        }
    }

    return determinant;
}

//-------------------------------------------------------------------
float matrix_determinant(int size, float Min[size][size])
{
    int i,j,k;
    float L[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE];
    float U[MATRIX_MAX_SIZE][MATRIX_MAX_SIZE];
    float determinant = 1.0;

    // fill variable length matrix with zeros
    for(i=0 ; i<size ; i++)
    {
        for(j=0 ; j<size ; j++)
        {
            L[i][j] = 0.0;
            U[i][j] = 0.0;

        }
    }

    // Creation of Identity matrix
    for(i=0 ; i<size ; i++)
    {
        U[i][i] = 1.0;
    }

    // LU Crout decomposition
    L[0][0] = Min[0][0];
    for(i=1 ; i<size ; i++)
    {
        L[i][0] = Min[i][0];
        U[0][i] = Min[0][i] / L[0][0];
    }
    for(j=1 ; j<=size-2 ; j++)
    {
        for(i=j ; i<=size-1 ; i++)
        {
            L[i][j] = Min[i][j];
            for(k=0 ; k<=j-1 ; k++)
            {
                L[i][j] -= L[i][k] * U[k][j];
            }
        }
        for(k=j+1 ; k<=size-1 ; k++)
        {
            U[j][k] = Min[j][k];
            for(i=0 ; i<=j-1 ; i++)
            {
                U[j][k] -= L[j][i] * U[i][k];
            }
            U[j][k] = U[j][k] / L[j][j];
        }
    }
    L[size-1][size-1] = Min[size-1][size-1];
    for(i=0 ; i<=size-2 ; i++)
    {
        L[size-1][size-1] -= L[size-1][i] * U[i][size-1];
    }

    // determinant
    for(i = 0 ; i<=size-1 ; i++)
    {
        determinant *= L[i][i] * U[i][i];
    }
    return determinant;
}

//-------------------------------------------------------------------
void matrix_product_square(int size, float A[size][size], float B[size][size], float C[size][size])
{
    int i,j,k;
    for(i=0 ; i<size ; i++)
    {
        for(j=0 ; j<size ; j++)
        {
            C[i][j] = 0.0;
            for(k=0 ; k<size ; k++)
            {
                C[i][j] += A[i][k]*B[k][j];
            }
        }
    }
}

//-------------------------------------------------------------------
void matrix_product(int lA, int cA, int lB, int cB, float A[lA][cA], float B[lB][cB], float C[lA][cB])
{
    int i,j,k;

    for (i=0; i<lA; i++)
    {
        for (j=0; j<cB; j++)
        {
            C[j][j]=0;
            for (k=0; k<cA; k++)
            {
                C[i][j] += A[i][k]*B[k][j];
            }
        }
    }
}

//-------------------------------------------------------------------
void matrix_diag_sqrt(int x, int y, float A[x][y], float B[x][y])
{
    int i,j;

    for (i=0; i<x; i++)
    {
        for (j=0; j<y; j++)
        {
            B[i][j] = sqrt(fabs(A[i][j]));
        }
    }
}

//-------------------------------------------------------------------
inline void matrix_eigen_cross_product(int n, int col1, int col2, float E[3][3][3], float x[3])
{
    x[0] = E[n][1][col1]*E[n][2][col2] - E[n][2][col1]*E[n][1][col2];
    x[1] = E[n][2][col1]*E[n][0][col2] - E[n][0][col1]*E[n][2][col2];
    x[2] = E[n][0][col1]*E[n][1][col2] - E[n][1][col1]*E[n][0][col2];
}

//-------------------------------------------------------------------
void matrix_eigen_non_collinearity(int *c1, int *c2, int n, float E[3][3][3])
{
    int col1 = 0;
    int col2 = 0;
    float x[3] = {0};

    // x = A(.:c1) ^ A(.:c2)
    col1 = 0;
    col2 = 1;
    matrix_eigen_cross_product(n, col1, col2, E, x);

    if((x[0] != 0.0) || (x[1] != 0.0) || (x[2] != 0.0))
    {
        * c1 = col1;
        * c2 = col2;
        return;
    }
    col1 = 0;
    col2 = 2;
    matrix_eigen_cross_product(n, col1, col2, E, x);


    if((x[0] != 0.0) || (x[1] != 0.0) || (x[2] != 0.0))
    {
        * c1 = col1;
        * c2 = col2;
        return;
    }
    col1 = 1;
    col2 = 2;
    matrix_eigen_cross_product(n, col1, col2, E, x);


    if((x[0] != 0.0) || (x[1] != 0.0) || (x[2] != 0.0))
    {
        * c1 = col1;
        * c2 = col2;
        return;
    }

    * c1 = -1;
    * c2 = -1;
}

//-------------------------------------------------------------------
int8_t matrix_eigen_vector_value_3x3(float A[3][3], float eigen_vector[3][3],float eigen_value[3][3])
{
    int   i   = 0;
    int   j   = 0;
    int   n   = 0;
    int   i1  = 0;
    int   i2  = 0;
    float p1  = 0.0;
    float p2  = 0.0;
    float p   = 0.0;
    float q   = 0.0;
    float r   = 0.0;
    float phi = 0.0;
    float eig[3]       = {0};
    float x  [3]       = {0};
    float E  [3][3][3] = {0};
    float B  [3][3]    = {0};

    p1 = square(A[0][1]) + square(A[0][2]) + square(A[1][2]);
    if(p1 == 0)
    {
        eig[0] = A[0][0];
        eig[1] = A[1][1];
        eig[2] = A[2][2];
    }
    else
    {
        q  = (A[0][0] + A[1][1] + A[2][2])/3.0;
        p2 = square(A[0][0]-q) + square(A[1][1]-q) + square(A[2][2]-q) + 2 * p1;
        p = sqrt(p2/6.0);

        // B = (1/p) * (A - q * eye(3,3));
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(i==j) // diagonal
                {
                    B[i][j]	= (A[i][j]-q)/p;
                }
                else
                {
                    B[i][j] = A[i][j] / p;
                }
            }
        }

        r = matrix_determinant(3,B) / 2;
        if      (r <= -1) {phi = PI/3.0     ;}
        else if (r >=  1) {phi = 0          ;}
        else              {phi = acos(r)/3.0;}

        eig[0] = q + 2 * p * cos(phi);
        eig[2] = q + 2 * p * cos(phi + (2*PI/3.0));
        eig[1] = 3 * q - eig[0] - eig[2];
    }

    for(i=0;i<3;i++)
    {
        eigen_value[i][i] = eig[i];
    }

    // En = A - eign * eye(3,3));
    for(n=0;n<3;n++)
    {
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(i==j) // diagonal
                {
                    E[n][i][j] = A[i][j]-eig[n];
                }
                else
                {
                    E[n][i][j] = A[i][j];
                }
            }
        }
        matrix_eigen_non_collinearity(&i1,&i2,n,E);
        if((i1!=-1) && (i2!=-1))
        {
            matrix_eigen_cross_product(n,i1,i2,E,x);
            for(i=0;i<3;i++)
            {
                eigen_vector[i][n] = x[i];
            }
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

//-------------------------------------------------------------------
void matrix_print(int l, int c, float Mat[l][c], char * name)
{
#ifndef DEBUG
    (void)l;
    (void)c;
    (void)Mat;
    (void)name;
#else
    int i,j;
    printf("\n%s:\n",name);
    for (i=0; i<l; i++)
    {
        for (j=0; j<c; j++)
        {
            printf("%f\t", Mat[i][j]);
        }
        printf("\n");
    }
#endif
}

//-------------------------------------------------------------------
int magcal_reset(void)
{
    int rc = 0;

    // first reset local context
    magcal_init_compensation_matrix();

    // save reset value to flash
    rc = al_database_set_mag_cal((uint8_t *)&magcal, sizeof(magcal_t));
    if ( rc )
    {
        debug_printf("unable to save reset magcal values\n");
        return -1;
    }

    return 0;
}

//-------------------------------------------------------------------
void magcal_abort(char * s)
{
#ifndef DEBUG
    (void)s;
#endif

    debug_printf("MAGCAL - ABORT - %s\n", s);

    // de-activate the magnetometer
    LSM9DS1_PowerDown();

    // attempt to retreive previous calibration matrix
    if(magcal_get_calibration_from_flash()<0)
    {
        debug_printf("MAGCAL - ABORT - unable to retreive magcal from flash\n");
        if(al_database_set_mag_cal((uint8_t *)&magcal, sizeof(magcal_t))<0)
        {
            debug_printf("MAGCAL - ABORT - unable to write unit matrix to flash\n");
        }

    }
}

