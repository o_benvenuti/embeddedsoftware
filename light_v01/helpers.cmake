MACRO (find_arm_toolchain)
    FIND_PROGRAM (xcc arm-none-eabi-gcc)
    FIND_PROGRAM (xar arm-none-eabi-ar)
    FIND_PROGRAM (objcopy arm-none-eabi-objcopy)
    FIND_PROGRAM (objsize arm-none-eabi-size)
    SET (CMAKE_AR           ${xar})
    SET (CMAKE_C_COMPILER   ${xcc}) 
ENDMACRO (find_arm_toolchain)

MACRO (configure_toolchain)
    SET (ARCH "-mcpu=cortex-m4")
    SET (GCC_WARN_DEFS "-Wall -Wextra") #Wpedantic Werror
    SET (GCC_FEAT_DEFS "-mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16")
    SET (GCC_FEAT_DEFS "${GCC_FEAT_DEFS} -std=gnu99 -specs=rdimon.specs")
    SET (CMAKE_C_FLAGS "${ARCH} ${GCC_FEAT_DEFS} ${GCC_WARN_DEFS} -ffunction-sections -fdata-sections") 
     
    SET (CMAKE_C_FLAGS_DEBUG "-Og -g -DDEBUG") 
    SET (CMAKE_C_FLAGS_RELEASE "-O3 -DNDEBUG") 
        
    SET (GXX_FEAT_DEFS "-specs=rdimon.specs -fshow-column -fno-strict-aliasing")
    SET (CMAKE_CXX_FLAGS "${ARCH} -fno-rtti -fno-exceptions ${GXX_FEAT_DEFS}")
    SET (CMAKE_CXX_FLAGS_DEBUG "-O0 -g -DDEBUG")
ENDMACRO (configure_toolchain)

MACRO (configure_system)
    SET (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    SET (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    SET (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    SET (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
    #cmake does not know about FreeRTOs 
    SET (CMAKE_SYSTEM_NAME Generic)
    SET (CMAKE_SYSTEM_PROCESSOR arm)
    SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DUSE_HAL_DRIVER -DSTM32L486xx")
ENDMACRO (configure_system)

#
# INCLUDE OS SPECIFIC STUFF 
#
MACRO (include_freertos)
    INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/FreeRTOS/Source/CMSIS_RTOS/
                        ${CMAKE_SOURCE_DIR}/FreeRTOS/Source/include
                        ${CMAKE_SOURCE_DIR}/FreeRTOS/Source/portable/GCC/ARM_CM4F)
    ADD_DEFINITIONS(-DUSE_FREERTOS)
ENDMACRO (include_freertos)

#
# INCLUDE PLATFORM SPECIFIC STUFF
#
MACRO (include_platform arg)
    IF ("${arg}" STREQUAL "STM32L486")
        INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/platform/STM32L486/CMSIS/Device/ST/STM32L4xx/Include
                            ${CMAKE_SOURCE_DIR}/platform/STM32L486/STM32L4xx_HAL_Driver/Inc
                            ${CMAKE_SOURCE_DIR}/platform/STM32L486/STM32L4xx_HAL_Driver/Inc/Legacy
                            ${CMAKE_SOURCE_DIR}/platform/STM32L486/CMSIS/Include
                            ${CMAKE_SOURCE_DIR}/platform/STM32L486/devices/Inc
                            ${CMAKE_SOURCE_DIR}/platform/STM32L486/config)
                            
        FILE (GLOB PLATFORM_HAL_FILES ${CMAKE_SOURCE_DIR}/platform/STM32L486/STM32L4xx_HAL_Driver/Src/*.c)
        FILE (GLOB PLATFORM_SOURCE_FILES ${CMAKE_SOURCE_DIR}/platform/STM32L486/devices/*.c)
        
        SET_PROPERTY (SOURCE ${CMAKE_SOURCE_DIR}/platform/STM32L486/startup/startup_stm32l486xx.s PROPERTY LANGUAGE C)
        
        SET (PLATFORM_SOURCE_FILES ${PLATFORM_SOURCE_FILES}
                                   ${CMAKE_SOURCE_DIR}/platform/STM32L486/startup/startup_stm32l486xx.s)
        
        SET (PLATFORM_SOURCE_FILES ${PLATFORM_SOURCE_FILES} ${PLATFORM_HAL_FILES})
    ENDIF ("${arg}" STREQUAL "STM32L486")
    
    # No matters the platform, this fixes newlib reentrant malloc's bug
    SET (PLATFORM_SOURCE_FILES ${PLATFORM_SOURCE_FILES}
                               ${CMAKE_SOURCE_DIR}/platform/serenity/001_newlib_dynmem_reentrant_support.c)
ENDMACRO (include_platform)

#
# INCLUDE STUFF (tend to disappear)
#
MACRO (include_app)
    INCLUDE_DIRECTORIES (${CMAKE_SOURCE_DIR}/apps/S1_TRACK/Inc)
ENDMACRO (include_app)

#
# INCLUDE LIBRARY HEADERS
#
MACRO (include_libraries)
    FOREACH (lib ${ARGV})
        SET (abs_path ${CMAKE_SOURCE_DIR}/libraries/${lib}/Inc/)
        IF (NOT IS_DIRECTORY ${abs_path})
            MESSAGE (FATAL_ERROR "${lib} directory ${abs_path} does not exist")
        ENDIF (NOT IS_DIRECTORY ${abs_path})
        INCLUDE_DIRECTORIES(${abs_path})
    ENDFOREACH (lib ${arg})
ENDMACRO (include_libraries)

#
# INCLUDE DRIVERS HEADERS
#
MACRO (include_drivers)
    FOREACH (driver ${ARGV})
        SET (abs_path ${CMAKE_SOURCE_DIR}/drivers/${driver}/Inc/)
        IF (NOT IS_DIRECTORY ${abs_path})
            MESSAGE (FATAL_ERROR "${driver} directory ${abs_path} does not exist")
        ENDIF (NOT IS_DIRECTORY ${abs_path})
        INCLUDE_DIRECTORIES(${abs_path})
    ENDFOREACH (driver ${ARGV})
ENDMACRO (include_drivers)

